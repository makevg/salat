//
//  SAProductService.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductService.h"
#import "SAProductCategoryParser.h"
#import "SAProductParser.h"
#import "ProductCategory.h"
#import "Product.h"
#import "SAProductModifierParser.h"
#import "ProductModifier.h"
#import "SACommon.h"
#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import <MagicalRecord/NSManagedObject+MagicalRequests.h>

static NSString *const kMenuUrl = @"menu";
static NSString *const kFavoritesUrl = @"menu/favorites";
static NSString *const kHitsUrl = @"menu/hits";
static NSString *const kVersionDefaultsName = @"versionDefaultsName";

@implementation SAProductService

#pragma mark - Public

- (NSArray *) obtainProductCategoriesLocal {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [ProductCategory MR_requestAllSortedBy:@"position"
                                                                ascending:YES
                                                            withPredicate:[NSPredicate predicateWithFormat:@"parent_id == 0"]
                                                                inContext:moc];
    return [ProductCategory MR_executeFetchRequest:fetchRequest inContext:moc];
}

- (NSArray *) obtainProductsLocalByCategoryId:(NSNumber *)categoryId {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [Product MR_requestAllSortedBy:@"position"
                                                        ascending:YES
                                                    withPredicate:[NSPredicate predicateWithFormat:@"category_id == %@", categoryId]
                                                        inContext:moc];
    return [Product MR_executeFetchRequest:fetchRequest inContext:moc];
}

- (void) obtainProductCategories:(void (^)(NSArray<ProductCategory *> *))completion error:(void (^)(NSError *))errorBlock {
    weakifySelf;
    completion([self obtainProductCategoriesLocal]);
    
    [self obtainMenuFromNetwork:^{
        completion( [weakSelf obtainProductCategoriesLocal]);
    }
                          error:errorBlock];
}

- (void) obtainProductsByCategoryId:(NSNumber *)categoryId copletion:(void (^)(NSArray<Product *> *))completion error:(void (^)(NSError *))errorBlock {
    weakifySelf;
    completion([self obtainProductsLocalByCategoryId:categoryId]);
    
    [self obtainMenuFromNetwork:^{
        completion([weakSelf obtainProductsLocalByCategoryId:categoryId]);
    }
                          error:errorBlock];
}

- (Product *)obtainProductByCategoryId:(NSNumber *)categoryId {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    return [Product MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"category_id == %@", categoryId]
                                    inContext:moc];
}

- (ProductCategory *)obtainProductCategoryById:(NSNumber *)categoryId {
    return [ProductCategory MR_findFirstByAttribute:@"id"
                                          withValue:categoryId
                                          inContext:[SALAT_DB produceContextForRead]];
}

- (void)obtainMenuFromNetwork:(void (^)())completion
                        error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    NSString *localVersion = [[NSUserDefaults standardUserDefaults] stringForKey:kVersionDefaultsName];
    NSDictionary *params = nil;

    if (localVersion) {
        params = @{
                @"version" : localVersion
        };
    }

    [SALAT_API requestGetToUrlString:kMenuUrl
                              params:params
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 NSDictionary *responseDict = responseObject;
                                 NSString *version = responseDict[@"result"][@"version"];

                                 if ([version isEqualToString:localVersion]) {
                                     completion();
                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                     NSArray *categoriesArray = responseDict[@"result"][@"categories"];
                                     NSArray *productsArray = responseDict[@"result"][@"products"];
                                     
                                     NSMutableArray* categoriesIds = [@[] mutableCopy];
                                     NSMutableArray* productIds = [@[] mutableCopy];
                                     
                                     for (NSDictionary *categoryDict in categoriesArray) {
                                         ProductCategory *productCategory = [ProductCategory MR_findFirstOrCreateByAttribute:@"id"
                                                                                                                   withValue:categoryDict[@"id"]
                                                                                                                   inContext:localContext];
                                         [strongSelf.productCategoryParser fillObject:productCategory fromResponse:categoryDict];
                                         [categoriesIds addObject:categoryDict[@"id"]];
                                     }
                                     
                                     [ProductCategory MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", categoriesIds]
                                                                          inContext:localContext];


                                     for (NSDictionary *productDict in productsArray) {
                                         ProductCategory *category = [ProductCategory MR_findFirstByAttribute:@"id"
                                                                                                    withValue:productDict[@"category_id"]
                                                                                                    inContext:localContext];

                                         if (category == nil)
                                             continue;

                                         Product *product = [Product MR_findFirstOrCreateByAttribute:@"id"
                                                                                           withValue:productDict[@"id"]
                                                                                           inContext:localContext];
                                         [strongSelf.productParser fillObject:product fromResponse:productDict];

                                         product.parent = category;

                                         NSDictionary *modifiersArray = productDict[@"modifiers"];
                                         NSMutableSet<ProductModifier *> *modifiers = [NSMutableSet<ProductModifier *> new];
                                         
                                         for (NSDictionary *modifierDict in modifiersArray) {
                                             ProductModifier *productModifier = [ProductModifier MR_findFirstOrCreateByAttribute:@"id"
                                                                                                                       withValue:modifierDict[@"id"]
                                                                                                                       inContext:localContext];
                                             [strongSelf.productModifierParser fillObject:productModifier fromResponse:modifierDict];
                                             [modifiers addObject:productModifier];
                                         }
                                         
                                         product.modifiers = modifiers;
                                         [productIds addObject:productDict[@"id"]];
                                     }
                                     
                                     [Product MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", productIds]
                                                                  inContext:localContext];
                                 }
                                      completion:^(BOOL contextDidSave, NSError *_) {
                                          [[NSUserDefaults standardUserDefaults] setObject:version
                                                                                    forKey:kVersionDefaultsName];
                                          completion();
                                      }];
                             }];
}

- (Product *)obtainProductById:(NSNumber *)productId {
    return [Product MR_findFirstByAttribute:@"id" withValue:productId inContext:[SALAT_DB produceContextForRead]];
}

- (void)obtainHits:(void (^)(NSArray <NSNumber *> *hits))completion error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:kHitsUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 NSArray *responseDict = responseObject[@"result"];
                                 NSMutableArray *hits = [@[] mutableCopy];

                                 for (NSDictionary *product in responseDict) {
                                     [hits addObject:product[@"id"]];
                                 }

                                 completion(hits);
                             }];
}

- (void)obtainMyFavId:(void (^)(NSNumber *favProductId))completion error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:kFavoritesUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 NSArray *responseDict = responseObject[@"result"];

                                 if ([responseDict count]) {
                                     completion(responseDict[0][@"id"]);
                                 } else {
                                     completion(nil);
                                 }
                             }];
}

@end

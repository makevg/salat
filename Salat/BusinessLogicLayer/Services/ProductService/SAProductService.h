//
//  SAProductService.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseService.h"

@class ProductCategory;
@class SAProductCategoryParser;
@class SAProductParser;
@class Product;
@class SAProductModifierParser;

@interface SAProductService : SABaseService

@property(nonatomic) SAProductCategoryParser *productCategoryParser;
@property(nonatomic) SAProductParser *productParser;
@property(nonatomic) SAProductModifierParser *productModifierParser;

- (void) obtainProductCategories:(void (^)(NSArray<ProductCategory *> *))completion error:(void (^)(NSError *))errorBlock;

- (void) obtainProductsByCategoryId:(NSNumber *)categoryId copletion:(void (^)(NSArray<Product *> *))completion error:(void (^)(NSError *))errorBlock;

- (Product *)obtainProductByCategoryId:(NSNumber *)categoryId;

- (ProductCategory *)obtainProductCategoryById:(NSNumber *)categoryId;

- (Product *)obtainProductById:(NSNumber *)productId;

- (void)obtainHits:(void (^)(NSArray <NSNumber *> *hits))completion error:(void (^)(NSError *))errorBlock;

- (void)obtainMyFavId:(void (^)(NSNumber *favProductId))completion error:(void (^)(NSError *))errorBlock;

@end

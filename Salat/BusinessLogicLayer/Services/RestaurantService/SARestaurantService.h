//
//  SARestaurantService.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseService.h"

@protocol SAPrototypeParser;
@class Restaurant;
@class DeliveryTime;

@interface SARestaurantService : SABaseService

@property(nonatomic) id <SAPrototypeParser> restaurantParser;
@property(nonatomic) id <SAPrototypeParser> deliveryAreaParser;
@property(nonatomic) id <SAPrototypeParser> deliveryTimeParser;
@property(nonatomic) id <SAPrototypeParser> paymentMethodParser;

- (Restaurant *)obtainRestaurantFromCache;

- (void)obtainRestaurant:(void (^)(Restaurant *))completion
                   error:(void (^)(NSError *))errorBlock;

- (void)obtainRestaurantInfoFromNetwork:(void (^)(Restaurant *))completion
                                  error:(void (^)(NSError *))errorBlock;

- (NSString *)checkWorking;

@end

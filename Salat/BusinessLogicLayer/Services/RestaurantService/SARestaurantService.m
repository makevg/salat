//
//  SARestaurantService.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARestaurantService.h"
#import "SAPrototypeParser.h"
#import "Restaurant.h"
#import "DeliveryTime.h"
#import "DeliveryArea.h"
#import "PaymentMethod.h"
#import "SADataFormatter.h"
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <DateTools/DateTools.h>

static NSString *const cRestaurantInfoUrl = @"restaurants";

@implementation SARestaurantService

#pragma mark - Public

- (Restaurant *)obtainRestaurantFromCache {
    return [Restaurant MR_findFirstInContext:[SALAT_DB produceContextForRead]];
}

- (void)obtainRestaurant:(void (^)(Restaurant *))completion
                   error:(void (^)(NSError *))errorBlock {
    Restaurant *restaurant = [self obtainRestaurantFromCache];
    if (restaurant) {
        completion(restaurant);
    }

    [self obtainRestaurantInfoFromNetwork:completion
                                    error:errorBlock];
}

- (void)obtainRestaurantInfoFromNetwork:(void (^)(Restaurant *))completion
                                  error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:cRestaurantInfoUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 NSDictionary *responseDict = responseObject[@"result"];

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                             Restaurant *restaurant = [Restaurant MR_findFirstOrCreateByAttribute:@"id"
                                                                                                        withValue:responseDict[@"id"]
                                                                                                        inContext:localContext];
                                             [strongSelf.restaurantParser fillObject:restaurant fromResponse:responseDict];

                                             NSArray *deliveryTimesList = responseDict[@"delivery_times"];
                                             [strongSelf saveTimes:deliveryTimesList toRestaurant:restaurant inContext:localContext];

                                             NSArray *deliveryAreasList = responseDict[@"delivery_areas"];
                                             [strongSelf saveAreas:deliveryAreasList toRestaurant:restaurant inContext:localContext];

                                             NSArray *paymentMethodsList = responseDict[@"payment_methods"];
                                             [strongSelf savePayMethods:paymentMethodsList toRestaurant:restaurant inContext:localContext];
                                         }
                                              completion:^(BOOL contextDidSave, NSError *_) {
                                                  Restaurant *restaurant = [strongSelf obtainRestaurantFromCache];
                                                  completion(restaurant);
                                              }];
                             }];
}

- (NSString *)checkWorking {
    NSDate *now = [NSDate date];
    NSDate *timeFromDay = [[NSCalendar currentCalendar] startOfDayForDate:now];
    NSTimeInterval distanceBetweenDates = [now secondsFrom:timeFromDay];

    NSInteger dow = ([now weekday] - 1);
    if (!dow)
        dow = 7; //воскресенье по api

    NSInteger nextDow = (dow % 7) + 1;
    NSTimeInterval secondsAgo = [timeFromDay timeIntervalSince1970];

    DeliveryTime *specialDay = [DeliveryTime MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"work_date == %@", @(secondsAgo)]
                                                             inContext:[SALAT_DB produceContextForRead]];

    NSString* (^printBlock)(NSInteger) = ^(NSInteger seconds) {
        NSInteger minutes = seconds / 60;
        NSString *closedString;

        if (minutes >= 60) {
            closedString = [NSString stringWithFormat:@"%@ %@", [SADataFormatter stringByHoursCount:minutes / 60], [SADataFormatter stringByMinutesCount:minutes % 60]];
        } else {
            closedString = [NSString stringWithFormat:@"%@", [SADataFormatter stringByMinutesCount:minutes % 60]];
        }

        return closedString;
    };

    NSString* (^checkNextDay)() = ^() {
        NSArray <DeliveryTime *> *nextDay = [DeliveryTime MR_findAllSortedBy:@"time_start"
                                                                   ascending:YES
                                                               withPredicate:[NSPredicate predicateWithFormat:@"day_number == %@", @(nextDow)]
                                                                   inContext:[SALAT_DB produceContextForRead]];

        for (DeliveryTime *day in nextDay) {
            if ([day.is_opened boolValue]) {
                return printBlock(86400 - (int) distanceBetweenDates + [day.time_start integerValue]);
            }
        }

        return @"более 24 часов";
    };

    if (specialDay) {
        //today is special day
        if ([specialDay.is_opened boolValue] &&
                [specialDay.time_start integerValue] <= distanceBetweenDates &&
                distanceBetweenDates < [specialDay.time_end integerValue]) {

            return nil;
        } else {
            //today isClosed
            return checkNextDay();
        }
    } else {
        //today is usual day
        NSArray <DeliveryTime *> *today = [DeliveryTime MR_findAllSortedBy:@"time_start"
                                                                 ascending:YES
                                                             withPredicate:[NSPredicate predicateWithFormat:@"day_number == %@", @(dow)]
                                                                 inContext:[SALAT_DB produceContextForRead]];

        for (DeliveryTime *day in today) {
            if ([day.is_opened boolValue] &&
                    [day.time_start integerValue] <= distanceBetweenDates &&
                    distanceBetweenDates < [day.time_end integerValue]) {
                return nil;
            }
            
            if ([day.is_opened boolValue] && [day.time_start integerValue] > distanceBetweenDates) {
                return printBlock([day.time_start integerValue] - (int) distanceBetweenDates);
            }
        }

        return checkNextDay();
    }
}

#pragma mark - Private

- (void)saveTimes:(NSArray *)times toRestaurant:(Restaurant *)restaurant inContext:(NSManagedObjectContext *)context {
    [restaurant removeDelivery_times:restaurant.delivery_times];

    for (NSDictionary *deliveryTimeDict in times) {
        DeliveryTime *deliveryTime = [DeliveryTime MR_findFirstOrCreateByAttribute:@"id"
                                                                         withValue:deliveryTimeDict[@"id"]
                                                                         inContext:context];
        [self.deliveryTimeParser fillObject:deliveryTime fromResponse:deliveryTimeDict];
        [restaurant addDelivery_timesObject:deliveryTime];
    }
}

- (void)saveAreas:(NSArray *)areas toRestaurant:(Restaurant *)restaurant inContext:(NSManagedObjectContext *)context {
    [restaurant removeDelivery_areas:restaurant.delivery_areas];

    for (NSDictionary *deliveryAreaDict in areas) {
        DeliveryArea *deliveryArea = [DeliveryArea MR_findFirstOrCreateByAttribute:@"id"
                                                                         withValue:deliveryAreaDict[@"id"]
                                                                         inContext:context];
        [self.deliveryAreaParser fillObject:deliveryArea fromResponse:deliveryAreaDict];
        [restaurant addDelivery_areasObject:deliveryArea];
    }
}

- (void)savePayMethods:(NSArray *)payMethods toRestaurant:(Restaurant *)restaurant inContext:(NSManagedObjectContext *)context {
    [restaurant removePayment_methods:restaurant.payment_methods];

    for (NSDictionary *paymentMethodDict in payMethods) {
        PaymentMethod *paymentMethod = [PaymentMethod MR_findFirstOrCreateByAttribute:@"id"
                                                                            withValue:paymentMethodDict[@"id"]
                                                                            inContext:context];
        [self.paymentMethodParser fillObject:paymentMethod fromResponse:paymentMethodDict];
        [restaurant addPayment_methodsObject:paymentMethod];
    }
}

@end

//
//  SAAddressService.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseService.h"
#import <CoreLocation/CoreLocation.h>

@class SAAddressPlainObject;
@class Address;

@interface SAAddressService : SABaseService

- (void)obtainAddressByLocation:(CLLocationCoordinate2D)location
                        success:(void (^)(SAAddressPlainObject *address))success
                          error:(void (^)(NSError *))errorBlock;

- (void)obtainAddressesSuccess:(void (^)(NSArray <Address *> *address))success
                         error:(void (^)(NSError *))errorBlock;

- (void)obtainGeocodeByStreet:(NSString *)street
                        house:(NSString *)house
                      success:(void (^)(CLLocationCoordinate2D coordinate))success
                        error:(void (^)(NSError *))errorBlock;

- (NSArray <Address *> *)obtainLocalAddresses;

- (void)saveAddress:(SAAddressPlainObject *)addressPlainObject
            success:(void (^)())success
              error:(void (^)(NSError *))errorBlock;

- (void)destroyAddress:(NSNumber *)addressId
               success:(void (^)())success
                 error:(void (^)(NSError *))errorBlock;

- (void)setMain:(NSNumber *)addressId
        success:(void (^)())success
          error:(void (^)(NSError *))errorBlock;

- (Address *)obtainAddressById:(NSNumber *)addressId;

@end

//
//  SAAddressService.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalFinders.h>
#import "SAAddressService.h"
#import "SAAddressPlainObject.h"
#import "Address.h"
#import "SAAddressParser.h"
#import "Profile.h"
#import "SACommon.h"
#import "SADataFormatter.h"

static NSString *const cGeocodeUrl = @"https://geocode-maps.yandex.ru/1.x/";
static NSString *const cUpdateUrl = @"users/addresses/%@/update";
static NSString *const cAddressesUrl = @"users/addresses";
static NSString *const cGeoCodeUrl = @"users/addresses/geocoder";
static NSString *const cSetMainUrl = @"users/addresses/%@/main";
static NSString *const cStoreUrl = @"users/addresses/store";
static NSString *const cDeleteUrl = @"users/addresses/%@/destroy";

@implementation SAAddressService

#pragma mark - Public

- (void)obtainAddressByLocation:(CLLocationCoordinate2D)location
                        success:(void (^)(SAAddressPlainObject *address))success
                          error:(void (^)(NSError *))errorBlock {
    NSString *locationString = [NSString stringWithFormat:@"%f,%f", location.longitude, location.latitude];

    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:cGeocodeUrl
                              params:@{@"format" : @"json",
                                      @"geocode" : locationString,
                                      @"kind" : @"house",
                                      @"results" : @(1)
                                       }
                           useDomain:NO
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 NSDictionary *responseDict = responseObject[@"response"];
                                 NSDictionary *geoObjCollectionDict = responseDict[@"GeoObjectCollection"];
                                 NSArray *featureMemberArray = geoObjCollectionDict[@"featureMember"];

                                 if ([featureMemberArray count] > 0) {
                                     NSDictionary *geoObjectDict = [featureMemberArray firstObject];
                                     NSDictionary *geocoderMetaData = geoObjectDict[@"GeoObject"][@"metaDataProperty"][@"GeocoderMetaData"];
                                     NSDictionary *localityDict = geocoderMetaData[@"AddressDetails"][@"Country"][@"AdministrativeArea"][@"SubAdministrativeArea"][@"Locality"];

                                     SAAddressPlainObject *address = [SAAddressPlainObject new];
                                     address.city = localityDict[@"LocalityName"];
                                     NSDictionary *thoroughfareDict = localityDict[@"Thoroughfare"];
                                     address.street = thoroughfareDict[@"ThoroughfareName"];
                                     NSDictionary *premiseDict = thoroughfareDict[@"Premise"];
                                     address.house = premiseDict[@"PremiseNumber"];

                                     NSString *positionString = geoObjectDict[@"GeoObject"][@"Point"][@"pos"];
                                     NSArray *positionArray = [positionString componentsSeparatedByString:@" "];
                                     address.location = [[CLLocation alloc] initWithLatitude:[[positionArray lastObject] floatValue]
                                                                                   longitude:[[positionArray firstObject] floatValue]];

                                     success(address);
                                 }
                             }];
}

- (NSArray <Address *> *)obtainLocalAddresses {
    Profile *profile = [Profile MR_findFirstInContext:[SALAT_DB produceContextForRead]];
    NSSet<Address *> *orders = profile.addresses;

    if (orders) {
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"updated_at"
                                                                   ascending:NO
                                                                  comparator:^NSComparisonResult(NSNumber *obj1, NSNumber *obj2) {
                                                                      return [obj1 compare:obj2];
                                                                  }];
        return [orders sortedArrayUsingDescriptors:@[descriptor]];
    }

    return @[];
}

- (void)obtainAddressesSuccess:(void (^)(NSArray <Address *> *address))success error:(void (^)(NSError *))errorBlock {
    success([self obtainLocalAddresses]);

    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:cAddressesUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }


                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                             NSArray *addressesList = responseObject[@"result"];
                                             NSMutableSet<Address *> *addresses = [NSMutableSet<Address *> new];
                                             SAAddressParser *parser = [SAAddressParser new];

                                             for (NSDictionary *addressDict in addressesList) {
                                                 Address *address = [Address MR_findFirstOrCreateByAttribute:@"id"
                                                                                                   withValue:addressDict[@"id"]
                                                                                                   inContext:localContext];
                                                 [parser fillObject:address fromResponse:addressDict];
                                                 [addresses addObject:address];
                                             }


                                             Profile *profile = [Profile MR_findFirstInContext:localContext];
                                             [profile setAddresses:addresses];
                                         }
                                              completion:^(BOOL contextDidSave, NSError *_) {
                                                  success([self obtainLocalAddresses]);
                                              }];
                             }];
}

- (void)obtainGeocodeByStreet:(NSString *)street
                        house:(NSString *)house
                      success:(void (^)(CLLocationCoordinate2D coordinate))success
                        error:(void (^)(NSError *))errorBlock {
    weakifySelf;

    [SALAT_API requestPostToUrlString:cGeoCodeUrl
                               params:@{
                                       @"street": street,
                                       @"house": house
                               }
                            useDomain:NO
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  strongifySelf;

                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  NSString *geopoint = responseObject[@"result"][@"geo_point"];
                                  if( geopoint ) {
                                      NSArray *geoPointArr = [geopoint componentsSeparatedByString:@";"];

                                      CLLocationCoordinate2D coordinate2D = CLLocationCoordinate2DMake(
                                              [[SADataFormatter numberByString:geoPointArr[0]] doubleValue],
                                              [[SADataFormatter numberByString:geoPointArr[1]] doubleValue]
                                      );

                                      success(coordinate2D);
                                  }
                              }];
}

- (void)saveAddress:(SAAddressPlainObject *)addressPlainObject success:(void (^)())success error:(void (^)(NSError *))errorBlock {
    NSString *url = nil;

    if (addressPlainObject.addressId != nil) {
        url = [NSString stringWithFormat:cUpdateUrl, addressPlainObject.addressId];
    } else {
        url = cStoreUrl;
    }

    NSMutableDictionary *params = [@{} mutableCopy];

    params[@"street"] = addressPlainObject.street;
    params[@"house"] = addressPlainObject.house;
    params[@"title"] = addressPlainObject.title;

    params[@"flat"] = addressPlainObject.flat;

    if ([addressPlainObject.floor length])
        params[@"floor"] = addressPlainObject.floor;

    params[@"entrance"] = addressPlainObject.entrance;
    params[@"intercom_code"] = addressPlainObject.intercomCode;

    if (addressPlainObject.location.coordinate.latitude > 0 && addressPlainObject.location.coordinate.longitude > 0)
        params[@"geo_point"] = [NSString stringWithFormat:@"%f;%f", addressPlainObject.location.coordinate.latitude, addressPlainObject.location.coordinate.longitude];

    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:url
                               params:params
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;

                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                              NSDictionary *responseDict = responseObject[@"result"];

                                              Address *address = [Address MR_findFirstOrCreateByAttribute:@"id"
                                                                                                withValue:responseDict[@"id"]
                                                                                                inContext:localContext];
                                              Profile *profile = [Profile MR_findFirstInContext:localContext];
                                              address.profile = profile;

                                              SAAddressParser *parser = [SAAddressParser new];

                                              [parser fillObject:address fromResponse:responseDict];
                                          }
                                               completion:^(BOOL contextDidSave, NSError *_) {
                                                   success();
                                               }];
                              }];
}

- (void)destroyAddress:(NSNumber *)addressId success:(void (^)())success error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:[NSString stringWithFormat:cDeleteUrl, addressId]
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                             [Address MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"id == %@", addressId]
                                                                          inContext:localContext];
                                         }
                                              completion:^(BOOL contextDidSave, NSError *_) {
                                                  success();
                                              }];
                             }];
}

- (void)setMain:(NSNumber *)addressId success:(void (^)())success error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:[NSString stringWithFormat:cSetMainUrl, addressId]
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                             Address *main_address = [Address MR_findFirstByAttribute:@"is_main"
                                                                                            withValue:@YES
                                                                                            inContext:localContext];
                                             if (main_address)
                                                 main_address.is_main = @NO;

                                             Address *address = [Address MR_findFirstByAttribute:@"id"
                                                                                       withValue:addressId
                                                                                       inContext:localContext];
                                             if (address)
                                                 address.is_main = @YES;
                                         }
                                              completion:^(BOOL contextDidSave, NSError *_) {
                                                  success();
                                              }];
                             }];
}


- (Address *)obtainAddressById:(NSNumber *)addressId {
    return [Address MR_findFirstByAttribute:@"id" withValue:addressId inContext:[SALAT_DB produceContextForRead]];
}

@end

//
//  SABaseService.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseService.h"

NSString *const kErrorDomain = @"ru.salat.api.error.domain";


@implementation SABaseService

- (NSError *)handleResponse:(NSURLResponse *)response
         withResponseObject:(id)responseObject
                      error:(NSError *)error
                      block:(void (^)(NSError *))errorBlock {
    NSDictionary *responseDict = responseObject;

    if (response && responseObject) {
        NSString *errorString = responseDict[@"error"];
        if (errorString && [errorString length] != 0) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;

            NSError *serverError = [NSError errorWithDomain:kErrorDomain
                                                       code:httpResponse.statusCode
                                                   userInfo:@{
                                                           NSLocalizedDescriptionKey : errorString
                                                   }];

            if (errorBlock)
                errorBlock(serverError);

            return serverError;
        }
    }

    if (error) {
        if (errorBlock) {
            NSString *errorString;

            if (response && [response isKindOfClass:[NSHTTPURLResponse class]]) {
                switch (((NSHTTPURLResponse *)response).statusCode) {
                    case 404:
                        errorString = @"Метод отсутствует на сервере";
                        break;
                    case 401:
                        errorString = @"Вы не авторизованы";
                        break;
                    case 502:
                        errorString = @"Сервер временно недоступен";
                        break;
                    case 500:
                    default:
                        errorString = @"Внутренняя ошибка сервера";
                }
            } else {
                errorString = @"Проблемы с подключением к интернету";
            }

            NSError *networkError = [NSError errorWithDomain:error.domain
                                                        code:error.code
                                                    userInfo:@{
                                                            NSLocalizedDescriptionKey : errorString
                                                    }];

            errorBlock(networkError);
        }

        return error;
    }

    return nil;
}

@end

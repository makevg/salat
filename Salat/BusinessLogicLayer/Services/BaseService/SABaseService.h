//
//  SABaseService.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAAPI.h"
#import "SADB.h"

extern NSString *const kErrorDomain;
extern NSInteger const kUnknownErrorCode;

@protocol ServiceResultDelegate;

@interface SABaseService : NSObject

- (NSError *)handleResponse:(NSURLResponse *)response
         withResponseObject:(id)responseObject
                      error:(NSError *)error
                      block:(void (^)(NSError *))errorBlock;

@end

//
//  SASalesService.h
//  Salat
//
//  Created by Maximychev Evgeny on 09.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SABaseService.h"

@class SASaleParser;
@class Sale;

@interface SASalesService : SABaseService

@property(nonatomic) SASaleParser *saleParser;

- (void)obtainSales:(void (^)(NSArray<Sale *> *data))completion
              error:(void (^)(NSError *))errorBlock;

- (void)obtainSalesFromNetwork:(void (^)(NSArray<Sale *> *data))completion
                         error:(void (^)(NSError *))errorBlock;

@end

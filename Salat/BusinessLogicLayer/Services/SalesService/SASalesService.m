//
//  SASalesService.m
//  Salat
//
//  Created by Maximychev Evgeny on 09.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASalesService.h"
#import "Sale.h"
#import "SASaleParser.h"
#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRequests.h>

NSString *const cSalesUrl = @"deals";

@implementation SASalesService

#pragma mark - Public

- (void)obtainSales:(void (^)(NSArray<Sale *> *data))completion
              error:(void (^)(NSError *))errorBlock {
    NSArray<Sale *> *sales = [self obtainSalesFromCache];
    if (sales) {
        completion(sales);
    }

    [self obtainSalesFromNetwork:completion
                           error:errorBlock];
}

- (void)obtainSalesFromNetwork:(void (^)(NSArray<Sale *> *data))completion
                         error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:cSalesUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                             NSDictionary *responseDict = responseObject;
                                             NSArray *salesList = responseDict[@"result"];
                                             NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                             for (NSDictionary *dictSales in salesList) {
                                                 Sale *sale = [Sale MR_findFirstOrCreateByAttribute:@"id"
                                                                                          withValue:dictSales[@"id"]
                                                                                          inContext:localContext];
                                                 [existsArrayIds addObject:dictSales[@"id"]];
                                                 [strongSelf.saleParser fillObject:sale fromResponse:dictSales];
                                             }

                                             [Sale MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", existsArrayIds]
                                                                       inContext:localContext];
                                         }
                                              completion:^(BOOL contextDidSave, NSError *_) {
                                                  NSArray<Sale *> *sales = [strongSelf obtainSalesFromCache];
                                                  completion(sales);
                                              }];
                             }];
}

#pragma mark - Private

- (NSArray<Sale *> *)obtainSalesFromCache {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [Sale MR_requestAllSortedBy:@"published_at"
                                                     ascending:NO
                                                 withPredicate:[NSPredicate predicateWithValue:YES]
                                                     inContext:moc];
    return [Sale MR_executeFetchRequest:fetchRequest inContext:moc];
}

@end

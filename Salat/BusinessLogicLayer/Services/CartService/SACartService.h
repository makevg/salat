//
//  SACartService.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseService.h"

extern NSString *const SACartServiceTotalAmountChangedNotificationName;
extern NSString *const SACartServiceTotalAmountChangedNotificationKey;

extern NSString *const SACartServiceProductTypeHit;
extern NSString *const SACartServiceProductTypeRecommendation;
extern NSString *const SACartServiceProductTypeSuggestion;

@class CartItem;
@class SACartItemModifierPlainObject;
@class SAOrderPlainObject;
@protocol SAPrototypeMapper;

@interface SACartService : SABaseService

+ (instancetype)sharedInstance;

- (void)plusItemWithProductId:(NSNumber *)prodId
                        title:(NSString *)title
                        descr:(NSString *)descr
                     imageUrl:(NSString *)imageUrl
                         cost:(NSNumber *)cost
                    modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers;

- (void)plusItemWithProductId:(NSNumber *)prodId
                        title:(NSString *)title
                        descr:(NSString *)descr
                     imageUrl:(NSString *)imageUrl
                         cost:(NSNumber *)cost
                  productType:(NSString *)productType
                    modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers;

- (void)minusItemWithProductId:(NSNumber *)prodId
                     modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers;

- (void)changeItemQuantity:(NSNumber *)quantity itemId:(NSNumber *)cartItemId;

- (void)repeatOrder:(SAOrderPlainObject *)orderPlainObject mapper:(id<SAPrototypeMapper>) itemModifierMapper;

//- (void)removeItemById:(NSNumber *)cartItemId;

//- (NSInteger)itemPositionById:(NSInteger)cartItemId;

- (NSNumber *)obtainItemQuantityByProductId:(NSNumber *)productId;

- (NSArray<CartItem *> *)obtainCartItems;

- (void)clearCart:(void (^)())completion;

- (BOOL)cartIsEmpty;

- (BOOL)hasSuggestion;

- (double)obtainTotalAmount;

@end

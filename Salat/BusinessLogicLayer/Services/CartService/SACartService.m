//
//  SACartService.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartService.h"
#import "CartItem.h"
#import "SACartItemModifierPlainObject.h"
#import "CartItemModifier.h"
#import "SAOrderPlainObject.h"
#import "SAOrderProductPlainObject.h"
#import "SAPrototypeMapper.h"
#import "Cart.h"
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRequests.h>

NSString *const SACartServiceTotalAmountChangedNotificationName = @"SACartServiceTotalAmountChangedNotificationName";
NSString *const SACartServiceTotalAmountChangedNotificationKey = @"SACartServiceTotalAmountChangedNotificationKey";

NSString *const SACartServiceProductTypeHit = @"hit";
NSString *const SACartServiceProductTypeRecommendation = @"recommendation";
NSString *const SACartServiceProductTypeSuggestion = @"suggestion";

@implementation SACartService

#pragma mark - Private

- (void)removeItemsWithPredicate:(NSPredicate *)predicate
                         context:(NSManagedObjectContext *)context {
    [CartItem MR_deleteAllMatchingPredicate:predicate
                                  inContext:context];
}

- (void)postNotify {
    double totalAmount = [self obtainTotalAmount];
    NSDictionary *notifyUserInfo = @{SACartServiceTotalAmountChangedNotificationKey : @(totalAmount)};
    [[NSNotificationCenter defaultCenter] postNotificationName:SACartServiceTotalAmountChangedNotificationName
                                                        object:nil
                                                      userInfo:notifyUserInfo];
}

- (CartItem *)getItemByProductId:(NSNumber *)productId
                       modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers
                         context:(NSManagedObjectContext *)context {
    NSMutableArray *modifiersIdsMutable = [NSMutableArray new];
    for (SACartItemModifierPlainObject *item in modifiers) {
        [modifiersIdsMutable addObject:item.itemModifierId];
    }

    NSArray *modIds = [modifiersIdsMutable sortedArrayUsingSelector:@selector(compare:)];
    NSMutableString *stringId = [NSMutableString new];
    for (NSNumber *modId in modIds) {
        [stringId appendFormat:@"_%@", modId];
    }

    [stringId appendFormat:@"_%@", productId];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", stringId];
    CartItem *item = [CartItem MR_findFirstWithPredicate:predicate
                                               inContext:context];

    return item;
}

- (void)plusItemWithProductId:(NSNumber *)prodId
                        title:(NSString *)title
                        descr:(NSString *)descr
                     imageUrl:(NSString *)imageUrl
                         cost:(NSNumber *)cost
                    modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers
                      context:(NSManagedObjectContext *)localContext {
    CartItem *existCartItem = [self getItemByProductId:prodId
                                             modifiers:modifiers
                                               context:localContext];

    if (existCartItem) {
        existCartItem.quantity = @([existCartItem.quantity integerValue] + 1);
        return;
    }

    CartItem *cartItem = [CartItem MR_createEntityInContext:localContext];
    cartItem.added = [NSDate new];

    if ([modifiers count] > 0) {
        double totalSum = 0;
        NSString *itemDescr = @"";
        NSMutableString *stringId = [NSMutableString new];
        NSMutableArray *modIdsMutable = [NSMutableArray new];

        NSMutableSet<CartItemModifier *> *cartItemModifiers = [NSMutableSet<CartItemModifier *> new];
        for (SACartItemModifierPlainObject *item in modifiers) {
            CartItemModifier *itemModifier = [CartItemModifier MR_createEntityInContext:localContext];
            [modIdsMutable addObject:item.itemModifierId];

            itemModifier.id = item.itemModifierId;
            itemModifier.title = item.title;
            itemModifier.price = item.price;
            itemModifier.quantity = item.quantity;
            [cartItemModifiers addObject:itemModifier];
            totalSum += [item.price doubleValue];
            if ([itemDescr length])
                itemDescr = [itemDescr stringByAppendingString:@"\r\n"];
            itemDescr = [itemDescr stringByAppendingString:[NSString stringWithFormat:@"%@", item.title]];
        }

        NSArray *modIds = [modIdsMutable sortedArrayUsingSelector:@selector(compare:)];
        for (NSNumber *modId in modIds) {
            [stringId appendFormat:@"_%@", modId];
        }

        [stringId appendFormat:@"_%@", prodId];

        cartItem.id = stringId;
        cartItem.cost = @([cost doubleValue] + totalSum);
        cartItem.modifiers = cartItemModifiers;

    } else {
        cartItem.id = [NSString stringWithFormat:@"_%@", prodId];
        cartItem.cost = cost;
    }

    cartItem.item_id = @([cartItem hash]);
    cartItem.product_id = prodId;
    cartItem.product_title = title;
    cartItem.product_descr = descr;
    cartItem.product_image_url = imageUrl;
    cartItem.quantity = @1;
}


#pragma mark - Public

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)plusItemWithProductId:(NSNumber *)prodId
                        title:(NSString *)title
                        descr:(NSString *)descr
                     imageUrl:(NSString *)imageUrl
                         cost:(NSNumber *)cost
                    modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers {
    [self plusItemWithProductId:prodId
                          title:title
                          descr:descr
                       imageUrl:imageUrl
                           cost:cost
                    productType:nil
                      modifiers:modifiers];
}

- (void)plusItemWithProductId:(NSNumber *)prodId
                        title:(NSString *)title
                        descr:(NSString *)descr
                     imageUrl:(NSString *)imageUrl
                         cost:(NSNumber *)cost
                  productType:(NSString *)productType
                    modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers {
    __weak __typeof(self) weakSelf = self;
    [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                [weakSelf plusItemWithProductId:prodId
                                          title:title
                                          descr:descr
                                       imageUrl:imageUrl
                                           cost:cost
                                      modifiers:modifiers
                                        context:localContext];

                if ([productType length]) {
                    Cart *cart = [Cart MR_findFirstInContext:localContext];
                    if (!cart)
                        cart = [Cart MR_createEntityInContext:localContext];

                    if ([productType isEqualToString:SACartServiceProductTypeHit])
                        cart.hit = prodId;

                    if ([productType isEqualToString:SACartServiceProductTypeSuggestion])
                        cart.suggestion = prodId;

                    if ([productType isEqualToString:SACartServiceProductTypeRecommendation])
                        cart.recommendation = prodId;
                }
            }
                 completion:^(BOOL contextDidSave, NSError *error) {
                     [weakSelf postNotify];
                 }];
}

- (void)minusItemWithProductId:(NSNumber *)prodId
                     modifiers:(NSArray<SACartItemModifierPlainObject *> *)modifiers {
    __weak __typeof(self) weakSelf = self;

    [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                CartItem *cartItem = [weakSelf getItemByProductId:prodId
                                                        modifiers:modifiers
                                                          context:localContext];

                if (cartItem) {
                    if ([cartItem.quantity integerValue] == 1) {
                        [cartItem MR_deleteEntityInContext:localContext];

                        if (![CartItem MR_findFirstInContext:localContext]) {
                            [Cart MR_truncateAllInContext:localContext];
                        }
                    } else {
                        cartItem.quantity = @([cartItem.quantity integerValue] - 1);
                    }
                }
            }
                 completion:^(BOOL contextDidSave, NSError *error) {
                     [weakSelf postNotify];
                 }];
}

- (void)changeItemQuantity:(NSNumber *)quantity itemId:(NSNumber *)cartItemId {
    __weak __typeof(self) weakSelf = self;
    [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                CartItem *item = [CartItem MR_findFirstByAttribute:@"item_id"
                                                         withValue:cartItemId
                                                         inContext:localContext];

                if (!item) {
                    return;
                }

                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_id == %@", item.item_id];
                [quantity integerValue] > 0 ? item.quantity = quantity : [weakSelf removeItemsWithPredicate:predicate
                                                                                                    context:localContext];

                if (![CartItem MR_findFirstInContext:localContext]) {
                    [Cart MR_truncateAllInContext:localContext];
                }
            }
                 completion:^(BOOL contextDidSave, NSError *error) {
                     [weakSelf postNotify];
                 }];
}

- (void)repeatOrder:(SAOrderPlainObject *)orderPlainObject mapper:(id <SAPrototypeMapper>)itemModifierMapper {
    __weak __typeof(self) weakSelf = self;
    [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                for (SAOrderProductPlainObject *product in orderPlainObject.products) {
                    NSMutableArray<SACartItemModifierPlainObject *> *itemModifiers = [NSMutableArray<SACartItemModifierPlainObject *> new];

                    for (SAOrderProductModifierPlainObject *productModifier in product.modifiers) {
                        SACartItemModifierPlainObject *itemModifier = [SACartItemModifierPlainObject new];
                        [itemModifierMapper fillObject:itemModifier withObject:productModifier];
                        [itemModifiers addObject:itemModifier];
                    }

                    [weakSelf plusItemWithProductId:product.productId
                                              title:product.title
                                              descr:product.descr
                                           imageUrl:product.imageUrl
                                               cost:product.price
                                          modifiers:itemModifiers
                                            context:localContext];

                    CartItem *productCartItem = [weakSelf getItemByProductId:product.productId
                                                                   modifiers:itemModifiers
                                                                     context:localContext];

                    productCartItem.quantity = @([productCartItem.quantity integerValue] + [product.quantity integerValue] - 1);
                }
            }
                 completion:^(BOOL contextDidSave, NSError *error) {
                     [weakSelf postNotify];
                 }];
}

- (NSNumber *)obtainItemQuantityByProductId:(NSNumber *)productId {
    return [self getItemByProductId:productId
                          modifiers:nil
                            context:[SALAT_DB produceContextForRead]].quantity;
}

- (NSArray<CartItem *> *)obtainCartItems {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [CartItem MR_requestAllSortedBy:@"added"
                                                         ascending:YES
                                                     withPredicate:[NSPredicate predicateWithValue:YES]
                                                         inContext:moc];
    return [CartItem MR_executeFetchRequest:fetchRequest inContext:moc];
}

- (void)clearCart:(void (^)())completion {
    __weak __typeof(self) weakSelf = self;
    [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                [weakSelf removeItemsWithPredicate:[NSPredicate predicateWithValue:YES] context:localContext];
                [Cart MR_truncateAllInContext:localContext];
            }
                 completion:^(BOOL contextDidSave, NSError *error) {
                     [weakSelf postNotify];
                     completion();
                 }];
}

- (BOOL)cartIsEmpty {
    return [[self obtainCartItems] count] == 0;
}

- (BOOL)hasSuggestion {
    return [[Cart MR_findFirstInContext:[SALAT_DB produceContextForRead]] suggestion] != nil;
}

- (double)obtainTotalAmount {
    double total = 0;
    NSArray<CartItem *> *items = [self obtainCartItems];
    for (CartItem *item in items) {
        total += ([item.cost doubleValue] * [item.quantity doubleValue]);
    }
    return (int) total;
}

@end

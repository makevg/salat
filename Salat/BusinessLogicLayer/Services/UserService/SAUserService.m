//
//  SAUserService.m
//  Salat
//
//  Created by Maximychev Evgeny on 12.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import "SAUserService.h"
#import "SAPrototypeParser.h"
#import "Profile.h"
#import "Address.h"
#import "Order.h"
#import "Review.h"
#import "SACommon.h"

static NSString *const cLogoutUrl = @"users/logout";
static NSString *const cLoginCodeUrl = @"users/login";
static NSString *const cSendCodeUrl = @"users/phone/sendcode";
static NSString *const cPhoneUpdateCodeUrl = @"users/phone/update";

static NSString *const cLoginProviderUrl = @"users/providers/login";
static NSString *const cRegisterProviderUrl = @"users/providers/register";
static NSString *const cBindProviderUrl = @"users/providers/store";
static NSString *const cUnbindProviderUrl = @"users/providers/destroy";

static NSString *const cProfileUpdateCodeUrl = @"users/profile/update";
static NSString *const cProfileUrl = @"users/profile";

@implementation SAUserService

- (bool)isAuthorized {
    return [self obtainLocalProfile] != nil;
}

- (void)logout:(void (^)())completion {
    [SALAT_API requestGetToUrlString:cLogoutUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 if (completion)
                                     completion();
                             }];

    [self clearProfileData];
}

- (void)loginWithProvider:(SocialRegisterType)provider
                    token:(NSString *)token
               completion:(void (^)(BOOL))completion
                    error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cLoginProviderUrl
                               params:@{
                                       @"provider" : [self stringSocialProvider:provider],
                                       @"token" : token
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  NSDictionary *resultDict = responseObject[@"result"];
                                  BOOL isLoggedIn = [resultDict[@"is_logged_in"] boolValue];

                                  if (isLoggedIn) {
                                      [strongSelf obtainProfileFromNetwork:^{
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:kLoginNotification
                                                                                                      object:nil];
                                                  completion(isLoggedIn);
                                              }
                                                                     error:errorBlock];

                                  } else {
                                      completion(isLoggedIn);
                                  }

                              }];
}

- (void)validatePhoneFirstStepWithPhone:(NSString *)phone
                             completion:(void (^)())completion
                                  error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cSendCodeUrl
                               params:@{
                                       @"phone_number" : phone
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  completion();
                              }];
}

- (void)loginWithPhone:(NSString *)phone
               smsCode:(NSString *)code
            completion:(void (^)())completion
                 error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cLoginCodeUrl
                               params:@{
                                       @"phone_number" : phone,
                                       @"sms_code" : code
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  [strongSelf obtainProfileFromNetwork:^{
                                              [[NSNotificationCenter defaultCenter] postNotificationName:kLoginNotification
                                                                                                  object:nil];
                                              completion();
                                          }
                                                                 error:errorBlock];
                              }];
}

- (void)obtainProfile:(void (^)(Profile *profile))completion
                error:(void (^)(NSError *))errorBlock {
    weakifySelf;

    Profile *profile = [self obtainProfileFromCache];
    if (profile) {
        completion(profile);
    }

    [self obtainProfileFromNetwork:^{
        strongifySelf;

        if (strongSelf) {
            Profile *profile_ = [strongSelf obtainProfileFromCache];
            if (profile_) {
                completion(profile_);
            }
        }
    }                        error:errorBlock];
}

- (void)updateProfileByFirstName:(NSString *)firstName
                      secondName:(NSString *)secondName
                      completion:(void (^)())completion
                           error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cProfileUpdateCodeUrl
                               params:@{
                                       @"first_name" : firstName,
                                       @"last_name" : secondName
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }
                                  [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                              Profile *profile = [Profile MR_findFirstInContext:localContext];
                                              profile.first_name = firstName;
                                              profile.last_name = secondName;
                                          }
                                               completion:^(BOOL contextDidSave, NSError *_) {
                                                   completion();
                                               }];

                              }];
}

- (void)updatePhone:(NSString *)phone
            smsCode:(NSString *)code
         completion:(void (^)(NSString *))completion
              error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cPhoneUpdateCodeUrl
                               params:@{
                                       @"phone_number" : phone,
                                       @"sms_code" : code
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  [strongSelf obtainProfileFromNetwork:^{
                                              Profile *profile = [weakSelf obtainLocalProfile];
                                              completion(profile.phone_number);
                                          }
                                                                 error:errorBlock];
                              }];
}

- (void)bindProvider:(SocialRegisterType)provider
               token:(NSString *)token
          completion:(void (^)())completion
               error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cBindProviderUrl
                               params:@{
                                       @"provider" : [self stringSocialProvider:provider],
                                       @"token" : token
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  completion();
                              }];
}

- (void)unbindProvider:(SocialRegisterType)provider
            completion:(void (^)())completion
                 error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cUnbindProviderUrl
                               params:@{
                                       @"provider" : [self stringSocialProvider:provider]
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  completion();
                              }];
}

- (void)registerWithProvider:(SocialRegisterType)provider
                       token:(NSString *)token hone:(NSString *)phone
                     smsCode:(NSString *)code
                  completion:(void (^)())completion
                       error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cRegisterProviderUrl
                               params:@{
                                       @"provider" : [self stringSocialProvider:provider],
                                       @"token" : token,
                                       @"phone_number" : phone,
                                       @"sms_code" : code
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;
                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  [strongSelf obtainProfileFromNetwork:^{
                                              [[NSNotificationCenter defaultCenter] postNotificationName:kLoginNotification
                                                                                                  object:nil];
                                              completion();
                                          }
                                                                 error:errorBlock];
                              }];
}

- (void)checkSession:(void (^)())completion error:(void (^)(NSError *))errorBlock {
    weakifySelf;
    
    void(^errorHandler)(NSError *) = ^(NSError *error) {
        dispatch_safe_main_async(^{
            errorBlock(error);
        });
    };
    
    [SALAT_API requestGetToUrlString:cProfileUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 NSError *newError = [self handleResponse:response
                                                       withResponseObject:responseObject
                                                                    error:error
                                                                    block:errorHandler];
                                 if (newError ) {
                                     return;
                                 }
                                 
                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                     NSDictionary *responseDict = responseObject;
                                     [weakSelf saveProfile:responseDict context:localContext];
                                 }            completion:^(BOOL contextDidSave, NSError *_) {
                                     completion();
                                 }];
                                 
                             }];
}

- (Profile *)obtainLocalProfile {
    return [Profile MR_findFirstInContext:[SALAT_DB produceContextForRead]];
}

#pragma mark - Private

- (Profile *)obtainProfileFromCache {
    return [Profile MR_findFirstInContext:[SALAT_DB produceContextForRead]];
}

- (void)clearProfileData {
    [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                [Profile MR_truncateAllInContext:localContext];
                [Order MR_truncateAllInContext:localContext];
                [Review MR_truncateAllInContext:localContext];
            }
                 completion:^(BOOL contextDidSave, NSError *_) {
                     [[NSNotificationCenter defaultCenter] postNotificationName:kLogoutNotification
                                                                         object:nil];
                 }];

    [SALAT_API clearCookies];
}

- (void)saveProfile: (NSDictionary *)responseDict context:(NSManagedObjectContext *)context {
    NSDictionary *profileDict = responseDict[@"result"];
    Profile *profile = [Profile MR_findFirstOrCreateByAttribute:@"id"
                                                      withValue:profileDict[@"id"]
                                                      inContext:context];
    [self.profileParser fillObject:profile fromResponse:profileDict];
    
    NSArray *addressesList = profileDict[@"addresses"];
    NSMutableSet<Address *> *addresses = [NSMutableSet<Address *> new];
    for (NSDictionary *addressDict in addressesList) {
        Address *address = [Address MR_findFirstOrCreateByAttribute:@"id"
                                                          withValue:addressDict[@"id"]
                                                          inContext:context];
        [self.addressParser fillObject:address fromResponse:addressDict];
        [addresses addObject:address];
    }
    
    [profile setAddresses:addresses];
}

- (void)obtainProfileFromNetwork:(void (^)())completion error:(void (^)(NSError *))errorBlock {
    weakifySelf;

    [SALAT_API requestGetToUrlString:cProfileUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 strongifySelf;

                                 NSError *newError = [self handleResponse:response
                                                                  withResponseObject:responseObject
                                                                               error:error
                                                                               block:errorBlock];
                                 if (newError ) {
                                     if( newError.code == 401 )
                                         [strongSelf logout:nil];

                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                     NSDictionary *responseDict = responseObject;
                                     [weakSelf saveProfile:responseDict context:localContext];
                                 }            completion:^(BOOL contextDidSave, NSError *_) {
                                     completion();
                                 }];

                             }];
}

#pragma mark - Private

- (NSString *)stringSocialProvider:(SocialRegisterType)socialRegisterType {
    switch (socialRegisterType) {
        case eSRTFB:
            return @"fb";
        case eSRTVK:
            return @"vk";
        default:
            return @"";
    }
}

@end

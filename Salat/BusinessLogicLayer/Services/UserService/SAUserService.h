//
//  SAUserService.h
//  Salat
//
//  Created by Maximychev Evgeny on 12.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SABaseService.h"
#import "SAServiceLayer.h"

@protocol SAPrototypeParser;
@class Profile;

typedef NS_ENUM(NSUInteger, SocialRegisterType) {
    eSRTVK = 0,
    eSRTFB,
    eSRTNONE
};

@interface SAUserService : SABaseService

@property(nonatomic) id <SAPrototypeParser> profileParser;
@property(nonatomic) id <SAPrototypeParser> addressParser;

- (bool)isAuthorized;

- (void)obtainProfile:(void (^)(Profile *profile))completion
                error:(void (^)(NSError *))errorBlock;

- (void)updateProfileByFirstName:(NSString *)firstName
                      secondName:(NSString *)secondName
                      completion:(void (^)())completion
                           error:(void (^)(NSError *))errorBlock;

- (void)updatePhone:(NSString *)phone
            smsCode:(NSString *)code
         completion:(void (^)(NSString *))completion
              error:(void (^)(NSError *))errorBlock;

- (void)checkSession:(void (^)())completion
               error:(void (^)(NSError *))error;

- (void)loginWithPhone:(NSString *)phone
               smsCode:(NSString *)code
            completion:(void (^)())completion
                 error:(void (^)(NSError *))errorBlock;

- (void)logout:(void (^)())completion;

- (void)loginWithProvider:(SocialRegisterType)provider
                    token:(NSString *)token
               completion:(void (^)(BOOL))completion
                    error:(void (^)(NSError *))errorBlock;

- (void)bindProvider:(SocialRegisterType)provider
               token:(NSString *)token
          completion:(void (^)())completion
               error:(void (^)(NSError *))errorBlock;

- (void)unbindProvider:(SocialRegisterType)provider
            completion:(void (^)())completion
                 error:(void (^)(NSError *))errorBlock;

- (void)registerWithProvider:(SocialRegisterType)provider
                       token:(NSString *)token
                        hone:(NSString *)phone
                     smsCode:(NSString *)code
                  completion:(void (^)())completion
                       error:(void (^)(NSError *))errorBlock;

- (void)validatePhoneFirstStepWithPhone:(NSString *)phone
                             completion:(void (^)())completion
                                  error:(void (^)(NSError *))errorBlock;

- (Profile *)obtainLocalProfile;

@end

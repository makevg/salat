//
//  SANewsService.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SABaseService.h"

@class SANewsParser;
@class News;

@interface SANewsService : SABaseService

@property(nonatomic) SANewsParser *newsParser;

- (void)obtainNews:(void (^)(NSArray<News *> *data))completion
             error:(void (^)(NSError *))errorBlock;

- (News *)obtainNewsById:(NSNumber *)newsId;

- (void)obtainNewsFromNetwork:(void(^)(NSArray<News *> *data))completion
                        error:(void (^)(NSError *))errorBlock;

@end

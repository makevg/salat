//
//  SANewsService.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsService.h"
#import "News.h"
#import "SANewsParser.h"
#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRequests.h>

NSString *const cNewsUrl = @"news";

@implementation SANewsService

#pragma mark - Public

- (void)obtainNews:(void (^)(NSArray<News *> *data))completion error:(void (^)(NSError *))errorBlock {
    NSArray<News *> *news = [self obtainNewsFromCache];
    if (news) {
        completion(news);
    }
    
    [self obtainNewsFromNetwork:completion
                          error:errorBlock];
}

- (News *)obtainNewsById:(NSNumber *)newsId {
    return [News MR_findFirstByAttribute:@"id" withValue:newsId inContext:[SALAT_DB produceContextForRead]];
}

- (void)obtainNewsFromNetwork:(void(^)(NSArray<News *> *data))completion error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:cNewsUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                             NSDictionary *responseDict = responseObject;
                                             NSArray *newsList = responseDict[@"result"];
                                             NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                             for (NSDictionary *dictNews in newsList) {
                                                 News *news = [News MR_findFirstOrCreateByAttribute:@"id"
                                                                                          withValue:dictNews[@"id"]
                                                                                          inContext:localContext];
                                                 [existsArrayIds addObject:dictNews[@"id"]];
                                                 [strongSelf.newsParser fillObject:news fromResponse:dictNews];
                                             }

                                             [News MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", existsArrayIds]
                                                                       inContext:localContext];
                                         }
                                              completion:^(BOOL contextDidSave, NSError *_) {
                                                  NSArray<News *> *news = [strongSelf obtainNewsFromCache];
                                                  completion(news);
                                              }];
                             }];
}

#pragma mark - Private

- (NSArray<News *> *)obtainNewsFromCache {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [News MR_requestAllSortedBy:@"published_at"
                                                     ascending:NO
                                                 withPredicate:[NSPredicate predicateWithValue:YES]
                                                     inContext:moc];
    return [News MR_executeFetchRequest:fetchRequest inContext:moc];
}

@end

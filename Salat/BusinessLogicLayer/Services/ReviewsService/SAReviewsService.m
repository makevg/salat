//
//  SAReviewsService.m
//  Salat
//
//  Created by Maximychev Evgeny on 28.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewParser.h"
#import "SAReviewsService.h"
#import "Review.h"
#import <MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalDataImport.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRequests.h>
#import <Crashlytics/Crashlytics/CLSLogging.h>

static NSString *const cReviewsUrl = @"feedbacks";
static NSString *const cReviewStoreUrl = @"feedbacks/store";

@implementation SAReviewsService

#pragma mark - Public

- (void)obtainReviews:(void (^)(NSArray<Review *> *data))completion
                error:(void (^)(NSError *))errorBlock {
    NSArray<Review *> *reviews = [self obtainReviewsFromCache];
    if (reviews) {
        completion(reviews);
    }
    
    [self obtainReviewsFromNetwork:completion
                             error:errorBlock];
}

- (NSArray<Review *> *)obtainMyReviews {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [Review MR_requestAllSortedBy:@"published_at"
                                                       ascending:NO
                                                   withPredicate:[NSPredicate predicateWithFormat:@"is_my == TRUE"]
                                                       inContext:moc];
    return [Review MR_executeFetchRequest:fetchRequest inContext:moc];
}

- (void)storeReviewWithContent:(NSString *)content
                        rating:(NSNumber *)rating
                    completion:(void (^)(void))completion
                         error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestPostToUrlString:cReviewStoreUrl
                               params:@{@"content" : content,
                                       @"rating" : rating
                               }
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  __strong __typeof(weakSelf) strongSelf = weakSelf;

                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  NSDictionary *responseDict = responseObject;
                                  CLSLog(@"%@", responseDict);

                                  completion();
                              }];
}

- (void)obtainReviewsFromNetwork:(void (^)(NSArray<Review *> *data))completion
                           error:(void (^)(NSError *))errorBlock {
    __weak __typeof(self) weakSelf = self;

    [SALAT_API requestGetToUrlString:cReviewsUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 __strong __typeof(weakSelf) strongSelf = weakSelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                             NSDictionary *responseDict = responseObject;
                                             NSArray *reviewsList = responseDict[@"result"];
                                             NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                             for (NSDictionary *dictReviews in reviewsList) {
                                                 Review *review = [Review MR_findFirstOrCreateByAttribute:@"id"
                                                                                                withValue:dictReviews[@"id"]
                                                                                                inContext:localContext];
                                                 [existsArrayIds addObject:dictReviews[@"id"]];
                                                 [self.reviewParser fillObject:review fromResponse:dictReviews];
                                             }

                                             [Review MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", existsArrayIds]
                                                                         inContext:localContext];
                                         }
                                              completion:^(BOOL contextDidSave, NSError *_) {
                                                  NSArray<Review *> *reviews = [strongSelf obtainReviewsFromCache];
                                                  completion(reviews);
                                              }];
                             }];
}

#pragma mark - Private

- (NSArray<Review *> *)obtainReviewsFromCache {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [Review MR_requestAllSortedBy:@"published_at"
                                                       ascending:NO
                                                   withPredicate:[NSPredicate predicateWithValue:YES]
                                                       inContext:moc];
    return [Review MR_executeFetchRequest:fetchRequest inContext:moc];
}

@end

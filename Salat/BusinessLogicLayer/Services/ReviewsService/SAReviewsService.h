//
//  SAReviewsService.h
//  Salat
//
//  Created by Maximychev Evgeny on 28.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SABaseService.h"

@class SAReviewParser;
@class Review;

@interface SAReviewsService : SABaseService

@property(nonatomic) SAReviewParser *reviewParser;

- (void)obtainReviews:(void (^)(NSArray<Review *> *data))completion
        error:(void (^)(NSError *))errorBlock;

- (NSArray *)obtainMyReviews;

- (void)storeReviewWithContent:(NSString *)content
                        rating:(NSNumber *)rating
                    completion:(void (^)(void))completion
        error:(void (^)(NSError *))errorBlock;

- (void)obtainReviewsFromNetwork:(void (^)(NSArray<Review *> *data))completion
                           error:(void (^)(NSError *))errorBlock;

@end

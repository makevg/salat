//
//  SAServiceLayer.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAServiceLayer.h"
#import "SAAPI.h"

@implementation SAServiceLayer

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(unauthorizedHandler:)
                                                     name:kUnauthorizedNotification
                                                   object:nil];
    }

    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
};

- (void)unauthorizedHandler:(NSNotification *)notification {
//    [SACommonService setLoggedIn:NO];
}

@end

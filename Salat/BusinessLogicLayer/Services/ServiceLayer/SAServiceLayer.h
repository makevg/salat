//
//  SAServiceLayer.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SACommon.h"

@class SABaseService;

@protocol ServiceResultDelegate <NSObject>

- (void)successResult:(id)object forService:(Class)service;

- (void)networkError:(NSError *)error forService:(Class)service;

- (void)serverError:(NSError *)error forService:(Class)service;

@end

#define SALAT_SERVICE [SAServiceLayer sharedInstance]

typedef void(^ServiceHandler)(NSFetchedResultsController *);

typedef void(^ServiceHandlerArray)(NSArray<NSFetchedResultsController *> *);

@interface SAServiceLayer : NSObject

+ (instancetype)sharedInstance;

@end

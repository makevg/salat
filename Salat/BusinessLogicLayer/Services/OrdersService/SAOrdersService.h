//
//  SAOrdersService.h
//  Salat
//
//  Created by Максимычев Е.О. on 21.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseService.h"

@protocol SAPrototypeParser;
@class Order;
@class SACheckoutPlainObject;
@class SACartCalculationPO;

typedef void(^SAOnlinePaySuccessHandler)(BOOL);
typedef void(^SAOnlinePayFailureHandler)(NSError *error);

@interface SAOrdersService : SABaseService

@property(nonatomic) id <SAPrototypeParser> orderParser;
@property(nonatomic) id <SAPrototypeParser> calcParser;
@property(nonatomic) id <SAPrototypeParser> orderProductParser;
@property(nonatomic) id <SAPrototypeParser> orderProductModifierParser;

- (void)obtainOrders:(void (^)(NSArray<Order *> *data))completion
               error:(void (^)(NSError *))errorBlock;

- (void)obtainOrdersOnlyNetwork:(void (^)(NSArray<Order *> *))completion
                          error:(void (^)(NSError *))errorBlock;

- (void)sendOrderWithData:(SACheckoutPlainObject *)data
               completion:(void (^)(NSInteger))completion
                    error:(void (^)(NSError *))errorBlock;

- (void)calculateCartData:(SACheckoutPlainObject *)data
               completion:(void (^)(SACartCalculationPO *))completion
                    error:(void (^)(NSError *))errorBlock;

- (Order *)obtainOrderById:(NSNumber *)orderId;

- (Order *)obtainLastOrder;

- (void)checkPayOrderId:(NSNumber *)orderId
     withSuccessHandler:(SAOnlinePaySuccessHandler)successHandler
         failureHandler:(SAOnlinePayFailureHandler)failureHandler;

@end

//
//  SAOrdersService.m
//  Salat
//
//  Created by Максимычев Е.О. on 21.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalFinders.h>
#import <MagicalRecord/MagicalRecord/NSManagedObject+MagicalRequests.h>
#import <MagicalRecord/MagicalRecord.h>
#import <Crashlytics/Crashlytics/CLSLogging.h>
#import "SAOrdersService.h"
#import "SAPrototypeParser.h"
#import "Order.h"
#import "OrderProduct.h"
#import "OrderProductModifier.h"
#import "SACheckoutPlainObject.h"
#import "SAAddressPlainObject.h"
#import "SACommon.h"
#import "SACartCalculationPO.h"
#import "CartItemModifier.h"
#import "CartItem.h"
#import "SACartItemPlainObject.h"
#import "SACartItemModifierPlainObject.h"
#import "OrderCalculateParser.h"

static NSString *const cOrdersUrl = @"orders";
static NSString *const cSendOrderUrl = @"orders/store";
static NSString *const cCalculateOrderUrl = @"orders/calculate";
static NSString *const cOrderOnlineCheckUrl = @"orders/%@/payments/check";

@implementation SAOrdersService

- (instancetype)init {
    self = [super init];

    self.calcParser = [OrderCalculateParser new];

    return self;
}

- (void)obtainOrdersOnlyNetwork:(void (^)(NSArray<Order *> *))completion
                          error:(void (^)(NSError *))errorBlock {
    weakifySelf;
    [self obtainOrdersFromNetwork:^{
        strongifySelf;
        if (strongSelf) {
            NSArray<Order *> *orders_ = [strongSelf obtainOrdersFromCache];
            
            if (orders_) {
                completion(orders_);
            }
        }
    }
                            error:errorBlock];
}

- (void)obtainOrders:(void (^)(NSArray<Order *> *data))completion
               error:(void (^)(NSError *))errorBlock {
    NSArray<Order *> *orders = [self obtainOrdersFromCache];
    if (orders) {
        completion(orders);
    }

    [self obtainOrdersOnlyNetwork:completion error:errorBlock];
}

- (void)sendOrderWithData:(SACheckoutPlainObject *)data
               completion:(void (^)(NSInteger))completion
                    error:(void (^)(NSError *))errorBlock {
    weakifySelf;

    NSMutableDictionary *params = [NSMutableDictionary new];
    NSMutableArray *cart = [NSMutableArray new];

    for (SACartItemPlainObject *orderProduct in data.products) {
        NSMutableArray *modifiers = [NSMutableArray new];

        for (SACartItemModifierPlainObject *modifierPlainObject in orderProduct.modifiers) {
            [modifiers addObject:@{
                    @"id" : modifierPlainObject.itemModifierId,
                    @"quantity" : modifierPlainObject.quantity
            }];
        }

        NSMutableDictionary *dic = [@{} mutableCopy];

        dic[@"id"] = orderProduct.productId;
        dic[@"quantity"] = orderProduct.quantity;
        dic[@"modifiers"] = modifiers;

        [cart addObject:dic];
    }

    params[@"street"] = data.address.street;
    params[@"house"] = data.address.house;

    if (data.address.addressId)
        params[@"address_id"] = data.address.addressId;

    if (data.address.flat)
        params[@"flat"] = data.address.flat;

    if (data.address.floor)
        params[@"floor"] = data.address.floor;

    if (data.address.entrance)
        params[@"entrance"] = data.address.entrance;

    if (data.address.intercomCode)
        params[@"intercom_code"] = data.address.intercomCode;

    if (data.address.location) {
        CLLocationCoordinate2D coordinate2D = data.address.location.coordinate;
        NSString *locationString = [NSString stringWithFormat:@"%f;%f", coordinate2D.longitude, coordinate2D.latitude];
        params[@"geo_point"] = locationString;
    }

    params[@"token"] = data.token;
    params[@"cart"] = cart;
    params[@"first_name"] = data.name;
    params[@"token"] = data.token;
    params[@"is_online_payment"] = @(data.isOnlinePayment);
    params[@"deliver_to"] = data.deliverToTime;

    CLSLog(@"%@", params);

    [SALAT_API requestPostToUrlString:cSendOrderUrl
                               params:params
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  strongifySelf;

                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  NSInteger orderId = [responseObject[@"result"][@"order_id"] integerValue];

                                  completion(orderId);
                              }];
}

- (void)calculateCartData:(SACheckoutPlainObject *)data completion:(void (^)(SACartCalculationPO *))completion error:(void (^)(NSError *))errorBlock {
    weakifySelf;

    NSMutableDictionary *address = [NSMutableDictionary new];
    NSMutableArray *cart = [NSMutableArray new];
    BOOL hasItems = NO;

    for (SACartItemPlainObject *cartItem in data.products) {
        NSMutableArray *modifiers = [NSMutableArray new];

        for (SACartItemModifierPlainObject *modifier in cartItem.modifiers) {
            [modifiers addObject:@{
                    @"id" : modifier.itemModifierId,
                    @"quantity" : modifier.quantity
            }];
        }

        NSMutableDictionary *dic = [@{} mutableCopy];

        dic[@"id"] = cartItem.productId;
        dic[@"quantity"] = cartItem.quantity;
        dic[@"modifiers"] = modifiers;

        [cart addObject:dic];
        hasItems = YES;
    }

    address[@"street"] = data.address.street;
    address[@"house"] = data.address.house;

    if (data.address.addressId)
        address[@"id"] = data.address.addressId;

    if (data.address.location) {
        CLLocationCoordinate2D coordinate2D = data.address.location.coordinate;
        address[@"geo_point"][@"latitude"] = @(coordinate2D.longitude);
        address[@"geo_point"][@"longitude"] = @(coordinate2D.latitude);
    }

    NSMutableDictionary *params = [NSMutableDictionary new];

    params[@"address"] = address;
    params[@"cart"] = cart;

    CLSLog(@"%@", params);

    [SALAT_API requestPostToUrlString:cCalculateOrderUrl
                               params:params
                            useDomain:YES
                              handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                  strongifySelf;

                                  if (strongSelf == nil)
                                      return;

                                  if ([strongSelf handleResponse:response
                                              withResponseObject:responseObject
                                                           error:error
                                                           block:errorBlock]) {
                                      return;
                                  }

                                  SACartCalculationPO *cartCalculationPO = [SACartCalculationPO new];

                                  [strongSelf.calcParser fillObject:cartCalculationPO
                                                       fromResponse:responseObject[@"result"]];

                                  if (hasItems) {
                                      [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                                  NSMutableArray *ids = [NSMutableArray new];
                                                  for (SACartCalculationItemPO *itemPO in cartCalculationPO.items) {
                                                      NSMutableArray *modifierIds = [NSMutableArray new];
                                                      CGFloat totalPrice = 0.f;

                                                      for (SACartCalculationItemModifierPO *modifierPO in itemPO.modifiers) {
                                                          [modifierIds addObject:modifierPO.id];
                                                      }

                                                      NSArray *modIds = [modifierIds sortedArrayUsingSelector:@selector(compare:)];
                                                      NSMutableString *stringId = [NSMutableString new];
                                                      for( NSNumber *modId in modIds ) {
                                                          [stringId appendFormat:@"_%@", modId];
                                                      }

                                                      [stringId appendFormat:@"_%@", itemPO.id];

                                                      for (SACartCalculationItemModifierPO *modifierPO in itemPO.modifiers) {
                                                          [modifierIds addObject:modifierPO.id];

                                                          NSPredicate *pred = [NSPredicate predicateWithFormat:@"id == %@ AND item.id == %@", modifierPO.id, stringId];
                                                          CartItemModifier *modifier = [CartItemModifier MR_findFirstWithPredicate:pred
                                                                                                                         inContext:localContext];

                                                          modifier.price = modifierPO.price;
                                                          totalPrice += [modifierPO.price doubleValue];
                                                      }

                                                      NSPredicate *pred = [NSPredicate predicateWithFormat:@"NOT (id IN %@) AND item.id == %@", modifierIds, stringId];
                                                      [CartItemModifier MR_deleteAllMatchingPredicate:pred
                                                                                            inContext:localContext];

                                                      [ids addObject:itemPO.id];
                                                      CartItem *item = [CartItem MR_findFirstByAttribute:@"id"
                                                                                               withValue:stringId
                                                                                               inContext:localContext];

                                                      item.cost = @([itemPO.price doubleValue] + totalPrice);
                                                  }

                                                  NSPredicate *pred = [NSPredicate predicateWithFormat:@"NOT (product_id IN %@)", ids];
                                                  [CartItem MR_deleteAllMatchingPredicate:pred
                                                                                inContext:localContext];
                                              }
                                                   completion:^(BOOL contextDidSave, NSError *_) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           completion(cartCalculationPO);
                                                       });
                                                   }];
                                  } else {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          completion(cartCalculationPO);
                                      });
                                  }
                              }];
}


- (Order *)obtainOrderById:(NSNumber *)orderId {
    return [Order MR_findFirstByAttribute:@"id" withValue:orderId inContext:[SALAT_DB produceContextForRead]];
}

#pragma mark - Private

- (NSArray<Order *> *)obtainOrdersFromCache {
    NSManagedObjectContext *moc = [SALAT_DB produceContextForRead];
    NSFetchRequest *fetchRequest = [Order MR_requestAllSortedBy:@"created_at"
                                                      ascending:NO
                                                  withPredicate:[NSPredicate predicateWithValue:YES]
                                                      inContext:moc];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    return [Order MR_executeFetchRequest:fetchRequest inContext:moc];
}

- (void)obtainOrdersFromNetwork:(void (^)())completion
                          error:(void (^)(NSError *))errorBlock {
    weakifySelf;

    [SALAT_API requestGetToUrlString:cOrdersUrl
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 strongifySelf;

                                 if (strongSelf == nil)
                                     return;

                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }

                                 [SALAT_DB saveWithBlock:^(NSManagedObjectContext *localContext) {
                                     NSDictionary *responseDict = responseObject;

//                                     if ([responseDict[@"result"] isKindOfClass:[NSString class]]) {
                                     [Order MR_truncateAllInContext:localContext];
//                                     }

                                     NSArray *ordersList = responseDict[@"result"];
//                                     NSMutableArray *existsArrayIds = [@[] mutableCopy];

                                     for (NSDictionary *dictOrder in ordersList) {
                                         [self parseOrder:dictOrder context:localContext];
                                     }

//                                     [Order MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (id IN %@)", existsArrayIds]
//                                                                inContext:localContext];
                                 }            completion:^(BOOL contextDidSave, NSError *_) {
                                     completion();
                                 }];
                             }];
}

- (Order *)obtainLastOrder {
    NSFetchRequest *request = [Order MR_requestAllSortedBy:@"created_at" ascending:NO inContext:[SALAT_DB produceContextForRead]];
    [request setFetchLimit:1];
    [request setReturnsObjectsAsFaults:NO];

    return [Order MR_executeFetchRequestAndReturnFirstObject:request inContext:[SALAT_DB produceContextForRead]];
}

- (void)checkPayOrderId:(NSNumber *)orderId
     withSuccessHandler:(SAOnlinePaySuccessHandler)completion
         failureHandler:(SAOnlinePayFailureHandler)errorBlock {
    weakifySelf;
    
    [SALAT_API requestGetToUrlString:[NSString stringWithFormat:cOrderOnlineCheckUrl, orderId]
                              params:nil
                           useDomain:YES
                             handler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                 strongifySelf;
                                 
                                 if (strongSelf == nil)
                                     return;
                                 
                                 if ([strongSelf handleResponse:response
                                             withResponseObject:responseObject
                                                          error:error
                                                          block:errorBlock]) {
                                     return;
                                 }
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     completion([responseObject[@"result"][@"is_paid"] boolValue]);
                                 });
                             }
     
     ];
}

#pragma mark - Private

- (void)parseOrder:(NSDictionary *)dictOrder context:(NSManagedObjectContext *)localContext {
    Order *order = [Order MR_findFirstOrCreateByAttribute:@"id"
                                                withValue:dictOrder[@"id"]
                                                inContext:localContext];
    [order removeProducts:order.products];
    [self.orderParser fillObject:order fromResponse:dictOrder];

    NSArray *productsArray = dictOrder[@"order_data"][@"cart"];
    NSMutableSet<OrderProduct *> *products = [NSMutableSet<OrderProduct *> new];

    for (NSDictionary *productDict in productsArray) {
        OrderProduct *orderProduct = [OrderProduct MR_createEntityInContext:localContext];
        [self.orderProductParser fillObject:orderProduct fromResponse:productDict];

        NSArray *modifiersArray = productDict[@"modifiers"];
        NSMutableSet<OrderProductModifier *> *modifiers = [NSMutableSet<OrderProductModifier *> new];

        for (NSDictionary *modifierDict in modifiersArray) {
            OrderProductModifier *orderProductModifier = [OrderProductModifier MR_createEntityInContext:localContext];
            [self.orderProductModifierParser fillObject:orderProductModifier fromResponse:modifierDict];
            [modifiers addObject:orderProductModifier];
        }

        [orderProduct addModifiers:modifiers];
        orderProduct.order = order;
        [products addObject:orderProduct];
    }

    [order addProducts:products];
}

@end

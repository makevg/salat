//
//  SAServiceComponentsAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAServiceComponentsAssembly.h"
#import "SAProductService.h"
#import "SAReviewsService.h"
#import "SASalesService.h"
#import "SACoreComponents.h"
#import "SANewsService.h"
#import "SACartService.h"
#import "SARestaurantService.h"
#import "SAAddressService.h"
#import "SAUserService.h"
#import "SAOrdersService.h"

@implementation SAServiceComponentsAssembly

#pragma mark - SAServiceComponents

- (SAProductService *)productService {
    return [TyphoonDefinition withClass:[SAProductService class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(productCategoryParser)
                                                    with:[self.coreComponents productCategoryParser]];
                              [definition injectProperty:@selector(productParser)
                                                    with:[self.coreComponents productParser]];
                              [definition injectProperty:@selector(productModifierParser)
                                                    with:[self.coreComponents productModifierParser]];
                          }];
}

- (SAReviewsService *)reviewService {
    return [TyphoonDefinition withClass:[SAReviewsService class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(reviewParser)
                                                    with:[self.coreComponents reviewParser]];
                          }];
}

- (SASalesService *)saleService {
    return [TyphoonDefinition withClass:[SASalesService class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(saleParser)
                                                    with:[self.coreComponents saleParser]];
                          }];
}

- (SANewsService *)newsService {
    return [TyphoonDefinition withClass:[SANewsService class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(newsParser)
                                                    with:[self.coreComponents newsParser]];
                          }];
}

- (SACartService *)cartService {
    return [TyphoonDefinition withClass:[SACartService class]
                          configuration:^(TyphoonDefinition *definition) {
                          }];
}

- (SARestaurantService *)restaurantService {
    return [TyphoonDefinition withClass:[SARestaurantService class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(restaurantParser)
                                                    with:[self.coreComponents restaurantParser]];
                              [definition injectProperty:@selector(deliveryAreaParser)
                                                    with:[self.coreComponents deliveryAreaParser]];
                              [definition injectProperty:@selector(deliveryTimeParser)
                                                    with:[self.coreComponents deliveryTimeParser]];
                              [definition injectProperty:@selector(paymentMethodParser)
                                                    with:[self.coreComponents paymentMethodParser]];
                          }];
}

- (SAAddressService *)addressService {
    return [TyphoonDefinition withClass:[SAAddressService class]
                          configuration:^(TyphoonDefinition *definition) {
                          }];
}

- (SAUserService *)userService {
    return [TyphoonDefinition withClass:[SAUserService class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(profileParser)
                                                    with:[self.coreComponents profileParser]];
                              [definition injectProperty:@selector(addressParser)
                                                    with:[self.coreComponents addressParser]];
                          }];
}

- (SAOrdersService *)ordersService {
    return [TyphoonDefinition withClass:[SAOrdersService class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(orderParser)
                                                    with:[self.coreComponents orderParser]];
                              [definition injectProperty:@selector(orderProductParser)
                                                    with:[self.coreComponents orderProductParser]];
                              [definition injectProperty:@selector(orderProductModifierParser)
                                                    with:[self.coreComponents orderProductModifierParser]];
                          }];
}

@end

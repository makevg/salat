//
//  SAServiceComponentsAssembly.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import "SAServiceComponents.h"

@protocol SACoreComponents;

@interface SAServiceComponentsAssembly : TyphoonAssembly <SAServiceComponents>

@property (nonatomic, readonly) TyphoonAssembly <SACoreComponents> *coreComponents;

@end

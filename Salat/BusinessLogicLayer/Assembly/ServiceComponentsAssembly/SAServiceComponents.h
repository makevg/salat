//
//  SAServiceComponents.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TyphoonAssembly.h"

@class SAProductService;
@class SAReviewsService;
@class SASalesService;
@class SANewsService;
@class SACartService;
@class SARestaurantService;
@class SAAddressService;
@class SAUserService;
@class SAOrdersService;

@protocol SAServiceComponents <NSObject>

- (SAProductService *)productService;
- (SAReviewsService *)reviewService;
- (SASalesService *)saleService;
- (SANewsService *)newsService;
- (SACartService *)cartService;
- (SARestaurantService *)restaurantService;
- (SAAddressService *)addressService;
- (SAUserService *)userService;
- (SAOrdersService *)ordersService;

@end

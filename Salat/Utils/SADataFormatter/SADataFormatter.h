//
//  SADataFormatter.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface SADataFormatter : NSObject

+ (NSDate *)dateByString:(NSString *)dateString;

+ (NSString *)stringByDate:(NSDate *)date;

+ (NSString *)stringByDate:(NSDate *)date withFormat:(NSString *)format;

+ (NSString *)stringByUnixTimeStamp:(NSNumber *)unixTimeStamp;

+ (NSString *)fullDateTimeStringByUnixTimeStamp:(NSNumber *)unixTimeStamp;

+ (NSNumber *)numberByString:(NSString *)string;

+ (NSNumber *)numberByObject:(NSObject *)object;

+ (NSString *)stringByObject:(NSObject *)object;

+ (NSString *)stringByHTML:(NSString *)htmlString;

+ (NSString *)stringByProductsCount:(NSInteger)count;

+ (NSString *)stringByHoursCount:(NSInteger)count;

+ (NSString *)stringByMinutesCount:(NSInteger)count;

@end

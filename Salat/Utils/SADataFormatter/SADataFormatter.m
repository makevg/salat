//
//  SADataFormatter.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SADataFormatter.h"

NSString *const cDateFormatDefault = @"dd.MM.yyyy";
NSString *const cDateFormatWithTime = @"dd.MM.yyyy HH:mm:ss";
NSString *const cTimeFormat = @"HH:mm";

@implementation SADataFormatter

+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"]];
    return dateFormatter;
}

+ (NSDate *)dateByString:(NSString *)dateString {
    NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:cDateFormatWithTime];
    return [dateFormatter dateFromString:dateString];
}

+ (NSString *)stringByDate:(NSDate *)date {
    return [self stringByDate:date withFormat:cDateFormatDefault];
}

+ (NSString *)stringByDate:(NSDate *)date withFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:format];
    return [dateFormatter stringFromDate:date];
}


+ (NSString *)stringByUnixTimeStamp:(NSNumber *)unixTimeStamp {
    long long int timeStamp = [unixTimeStamp longLongValue];
    NSTimeInterval interval = timeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [self stringByDate:date];
}

+ (NSString *)fullDateTimeStringByUnixTimeStamp:(NSNumber *)unixTimeStamp {
    long long int timeStamp = [unixTimeStamp longLongValue];
    NSTimeInterval interval = timeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDate *today = [NSDate date];
    NSDate *yesterday = [today dateByAddingTimeInterval:-86400.0];
    if ([self isSameDayWithFirstDate:date secondDate:today]) {
        NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:cTimeFormat];
        return [NSString stringWithFormat:@"Сегодня, %@", [dateFormatter stringFromDate:date]];
    } else if ([self isSameDayWithFirstDate:date secondDate:yesterday]) {
        NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:cTimeFormat];
        return [NSString stringWithFormat:@"Вчера, %@", [dateFormatter stringFromDate:date]];
    } else {
        NSDateFormatter *dateFormatter = [self dateFormatterWithFormat:@"dd MMM HH:mm"];
        return [dateFormatter stringFromDate:date];
    }
}

+ (BOOL)isSameDayWithFirstDate:(NSDate *)firstDate secondDate:(NSDate *)secondDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unitFlags = NSCalendarUnitDay;
    NSDateComponents *comp1 = [calendar components:unitFlags fromDate:firstDate];
    NSDateComponents *comp2 = [calendar components:unitFlags fromDate:secondDate];

    return [comp1 day] == [comp2 day];
}

+ (NSNumber *)numberByString:(NSString *)string {
    if ([string isKindOfClass:[NSNull class]]) string = @"0";
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    return [numberFormatter numberFromString:string];
}

+ (NSNumber *)numberByObject:(NSObject *)object {
    if (!object || [object isKindOfClass:[NSNull class]])
        return nil;

    if ([object isKindOfClass:[NSNumber class]])
        return (NSNumber *) object;

    if ([object isKindOfClass:[NSString class]]) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.locale = [NSLocale currentLocale];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        f.decimalSeparator = @".";
        NSNumber *number = [f numberFromString:(NSString *) object];
        return number;
    }

    return nil;
}

+ (NSString *)stringByObject:(NSObject *)object {
    if (!object || [object isKindOfClass:[NSNull class]])
        return @"";

    if ([object isKindOfClass:[NSString class]])
        return (NSString *) object;

    if ([object isKindOfClass:[NSNumber class]])
        return [((NSNumber *) object) stringValue];

    return @"";
}

+ (NSString *)stringByHTML:(NSString *)htmlString {
    htmlString = [self stringByObject:htmlString];
    NSRange r;
    while ((r = [htmlString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        htmlString = [htmlString stringByReplacingCharactersInRange:r withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&quot;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@":&#41;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&#41;" withString:@""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&#40;" withString:@""];
    return htmlString;
}

+ (NSString *)stringByProductsCount:(NSInteger)count {
    NSString *str;
    NSInteger n = count % 100;
    NSInteger ost = n % 10;
    if (n >= 10 && n < 20) {
        str = @"блюд";
    } else if (ost > 1 && ost < 5) {
        str = @"блюда";
    } else if (ost == 1) {
        str = @"блюдо";
    } else {
        str = @"блюд";
    }

    return [NSString stringWithFormat:@"%li %@", (long) count, str];
}

+ (NSString *)stringByHoursCount:(NSInteger)count {
    NSString *str;
    NSInteger n = count % 100;
    NSInteger ost = n % 10;
    if (n >= 10 && n < 20) {
        str = @"часов";
    } else if (ost > 1 && ost < 5) {
        str = @"часа";
    } else if (ost == 1) {
        str = @"час";
    } else {
        str = @"часов";
    }

    return [NSString stringWithFormat:@"%li %@", (long) count, str];
}

+ (NSString *)stringByMinutesCount:(NSInteger)count {
    NSString *str;
    NSInteger n = count % 100;
    NSInteger ost = n % 10;
    if (n >= 10 && n < 20) {
        str = @"минут";
    } else if (ost > 1 && ost < 5) {
        str = @"минуты";
    } else if (ost == 1) {
        str = @"минута";
    } else {
        str = @"минут";
    }

    return [NSString stringWithFormat:@"%li %@", (long) count, str];
}


@end

//
//  Restaurant+CoreDataProperties.m
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Restaurant+CoreDataProperties.h"

@implementation Restaurant (CoreDataProperties)

@dynamic id;
@dynamic title;
@dynamic descr;
@dynamic url;
@dynamic city;
@dynamic phone;
@dynamic logo;
@dynamic delivery_areas;
@dynamic delivery_times;
@dynamic payment_methods;

@end

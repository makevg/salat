//
//  CartItem+CoreDataProperties.h
//  
//
//  Created by Maximychev Evgeny on 08.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CartItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface CartItem (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSDate *added;
@property (nullable, nonatomic, retain) NSNumber *cost;
@property (nullable, nonatomic, retain) NSNumber *item_id;
@property (nullable, nonatomic, retain) NSString *product_descr;
@property (nullable, nonatomic, retain) NSNumber *product_id;
@property (nullable, nonatomic, retain) NSString *product_image_url;
@property (nullable, nonatomic, retain) NSString *product_title;
@property (nullable, nonatomic, retain) NSNumber *quantity;
@property (nullable, nonatomic, retain) NSSet<CartItemModifier *> *modifiers;

@end

@interface CartItem (CoreDataGeneratedAccessors)

- (void)addModifiersObject:(CartItemModifier *)value;
- (void)removeModifiersObject:(CartItemModifier *)value;
- (void)addModifiers:(NSSet<CartItemModifier *> *)values;
- (void)removeModifiers:(NSSet<CartItemModifier *> *)values;

@end

NS_ASSUME_NONNULL_END

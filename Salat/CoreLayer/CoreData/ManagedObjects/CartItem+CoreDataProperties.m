//
//  CartItem+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 08.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CartItem+CoreDataProperties.h"

@implementation CartItem (CoreDataProperties)

@dynamic id;
@dynamic added;
@dynamic cost;
@dynamic item_id;
@dynamic product_descr;
@dynamic product_id;
@dynamic product_image_url;
@dynamic product_title;
@dynamic quantity;
@dynamic modifiers;

@end

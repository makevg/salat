//
//  OrderProduct+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OrderProduct+CoreDataProperties.h"

@implementation OrderProduct (CoreDataProperties)

@dynamic id;
@dynamic quantity;
@dynamic price;
@dynamic sum;
@dynamic title;
@dynamic descr;
@dynamic image_url;
@dynamic modifiers;
@dynamic order;

@end

//
//  Cart+CoreDataProperties.m
//  
//
//  Created by mtx on 16.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Cart+CoreDataProperties.h"

@implementation Cart (CoreDataProperties)

@dynamic hit;
@dynamic recommendation;
@dynamic suggestion;

@end

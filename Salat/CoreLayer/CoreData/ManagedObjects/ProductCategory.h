//
//  ProductCategory.h
//  
//
//  Created by Максимычев Е.О. on 17.05.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

NS_ASSUME_NONNULL_BEGIN

@interface ProductCategory : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "ProductCategory+CoreDataProperties.h"

//
//  News+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 07.02.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "News+CoreDataProperties.h"

@implementation News (CoreDataProperties)

@dynamic id;
@dynamic title;
@dynamic published_at;
@dynamic url;
@dynamic image_url;
@dynamic content;

@end

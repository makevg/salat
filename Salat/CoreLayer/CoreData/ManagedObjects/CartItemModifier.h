//
//  CartItemModifier.h
//  
//
//  Created by Maximychev Evgeny on 08.07.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CartItem;

NS_ASSUME_NONNULL_BEGIN

@interface CartItemModifier : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CartItemModifier+CoreDataProperties.h"

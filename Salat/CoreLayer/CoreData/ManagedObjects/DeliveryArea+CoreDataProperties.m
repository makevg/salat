//
//  DeliveryArea+CoreDataProperties.m
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DeliveryArea+CoreDataProperties.h"

@implementation DeliveryArea (CoreDataProperties)

@dynamic id;
@dynamic title;
@dynamic price;
@dynamic polygon;
@dynamic restaurant;

@end

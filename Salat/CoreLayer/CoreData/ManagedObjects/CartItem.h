//
//  CartItem.h
//  
//
//  Created by Максимычев Е.О. on 02.06.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class CartItemModifier;

@interface CartItem : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CartItem+CoreDataProperties.h"

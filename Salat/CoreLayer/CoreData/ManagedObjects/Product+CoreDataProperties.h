//
//  Product+CoreDataProperties.h
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product.h"

NS_ASSUME_NONNULL_BEGIN

@interface Product (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *category_id;
@property (nullable, nonatomic, retain) NSString *descr;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSString *preview_small_url;
@property (nullable, nonatomic, retain) NSString *preview_url;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSNumber *position;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) ProductCategory *parent;
@property (nullable, nonatomic, retain) NSSet<ProductModifier *> *modifiers;

@end

@interface Product (CoreDataGeneratedAccessors)

- (void)addModifiersObject:(ProductModifier *)value;
- (void)removeModifiersObject:(ProductModifier *)value;
- (void)addModifiers:(NSSet<ProductModifier *> *)values;
- (void)removeModifiers:(NSSet<ProductModifier *> *)values;

@end

NS_ASSUME_NONNULL_END

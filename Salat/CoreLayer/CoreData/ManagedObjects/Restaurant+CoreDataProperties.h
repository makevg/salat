//
//  Restaurant+CoreDataProperties.h
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Restaurant.h"

NS_ASSUME_NONNULL_BEGIN

@interface Restaurant (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *descr;
@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSString *logo;
@property (nullable, nonatomic, retain) NSSet<DeliveryArea *> *delivery_areas;
@property (nullable, nonatomic, retain) NSSet<DeliveryTime *> *delivery_times;
@property (nullable, nonatomic, retain) NSSet<PaymentMethod *> *payment_methods;

@end

@interface Restaurant (CoreDataGeneratedAccessors)

- (void)addDelivery_areasObject:(DeliveryArea *)value;
- (void)removeDelivery_areasObject:(DeliveryArea *)value;
- (void)addDelivery_areas:(NSSet<DeliveryArea *> *)values;
- (void)removeDelivery_areas:(NSSet<DeliveryArea *> *)values;

- (void)addDelivery_timesObject:(DeliveryTime *)value;
- (void)removeDelivery_timesObject:(DeliveryTime *)value;
- (void)addDelivery_times:(NSSet<DeliveryTime *> *)values;
- (void)removeDelivery_times:(NSSet<DeliveryTime *> *)values;

- (void)addPayment_methodsObject:(PaymentMethod *)value;
- (void)removePayment_methodsObject:(PaymentMethod *)value;
- (void)addPayment_methods:(NSSet<PaymentMethod *> *)values;
- (void)removePayment_methods:(NSSet<PaymentMethod *> *)values;

@end

NS_ASSUME_NONNULL_END

//
//  OrderProductModifier.h
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class OrderProduct;

@interface OrderProductModifier : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "OrderProductModifier+CoreDataProperties.h"

//
//  CartItemModifier+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 08.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CartItemModifier+CoreDataProperties.h"

@implementation CartItemModifier (CoreDataProperties)

@dynamic id;
@dynamic quantity;
@dynamic price;
@dynamic title;
@dynamic item;

@end

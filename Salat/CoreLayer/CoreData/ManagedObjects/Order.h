//
//  Order.h
//  
//
//  Created by Максимычев Е.О. on 22.06.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class OrderProduct;

@interface Order : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Order+CoreDataProperties.h"

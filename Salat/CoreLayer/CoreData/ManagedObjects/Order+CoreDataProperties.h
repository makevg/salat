//
//  Order+CoreDataProperties.h
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Order.h"

NS_ASSUME_NONNULL_BEGIN

@interface Order (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *address_city_id;
@property (nullable, nonatomic, retain) NSString *address_entrance;
@property (nullable, nonatomic, retain) NSString *address_flat;
@property (nullable, nonatomic, retain) NSNumber *address_floor;
@property (nullable, nonatomic, retain) NSString *address_house;
@property (nullable, nonatomic, retain) NSString *address_intercom_code;
@property (nullable, nonatomic, retain) NSString *address_street;
@property (nullable, nonatomic, retain) NSNumber *cart_sum;
@property (nullable, nonatomic, retain) NSNumber *created_at;
@property (nullable, nonatomic, retain) NSNumber *delivery_sum;
@property (nullable, nonatomic, retain) NSNumber *delivery_time;
@property (nullable, nonatomic, retain) NSString *guid;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSNumber *status;
@property (nullable, nonatomic, retain) NSNumber *total_sum;
@property (nullable, nonatomic, retain) NSNumber *updated_at;
@property (nullable, nonatomic, retain) NSString *discount_title;
@property (nullable, nonatomic, retain) NSNumber *discount_percent;
@property (nullable, nonatomic, retain) NSNumber *discount_sum;
@property (nullable, nonatomic, retain) NSSet<OrderProduct *> *products;

@end

@interface Order (CoreDataGeneratedAccessors)

- (void)addProductsObject:(OrderProduct *)value;
- (void)removeProductsObject:(OrderProduct *)value;
- (void)addProducts:(NSSet<OrderProduct *> *)values;
- (void)removeProducts:(NSSet<OrderProduct *> *)values;

@end

NS_ASSUME_NONNULL_END

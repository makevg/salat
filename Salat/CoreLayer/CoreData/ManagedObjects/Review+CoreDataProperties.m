//
//  Review+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 28.02.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Review+CoreDataProperties.h"

@implementation Review (CoreDataProperties)

@dynamic id;
@dynamic user_name;
@dynamic published_at;
@dynamic rate;
@dynamic descr;
@dynamic is_my;

@end

//
//  Profile.h
//  
//
//  Created by Максимычев Е.О. on 22.06.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Address;

NS_ASSUME_NONNULL_BEGIN

@interface Profile : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Profile+CoreDataProperties.h"

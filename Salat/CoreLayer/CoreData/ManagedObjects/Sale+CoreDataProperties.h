//
//  Sale+CoreDataProperties.h
//  
//
//  Created by Maximychev Evgeny on 09.02.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Sale.h"

NS_ASSUME_NONNULL_BEGIN

@interface Sale (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSNumber *published_at;
@property (nullable, nonatomic, retain) NSNumber *is_permanent;
@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) NSString *image_url;

@end

NS_ASSUME_NONNULL_END

//
//  Product+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

@dynamic category_id;
@dynamic descr;
@dynamic id;
@dynamic image_url;
@dynamic preview_small_url;
@dynamic preview_url;
@dynamic price;
@dynamic title;
@dynamic url;
@dynamic parent;
@dynamic modifiers;
@dynamic position;

@end

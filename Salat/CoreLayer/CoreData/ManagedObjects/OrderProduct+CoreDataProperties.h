//
//  OrderProduct+CoreDataProperties.h
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OrderProduct.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderProduct (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *quantity;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSNumber *sum;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *descr;
@property (nullable, nonatomic, retain) NSString *image_url;
@property (nullable, nonatomic, retain) NSSet<OrderProductModifier *> *modifiers;
@property (nullable, nonatomic, retain) Order *order;

@end

@interface OrderProduct (CoreDataGeneratedAccessors)

- (void)addModifiersObject:(OrderProductModifier *)value;
- (void)removeModifiersObject:(OrderProductModifier *)value;
- (void)addModifiers:(NSSet<OrderProductModifier *> *)values;
- (void)removeModifiers:(NSSet<OrderProductModifier *> *)values;

@end

NS_ASSUME_NONNULL_END

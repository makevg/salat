//
//  DeliveryTime+CoreDataProperties.h
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DeliveryTime.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryTime (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *day_number;
@property (nullable, nonatomic, retain) NSNumber *time_start;
@property (nullable, nonatomic, retain) NSNumber *time_end;
@property (nullable, nonatomic, retain) NSNumber *work_date;
@property (nullable, nonatomic, retain) NSNumber *is_opened;
@property (nullable, nonatomic, retain) Restaurant *restaurant;

@end

NS_ASSUME_NONNULL_END

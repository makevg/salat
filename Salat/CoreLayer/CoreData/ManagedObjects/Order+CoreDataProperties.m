//
//  Order+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Order+CoreDataProperties.h"

@implementation Order (CoreDataProperties)

@dynamic address_city_id;
@dynamic address_entrance;
@dynamic address_flat;
@dynamic address_floor;
@dynamic address_house;
@dynamic address_intercom_code;
@dynamic address_street;
@dynamic cart_sum;
@dynamic created_at;
@dynamic delivery_sum;
@dynamic delivery_time;
@dynamic guid;
@dynamic id;
@dynamic price;
@dynamic status;
@dynamic total_sum;
@dynamic updated_at;
@dynamic discount_title;
@dynamic discount_sum;
@dynamic discount_percent;
@dynamic products;

@end

//
//  DeliveryTime.h
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Restaurant;

NS_ASSUME_NONNULL_BEGIN

@interface DeliveryTime : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "DeliveryTime+CoreDataProperties.h"

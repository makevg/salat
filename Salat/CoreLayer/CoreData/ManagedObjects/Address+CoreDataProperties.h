//
//  Address+CoreDataProperties.h
//  
//
//  Created by mtx on 14.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Address.h"

@class Profile;

NS_ASSUME_NONNULL_BEGIN

@interface Address (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSNumber *city_id;
@property (nullable, nonatomic, retain) NSNumber *created_at;
@property (nullable, nonatomic, retain) NSNumber *delivery_sum;
@property (nullable, nonatomic, retain) NSString *entrance;
@property (nullable, nonatomic, retain) NSString *flat;
@property (nullable, nonatomic, retain) NSString *floor;
@property (nullable, nonatomic, retain) NSString *house;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *intercom_code;
@property (nullable, nonatomic, retain) NSNumber *is_main;
@property (nullable, nonatomic, retain) NSString *latitude;
@property (nullable, nonatomic, retain) NSString *longitude;
@property (nullable, nonatomic, retain) NSString *street;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSNumber *updated_at;
@property (nullable, nonatomic, retain) NSNumber *user_id;
@property (nullable, nonatomic, retain) Profile *profile;

@end

NS_ASSUME_NONNULL_END

//
//  Profile+CoreDataProperties.m
//  
//
//  Created by Максимычев Е.О. on 22.06.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Profile+CoreDataProperties.h"
#import "SAProfilePlainObject.h"

@implementation Profile (CoreDataProperties)

@dynamic id;
@dynamic email;
@dynamic first_name;
@dynamic last_name;
@dynamic vk_name;
@dynamic fb_name;
@dynamic phone_number;
@dynamic addresses;

@end

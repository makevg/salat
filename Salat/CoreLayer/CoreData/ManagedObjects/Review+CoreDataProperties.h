//
//  Review+CoreDataProperties.h
//  
//
//  Created by Maximychev Evgeny on 28.02.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Review.h"

NS_ASSUME_NONNULL_BEGIN

@interface Review (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *user_name;
@property (nullable, nonatomic, retain) NSNumber *published_at;
@property (nullable, nonatomic, retain) NSNumber *rate;
@property (nullable, nonatomic, retain) NSString *descr;
@property (nullable, nonatomic, retain) NSNumber *is_my;

@end

NS_ASSUME_NONNULL_END

//
//  CartItemModifier+CoreDataProperties.h
//  
//
//  Created by Maximychev Evgeny on 08.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CartItemModifier.h"

NS_ASSUME_NONNULL_BEGIN

@interface CartItemModifier (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *quantity;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) CartItem *item;

@end

NS_ASSUME_NONNULL_END

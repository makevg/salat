//
//  DeliveryTime+CoreDataProperties.m
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DeliveryTime+CoreDataProperties.h"

@implementation DeliveryTime (CoreDataProperties)

@dynamic id;
@dynamic day_number;
@dynamic time_start;
@dynamic time_end;
@dynamic work_date;
@dynamic is_opened;
@dynamic restaurant;

@end

//
//  Address+CoreDataProperties.m
//  
//
//  Created by mtx on 14.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Address+CoreDataProperties.h"
#import "Profile.h"

@implementation Address (CoreDataProperties)

@dynamic city;
@dynamic city_id;
@dynamic created_at;
@dynamic delivery_sum;
@dynamic entrance;
@dynamic flat;
@dynamic floor;
@dynamic house;
@dynamic id;
@dynamic intercom_code;
@dynamic is_main;
@dynamic latitude;
@dynamic longitude;
@dynamic street;
@dynamic title;
@dynamic updated_at;
@dynamic user_id;
@dynamic profile;

@end

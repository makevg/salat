//
//  ProductCategory+CoreDataProperties.m
//  
//
//  Created by Максимычев Е.О. on 17.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProductCategory+CoreDataProperties.h"

@implementation ProductCategory (CoreDataProperties)

@dynamic id;
@dynamic title;
@dynamic url;
@dynamic background_image_url;
@dynamic icon;
@dynamic parent_id;
@dynamic position;
@dynamic is_wok;

@end

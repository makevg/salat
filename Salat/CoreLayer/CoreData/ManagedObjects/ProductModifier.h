//
//  ProductModifier.h
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

NS_ASSUME_NONNULL_BEGIN

@interface ProductModifier : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "ProductModifier+CoreDataProperties.h"

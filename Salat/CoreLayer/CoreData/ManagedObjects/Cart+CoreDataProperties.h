//
//  Cart+CoreDataProperties.h
//  
//
//  Created by mtx on 16.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Cart.h"

NS_ASSUME_NONNULL_BEGIN

@interface Cart (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *hit;
@property (nullable, nonatomic, retain) NSNumber *recommendation;
@property (nullable, nonatomic, retain) NSNumber *suggestion;

@end

NS_ASSUME_NONNULL_END

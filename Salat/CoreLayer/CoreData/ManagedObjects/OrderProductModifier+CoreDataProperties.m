//
//  OrderProductModifier+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 06.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OrderProductModifier+CoreDataProperties.h"

@implementation OrderProductModifier (CoreDataProperties)

@dynamic id;
@dynamic quantity;
@dynamic price;
@dynamic sum;
@dynamic product_id;
@dynamic modifier_category_id;
@dynamic title;
@dynamic descr;
@dynamic image_url;
@dynamic preview_url;
@dynamic product;

@end

//
//  Sale+CoreDataProperties.m
//  
//
//  Created by Maximychev Evgeny on 09.02.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Sale+CoreDataProperties.h"

@implementation Sale (CoreDataProperties)

@dynamic id;
@dynamic title;
@dynamic content;
@dynamic published_at;
@dynamic is_permanent;
@dynamic url;
@dynamic image_url;

@end

//
//  PaymentMethod+CoreDataProperties.m
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PaymentMethod+CoreDataProperties.h"

@implementation PaymentMethod (CoreDataProperties)

@dynamic id;
@dynamic title;
@dynamic descr;
@dynamic restaurant;

@end

//
//  PaymentMethod+CoreDataProperties.h
//  
//
//  Created by Максимычев Е.О. on 23.05.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PaymentMethod.h"

NS_ASSUME_NONNULL_BEGIN

@interface PaymentMethod (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *descr;
@property (nullable, nonatomic, retain) Restaurant *restaurant;

@end

NS_ASSUME_NONNULL_END

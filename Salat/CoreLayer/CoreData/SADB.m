//
//  SADB.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SADB.h"
#import <MagicalRecord/MagicalRecordInternal.h>
#import <MagicalRecord/MagicalRecord+Setup.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+Options.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalSaves.h>

static NSString *const kSACoreDataStoreName = @"Salat";

@interface SADB ()
@property (nonatomic) NSManagedObjectContext *managedObjectContext;
@end

@implementation SADB

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:kSACoreDataStoreName];
#ifdef DEBUG
        [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelVerbose];
#else
        [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelOff];
#endif
        
    });
    return sharedInstance;
}

#pragma mark - Core Data Saving support

- (NSManagedObjectContext *)produceContextForRead {
    return [NSManagedObjectContext MR_defaultContext];
}

- (NSManagedObjectContext *)produceContextForChange {
    return [NSManagedObjectContext MR_rootSavingContext];
}

- (void)saveWithBlock:(void (^)(NSManagedObjectContext *localContext))block
           completion:(SASaveCompletionHandler)completion {
    NSManagedObjectContext *localContext = [self produceContextForChange];
    
    [localContext performBlock:^{
        if (block) {
            block(localContext);
        }
        
        [localContext MR_saveWithOptions:MRSaveParentContexts
                              completion:completion];
    }];
}

- (void)saveContext {
    if (self.managedObjectContext != nil) {
        NSError *error = nil;
        if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end

//
//  SADB.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define SALAT_DB [SADB sharedInstance]

typedef void (^SASaveCompletionHandler)(BOOL contextDidSave, NSError *error);

@interface SADB : NSObject

+ (instancetype)sharedInstance;

- (NSManagedObjectContext*)produceContextForRead;

- (void)saveWithBlock:(void (^)(NSManagedObjectContext *localContext))block
           completion:(SASaveCompletionHandler)completion;

- (void)saveContext;

@end

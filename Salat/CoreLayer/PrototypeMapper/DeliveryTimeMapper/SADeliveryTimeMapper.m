//
//  SADeliveryTimeMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SADeliveryTimeMapper.h"
#import "SADeliveryTimePlainObject.h"
#import "DeliveryTime.h"

@implementation SADeliveryTimeMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SADeliveryTimePlainObject *)filledObject withObject:(DeliveryTime *)object {
    filledObject.objectId = object.id;
    filledObject.dayNumber = object.day_number;
    filledObject.timeStart = object.time_start;
    filledObject.timeEnd = object.time_end;
    filledObject.isOpened = [object.is_opened boolValue];
    filledObject.workDate = object.work_date;
}

@end

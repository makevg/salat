//
//  SAPaymentMethodMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPaymentMethodMapper.h"
#import "SAPaymentMethodPlainObject.h"
#import "PaymentMethod.h"

@implementation SAPaymentMethodMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAPaymentMethodPlainObject *)filledObject withObject:(PaymentMethod *)object {
    filledObject.objectId = object.id;
    filledObject.title = object.title;
    filledObject.descr = object.descr;
}

@end

//
//  SAReviewMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewMapper.h"
#import "Review.h"
#import "SAReviewPlainObject.h"

@implementation SAReviewMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAReviewPlainObject *)filledObject withObject:(Review *)object {
    filledObject.reviewId = object.id;
    filledObject.userName = object.user_name;
    filledObject.descr = object.descr;
    filledObject.publishedAt = object.published_at;
    filledObject.rate = object.rate;
    filledObject.isMy = object.is_my;
}

@end

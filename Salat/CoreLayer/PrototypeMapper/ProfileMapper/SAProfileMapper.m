//
// Created by Максимычев Е.О. on 27.06.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAProfileMapper.h"
#import "SAProfilePlainObject.h"
#import "Profile.h"

@implementation SAProfileMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAProfilePlainObject *)filledObject withObject:(Profile *)object {
    filledObject.profileId = object.id;
    filledObject.firstName = object.first_name;
    filledObject.lastName = object.last_name;
    filledObject.email = object.email;
    filledObject.phoneNumber = object.phone_number;
    filledObject.fbName = object.fb_name;
    filledObject.vkName = object.vk_name;
}

@end
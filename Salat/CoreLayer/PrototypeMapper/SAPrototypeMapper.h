//
//  SAPrototypeMapper.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAPrototypeMapper <NSObject>

- (void)fillObject:(id)filledObject
        withObject:(id)object;

@end

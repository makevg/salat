//
//  SACartItemMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartItemMapper.h"
#import "SACartItemPlainObject.h"
#import "CartItem.h"
#import "SACartItemModifierPlainObject.h"
#import "SACartItemModifierMapper.h"
#import "CartItemModifier.h"

@implementation SACartItemMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SACartItemPlainObject *)filledObject withObject:(CartItem *)object {
    filledObject.itemId = object.item_id;
    filledObject.productId = object.product_id;
    filledObject.productTitle = object.product_title;
    filledObject.productDescr = object.product_descr;
    filledObject.productImageUrl = object.product_image_url;
    filledObject.cost = object.cost;
    filledObject.quantity = object.quantity;

    NSMutableArray <SACartItemModifierPlainObject *>*modifiers = [NSMutableArray new];

    for(CartItemModifier *modifier in object.modifiers) {
        SACartItemModifierPlainObject* plainModifier = [SACartItemModifierPlainObject new];

        plainModifier.itemModifierId = modifier.id;
        plainModifier.title = modifier.title;
        plainModifier.price = modifier.price;
        plainModifier.quantity = modifier.quantity;

        [modifiers addObject:plainModifier];
    }

    filledObject.modifiers = [modifiers copy];
}

@end

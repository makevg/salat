//
//  SACartItemMapper.h
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeMapper.h"

@interface SACartItemMapper : NSObject <SAPrototypeMapper>

@end

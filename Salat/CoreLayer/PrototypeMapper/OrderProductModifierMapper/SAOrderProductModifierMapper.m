//
// Created by Maximychev Evgeny on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAOrderProductModifierMapper.h"
#import "SAOrderProductModifierPlainObject.h"
#import "OrderProductModifier.h"

@implementation SAOrderProductModifierMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAOrderProductModifierPlainObject *)filledObject withObject:(OrderProductModifier *)object {
    filledObject.modifierId = object.id;
    filledObject.productId = object.product_id;
    filledObject.modifierCategoryId = object.modifier_category_id;
    filledObject.quantity = object.quantity;
    filledObject.price = object.price;
    filledObject.sum = object.sum;
    filledObject.title = object.title;
    filledObject.descr = object.descr;
    filledObject.imageUrl = object.image_url;
    filledObject.previewUrl = object.preview_url;
}

@end
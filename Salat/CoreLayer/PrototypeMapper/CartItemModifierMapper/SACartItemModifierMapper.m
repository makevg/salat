//
// Created by Maximychev Evgeny on 08.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SACartItemModifierMapper.h"
#import "SAProductModifierPlainObject.h"
#import "SACartItemModifierPlainObject.h"

@implementation SACartItemModifierMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SACartItemModifierPlainObject *)filledObject withObject:(SAProductModifierPlainObject *)object {
    filledObject.itemModifierId = object.modifierId;
    filledObject.title = object.title;
    filledObject.price = object.price;
    filledObject.quantity = object.quantity;
}
@end
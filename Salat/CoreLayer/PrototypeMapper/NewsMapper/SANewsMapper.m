//
//  SANewsMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsMapper.h"
#import "SANewsPlainObject.h"
#import "News.h"

@implementation SANewsMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SANewsPlainObject *)filledObject withObject:(News *)object {
    filledObject.newsId = object.id;
    filledObject.title = object.title;
    filledObject.content = object.content;
    filledObject.publishedAt = object.published_at;
    filledObject.imageUrl = object.image_url;
    filledObject.url = object.url;
}

@end

//
//  SAProductCategoryMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductCategoryMapper.h"
#import "ProductCategory.h"
#import "SAProductCategoryPlainObject.h"

@implementation SAProductCategoryMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAProductCategoryPlainObject *)filledObject withObject:(ProductCategory *)object {
    filledObject.objectId = object.id;
    filledObject.title = object.title;
    filledObject.url = object.url;
    filledObject.backgroundImageUrl = object.background_image_url;
    filledObject.icon = object.icon;
    filledObject.parentId = object.parent_id;
    filledObject.isWok = object.is_wok;
}

@end

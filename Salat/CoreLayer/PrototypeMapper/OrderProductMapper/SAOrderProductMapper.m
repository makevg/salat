//
// Created by Maximychev Evgeny on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAOrderProductMapper.h"
#import "SAOrderProductPlainObject.h"
#import "OrderProduct.h"

@implementation SAOrderProductMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAOrderProductPlainObject *)filledObject withObject:(OrderProduct *)object {
    filledObject.productId = object.id;
    filledObject.quantity = object.quantity;
    filledObject.price = object.price;
    filledObject.sum = object.sum;
    filledObject.title = object.title;
    filledObject.descr = object.descr;
    filledObject.imageUrl = object.image_url;
}

@end
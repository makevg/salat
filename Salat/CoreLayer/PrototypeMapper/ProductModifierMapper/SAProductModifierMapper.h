//
//  SAProductModifierMapper.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeMapper.h"

@interface SAProductModifierMapper : NSObject <SAPrototypeMapper>

@end

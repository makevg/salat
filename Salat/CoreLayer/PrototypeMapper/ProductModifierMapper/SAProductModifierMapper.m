//
//  SAProductModifierMapper.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductModifierMapper.h"
#import "ProductModifier.h"
#import "SAProductModifierPlainObject.h"

@implementation SAProductModifierMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAProductModifierPlainObject *)filledObject withObject:(ProductModifier *)object {
    filledObject.modifierId = object.id;
    filledObject.productId = object.product_id;
    filledObject.modifierCategoryId = object.modifier_category_id;
    filledObject.price = object.price;
    filledObject.sum = object.sum;
    filledObject.quantity = object.quantity;
    filledObject.title = object.title;
    filledObject.descr = object.descr;
    filledObject.imageUrl = object.image_url;
    filledObject.previewUrl = object.preview_url;
}

@end

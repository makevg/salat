//
// Created by Maximychev Evgeny on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAOrderMapper.h"
#import "SAOrderPlainObject.h"
#import "Order.h"

@implementation SAOrderMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAOrderPlainObject *)filledObject withObject:(Order *)object {
    filledObject.orderId = object.id;
    filledObject.addressCityId = object.address_city_id;
    filledObject.addressEntrance = object.address_entrance;
    filledObject.addressFlat = object.address_flat;
    filledObject.addressFloor = object.address_floor;
    filledObject.addressHouse = object.address_house;
    filledObject.addressIntercomCode = object.address_intercom_code;
    filledObject.addressStreet = object.address_street;
    filledObject.cartSum = object.cart_sum;
    filledObject.createdAt = object.created_at;
    filledObject.deliverySum = object.delivery_sum;
    filledObject.deliveryTime = object.delivery_time;
    filledObject.guid = object.guid;
    filledObject.price = object.price;
    filledObject.status = object.status;
    filledObject.totalSum = object.total_sum;
    filledObject.updatedAt = object.updated_at;
    filledObject.discountTitle = object.discount_title;
    filledObject.discountSum = object.discount_sum;
    filledObject.discountPercent = object.discount_percent;
}

@end
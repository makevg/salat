//
//  SADeliveryAreaMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SADeliveryAreaMapper.h"
#import "SADeliveryAreaPlainObject.h"
#import "DeliveryArea.h"

@implementation SADeliveryAreaMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SADeliveryAreaPlainObject *)filledObject withObject:(DeliveryArea *)object {
    filledObject.objectId = object.id;
    filledObject.title = object.title;
    filledObject.price = object.price;
    filledObject.polygon = object.polygon;
}

@end

//
//  SASaleMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASaleMapper.h"
#import "SASalePlainObject.h"
#import "Sale.h"

@implementation SASaleMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SASalePlainObject *)filledObject withObject:(Sale *)object {
    filledObject.saleId = object.id;
    filledObject.title = object.title;
    filledObject.content = object.content;
    filledObject.publishedAt = object.published_at;
    filledObject.isPermanent = object.is_permanent;
    filledObject.imageUrl = object.image_url;
    filledObject.url = object.url;
}

@end

//
//  SAProductMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductMapper.h"
#import "SAProductPlainObject.h"
#import "Product.h"

@implementation SAProductMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAProductPlainObject *)filledObject withObject:(Product *)object {
    filledObject.objectId = object.id;
    filledObject.categoryId = object.category_id;
    filledObject.title = object.title;
    filledObject.descr = object.descr;
    filledObject.price = object.price;
    filledObject.url = object.url;
    filledObject.imageUrl = object.image_url;
    filledObject.previewUrl = object.preview_url;
    filledObject.previewSmallUrl = object.preview_small_url;
}

@end

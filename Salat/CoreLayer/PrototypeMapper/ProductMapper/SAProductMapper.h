//
//  SAProductMapper.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeMapper.h"

@interface SAProductMapper : NSObject <SAPrototypeMapper>

@end

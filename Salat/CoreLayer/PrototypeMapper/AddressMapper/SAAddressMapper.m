//
// Created by Максимычев Е.О. on 27.06.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAAddressMapper.h"
#import "SAAddressPlainObject.h"
#import "Address.h"

@implementation SAAddressMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SAAddressPlainObject *)filledObject withObject:(Address *)object {
    filledObject.addressId = object.id;
    filledObject.city = object.city;
    filledObject.street = object.street;
    filledObject.house = object.house;
    filledObject.createdAt = object.updated_at;
    filledObject.isMain = object.is_main;
    filledObject.entrance = object.entrance;
    filledObject.flat = object.flat;
    filledObject.floor = object.floor;
    filledObject.intercomCode = object.intercom_code;
    filledObject.title = object.title;
    filledObject.deliverySum = object.delivery_sum;
    filledObject.location = [[CLLocation alloc] initWithLatitude:[object.latitude doubleValue] longitude:[object.longitude doubleValue]];
}

@end
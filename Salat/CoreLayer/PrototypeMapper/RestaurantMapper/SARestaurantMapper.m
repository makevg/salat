//
//  SARestaurantMapper.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARestaurantMapper.h"
#import "SARestaurantPlainObject.h"
#import "Restaurant.h"

@implementation SARestaurantMapper

#pragma mark - SAPrototypeMapper

- (void)fillObject:(SARestaurantPlainObject *)filledObject withObject:(Restaurant *)object {
    filledObject.objectId = object.id;
    filledObject.title = object.title;
    filledObject.descr = object.descr;
    filledObject.phone = object.phone;
    filledObject.city = object.city;
    filledObject.url = object.url;
    filledObject.logo = object.logo;
}

#pragma mark - Private

@end

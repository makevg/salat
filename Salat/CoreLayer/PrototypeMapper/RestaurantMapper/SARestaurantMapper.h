//
//  SARestaurantMapper.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeMapper.h"

@interface SARestaurantMapper : NSObject <SAPrototypeMapper>

@end

//
// Created by Maximychev Evgeny on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAOrderProductModifierParser.h"
#import "OrderProductModifier.h"
#import "SADataFormatter.h"

@implementation SAOrderProductModifierParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(OrderProductModifier *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.quantity = [SADataFormatter numberByObject:response[@"quantity"]];
    filledObject.price = [SADataFormatter numberByObject:response[@"price"]];
    filledObject.sum = [SADataFormatter numberByString:response[@"sum"]];
    NSDictionary *dataDict = response[@"data"];
    filledObject.modifier_category_id = [SADataFormatter numberByObject:dataDict[@"modifier_category_id"]];
    filledObject.product_id = [SADataFormatter numberByObject:dataDict[@"product_id"]];
    filledObject.title = [SADataFormatter stringByObject:dataDict[@"title"]];
    filledObject.descr = [SADataFormatter stringByObject:dataDict[@"description"]];

    NSDictionary *image = response[@"image"];
    if(![image isKindOfClass:[NSNull class]]) {
        filledObject.image_url = [SADataFormatter stringByObject:image[@"path"]];
    }

    NSDictionary *preview = response[@"preview"];
    if(![preview isKindOfClass:[NSNull class]]) {
        filledObject.preview_url = [SADataFormatter stringByObject:preview[@"path"]];
    }
}

@end
//
//  SAReviewParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewParser.h"
#import "Review.h"
#import "SADataFormatter.h"

@implementation SAReviewParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(Review *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = response[@"id"];
    filledObject.user_name = response[@"username"];
    filledObject.published_at = response[@"created_at"];
    filledObject.descr = [SADataFormatter stringByHTML:response[@"content"]];
    filledObject.rate = response[@"rating"];
    filledObject.is_my = response[@"is_my"];
}

@end

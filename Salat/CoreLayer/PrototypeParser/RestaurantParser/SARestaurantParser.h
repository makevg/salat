//
//  SARestaurantParser.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeParser.h"

@interface SARestaurantParser : NSObject <SAPrototypeParser>

@end

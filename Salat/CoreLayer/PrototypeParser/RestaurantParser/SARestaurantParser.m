//
//  SARestaurantParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARestaurantParser.h"
#import "Restaurant.h"
#import "SADataFormatter.h"

@implementation SARestaurantParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(Restaurant *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.descr = [SADataFormatter stringByObject:response[@"description"]];
    filledObject.phone = [SADataFormatter stringByObject:response[@"phone"]];
    filledObject.city = [SADataFormatter stringByObject:response[@"city"][@"title"]];
    filledObject.url = [SADataFormatter stringByObject:response[@"url"]];
    filledObject.logo = [SADataFormatter stringByObject:response[@"logo"][@"path"]];
}

@end

//
//  SAProfileParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 22.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileParser.h"
#import "SADataFormatter.h"
#import "Profile.h"

@implementation SAProfileParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(Profile *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.first_name = [SADataFormatter stringByObject:response[@"first_name"]];
    filledObject.last_name = [SADataFormatter stringByObject:response[@"last_name"]];
    filledObject.vk_name = nil;
    filledObject.fb_name = nil;

    for (NSDictionary *provider in response[@"providers"]) {
        NSString *name = [NSString stringWithFormat:@"%@ %@",
                                                    [SADataFormatter stringByObject:provider[@"first_name"]],
                                                    [SADataFormatter stringByObject:provider[@"last_name"]]
        ];

        if ([provider[@"provider"] isEqualToString:@"vk"]) {
            filledObject.vk_name = name;
        } else {
            filledObject.fb_name = name;
        }
    }

    filledObject.email = [SADataFormatter stringByObject:response[@"email"]];
    filledObject.phone_number = [SADataFormatter stringByObject:response[@"phone_number_formatted"]];
}

@end

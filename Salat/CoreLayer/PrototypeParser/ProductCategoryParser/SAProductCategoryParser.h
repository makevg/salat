//
//  SAProductCategoryParser.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeParser.h"

@interface SAProductCategoryParser : NSObject <SAPrototypeParser>

@end

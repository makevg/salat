//
//  SAProductCategoryParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductCategoryParser.h"
#import "ProductCategory.h"
#import "SADataFormatter.h"

@implementation SAProductCategoryParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(ProductCategory *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.url = [SADataFormatter stringByObject:response[@"url"]];
    filledObject.is_wok = [SADataFormatter numberByObject:response[@"is_wok"]];

    NSDictionary *backgroundImageDict = response[@"background"];
    if (![backgroundImageDict isKindOfClass:[NSNull class]]) {
        filledObject.background_image_url = [SADataFormatter stringByObject:response[@"background"][@"path"]];
    }

    NSDictionary *iconDict = response[@"icon"];
    if (![iconDict isKindOfClass:[NSNull class]]) {
        filledObject.icon = [SADataFormatter stringByObject:response[@"icon"][@"path"]];
    }

    filledObject.parent_id = [SADataFormatter numberByObject:response[@"parent_id"]];
    filledObject.position = [SADataFormatter numberByObject:response[@"position"]];
    
}

@end

//
//  SAPaymentMethodParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPaymentMethodParser.h"
#import "PaymentMethod.h"
#import "SADataFormatter.h"

@implementation SAPaymentMethodParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(PaymentMethod *)filledObject fromResponse:(id)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.descr = [SADataFormatter stringByObject:response[@"description"]];
}

@end

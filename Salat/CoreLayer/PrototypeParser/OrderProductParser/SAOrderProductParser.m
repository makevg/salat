//
// Created by Maximychev Evgeny on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAOrderProductParser.h"
#import "OrderProduct.h"
#import "SADataFormatter.h"

@implementation SAOrderProductParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(OrderProduct *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.quantity = [SADataFormatter numberByObject:response[@"quantity"]];
    filledObject.price = [SADataFormatter numberByObject:response[@"price"]];
    filledObject.sum = [SADataFormatter numberByObject:response[@"sum"]];
    NSDictionary *dataDict = response[@"data"];
    filledObject.title = [SADataFormatter stringByObject:dataDict[@"title"]];
    filledObject.descr = [SADataFormatter stringByObject:dataDict[@"description"]];

    NSDictionary *image = dataDict[@"image"];
    if(![image isKindOfClass:[NSNull class]]) {
        filledObject.image_url = [SADataFormatter stringByObject:image[@"path"]];
    }
}

@end
//
//  SASaleParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASaleParser.h"
#import "Sale.h"
#import "SADataFormatter.h"

@implementation SASaleParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(Sale *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = response[@"id"];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.content = [SADataFormatter stringByHTML:response[@"content"]];
    filledObject.published_at = response[@"published_at"];
    filledObject.is_permanent = response[@"is_permanent"];
    filledObject.url = [SADataFormatter stringByObject:response[@"url"]];

    NSDictionary *imageDict = response[@"image"];
    if (![imageDict isKindOfClass:[NSNull class]]) {
        filledObject.image_url = [SADataFormatter stringByObject:imageDict[@"path"]];
    }
}

@end

//
//  SAAddressParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 22.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddressParser.h"
#import "Address.h"
#import "SADataFormatter.h"

@implementation SAAddressParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(Address *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.city_id = [SADataFormatter numberByObject:response[@"city_id"]];
    filledObject.user_id = [SADataFormatter numberByObject:response[@"user_id"]];
    filledObject.created_at = [SADataFormatter numberByObject:response[@"created_at"]];
    filledObject.updated_at = [SADataFormatter numberByObject:response[@"updated_at"]];
    filledObject.entrance = [SADataFormatter stringByObject:response[@"entrance"]];
    filledObject.house = [SADataFormatter stringByObject:response[@"house"]];
    filledObject.flat = [SADataFormatter stringByObject:response[@"flat"]];
    filledObject.floor = [SADataFormatter stringByObject:response[@"floor"]];
    filledObject.intercom_code = [SADataFormatter stringByObject:response[@"intercom_code"]];
    filledObject.is_main = [SADataFormatter numberByObject:response[@"is_main"]];
    filledObject.street = [SADataFormatter stringByObject:response[@"street"]];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.delivery_sum = [SADataFormatter numberByObject:response[@"delivery"][@"sum"]];

    NSString *geoPointString = [SADataFormatter stringByObject:response[@"geo_point"]];
    NSArray *geoPointArr = [geoPointString componentsSeparatedByString:@";"];
    filledObject.latitude = [geoPointArr firstObject];
    filledObject.longitude = [geoPointArr lastObject];
}

@end

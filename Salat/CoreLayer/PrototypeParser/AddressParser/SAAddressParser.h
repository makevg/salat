//
//  SAAddressParser.h
//  Salat
//
//  Created by Максимычев Е.О. on 22.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeParser.h"

@interface SAAddressParser : NSObject <SAPrototypeParser>

@end

//
//  SAProductParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductParser.h"
#import "Product.h"
#import "SADataFormatter.h"

@implementation SAProductParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(Product *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.category_id = [SADataFormatter numberByObject:response[@"category_id"]];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.descr = [SADataFormatter stringByObject:response[@"description"]];
    filledObject.url = [SADataFormatter stringByObject:response[@"url"]];
    filledObject.price = [SADataFormatter numberByObject:response[@"price"]];

    NSDictionary *image = response[@"image"];
    if(![image isKindOfClass:[NSNull class]]) {
        filledObject.image_url = [SADataFormatter stringByObject:image[@"path"]];
    }

    NSDictionary *preview = response[@"preview"];
    if(![preview isKindOfClass:[NSNull class]]) {
        filledObject.preview_url = [SADataFormatter stringByObject:preview[@"path"]];
    }

    NSDictionary *preview_small = response[@"preview_small"];
    if(![preview_small isKindOfClass:[NSNull class]]) {
        filledObject.preview_small_url = [SADataFormatter stringByObject:preview_small[@"path"]];
    }

    filledObject.position = [SADataFormatter numberByObject:response[@"position"]];
}

@end

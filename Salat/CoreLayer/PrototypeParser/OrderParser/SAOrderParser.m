//
//  SAOrderParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 22.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderParser.h"
#import "Order.h"
#import "SADataFormatter.h"

@implementation SAOrderParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(Order *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.guid = [SADataFormatter stringByObject:response[@"guid"]];
    filledObject.status = [SADataFormatter numberByObject:response[@"status"]];
    filledObject.price = [SADataFormatter numberByObject:response[@"price"]];
    filledObject.address_city_id = [SADataFormatter numberByObject:response[@"address_city_id"]];
    filledObject.address_street = [SADataFormatter stringByObject:response[@"address_street"]];
    filledObject.address_house = [SADataFormatter stringByObject:response[@"address_house"]];
    filledObject.address_flat = [SADataFormatter stringByObject:response[@"address_flat"]];
    filledObject.address_floor = [SADataFormatter numberByObject:response[@"address_floor"]];
    filledObject.address_entrance = [SADataFormatter stringByObject:response[@"address_entrance"]];
    filledObject.address_intercom_code = [SADataFormatter stringByObject:response[@"address_intercom_code"]];
    filledObject.created_at = [SADataFormatter numberByObject:response[@"created_at"]];
    filledObject.updated_at = [SADataFormatter numberByObject:response[@"updated_at"]];

    NSDictionary *orderData = response[@"order_data"];
    filledObject.cart_sum = [SADataFormatter numberByString:orderData[@"cart_sum"]];
    filledObject.total_sum = [SADataFormatter numberByString:orderData[@"total_sum"]];
    filledObject.delivery_sum = [SADataFormatter numberByString:orderData[@"delivery_sum"]];
    filledObject.delivery_time = [SADataFormatter numberByObject:orderData[@"delivery_time"]];
    NSArray *discounts = orderData[@"discounts"];
    
    if( ![discounts isKindOfClass:[NSArray class]] )
        return;
        
    for (NSDictionary *discountDict in discounts) {
        filledObject.discount_title = [SADataFormatter stringByObject:discountDict[@"description"]];
        filledObject.discount_percent = [SADataFormatter numberByObject:discountDict[@"percent"]];
        filledObject.discount_sum = [SADataFormatter numberByString:discountDict[@"sum"]];
    }
}

@end

//
//  SADeliveryAreaParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SADeliveryAreaParser.h"
#import "DeliveryArea.h"
#import "SADataFormatter.h"

@implementation SADeliveryAreaParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(DeliveryArea *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.price = [SADataFormatter numberByObject:response[@"price"]];
    filledObject.polygon = [SADataFormatter stringByObject:response[@"polygon"]];
}

@end

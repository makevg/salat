//
//  SANewsParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsParser.h"
#import "News.h"
#import "SADataFormatter.h"

@implementation SANewsParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(News *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = response[@"id"];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];

    NSString *content = [SADataFormatter stringByObject:response[@"content"]];
    content = [NSString stringWithFormat:@"<body style=\"font-family:'PTSans-Regular';line-height: 13pt;color:#ACABA6;\">%@</body>", content];

    filledObject.content = content;
    filledObject.url = [SADataFormatter stringByObject:response[@"url"]];

    NSDictionary *imageDict = response[@"image"];
    if (![imageDict isKindOfClass:[NSNull class]]) {
        filledObject.image_url = [SADataFormatter stringByObject:imageDict[@"path"]];
    }

    filledObject.published_at = response[@"published_at"];
}

@end

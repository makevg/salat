//
//  SANewsParser.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAPrototypeParser.h"

@interface SANewsParser : NSObject <SAPrototypeParser>

@end

//
// Created by mtx on 24.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "OrderCalculateParser.h"
#import "SADataFormatter.h"
#import "SACartCalculationPO.h"


@implementation OrderCalculateParser

- (void)fillObject:(SACartCalculationPO *)filledObject fromResponse:(NSDictionary *)response {
    NSMutableArray *items = [NSMutableArray new];
    NSDictionary *realItems = response[@"cart"];

    for( NSDictionary *realItem in realItems ) {
        SACartCalculationItemPO *itemPO = [SACartCalculationItemPO new];

        itemPO.id = [SADataFormatter numberByObject:realItem[@"id"]];
        itemPO.price = [SADataFormatter numberByObject:realItem[@"price"]];

        NSArray *modifiers = realItem[@"modifiers"];
        NSMutableArray *itemModifiers = [NSMutableArray new];

        for( NSDictionary *modifier in modifiers ) {
            SACartCalculationItemModifierPO *itemModifierPO = [SACartCalculationItemModifierPO new];
            itemModifierPO.id = [SADataFormatter numberByObject:modifier[@"id"]];
            itemModifierPO.price = [SADataFormatter numberByObject:modifier[@"price"]];

            [itemModifiers addObject:itemModifierPO];
        }

        itemPO.modifiers = itemModifiers;
        [items addObject:itemPO];
    }

    filledObject.items = items;

    for( NSDictionary *discount in response[@"discounts"] ) {
        if([discount isKindOfClass:[NSNull class]])
            break;

        SACartCalculationDiscountPO *discountPO = [SACartCalculationDiscountPO new];
        discountPO.descr = discount[@"description"];
        discountPO.percent = [SADataFormatter numberByObject:discount[@"percent"]];
        discountPO.sum = [SADataFormatter numberByObject:discount[@"sum"]];
        filledObject.discountInfo = discountPO;
        break;
    }

//    //for test
//    if( !filledObject.discountInfo ) {
//        SACartCalculationDiscountPO *discountPO = [SACartCalculationDiscountPO new];
//        discountPO.descr = @"абракадабра";
//        discountPO.percent = @10;
//        discountPO.sum = @10;
//        filledObject.discountInfo = discountPO;
//    }

    for( NSDictionary *suggestion in response[@"suggestions"] ) {
        SACartCalculationSuggestionPO *suggestionPO = [SACartCalculationSuggestionPO new];
        suggestionPO.productId = [SADataFormatter numberByObject:suggestion[@"right_product"][@"id"]];
        filledObject.suggestionProduct = suggestionPO;
        break;
    }

//    //for test
//    if(!filledObject.suggestionProduct) {
//        SACartCalculationSuggestionPO *suggestionPO = [SACartCalculationSuggestionPO new];
//        suggestionPO.productId = @3;
//        filledObject.suggestionProduct = suggestionPO;
//    }

    SACartCalculationDeliveryPO *deliveryPO = [SACartCalculationDeliveryPO new];
    deliveryPO.sum = [SADataFormatter numberByObject:response[@"delivery"][@"sum"]];
    filledObject.deliveryInfo = deliveryPO;
}

@end
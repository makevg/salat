//
//  SADeliveryTimeParser.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SADeliveryTimeParser.h"
#import "DeliveryTime.h"
#import "SADataFormatter.h"

@implementation SADeliveryTimeParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(DeliveryTime *)filledObject fromResponse:(id)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.day_number = [SADataFormatter numberByObject:response[@"day_number"]];
    filledObject.time_start = [SADataFormatter numberByObject:response[@"time_start"]];
    filledObject.time_end = [SADataFormatter numberByObject:response[@"time_end"]];
    filledObject.is_opened = response[@"is_opened"];
    filledObject.work_date = [SADataFormatter numberByObject:response[@"work_date"]];
}

@end

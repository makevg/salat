//
//  SAProductModifierParser.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPrototypeParser.h"

@interface SAProductModifierParser : NSObject <SAPrototypeParser>

@end

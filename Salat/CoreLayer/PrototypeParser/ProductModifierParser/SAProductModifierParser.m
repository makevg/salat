//
//  SAProductModifierParser.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductModifierParser.h"
#import "ProductModifier.h"
#import "SADataFormatter.h"

@implementation SAProductModifierParser

#pragma mark - SAPrototypeParser

- (void)fillObject:(ProductModifier *)filledObject fromResponse:(NSDictionary *)response {
    filledObject.id = [SADataFormatter numberByObject:response[@"id"]];
    filledObject.price = [SADataFormatter numberByObject:response[@"price"]];
    filledObject.product_id = [SADataFormatter numberByObject:response[@"product_id"]];
    filledObject.modifier_category_id = [SADataFormatter numberByObject:response[@"modifier_category_id"]];
    filledObject.title = [SADataFormatter stringByObject:response[@"title"]];
    filledObject.descr = [SADataFormatter stringByObject:response[@"description"]];

    NSDictionary *image = response[@"image"];
    if(![image isKindOfClass:[NSNull class]]) {
        filledObject.image_url = [SADataFormatter stringByObject:image[@"path"]];
    }

    NSDictionary *preview = response[@"preview"];
    if(![preview isKindOfClass:[NSNull class]]) {
        filledObject.preview_url = [SADataFormatter stringByObject:preview[@"path"]];
    }

    filledObject.position = [SADataFormatter numberByObject:response[@"position"]];
}

@end

//
//  SAPrototypeParser.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAPrototypeParser <NSObject>

- (void)fillObject:(id)filledObject
      fromResponse:(id)response;

@end

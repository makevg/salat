//
//  SACoreComponents.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TyphoonAssembly.h"

@protocol SAPrototypeMapper;
@protocol SAPrototypeParser;

@protocol SACoreComponents <NSObject>

#pragma mark - Mappers

- (id<SAPrototypeMapper>)productCategoryMapper;
- (id<SAPrototypeMapper>)productMapper;
- (id<SAPrototypeMapper>)productModifierMapper;
- (id<SAPrototypeMapper>)reviewMapper;
- (id<SAPrototypeMapper>)saleMapper;
- (id<SAPrototypeMapper>)newsMapper;
- (id<SAPrototypeMapper>)deliveryAreaMapper;
- (id<SAPrototypeMapper>)deliveryTimeMapper;
- (id<SAPrototypeMapper>)paymentMethodMapper;
- (id<SAPrototypeMapper>)restaurantMapper;
- (id<SAPrototypeMapper>)cartItemMapper;
- (id<SAPrototypeMapper>)orderMapper;
- (id<SAPrototypeMapper>)orderProductMapper;
- (id<SAPrototypeMapper>)orderProductModifierMapper;
- (id<SAPrototypeMapper>)profileMapper;
- (id<SAPrototypeMapper>)addressMapper;
- (id<SAPrototypeMapper>)cartItemModifierMapper;

#pragma mark - Parsers

- (id<SAPrototypeParser>)productCategoryParser;
- (id<SAPrototypeParser>)productParser;
- (id<SAPrototypeParser>)productModifierParser;
- (id<SAPrototypeParser>)reviewParser;
- (id<SAPrototypeParser>)saleParser;
- (id<SAPrototypeParser>)newsParser;
- (id<SAPrototypeParser>)deliveryAreaParser;
- (id<SAPrototypeParser>)deliveryTimeParser;
- (id<SAPrototypeParser>)paymentMethodParser;
- (id<SAPrototypeParser>)restaurantParser;
- (id<SAPrototypeParser>)orderParser;
- (id<SAPrototypeParser>)orderProductParser;
- (id<SAPrototypeParser>)orderProductModifierParser;
- (id<SAPrototypeParser>)profileParser;
- (id<SAPrototypeParser>)addressParser;

@end

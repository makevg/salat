//
//  SACoreComponentsAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACoreComponentsAssembly.h"
#import "SAProductCategoryMapper.h"
#import "SAProductMapper.h"
#import "SAReviewMapper.h"
#import "SASaleMapper.h"
#import "SAProductCategoryParser.h"
#import "SAProductParser.h"
#import "SAReviewParser.h"
#import "SASaleParser.h"
#import "SANewsMapper.h"
#import "SANewsParser.h"
#import "SADeliveryAreaMapper.h"
#import "SADeliveryTimeMapper.h"
#import "SAPaymentMethodMapper.h"
#import "SARestaurantMapper.h"
#import "SADeliveryAreaParser.h"
#import "SADeliveryTimeParser.h"
#import "SAPaymentMethodParser.h"
#import "SARestaurantParser.h"
#import "SACartItemMapper.h"
#import "SAOrderParser.h"
#import "SAProfileParser.h"
#import "SAAddressParser.h"
#import "SAProfileMapper.h"
#import "SAAddressMapper.h"
#import "SAProductModifierMapper.h"
#import "SAProductModifierParser.h"
#import "SAOrderMapper.h"
#import "SAOrderProductMapper.h"
#import "SAOrderProductParser.h"
#import "SAOrderProductModifierParser.h"
#import "SAOrderProductModifierMapper.h"
#import "SACartItemModifierMapper.h"

@implementation SACoreComponentsAssembly

#pragma mark - Mappers

- (id <SAPrototypeMapper>)productCategoryMapper {
    return [TyphoonDefinition withClass:[SAProductCategoryMapper class]];
}

- (id <SAPrototypeMapper>)productMapper {
    return [TyphoonDefinition withClass:[SAProductMapper class]];
}

- (id <SAPrototypeMapper>)productModifierMapper {
    return [TyphoonDefinition withClass:[SAProductModifierMapper class]];
}

- (id <SAPrototypeMapper>)reviewMapper {
    return [TyphoonDefinition withClass:[SAReviewMapper class]];
}

- (id <SAPrototypeMapper>)saleMapper {
    return [TyphoonDefinition withClass:[SASaleMapper class]];
}

- (id <SAPrototypeMapper>)newsMapper {
    return [TyphoonDefinition withClass:[SANewsMapper class]];
}

- (id <SAPrototypeMapper>)deliveryAreaMapper {
    return [TyphoonDefinition withClass:[SADeliveryAreaMapper class]];
}

- (id <SAPrototypeMapper>)deliveryTimeMapper {
    return [TyphoonDefinition withClass:[SADeliveryTimeMapper class]];
}

- (id <SAPrototypeMapper>)paymentMethodMapper {
    return [TyphoonDefinition withClass:[SAPaymentMethodMapper class]];
}

- (id <SAPrototypeMapper>)restaurantMapper {
    return [TyphoonDefinition withClass:[SARestaurantMapper class]];
}

- (id <SAPrototypeMapper>)cartItemMapper {
    return [TyphoonDefinition withClass:[SACartItemMapper class]];
}

- (id <SAPrototypeMapper>)orderMapper {
    return [TyphoonDefinition withClass:[SAOrderMapper class]];
}

- (id <SAPrototypeMapper>)orderProductMapper {
    return [TyphoonDefinition withClass:[SAOrderProductMapper class]];
}

- (id <SAPrototypeMapper>)orderProductModifierMapper {
    return [TyphoonDefinition withClass:[SAOrderProductModifierMapper class]];
}

- (id <SAPrototypeMapper>)profileMapper {
    return [TyphoonDefinition withClass:[SAProfileMapper class]];
}

- (id <SAPrototypeMapper>)addressMapper {
    return [TyphoonDefinition withClass:[SAAddressMapper class]];
}

- (id <SAPrototypeMapper>)cartItemModifierMapper {
    return [TyphoonDefinition withClass:[SACartItemModifierMapper class]];
}


#pragma mark - Parsers

- (id <SAPrototypeParser>)productCategoryParser {
    return [TyphoonDefinition withClass:[SAProductCategoryParser class]];
}

- (id <SAPrototypeParser>)productParser {
    return [TyphoonDefinition withClass:[SAProductParser class]];
}

- (id <SAPrototypeParser>)productModifierParser {
    return [TyphoonDefinition withClass:[SAProductModifierParser class]];
}

- (id <SAPrototypeParser>)reviewParser {
    return [TyphoonDefinition withClass:[SAReviewParser class]];
}

- (id <SAPrototypeParser>)saleParser {
    return [TyphoonDefinition withClass:[SASaleParser class]];
}

- (id <SAPrototypeParser>)newsParser {
    return [TyphoonDefinition withClass:[SANewsParser class]];
}

- (id <SAPrototypeParser>)deliveryAreaParser {
    return [TyphoonDefinition withClass:[SADeliveryAreaParser class]];
}

- (id <SAPrototypeParser>)deliveryTimeParser {
    return [TyphoonDefinition withClass:[SADeliveryTimeParser class]];
}

- (id <SAPrototypeParser>)paymentMethodParser {
    return [TyphoonDefinition withClass:[SAPaymentMethodParser class]];
}

- (id <SAPrototypeParser>)restaurantParser {
    return [TyphoonDefinition withClass:[SARestaurantParser class]];
}

- (id <SAPrototypeParser>)orderParser {
    return [TyphoonDefinition withClass:[SAOrderParser class]];
}

- (id <SAPrototypeParser>)orderProductParser {
    return [TyphoonDefinition withClass:[SAOrderProductParser class]];
}

- (id <SAPrototypeParser>)orderProductModifierParser {
    return [TyphoonDefinition withClass:[SAOrderProductModifierParser class]];
}

- (id <SAPrototypeParser>)profileParser {
    return [TyphoonDefinition withClass:[SAProfileParser class]];
}

- (id <SAPrototypeParser>)addressParser {
    return [TyphoonDefinition withClass:[SAAddressParser class]];
}

@end

//
//  SACoreComponentsAssembly.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import "SACoreComponents.h"

@interface SACoreComponentsAssembly : TyphoonAssembly <SACoreComponents>

@end

//
//  SAAPI.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kNetworkChangeStateNotification;
extern NSString *const kUnauthorizedNotification;
extern NSString *const kLogoutNotification;
extern NSString *const kLoginNotification;

#define SALAT_API [SAAPI sharedInstance]

typedef void (^SAAPIHandler)(NSURLResponse *response, id responseObject, NSError *error);

@interface SAAPI : NSObject

+ (instancetype)sharedInstance;

- (BOOL)isOnline;

- (BOOL)requestGetToUrlString:(NSString *)urlString
                       params:(NSDictionary *)params
                    useDomain:(BOOL)useDomain
                      handler:(SAAPIHandler)handler;

- (BOOL)requestPostToUrlString:(NSString *)urlString
                        params:(NSDictionary *)params
                     useDomain:(BOOL)useDomain
                       handler:(SAAPIHandler)handler;

- (void)clearCookies;

@end

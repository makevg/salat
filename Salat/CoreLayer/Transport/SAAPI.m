//
//  SAAPI.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAPI.h"
#import <Crashlytics/Crashlytics/CLSLogging.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>

static NSString *const kDomain = @"https://api.675675.ru";
//static NSString *const kDomain = @"https://api.salat.mostcreative.ru";

#ifdef DEBUG
#define NSDebugLog(log) NSLog(@"%s, %s", __PRETTY_FUNCTION__, log)
#else
#define NSDebugLog(log)
#endif

NSString *const kNetworkChangeStateNotification = @"ru.salat.networking.reachibility.change";
NSString *const kUnauthorizedNotification = @"ru.salat.networking.unauthorized";
NSString *const kLogoutNotification = @"ru.salat.networking.logout";
NSString *const kLoginNotification = @"ru.salat.networking.login";

const char *const kApiQueueName = "ru.salat.app.apiQueue";

@interface SAAPI () {
    AFURLSessionManager *session;
    AFHTTPSessionManager *httpSession;
    dispatch_queue_t completitionQueue;
    AFNetworkReachabilityStatus currentOnlineState;
}

@end

@implementation SAAPI

- (BOOL)lazyLoad {
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    session = [[AFURLSessionManager alloc] initWithSessionConfiguration:config];
    httpSession = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:config];
    httpSession.requestSerializer = [AFJSONRequestSerializer serializer];
    completitionQueue = dispatch_queue_create(kApiQueueName, NULL);
    session.completionQueue = completitionQueue;
    currentOnlineState = AFNetworkReachabilityStatusUnknown;
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        BOOL reachable;
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                NSDebugLog("No Internet Connection");
                reachable = NO;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSDebugLog("WIFI");
                reachable = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSDebugLog("3G");
                reachable = YES;
                break;
            default:
                NSDebugLog("Unknown network status");
                reachable = NO;
                break;
        }
        
        if (status != currentOnlineState && currentOnlineState != AFNetworkReachabilityStatusUnknown)
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkChangeStateNotification
                                                                object:@(reachable)];
        
        currentOnlineState = status;
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    return true;
}

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance lazyLoad];
    });
    return sharedInstance;
}

- (BOOL)isOnline {
    return currentOnlineState == AFNetworkReachabilityStatusUnknown || [[AFNetworkReachabilityManager sharedManager] isReachable];
}

- (BOOL)requestGetToUrlString:(NSString *)urlString
                       params:(NSDictionary *)params
                    useDomain:(BOOL)useDomain
                      handler:(SAAPIHandler)handler {
    return [self requestToUrlString:urlString params:params handler:handler method:@"GET" useDomain:useDomain];
}

- (BOOL)requestPostToUrlString:(NSString *)urlString
                        params:(NSDictionary *)params
                     useDomain:(BOOL)useDomain
                       handler:(SAAPIHandler)handler {
    return [self requestToUrlString:urlString params:params handler:handler method:@"POST" useDomain:useDomain];
}

- (void)clearCookies {
    NSHTTPCookieStorage* storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    for (NSHTTPCookie *each in storage.cookies) {
        [storage deleteCookie:each];
    }
}

#pragma mark - private

- (BOOL)requestToUrlString:(NSString *)urlString
                    params:(NSDictionary *)params
                   handler:(SAAPIHandler)handler
                    method:(NSString *)method
                 useDomain:(BOOL)useDomain {
    
    NSString *url = useDomain ? [NSString stringWithFormat:@"%@/%@", kDomain, urlString] : urlString;
    
    NSError *err;
    NSMutableURLRequest *req = [httpSession.requestSerializer requestWithMethod:method
                                                                      URLString:url
                                                                     parameters:params
                                                                          error:&err];

    if( params )
        NSLog(@"%@", params);

    if (err) {
        return FALSE;
    }
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:req
                                                completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                                    if (error) {
                                                        if (response) {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            if (httpResponse.statusCode == 401) {
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:kUnauthorizedNotification
                                                                                                                    object:nil];
                                                            }
                                                        }

                                                        CLS_LOG(@"Error: %@", error);
                                                    } else {
                                                        CLS_LOG(@"%@ %@", response, responseObject);
                                                    }

                                                    if (handler)
                                                        handler(response, responseObject, error);
                                                }];
    
    if (!dataTask)
        return FALSE;
    
    [dataTask resume];
    
    return TRUE;
}

@end

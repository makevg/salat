//
//  STKWebKitViewController.h
//  STKWebKitViewController
//
//  Created by Marc on 03.09.14.
//  Copyright (c) 2014 sticksen. All rights reserved.
//
#import <WebKit/WebKit.h>
@class STKWebKitViewController;

@protocol STKWebKitModalViewControllerDelegate<NSObject>

@required
- (void)didPressedCloseButton;

@end

@interface STKWebKitModalViewController : UINavigationController

@property (nonatomic, weak) id<STKWebKitModalViewControllerDelegate> closeDelegate;

- (instancetype)initWithURL:(NSURL *)url;
- (instancetype)initWithURL:(NSURL *)url userScript:(WKUserScript *)script;

- (instancetype)initWithAddress:(NSString *)urlString;
- (instancetype)initWithAddress:(NSString *)string userScript:(WKUserScript *)script;

- (instancetype)initWithRequest:(NSURLRequest *)request;
- (instancetype)initWithRequest:(NSURLRequest *)request userScript:(WKUserScript *)script NS_DESIGNATED_INITIALIZER;

@property (nonatomic, readonly) STKWebKitViewController *webKitViewController;

@end

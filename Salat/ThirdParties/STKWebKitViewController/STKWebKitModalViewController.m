//
//  STKWebKitViewController.m
//  STKWebKitViewController
//
//  Created by Marc on 03.09.14.
//  Copyright (c) 2014 sticksen. All rights reserved.
//

#import "STKWebKitModalViewController.h"
#import "STKWebKitViewController.h"

NSString *const cCloseVCImageName = @"CloseWhite";

@interface STKWebKitModalViewController ()


@end

@implementation STKWebKitModalViewController

- (instancetype)init {
    return [self initWithRequest:nil];
}

- (instancetype)initWithAddress:(NSString *)urlString {
    return [self initWithURL:[NSURL URLWithString:urlString]];
}

- (instancetype)initWithURL:(NSURL *)url {
    return [self initWithURL:url userScript:nil];
}

- (instancetype)initWithAddress:(NSString *)string userScript:(WKUserScript *)script {
    return [self initWithURL:[NSURL URLWithString:string] userScript:script];
}

- (instancetype)initWithURL:(NSURL *)url userScript:(WKUserScript *)script {
    return [self initWithRequest:[NSURLRequest requestWithURL:url] userScript:script];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:cCloseVCImageName]
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(closeController)];

    [self.webKitViewController.navigationItem setRightBarButtonItem:clearButton animated:YES];
}

- (instancetype)initWithRequest:(NSURLRequest *)request {
    return [self initWithRequest:request userScript:nil];
}

- (instancetype)initWithRequest:(NSURLRequest *)request userScript:(WKUserScript *)script {
    _webKitViewController = [[STKWebKitViewController alloc] initWithRequest:request userScript:script];
    if (self = [super initWithRootViewController:self.webKitViewController]) {
    }
    return self;
}

- (void)closeController {
    [self dismissViewControllerAnimated:YES completion:^{
        if( self.closeDelegate )
            [self.closeDelegate didPressedCloseButton];
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end

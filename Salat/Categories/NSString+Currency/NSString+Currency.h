//
//  NSString+Currency.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Currency)

+ (NSString *)priceWithCurrencySymbol:(NSNumber *)price;
+ (NSNumber *)priceFromString:(NSString *)string;

@end

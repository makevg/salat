//
//  NSString+Currency.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "NSString+Currency.h"

@implementation NSString (Currency)

#pragma mark - Public

+ (NSString *)priceWithCurrencySymbol:(NSNumber *)price {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setCurrencyCode:@"RUB"];
    [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    [numberFormatter setMaximumFractionDigits:0];
    return [numberFormatter stringFromNumber:price];
}

+ (NSNumber *)priceFromString:(NSString *)string {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setCurrencyCode:@"RUB"];
    [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru"]];
    [numberFormatter setMaximumFractionDigits:0];
    return [numberFormatter numberFromString:string];
}

@end

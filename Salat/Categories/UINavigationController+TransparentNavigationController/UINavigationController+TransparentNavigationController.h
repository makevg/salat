//
//  UINavigationController+TransparentNavigationController.h
//  Salat
//
//  Created by mtx on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (TransparentNavigationController)

- (void)presentTransparentNavigationBar;
- (void)hideTransparentNavigationBar;

@end

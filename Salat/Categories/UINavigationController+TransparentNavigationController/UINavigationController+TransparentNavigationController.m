//
//  UINavigationController+TransparentNavigationController.m
//  Salat
//
//  Created by mtx on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "UINavigationController+TransparentNavigationController.h"
#import "SAStyle.h"

@implementation UINavigationController (TransparentNavigationController)

- (void)presentTransparentNavigationBar {
    [UIView beginAnimations:@"naviHide" context:nil];
    [self.navigationBar setTranslucent:YES];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [UIView commitAnimations];
}

- (void)hideTransparentNavigationBar {
    [UIView beginAnimations:@"naviHide" context:nil];
    [self.navigationBar setTranslucent:NO];
    [UIView commitAnimations];
}


@end

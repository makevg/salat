//
//  UIImageView+AsyncLoad.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "UIImageView+AsyncLoad.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UIImageView (AsyncLoad)

- (void)loadAsyncFromUrl:(NSString *)photoUrl {
    [self loadAsyncFromUrl:photoUrl placeHolder:@"noimage"];
}

- (void)loadAsyncFromUrlNoPlace:(NSString *)photoUrl {
    [self sd_setImageWithURL:[NSURL URLWithString:photoUrl]
            placeholderImage:nil
                     options:(SDWebImageOptions) (SDWebImageLowPriority | SDWebImageRetryFailed)
                    progress:nil
                   completed:^(UIImage *image_, NSError *error, SDImageCacheType cacheType_, NSURL *url) {
                       if (!image_)
                           return;
                   }];

}

- (void)loadAsyncFromUrl:(NSString *)photoUrl placeHolder:(NSString *)placeholder {
    [self sd_setImageWithURL:[NSURL URLWithString:photoUrl]
            placeholderImage:[UIImage imageNamed:placeholder]
                     options:(SDWebImageOptions) (SDWebImageLowPriority | SDWebImageRetryFailed)
                    progress:nil
                   completed:^(UIImage *image_, NSError *error, SDImageCacheType cacheType_, NSURL *url) {
                       if (!image_)
                           return;
                   }];
}

@end

//
//  UIImageView+AsyncLoad.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AsyncLoad)

- (void)loadAsyncFromUrl:(NSString *)photoUrl;

- (void)loadAsyncFromUrlNoPlace:(NSString *)photoUrl;

- (void)loadAsyncFromUrl:(NSString *)photoUrl placeHolder:(NSString *)placeholder;

@end

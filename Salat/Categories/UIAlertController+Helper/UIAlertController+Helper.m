//
// Created by mtx on 05.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "UIAlertController+Helper.h"

@implementation UIAlertController (Helper)

+ (UIAlertController *)alertWithTitle:(NSString *)title
                              message:(NSString *)message
                              handler:(void(^)(UIAlertAction *action))handler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:handler];
    [alertController addAction:action];
    return alertController;
}

@end
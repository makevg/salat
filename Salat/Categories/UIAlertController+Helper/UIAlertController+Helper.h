//
// Created by mtx on 05.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Helper)

+ (UIAlertController *)alertWithTitle:(NSString *)title
                              message:(NSString *)message
                              handler:(void(^)(UIAlertAction *action))handler;

@end
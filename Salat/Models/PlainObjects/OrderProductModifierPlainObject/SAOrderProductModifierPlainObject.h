//
// Created by Maximychev Evgeny on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAOrderProductModifierPlainObject : NSObject

@property (nonatomic) NSNumber *modifierId;
@property (nonatomic) NSNumber *quantity;
@property (nonatomic) NSNumber *price;
@property (nonatomic) NSNumber *sum;
@property (nonatomic) NSNumber *productId;
@property (nonatomic) NSNumber *modifierCategoryId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descr;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSString *previewUrl;

@end
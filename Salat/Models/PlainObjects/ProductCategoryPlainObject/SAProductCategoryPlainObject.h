//
//  SAProductCategoryPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAProductCategoryPlainObject : NSObject

@property (nonatomic) NSNumber *objectId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *backgroundImageUrl;
@property (nonatomic) NSString *icon;
@property (nonatomic) NSNumber *parentId;
@property (nonatomic) NSNumber *isWok;

@end

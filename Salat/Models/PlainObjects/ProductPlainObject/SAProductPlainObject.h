//
//  SAProductPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductModifierPlainObject;

@interface SAProductPlainObject : NSObject

@property (nonatomic) NSNumber *objectId;
@property (nonatomic) NSNumber *categoryId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descr;
@property (nonatomic) NSString *url;
@property (nonatomic) NSNumber *price;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSString *previewUrl;
@property (nonatomic) NSString *previewSmallUrl;
@property (nonatomic) NSNumber *quantity;
@property (nonatomic) NSArray<SAProductModifierPlainObject *> * modifiers;

@end

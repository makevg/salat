//
//  SAOrderPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAOrderProductPlainObject;

@interface SAOrderPlainObject : NSObject

@property (nonatomic) NSNumber *orderId;
@property (nonatomic) NSNumber *addressCityId;
@property (nonatomic) NSString *addressEntrance;
@property (nonatomic) NSString *addressFlat;
@property (nonatomic) NSNumber *addressFloor;
@property (nonatomic) NSString *addressHouse;
@property (nonatomic) NSString *addressIntercomCode;
@property (nonatomic) NSString *addressStreet;
@property (nonatomic) NSNumber *cartSum;
@property (nonatomic) NSNumber *createdAt;
@property (nonatomic) NSNumber *deliverySum;
@property (nonatomic) NSNumber *deliveryTime;
@property (nonatomic) NSString *guid;
@property (nonatomic) NSNumber *price;
@property (nonatomic) NSNumber *status;
@property (nonatomic) NSNumber *totalSum;
@property (nonatomic) NSNumber *updatedAt;
@property (nonatomic) NSString *discountTitle;
@property (nonatomic) NSNumber *discountPercent;
@property (nonatomic) NSNumber *discountSum;
@property (nonatomic) NSArray<SAOrderProductPlainObject *> *products;

@end

//
//  SAOrderPlainObject.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderPlainObject.h"
#import "SACartItemPlainObject.h"
#import "SAOrderProductPlainObject.h"
#import "SACheckoutPlainObject.h"

@implementation SAOrderPlainObject

@end

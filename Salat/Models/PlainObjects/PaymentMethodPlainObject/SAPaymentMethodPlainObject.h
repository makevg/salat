//
//  SAPaymentMethodPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAPaymentMethodPlainObject : NSObject

@property (nonatomic) NSNumber *objectId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descr;

@end

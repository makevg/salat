//
//  SAReviewsListRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAReviewsListRouter : NSObject <SAReviewsListRouterInput>

@property (weak, nonatomic) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

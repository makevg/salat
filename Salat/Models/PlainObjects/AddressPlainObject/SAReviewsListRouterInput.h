//
//  SAReviewsListRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAReviewsListRouterInput <NSObject>

- (void)openWriteNewReviewModule;
- (void)openSignInModule;

@end

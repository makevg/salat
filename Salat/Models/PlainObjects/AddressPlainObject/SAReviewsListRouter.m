//
//  SAReviewsListRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListRouter.h"

static NSString *const kOpenNewReviewInReviewsSegue = @"openNewReviewInReviewsSegue";
static NSString *const kOpenSignInModuleInReviewsSegue = @"openSignInModuleInReviewsSegue";

@implementation SAReviewsListRouter

#pragma mark - SAReviewsListRouterInput

- (void)openWriteNewReviewModule {
    [self.transitionHandler openModuleUsingSegue:kOpenNewReviewInReviewsSegue];
}

- (void)openSignInModule {
    [self.transitionHandler openModuleUsingSegue:kOpenSignInModuleInReviewsSegue];
}

@end

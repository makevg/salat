//
//  SAAddressPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SAAddressPlainObject : NSObject

@property(nonatomic) NSNumber *addressId;
@property(nonatomic) NSString *city;
@property(nonatomic) NSString *street;
@property(nonatomic) NSString *house;
@property(nonatomic) NSNumber *cityId;
@property(nonatomic) NSNumber *createdAt;
@property(nonatomic) NSString *entrance;
@property(nonatomic) NSString *flat;
@property(nonatomic) NSString *floor;
@property(nonatomic) NSString *intercomCode;
@property(nonatomic) NSNumber *isMain;
@property(nonatomic) NSString *title;
@property(nonatomic) CLLocation *location;
@property(nonatomic) NSNumber *deliverySum;

@end

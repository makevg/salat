//
//  SASalePlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SASalePlainObject : NSObject

@property (nonatomic) NSNumber *saleId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *content;
@property (nonatomic) NSNumber *publishedAt;
@property (nonatomic) NSNumber *isPermanent;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *imageUrl;

@end

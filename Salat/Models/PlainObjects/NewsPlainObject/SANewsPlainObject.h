//
//  SANewsPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SANewsPlainObject : NSObject

@property (nonatomic) NSNumber *newsId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSNumber *publishedAt;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSString *content;

@end

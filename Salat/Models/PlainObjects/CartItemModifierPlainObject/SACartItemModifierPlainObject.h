//
// Created by Maximychev Evgeny on 08.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SACartItemModifierPlainObject : NSObject

@property (nonatomic) NSNumber *itemModifierId;
@property (nonatomic) NSNumber *quantity;
@property (nonatomic) NSNumber *price;
@property (nonatomic) NSString *title;

@end
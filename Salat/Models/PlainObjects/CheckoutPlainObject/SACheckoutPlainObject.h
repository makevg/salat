//
// Created by mtx on 07.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAddressPlainObject;
@class SACartItemPlainObject;

@interface SACheckoutPlainObject : NSObject

@property(nonatomic, strong) NSArray<SACartItemPlainObject *> *products;
@property(nonatomic, strong) SAAddressPlainObject *address;
@property(nonatomic, assign) Boolean isOnlinePayment;
@property(nonatomic, copy) NSString *deliverToTime;
@property(nonatomic, copy) NSString *token;
@property(nonatomic, copy) NSString *name;

@end
//
// Created by mtx on 24.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SACartCalculationItemModifierPO : NSObject

@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSNumber *price;

@end

@interface SACartCalculationItemPO : NSObject

@property(nonatomic, strong) NSArray <SACartCalculationItemModifierPO *> *modifiers;
@property(nonatomic, strong) NSNumber *id;
@property(nonatomic, strong) NSNumber *price;

@end

@interface SACartCalculationDiscountPO : NSObject

@property(nonatomic, strong) NSNumber *sum;
@property(nonatomic, strong) NSNumber *percent;
@property(nonatomic, copy) NSString *descr;

@end

@interface SACartCalculationDeliveryPO : NSObject

@property (nonatomic, strong) NSNumber *sum;

@end

@interface SACartCalculationSuggestionPO : NSObject

@property(nonatomic, strong) NSNumber *productId;

@end

@interface SACartCalculationPO : NSObject

@property(nonatomic, strong) NSArray<SACartCalculationItemPO *> *items;

@property(nonatomic, strong) SACartCalculationDiscountPO *discountInfo;

@property(nonatomic, strong) SACartCalculationDeliveryPO *deliveryInfo;

@property(nonatomic, strong) SACartCalculationSuggestionPO *suggestionProduct;

@end
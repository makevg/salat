//
//  SARestaurantPlainObject.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARestaurantPlainObject.h"
#import "SADeliveryTimePlainObject.h"
#import "SADeliveryAreaPlainObject.h"
#import "SAPaymentMethodPlainObject.h"

@implementation SARestaurantPlainObject

@end

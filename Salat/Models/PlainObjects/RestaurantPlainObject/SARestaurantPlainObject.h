//
//  SARestaurantPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SADeliveryTimePlainObject;
@class SADeliveryAreaPlainObject;
@class SAPaymentMethodPlainObject;

@interface SARestaurantPlainObject : NSObject

@property (nonatomic) NSNumber *objectId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descr;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *city;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *logo;
@property (nonatomic) NSArray<SADeliveryTimePlainObject *> *deliveryTimes;
@property (nonatomic) NSArray<SADeliveryAreaPlainObject *> *deliveryAreas;
@property (nonatomic) NSArray<SAPaymentMethodPlainObject *> *payMethods;

@end

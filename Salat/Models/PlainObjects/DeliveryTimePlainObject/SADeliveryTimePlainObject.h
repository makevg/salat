//
//  SADeliveryTimePlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SADeliveryTimePlainObject : NSObject

@property (nonatomic) NSNumber *objectId;
@property (nonatomic) NSNumber *dayNumber;
@property (nonatomic) NSNumber *timeStart;
@property (nonatomic) NSNumber *timeEnd;
@property (nonatomic) NSNumber *workDate;
@property (nonatomic) BOOL isOpened;

@end

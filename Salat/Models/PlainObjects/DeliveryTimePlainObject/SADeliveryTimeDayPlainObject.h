//
// Created by mtx on 17.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SADeliveryTimeDayPlainObject : NSObject

@property(nonatomic, strong) NSString *workTimes;
@property(nonatomic, strong) NSNumber *dow;

- (instancetype)initWithWorkTimes:(NSString *)workTimes dow:(NSNumber *)dow;

+ (instancetype)objectWithWorkTimes:(NSString *)workTimes dow:(NSNumber *)dow;


@end
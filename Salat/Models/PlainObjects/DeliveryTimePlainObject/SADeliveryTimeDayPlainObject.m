//
// Created by mtx on 17.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SADeliveryTimeDayPlainObject.h"

@implementation SADeliveryTimeDayPlainObject

- (instancetype)initWithWorkTimes:(NSString *)workTimes dow:(NSNumber *)dow {
    self = [super init];
    if (self) {
        self.workTimes = workTimes;
        self.dow = dow;
    }

    return self;
}

+ (instancetype)objectWithWorkTimes:(NSString *)workTimes dow:(NSNumber *)dow {
    return [[self alloc] initWithWorkTimes:workTimes dow:dow];
}


@end
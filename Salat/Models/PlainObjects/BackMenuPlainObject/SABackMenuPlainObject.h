//
//  SABackMenuPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SABackMenuPlainObject : NSObject

@property (nonatomic) NSString *icon;
@property (nonatomic) NSString *title;

@end

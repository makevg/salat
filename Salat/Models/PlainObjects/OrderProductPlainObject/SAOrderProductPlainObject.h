//
// Created by Maximychev Evgeny on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAOrderProductModifierPlainObject;

@interface SAOrderProductPlainObject : NSObject

@property (nonatomic) NSNumber *productId;
@property (nonatomic) NSNumber *quantity;
@property (nonatomic) NSNumber *price;
@property (nonatomic) NSNumber *sum;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *descr;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSArray<SAOrderProductModifierPlainObject *> *modifiers;

@end
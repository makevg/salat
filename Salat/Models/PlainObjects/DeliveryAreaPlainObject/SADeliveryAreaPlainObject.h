//
//  SADeliveryAreaPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SADeliveryAreaPlainObject : NSObject

@property (nonatomic) NSNumber *objectId;
@property (nonatomic) NSString *title;
@property (nonatomic) NSNumber *price;
@property (nonatomic) NSString *polygon;

@end

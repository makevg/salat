//
//  SAReviewPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAReviewPlainObject : NSObject

@property (nonatomic) NSNumber *reviewId;
@property (nonatomic) NSString *userName;
@property (nonatomic) NSNumber *publishedAt;
@property (nonatomic) NSNumber *rate;
@property (nonatomic) NSString *descr;
@property (nonatomic) NSNumber *isMy;

@end

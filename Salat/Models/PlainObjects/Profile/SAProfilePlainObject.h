//
// Created by Максимычев Е.О. on 27.06.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAddressPlainObject;

@interface SAProfilePlainObject : NSObject

@property (nonatomic) NSNumber *profileId;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *phoneNumber;
@property (nonatomic) NSString *vkName;
@property (nonatomic) NSString *fbName;
@property (nonatomic) NSArray<SAAddressPlainObject *> *addresses;

@end
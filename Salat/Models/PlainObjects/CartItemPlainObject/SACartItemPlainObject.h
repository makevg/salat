//
//  SACartItemPlainObject.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SACartItemModifierPlainObject;

@interface SACartItemPlainObject : NSObject

@property(nonatomic) NSNumber *itemId;
@property(nonatomic) NSNumber *productId;
@property(nonatomic) NSString *productTitle;
@property(nonatomic) NSString *productDescr;
@property(nonatomic) NSString *productImageUrl;
@property(nonatomic) NSNumber *cost;
@property(nonatomic) NSNumber *quantity;
@property(nonatomic) NSArray<SACartItemModifierPlainObject *> *modifiers;

@end

//
//  AppDelegate.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "AppDelegate.h"
#import "SAStyle.h"
#import "SADB.h"
#import "SAAPI.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <VK-ios-sdk/VKSdk.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <YandexMapKit/YandexMapKit.h>

static NSString *const kYandexMapsApiKey = @"uR7VUpNn5VJjrPwFVJoYDEb~WcDoQg~ZEEyNTdOA9q00yonse6xkFXZDIYzUvTciv81LgXNAg~HqmVcSJLP-UltvjYzNI5V3jPP-b-yW8ds=";

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];
    [YMKConfiguration sharedInstance].apiKey = kYandexMapsApiKey;

    self.window.backgroundColor = [SAStyle backgroundColor];
    [self configureNavBar];

    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    return YES;
}

- (void)configureNavBar {
    [[UINavigationBar appearance] setBarTintColor:[SAStyle backgroundColor]];
    [[UINavigationBar appearance] setTintColor:[SAStyle whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [SAStyle lightGreenColor],
            NSFontAttributeName : [SAStyle regularFontOfSize:17.f]}];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setTranslucent:NO];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL res = [VKSdk processOpenURL:url fromApplication:sourceApplication];

    if (!res) {
        res = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                             openURL:url
                                                   sourceApplication:sourceApplication
                                                          annotation:annotation];

    }

    return res;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [SALAT_DB saveContext];
}

@end

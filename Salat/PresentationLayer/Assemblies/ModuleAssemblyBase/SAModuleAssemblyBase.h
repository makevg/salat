//
//  SAModuleAssemblyBase.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import "SAServiceComponents.h"
#import "SACoreComponents.h"
#import "SACartButtonComponents.h"

@interface SAModuleAssemblyBase : TyphoonAssembly

@property (nonatomic, readonly) TyphoonAssembly <SAServiceComponents> *serviceComponents;
@property (nonatomic, readonly) TyphoonAssembly <SACoreComponents> *coreComponents;
@property (nonatomic, readonly) TyphoonAssembly <SACartButtonComponents> *cartButtonComponents;

@end

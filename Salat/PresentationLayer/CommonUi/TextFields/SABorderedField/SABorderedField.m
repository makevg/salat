//
//  SABorderedField.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABorderedField.h"
#import "SAStyle.h"

IB_DESIGNABLE
@interface SABorderedField()

@property (nonatomic, weak) IBInspectable UIColor* borderColor;

@end

@implementation SABorderedField {
    CALayer *bottomBorder;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configure];
}

#pragma mark - Configure

- (void)configure {
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self setBottomBorder];
}

- (void)setBottomBorder {
    bottomBorder = [CALayer layer];
    CGFloat borderWidth = 1.0f;

    bottomBorder.borderColor = [[SAStyle lightGrayColor] CGColor];
    bottomBorder.frame = CGRectMake(0, self.frame.size.height - borderWidth,
                                    self.frame.size.width,
                                    self.frame.size.height);
    bottomBorder.borderWidth = borderWidth;
    
    [self.layer addSublayer:bottomBorder];
    self.layer.masksToBounds = YES;
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    bottomBorder.borderColor = _borderColor.CGColor;
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];

    bottomBorder.hidden = !enabled;
}

- (void)prepareForInterfaceBuilder {
    [self setBottomBorder];
}

@end

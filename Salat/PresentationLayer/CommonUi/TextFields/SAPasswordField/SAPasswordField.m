//
//  SAPasswordField.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAPasswordField.h"

@implementation SAPasswordField {
    BOOL show;
    UIButton *rightButton;
}

#pragma mark - Configure 

- (void)configure {
    [super configure];
    
    show = NO;
    rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [self rightButtonImageByState:show];
    rightButton.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 10);
    [rightButton addTarget:self
                    action:@selector(showPassword)
          forControlEvents:UIControlEventTouchUpInside];
    self.rightView = rightButton;
    self.rightViewMode = UITextFieldViewModeWhileEditing;
}

- (void)showPassword {
    self.secureTextEntry = show;
    show = !show;
    [self rightButtonImageByState:show];
}

- (void)rightButtonImageByState:(BOOL)state {
    UIImage *image = state ? [UIImage imageNamed:@"show_pswd"] : [UIImage imageNamed:@"hide_pswd"];
    [rightButton setImage:image forState:UIControlStateNormal];
}

@end

//
//  SAAuthField.h
//  Salat
//
//  Created by Maximychev Evgeny on 03.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SHSPhoneComponent/SHSPhoneTextField.h>

@interface SAAuthField : SHSPhoneTextField

- (void)configure;

@end

//
//  SAAuthField.m
//  Salat
//
//  Created by Maximychev Evgeny on 03.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAuthField.h"
#import "SAStyle.h"

@implementation SAAuthField

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configure];
}

#pragma mark - Configure

- (void)configure {
    self.backgroundColor = [UIColor clearColor];
    self.textColor = [SAStyle whiteColor];
    [self setValue:[SAStyle lightGreenColor]
        forKeyPath:@"_placeholderLabel.textColor"];
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self setBottomBorder];
    [self.formatter setDefaultOutputPattern:@"(###) ###-##-##"];
    [self.formatter setPrefix:@"+7 "];
}

- (void)setBottomBorder {
    CALayer *bottomBorder = [CALayer layer];
    CGFloat borderWidth = 1.0f;
    bottomBorder.borderColor = [[SAStyle whiteColor] CGColor];
    bottomBorder.frame = CGRectMake(0, self.frame.size.height - borderWidth,
                                    self.frame.size.width,
                                    self.frame.size.height);
    bottomBorder.borderWidth = borderWidth;
    
    [self.layer addSublayer:bottomBorder];
    self.layer.masksToBounds = YES;
}

@end

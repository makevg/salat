//
// Created by mtx on 09.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGSwipeTableCell.h"


@interface SABaseSwipebleCell : MGSwipeTableCell

+ (NSString *)cellIdentifier;
+ (CGFloat)cellHeight;

- (void)setModel:(id)model;
- (void)configureCell;

@end
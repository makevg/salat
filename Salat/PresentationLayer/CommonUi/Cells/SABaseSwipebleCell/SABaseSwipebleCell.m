//
// Created by mtx on 09.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SABaseSwipebleCell.h"


@implementation SABaseSwipebleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configureCell];
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return NSStringFromClass([self class]);
}

+ (CGFloat)cellHeight {
    return 44.f;
}

- (void)setModel:(id)model {
    // Abstract method.
}

- (void)configureCell {
    // Abstract method.
}

@end

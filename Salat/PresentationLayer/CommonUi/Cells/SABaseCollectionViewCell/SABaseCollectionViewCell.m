//
//  SABaseCollectionViewCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseCollectionViewCell.h"

@implementation SABaseCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configureCell];
}

#pragma mark - Public

+ (NSString *)cellIdentifier {
    return NSStringFromClass([self class]);
}

- (void)setModel:(id)model {
    // Abstract method.
}

- (void)configureCell {
    // Abstract method.
}

@end

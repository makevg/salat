//
//  SAAddToCartView.m
//  Salat
//
//  Created by Maximychev Evgeny on 18.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddToCartView.h"
#import "SABaseButton.h"

@interface SAAddToCartView ()
@property(strong, nonatomic) IBOutlet UIView *mainView;
@property(weak, nonatomic) IBOutlet UIView *buttonsView;
@property(weak, nonatomic) IBOutlet UILabel *counterLabel;
@property(weak, nonatomic) IBOutlet UIView *buttonView;
@property(weak, nonatomic) IBOutlet SABaseButton *addToCartButton;
@end

@implementation SAAddToCartView {
    NSUInteger counter;
}

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

#pragma mark - Public

- (void)updateWithQuantity:(NSNumber *)quantity {
    counter = [quantity unsignedIntegerValue];
    counter > 0 ? [self setNormalState] : [self setAddToCartState];
    [self updateCounterLabel];
}

#pragma mark - Private

- (void)commonInit {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.mainView];
    [self configureView];
}

- (void)configureView {
    counter = 0;
    [self prepareConstraints];
    [self prepareSubviews];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"view" : self.mainView}]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:@{@"view" : self.mainView}]];
}

- (void)prepareSubviews {
    [self.addToCartButton setTitleColor:[SAStyle backgroundColor] forState:UIControlStateNormal];
    self.buttonsView.backgroundColor = [SAStyle backgroundColor];
    self.counterLabel.font = [SAStyle boldFontOfSize:17.f];
}

- (void)updateCounterLabel {
    self.counterLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long) counter];
}

- (void)setNormalState {
    self.buttonView.hidden = YES;
    self.buttonsView.hidden = NO;
}

- (void)setAddToCartState {
    self.buttonView.hidden = NO;
    self.buttonsView.hidden = YES;
}

#pragma mark - Actions

- (IBAction)tappedAddToCartButton:(id)sender {
    counter++;
    [self updateCounterLabel];
    [self setNormalState];
    if (self.delegate && [self.delegate respondsToSelector:@selector(addToCartViewDidTapPlus:)]) {
        [self.delegate addToCartViewDidTapPlus:self];
    }
}

- (IBAction)tappedRemoveButton:(id)sender {
    if (counter > 1) {
        counter--;
        [self updateCounterLabel];
    } else {
        counter = 0;
        [self setAddToCartState];
    }

    if ([self.delegate respondsToSelector:@selector(addToCartViewDidTapMinus:)]) {
        [self.delegate addToCartViewDidTapMinus:self];
    }
}

- (IBAction)tappedAddButton:(id)sender {
    counter++;
    [self updateCounterLabel];
    if ([self.delegate respondsToSelector:@selector(addToCartViewDidTapPlus:)]) {
        [self.delegate addToCartViewDidTapPlus:self];
    }
}

@end

//
//  SAAddToCartView.h
//  Salat
//
//  Created by Maximychev Evgeny on 18.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@protocol SAAddToCartViewDelegate;

@interface SAAddToCartView : SABaseView

@property(weak, nonatomic) id <SAAddToCartViewDelegate> delegate;

- (void)updateWithQuantity:(NSNumber *)quantity;

@end

@protocol SAAddToCartViewDelegate <NSObject>

@required

- (void)addToCartViewDidTapPlus:(SAAddToCartView *)view;

- (void)addToCartViewDidTapMinus:(SAAddToCartView *)view;

@end
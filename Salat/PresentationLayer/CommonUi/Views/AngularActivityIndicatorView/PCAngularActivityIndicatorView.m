//
//  PCAngularActivityIndicatorView.m
//
//  Copyright (c) 2014 Phillip Caudell phillipcaudell@gmail.com
//
//  The MIT License
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "PCAngularActivityIndicatorView.h"

@interface PCAngularActivityIndicatorView ()

@property(nonatomic, strong) CAShapeLayer *shapeLayer;
@property(nonatomic, strong) UIView *contentView;

@end

@implementation PCAngularActivityIndicatorView

- (id)init {
    return [self initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleDefault];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        CGFloat lineWidth = 4.0;
        CGFloat duration = 0.8;

        [self commonInit:self.frame
               lineWidth:lineWidth
                duration:duration];
    }
    return self;
}

- (id)initWithActivityIndicatorStyle:(PCAngularActivityIndicatorViewStyle)style {
    CGRect frame;
    CGFloat lineWidth;
    CGFloat duration;

    switch (style) {
        case PCAngularActivityIndicatorViewStyleSmall:
            frame = CGRectMake(0, 0, 20, 20);
            lineWidth = 2.0;
            duration = 0.8;
            break;
        default:
        case PCAngularActivityIndicatorViewStyleDefault:
            frame = CGRectMake(0, 0, 30, 30);
            lineWidth = 4.0;
            duration = 0.8;
            break;
        case PCAngularActivityIndicatorViewStyleLarge:
            frame = CGRectMake(0, 0, 60, 60);
            lineWidth = 8.0;
            duration = 1.0;
            break;
    }

    if (self = [super initWithFrame:frame]) {
        [self commonInit:frame
               lineWidth:lineWidth
                duration:duration];
    }

    return self;
}

#pragma mark - Private

- (void)commonInit:(CGRect)frame lineWidth:(CGFloat)lineWidth duration:(CGFloat)duration {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentView = [[UIView alloc] initWithFrame:self.bounds];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.contentView];
    [self addConstraints:@[
            [NSLayoutConstraint constraintWithItem:self.contentView
                                         attribute:NSLayoutAttributeLeft
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:self
                                         attribute:NSLayoutAttributeLeft
                                        multiplier:1
                                          constant:0],
            [NSLayoutConstraint constraintWithItem:self.contentView
                                         attribute:NSLayoutAttributeRight
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:self
                                         attribute:NSLayoutAttributeRight
                                        multiplier:1
                                          constant:0],
            [NSLayoutConstraint constraintWithItem:self.contentView
                                         attribute:NSLayoutAttributeTop
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:self
                                         attribute:NSLayoutAttributeTop
                                        multiplier:1
                                          constant:0],
            [NSLayoutConstraint constraintWithItem:self.contentView
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:self
                                         attribute:NSLayoutAttributeBottom
                                        multiplier:1
                                          constant:0],
            [NSLayoutConstraint constraintWithItem:self
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:nil
                                         attribute:NSLayoutAttributeNotAnAttribute
                                        multiplier:1
                                          constant:frame.size.width],
            [NSLayoutConstraint constraintWithItem:self
                                         attribute:NSLayoutAttributeHeight
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:nil
                                         attribute:NSLayoutAttributeNotAnAttribute
                                        multiplier:1
                                          constant:frame.size.height]
    ]];

    CGFloat radius = frame.size.width / 2;

    self.shapeLayer = [CAShapeLayer layer];
    self.shapeLayer.frame = self.bounds;
    self.shapeLayer.lineWidth = lineWidth;
    self.shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    self.shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, (CGFloat) (2.0 * radius), (CGFloat) (2.0 * radius))
                                                      cornerRadius:radius].CGPath;
    self.shapeLayer.lineCap = kCALineJoinRound;
    self.shapeLayer.hidden = YES;
    [self.contentView.layer insertSublayer:self.shapeLayer atIndex:0];

    // Defaults
    self.hidesWhenStopped = YES;
    self.duration = duration;
    self.color = [UIColor lightGrayColor];
    self.backgroundColor = [UIColor clearColor];
}

- (void)startAnimating {
    if (self.isAnimating) {
        return;
    }

    _animating = YES;

    CAKeyframeAnimation *inAnimation = [CAKeyframeAnimation animationWithKeyPath:@"strokeEnd"];
    inAnimation.duration = self.duration;
    inAnimation.values = @[@(0), @(1)];

    CAKeyframeAnimation *outAnimation = [CAKeyframeAnimation animationWithKeyPath:@"strokeStart"];
    outAnimation.duration = self.duration;
    outAnimation.values = @[@(0), @(0.8), @(1)];
    outAnimation.beginTime = self.duration / 1.5;

    CAAnimationGroup *groupAnimation = [CAAnimationGroup animation];
    groupAnimation.animations = @[inAnimation, outAnimation];
    groupAnimation.duration = self.duration + outAnimation.beginTime;
    groupAnimation.repeatCount = INFINITY;
    groupAnimation.removedOnCompletion = NO;
    groupAnimation.delegate = self;

    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fromValue = @(0);
    rotationAnimation.toValue = @(M_PI * 2);
    rotationAnimation.duration = self.duration * 1.5;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.repeatCount = INFINITY;

    [self.shapeLayer addAnimation:rotationAnimation forKey:nil];
    [self.shapeLayer addAnimation:groupAnimation forKey:nil];

    self.shapeLayer.hidden = NO;
    self.hidden = NO;
}

- (void)stopAnimating {
    _animating = NO;

    self.shapeLayer.hidden = self.hidesWhenStopped;

    [UIView animateWithDuration:0.5 animations:^{

        // Nice fade and ride
        self.contentView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.contentView.alpha = 0.0;

    }                completion:^(BOOL finished) {
        /// ...and reset back
        self.contentView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.contentView.alpha = 1.0;

        [self.shapeLayer removeAllAnimations];
    }];
}

- (void)setColor:(UIColor *)color {
    [self willChangeValueForKey:@"color"];

    _color = color;
    self.shapeLayer.strokeColor = [color CGColor];

    [self didChangeValueForKey:@"color"];
}

@end

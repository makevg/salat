//
//  TSMessageView.m
//  Felix Krause
//
//  Created by Felix Krause on 24.08.12.
//  Copyright (c) 2012 Felix Krause. All rights reserved.
//

#import "TSMessageView.h"
#import "SAStyle.h"

#define TSMessageViewMinimumPadding 15.0

#define TSDesignFileName @"TSMessagesDefaultDesign"

static NSMutableDictionary *_notificationDesign;

@interface TSMessage (TSMessageView)
- (void)fadeOutNotification:(TSMessageView *)currentView; // private method of TSMessage, but called by TSMessageView in -[fadeMeOut]
@end

@interface TSMessageView () <UIGestureRecognizerDelegate>

/** The displayed title of this message */
@property(nonatomic, strong) NSString *title;

/** The view controller this message is displayed in */
@property(nonatomic, strong) UIViewController *viewController;


/** Internal properties needed to resize the view on device rotation properly */
@property(nonatomic, strong) UILabel *titleLabel;

@property(copy) void (^callback)();

- (CGFloat)updateHeightOfMessageView;

- (void)layoutSubviews;

@end

@implementation TSMessageView {
    TSMessageNotificationType notificationType;
}
- (id)initWithTitle:(NSString *)title
         titleColor:(UIColor *)titleColor
          titleFont:(UIFont *)titleFont
               type:(TSMessageNotificationType)aNotificationType
           duration:(CGFloat)duration
   inViewController:(UIViewController *)viewController
         atPosition:(TSMessageNotificationPosition)position {
    self = [super init];
    if (self) {
        _title = title;
        _duration = duration;
        _viewController = viewController;
        _messagePosition = position;

        CGFloat screenWidth = self.viewController.view.bounds.size.width;
        notificationType = aNotificationType;
        switch (notificationType) {
            case TSMessageNotificationTypeMessage:
                [self setBackgroundColor:[SAStyle normalMessageColor]];
                break;
            case TSMessageNotificationTypeError:
                [self setBackgroundColor:[SAStyle errorMessageColor]];
                break;
            case TSMessageNotificationTypeSuccess:
                [self setBackgroundColor:[SAStyle orangeColor]];
                break;
            case TSMessageNotificationTypeWarning:
                [self setBackgroundColor:[SAStyle orangeColor]];
                break;
            default:
                break;
        }

        // Set up title label
        [self configureTitleLabelWith:title
                           titleColor:titleColor
                            titleFont:titleFont
                                width:screenWidth];

        CGFloat actualHeight = [self updateHeightOfMessageView]; // this call also takes care of positioning the labels
        CGFloat topPosition = -actualHeight;

        if (self.messagePosition == TSMessageNotificationPositionBottom) {
            topPosition = self.viewController.view.bounds.size.height;
        }
        self.frame = CGRectMake(0.0, topPosition, screenWidth, actualHeight);
        if (self.messagePosition == TSMessageNotificationPositionTop) {
            self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        } else {
            self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
        }
    }
    return self;
}

- (void)configureTitleLabelWith:(NSString *)title
                     titleColor:(UIColor *)titleColor
                      titleFont:(UIFont *)titleFont
                          width:(CGFloat)width {
    NSDictionary *attributes = @{NSFontAttributeName : titleFont};
    CGRect rect = [title boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributes
                                      context:nil];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, width, rect.size.height)];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.titleLabel setText:title];
    [self.titleLabel setTextColor:titleColor];
    [self.titleLabel setBackgroundColor:[UIColor clearColor]];
    [self.titleLabel setFont:titleFont];
    [self addSubview:self.titleLabel];


}

- (CGFloat)updateHeightOfMessageView {
    return 50 + CGRectGetHeight(self.titleLabel.frame);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateHeightOfMessageView];
}

- (void)fadeMeOut {
    [[TSMessage sharedMessage] performSelectorOnMainThread:@selector(fadeOutNotification:) withObject:self waitUntilDone:NO];
}

- (void)didMoveToWindow {
    [super didMoveToWindow];
    if (self.duration == TSMessageNotificationDurationEndless && self.superview && !self.window) {
        // view controller was dismissed, let's fade out
        [self fadeMeOut];
    }
}

- (void)handleTap:(UITapGestureRecognizer *)tapGesture {
    if (tapGesture.state == UIGestureRecognizerStateRecognized) {
        if (self.callback) {
            self.callback();
        }
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return !([touch.view isKindOfClass:[UIControl class]]);
}

@end

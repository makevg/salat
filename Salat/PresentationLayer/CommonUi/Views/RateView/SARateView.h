//
//  SARateView.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    RateViewAlignmentLeft,
    RateViewAlignmentCenter,
    RateViewAlignmentRight
} RateViewAlignment;

@protocol SARateViewDelegate;

@interface SARateView : UIView

@property (nonatomic) RateViewAlignment alignment;
@property (nonatomic) CGFloat rate;
@property (nonatomic) CGFloat padding;
@property (nonatomic) BOOL editable;
@property (nonatomic) UIImage *ratePressedImage;
@property (nonatomic) UIImage *ratePrimaryImage;
@property (weak, nonatomic) id<SARateViewDelegate> delegate;

- (SARateView *)initWithFrame:(CGRect)frame;
- (SARateView *)initWithFrame:(CGRect)frame
                  ratePressed:(UIImage *)ratePressedImage
                  ratePrimary:(UIImage *)ratePrimaryImage;

@end

@protocol SARateViewDelegate

- (void)rateView:(SARateView *)rateView changedToNewRate:(NSNumber *)rate;

@end

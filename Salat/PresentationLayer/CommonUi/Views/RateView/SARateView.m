//
//  SARateView.m
//  Salat
//
//  Created by Максимычев Е.О. on 18.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARateView.h"

static NSString *const cRatePressedImageName = @"Rate_pressed";
static NSString *const cRatePrimaryImageName = @"Rate_primary";

static CGFloat const cDefaultPadding = 4.f;
static NSInteger const cDefaultNumberOfRates = 5;

@implementation SARateView {
    CGPoint origin;
    NSInteger numOfRates;
}

- (SARateView *)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame
                   ratePressed:[UIImage imageNamed:cRatePressedImageName]
                   ratePrimary:[UIImage imageNamed:cRatePrimaryImageName]];
}

- (SARateView *)initWithFrame:(CGRect)frame
                  ratePressed:(UIImage *)ratePressedImage
                  ratePrimary:(UIImage *)ratePrimaryImage {
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        _ratePressedImage = ratePressedImage;
        _ratePrimaryImage = ratePrimaryImage;
        [self commonSetup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        _ratePressedImage = [UIImage imageNamed:cRatePressedImageName];
        _ratePrimaryImage = [UIImage imageNamed:cRatePrimaryImageName];
        [self commonSetup];
    }
    return self;
}

- (void)commonSetup {
    _padding = cDefaultPadding;
    numOfRates = cDefaultNumberOfRates;
    self.alignment = RateViewAlignmentLeft;
    self.editable = NO;
}

- (void)drawRect:(CGRect)rect {
    switch (_alignment) {
        case RateViewAlignmentLeft:
            origin = CGPointMake(0, 0);
            break;
        case RateViewAlignmentCenter:
            origin = CGPointMake((self.bounds.size.width - numOfRates * self.ratePressedImage.size.width - (numOfRates - 1) * self.padding)/2, 0);
            break;
        case RateViewAlignmentRight:
            origin = CGPointMake(self.bounds.size.width - numOfRates * self.ratePressedImage.size.width - (numOfRates - 1) * self.padding, 0);
            break;
    }
    
    float x = origin.x;
    for(int i = 0; i < numOfRates; i++) {
        [self.ratePrimaryImage drawAtPoint:CGPointMake(x, origin.y)];
        x += self.ratePressedImage.size.width + self.padding;
    }
    
    
    float floor = floorf(self.rate);
    x = origin.x;
    for (int i = 0; i < floor; i++) {
        [self.ratePressedImage drawAtPoint:CGPointMake(x, origin.y)];
        x += self.ratePressedImage.size.width + self.padding;
    }
    
    if (numOfRates - floor > 0.01) {
        UIRectClip(CGRectMake(x, origin.y, self.ratePressedImage.size.width * (self.rate - floor), self.ratePressedImage.size.height));
        [self.ratePressedImage drawAtPoint:CGPointMake(x, origin.y)];
    }
}

- (void)setRate:(CGFloat)rate {
    _rate = rate;
    [self setNeedsDisplay];
    [self notifyDelegate];
}

- (void)setAlignment:(RateViewAlignment)alignment {
    _alignment = alignment;
    [self setNeedsLayout];
}

- (void)setEditable:(BOOL)editable {
    _editable = editable;
    self.userInteractionEnabled = _editable;
}

- (void)setRatePressedImage:(UIImage *)ratePressedImage {
    if (ratePressedImage != _ratePressedImage) {
        _ratePressedImage = ratePressedImage;
        [self setNeedsDisplay];
    }
}

- (void)setRatePrimaryImage:(UIImage *)ratePrimaryImage {
    if (ratePrimaryImage != _ratePrimaryImage) {
        _ratePrimaryImage = ratePrimaryImage;
        [self setNeedsDisplay];
    }
}

- (void)handleTouchAtLocation:(CGPoint)location {
    for(long i = numOfRates - 1; i > -1; i--) {
        if (location.x > origin.x + i * (self.ratePressedImage.size.width + self.padding) - self.padding / 2.) {
            self.rate = i + 1;
            return;
        }
    }
    self.rate = 0;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)notifyDelegate {
    if (self.delegate) {
        [self.delegate rateView:self changedToNewRate:@(self.rate)];
    }
}

@end
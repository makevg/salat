//
//  SABaseView.h
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAStyle.h"

@interface SABaseView : UIView

+ (UIView *)loadViewFromNib:(NSString *)nibName;

- (void)setup;
- (void)setModel:(id)model;

@end

//
//  SABaseView.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@implementation SABaseView

#pragma mark - Init

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setup];
}

#pragma mark - Public

+ (UIView *)loadViewFromNib:(NSString *)nibName {
    NSArray *viewNib = [[NSBundle mainBundle] loadNibNamed:nibName
                                                     owner:self
                                                   options:nil];
    return (UIView *)[viewNib firstObject];
}

- (void)setup {
    // Abstract method.
}

- (void)setModel:(id)model {
    // Abstract method.
}

@end

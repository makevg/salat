//
//  SABaseCollectionVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseCollectionVC.h"

@interface SABaseCollectionVC ()

@end

@implementation SABaseCollectionVC {
    BOOL forceHideLoading;
}

#pragma mark - Public

- (UICollectionView *)getCollectionView {
    return nil;
}

- (void)configureController {
    forceHideLoading = NO;
    [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                               forView:[self getCollectionView]];
    [self getCollectionView].dataSource = self;
    [self getCollectionView].delegate = self;
}

#pragma mark - Private

- (void)setFetchResult:(NSFetchedResultsController *)fetchResult {
    _fetchResult = fetchResult;
    _fetchResult.delegate = self;
    forceHideLoading = [[self.fetchResult fetchedObjects] count] != 0;
    
    if (forceHideLoading)
        [self hideLoadingState];
}

- (void)showLoadingState {
    if (![[self.fetchResult fetchedObjects] count])
        [super showLoadingState];
}

- (void)hideLoadingState {
//    if (forceHideLoading || ![[SALAT_DB produceContextForRead] hasChanges])
//        [super hideLoadingState];
};

#pragma mark - UICollectionViewDatasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [[self.fetchResult sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchResult sections][(NSUInteger) section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - NSFetchResultControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    forceHideLoading = YES;
    [self hideLoadingState];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //[[self getCollectionView] reloadEmptyDataSet];
    [[self getCollectionView] reloadData];
}

@end

//
//  SABaseCollectionVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import <CoreData/CoreData.h>

@interface SABaseCollectionVC : SABaseVC <UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchResult;

- (UICollectionView *)getCollectionView;

@end

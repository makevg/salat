//
//  SABaseVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCAngularActivityIndicatorView.h"

@class SACartButtonItem;

@protocol SABaseVC <NSObject>

+ (NSString *)storyboardName;

+ (NSString *)identifier;

+ (BOOL)isInitial;

@end

@interface SABaseVC : UIViewController <SABaseVC>

@property (nonatomic) SACartButtonItem *cartButton;

- (void)configureController;

- (void)showMessage:(NSString *)message;

- (void)showErrorMessage:(NSString *)message;

- (void)networkStateDidChanged:(NSNotification *)notification;

- (BOOL)showCartButton;

- (PCAngularActivityIndicatorView *)getLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style
                                                         forView:(UIView *)view;

- (BOOL)isLoading;
- (void)showLoadingState;
- (void)showLoadingStateForce;
- (void)hideLoadingState;

- (void)requestData;

@end

//
//  SABaseVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "TSMessage.h"
#import "SAStyle.h"
#import "SACartButtonItem.h"
#import "SACartButtonComponents.h"
#import "SAAPI.h"

static NSString *const cBackButtonTitle = @"Назад";
static NSString *const cCartMessage = @"Корзина пуста";
static NSString *const kNetworkErrorStateMessage = @"Проблемы с подключением к интернету";

@interface SABaseVC ()

@end

@implementation SABaseVC {
    PCAngularActivityIndicatorView *_loadingIndicator;
    BOOL _dataNotLoaded;
    NSError *_error;
}

#pragma mark - SABaseVC

+ (NSString *)storyboardName {
    return nil;
}

+ (NSString *)identifier {
    return NSStringFromClass([self class]);
}

+ (BOOL)isInitial {
    return NO;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self localConfigure];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkStateDidChanged:)
                                                 name:kNetworkChangeStateNotification
                                               object:nil];
    if (self.cartButton) {
        [self prepareCartButton];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNetworkChangeStateNotification
                                                  object:nil];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    if (![notification.object boolValue]) {
        [self showErrorMessage:kNetworkErrorStateMessage];
    }
}

#pragma mark - Private

- (void)localConfigure {
    _dataNotLoaded = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:cBackButtonTitle
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    [self configureController];
}

- (void)prepareCartButton {
    [self.cartButton configureModule];
    self.navigationItem.rightBarButtonItem = self.cartButton;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (UIViewController *)controllerFromStoryboard:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [sb instantiateInitialViewController];
}

#pragma mark - Public

- (void)configureController {
    // Abstract method.
}

- (void)showMessage:(NSString *)message {
    [TSMessage showNotificationWithMessage:message];
}

- (void)showErrorMessage:(NSString *)message {
    [TSMessage showNotificationWithTitle:message
                              titleColor:[SAStyle whiteColor]
                               titleFont:[SAStyle regularFontOfSize:18.f]
                                    type:TSMessageNotificationTypeError
                                duration:2.f];
}

- (BOOL)showCartButton {
    return NO;
}

- (PCAngularActivityIndicatorView *)getLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style forView:(UIView *)view {
    PCAngularActivityIndicatorView *loadingIndicator = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    loadingIndicator.color = [SAStyle whiteColor];
    loadingIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    [view addSubview:loadingIndicator];
    [view addConstraints:@[
                           [NSLayoutConstraint constraintWithItem:loadingIndicator
                                                        attribute:NSLayoutAttributeCenterX
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:view
                                                        attribute:NSLayoutAttributeCenterX
                                                       multiplier:1
                                                         constant:0],
                           [NSLayoutConstraint constraintWithItem:loadingIndicator
                                                        attribute:NSLayoutAttributeCenterY
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:view
                                                        attribute:NSLayoutAttributeCenterY
                                                       multiplier:1
                                                         constant:0]
                           ]];
    
    return loadingIndicator;
}

- (BOOL)isLoading {
    return [_loadingIndicator isAnimating];
}

- (void)showLoadingState {
    [_loadingIndicator startAnimating];
}

- (void)showLoadingStateForce {
    [_loadingIndicator startAnimating];
}

- (void)hideLoadingState {
    [_loadingIndicator stopAnimating];
}

- (void)requestData {
    [self showLoadingState];
}

@end

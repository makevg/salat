//
//  SABaseTableVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SABaseVC.h"
#import "SABAseView.h"

@interface SABaseTableVC : SABaseVC <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property(nonatomic) NSFetchedResultsController *fetchResult;

- (UITableView *)getTableView;

- (void)scrollToTop;

@end

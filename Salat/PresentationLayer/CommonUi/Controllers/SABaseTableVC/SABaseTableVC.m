//
//  SABaseTableVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseTableVC.h"

@interface SABaseTableVC ()

@end

@implementation SABaseTableVC

#pragma mark - Public

- (UITableView *)getTableView {
    return nil;
}

- (void)setFetchResult:(NSFetchedResultsController *)fetchResult {
    _fetchResult = fetchResult;
    _fetchResult.delegate = self;
    
    if ([[self.fetchResult fetchedObjects] count])
        [self hideLoadingState];
}

- (void)configureController {
    [self prepareTableView];
}

- (void)prepareTableView {
    [self getTableView].dataSource = self;
    [self getTableView].delegate = self;
}

- (void)scrollToTop {
    NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:0
                                                        inSection:0];
    [[self getTableView] scrollToRowAtIndexPath:indexPathToScroll
                               atScrollPosition:UITableViewScrollPositionTop
                                       animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchResult sections][(NSUInteger) section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - NSFetchResultControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self hideLoadingState];
    
    [[self getTableView] beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [[self getTableView] insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                               withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [[self getTableView] deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                               withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [[self getTableView] insertRowsAtIndexPaths:@[newIndexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [[self getTableView] deleteRowsAtIndexPaths:@[indexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            //[self configureCell:[[self tableView] cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [[self getTableView] deleteRowsAtIndexPaths:@[indexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            [[self getTableView] insertRowsAtIndexPaths:@[newIndexPath]
                                       withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [[self getTableView] endUpdates];
}

@end

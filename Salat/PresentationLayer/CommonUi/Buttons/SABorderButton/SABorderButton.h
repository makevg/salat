//
//  SABorderButton.h
//  Salat
//
//  Created by Maximychev Evgeny on 03.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseButton.h"

IB_DESIGNABLE
@interface SABorderButton : SABaseButton

@property(nonatomic, strong) IBInspectable UIColor *backNormal;
@property(nonatomic, strong) IBInspectable UIColor *backHighlighted;
@property(nonatomic, strong) IBInspectable UIColor *backDisabled;

@property(nonatomic, strong) IBInspectable UIColor *borderNormal;
@property(nonatomic, strong) IBInspectable UIColor *borderHighlighted;
@property(nonatomic, strong) IBInspectable UIColor *borderDisabled;

@property(nonatomic, strong) IBInspectable UIColor *titleNormal;
@property(nonatomic, strong) IBInspectable UIColor *titleHighlighted;
@property(nonatomic, strong) IBInspectable UIColor *titleDisabled;

@end

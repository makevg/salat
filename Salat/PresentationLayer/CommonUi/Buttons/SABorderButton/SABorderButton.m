//
//  SABorderButton.m
//  Salat
//
//  Created by Maximychev Evgeny on 03.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABorderButton.h"

@implementation SABorderButton

#pragma mark - Configure

- (void)configure {
    self.layer.cornerRadius = 2.5f;
    self.layer.borderWidth = 1.f;
}

- (void)setTitleNormal:(UIColor *)titleNormal {
    _titleNormal = titleNormal;
    [self setTitleColor:self.titleNormal forState:UIControlStateNormal];
}

- (void)setTitleDisabled:(UIColor *)titleDisabled {
    _titleDisabled = titleDisabled;
    [self setTitleColor:self.titleDisabled forState:UIControlStateDisabled];
}

- (void)setTitleHighlighted:(UIColor *)titleHighlighted {
    _titleHighlighted = titleHighlighted;
    [self setTitleColor:self.titleHighlighted forState:UIControlStateHighlighted];
}

- (void)setBackNormal:(UIColor *)backNormal {
    _backNormal = backNormal;
    self.backgroundColor = self.backNormal;
}

- (void)setBorderNormal:(UIColor *)borderNormal {
    _borderNormal = borderNormal;
    self.layer.borderColor = self.borderNormal.CGColor;
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];

    self.backgroundColor = highlighted ? self.backHighlighted : self.backNormal;
    self.layer.borderColor = highlighted ? self.borderHighlighted.CGColor : self.borderNormal.CGColor;
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];

    self.backgroundColor = enabled ? self.backNormal : self.backDisabled;
    self.layer.borderColor = enabled ? self.borderNormal.CGColor : self.borderDisabled.CGColor;
}

- (void)prepareForInterfaceBuilder {
    self.layer.cornerRadius = 2.5f;
    self.layer.borderWidth = 1.f;
    self.enabled = true;
}

@end

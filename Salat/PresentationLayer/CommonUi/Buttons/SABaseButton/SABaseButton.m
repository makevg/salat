//
//  SABaseButton.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseButton.h"

@implementation SABaseButton

#pragma mark - Init

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self localInit];
    }
    return self;
}

#pragma mark - Private

- (void)localInit {
    [self setTitleColor:[SAStyle whiteColor] forState:UIControlStateNormal];
    self.titleLabel.font = [SAStyle defaultFontOfSize:14.f];
    [self configure];
}

#pragma mark - Public

- (void)configure {
    // Abstract method.
}

@end

//
//  SAPinView.h
//  Salat
//
//  Created by mtx on 08.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kDefinedPinsCount 4

@protocol SAPinViewDelegate;

@interface SAPinView : UIView

//Pins image
@property(nonatomic, strong) UIImage *normalPinImage;
@property(nonatomic, strong) UIImage *selectedPinImage;

//Getter/Setter for pin code NSString
@property(nonatomic, strong) NSString *pinCode;

//Delegate
@property(nonatomic, weak) IBOutlet id <SAPinViewDelegate> delegate;

- (void)resetPinCode;

@end

@protocol SAPinViewDelegate <NSObject>
@required
- (void)pinCodeView:(SAPinView *)view didEnterPin:(NSString *)pinCode;
@end
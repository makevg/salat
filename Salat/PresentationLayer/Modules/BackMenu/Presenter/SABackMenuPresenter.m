//
//  SABackMenuPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuPresenter.h"
#import "SABackMenuViewInput.h"
#import "SABackMenuInteractorInput.h"
#import "SABackMenuRouterInput.h"

typedef NS_ENUM(NSInteger, SABackMenuSectionType) {
    SABackMenuSectionTypeMenu = 0,
    SABackMenuSectionTypeOrders,
    SABackMenuSectionTypeReviews,
    SABackMenuSectionTypeNews,
    SABackMenuSectionTypeSales,
    SABackMenuSectionTypeAboutDelivery
};

@implementation SABackMenuPresenter

#pragma mark - SABackMenuViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.interactor obtainBackMenuItems];
}

- (void)didTapMenuCellAtIndexPath:(NSIndexPath *)indexPath {
    SABackMenuSectionType sectionType = (SABackMenuSectionType) indexPath.row;
    switch (sectionType) {
        case SABackMenuSectionTypeMenu:
            [self.router openMenu];
            break;
        case SABackMenuSectionTypeOrders:
            if ([self.interactor isAuthorized]) {
                [self.router openOrders];
            } else {
                [self.router openNotAuthorized];
            }
            break;
        case SABackMenuSectionTypeReviews:
            [self.router openReviews];
            break;
        case SABackMenuSectionTypeNews:
            [self.router openNews];
            break;
        case SABackMenuSectionTypeSales:
            [self.router openSales];
            break;
        case SABackMenuSectionTypeAboutDelivery:
            [self.router openAboutDelivery];
            break;
    }
}

- (void)didTapCallButton {
    [self.interactor makeCall];
}

- (void)didTapProfileHeader {
    if ([self.interactor isAuthorized]) {
        [self.router openProfileModule];
    } else {
        [self.router openSignInModule];
    }
}

#pragma mark - SABackMenuInteractorOutput

- (void)didObtainBackMenuItems:(NSArray<SABackMenuPlainObject *> *)items {
    [self.view updateWithMenuItems:items];
}

@end

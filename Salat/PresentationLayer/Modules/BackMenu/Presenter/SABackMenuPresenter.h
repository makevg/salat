//
//  SABackMenuPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SABackMenuInteractorOutput.h"
#import "SABackMenuViewOutput.h"

@protocol SABackMenuViewInput;
@protocol SABackMenuInteractorInput;
@protocol SABackMenuRouterInput;

@interface SABackMenuPresenter : NSObject <SABackMenuViewOutput, SABackMenuInteractorOutput>

@property (weak, nonatomic) id<SABackMenuViewInput> view;
@property (nonatomic) id<SABackMenuInteractorInput> interactor;
@property (nonatomic) id<SABackMenuRouterInput> router;

@end

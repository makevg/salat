//
//  SABackMenuInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABackMenuInteractorInput.h"

@protocol SABackMenuInteractorOutput;
@class SARestaurantService;
@class SAUserService;

@interface SABackMenuInteractor : NSObject <SABackMenuInteractorInput>

@property (weak, nonatomic) id<SABackMenuInteractorOutput> output;
@property (nonatomic) SARestaurantService *restaurantService;
@property (nonatomic) SAUserService *userService;

@end

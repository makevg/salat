//
//  SABackMenuInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SABackMenuInteractorInput <NSObject>

- (void)obtainBackMenuItems;
- (void)makeCall;
- (bool)isAuthorized;
- (void)makeLogout;

@end

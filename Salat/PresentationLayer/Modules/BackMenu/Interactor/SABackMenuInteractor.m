//
//  SABackMenuInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuInteractor.h"
#import "SABackMenuInteractorOutput.h"
#import "SARestaurantService.h"
#import "SABackMenuPlainObject.h"
#import "SAUserService.h"

static NSString *const kOrganizationPhone = @"+7 (4852) 675-675";

@implementation SABackMenuInteractor

- (void)obtainBackMenuItems {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"BackMenu"
                                                          ofType:@"plist"];
    NSArray<SABackMenuPlainObject *> *items = [self getBackMenuItemsFromFile:plistPath];
    [self.output didObtainBackMenuItems:items];
}

- (void)makeCall {
    NSString *phone = kOrganizationPhone;
    NSString *cleanedString = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (bool)isAuthorized {
    return [self.userService isAuthorized];
}

- (void)makeLogout {
    [self.userService logout:nil];
}

#pragma mark - Private

- (NSArray<SABackMenuPlainObject *> *)getBackMenuItemsFromFile:(NSString *)filePath {
    NSArray *backMenuItems = [NSArray arrayWithContentsOfFile:filePath];
    NSMutableArray<SABackMenuPlainObject *> *items = [NSMutableArray<SABackMenuPlainObject *> new];
    for (NSDictionary *itemDict in backMenuItems) {
        SABackMenuPlainObject *backMenuPlainObject = [SABackMenuPlainObject new];
        backMenuPlainObject.icon = itemDict[@"icon"];
        backMenuPlainObject.title = itemDict[@"title"];
        [items addObject:backMenuPlainObject];
    }
    return items;
}

@end

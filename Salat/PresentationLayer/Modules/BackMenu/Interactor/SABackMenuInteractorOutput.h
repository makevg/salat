//
//  SABackMenuInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SABackMenuPlainObject;

@protocol SABackMenuInteractorOutput <NSObject>

- (void)didObtainBackMenuItems:(NSArray<SABackMenuPlainObject *> *)items;

@end

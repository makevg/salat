//
//  SABackMenuVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuVC.h"
#import "SABackMenuView.h"
#import "SABackMenuHeader.h"
#import "SABackMenuViewOutput.h"
#import "SABackMenuDataDisplayManager.h"
#import "UIAlertController+Helper.h"

NSString *const cBackMenuVCIdentifier = @"leftMenuViewController";

@interface SABackMenuVC () <SABackMenuHeaderDelegate, SABackMenuDataDisplayManagerDelegate>
@property (strong, nonatomic) IBOutlet SABackMenuView *contentView;
@end

@implementation SABackMenuVC

- (void)networkStateDidChanged:(NSNotification *)notification {
}

#pragma mark - Public

+ (NSString *)identifier {
    return cBackMenuVCIdentifier;
}

- (void)configureController {
    [super configureController];
    
    self.contentView.headerView.delegate = self;
    [self prepareDisplayManager];
    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Private

- (void)prepareDisplayManager {
    self.displayManager.tableView = self.contentView.tableView;
    self.displayManager.delegate = self;
}

#pragma mark - Actions

- (IBAction)tappedCallButton:(id)sender {
    [self.output didTapCallButton];
}

#pragma mark - SABackMenuHeaderDelegate

- (void)didTapHeader:(SABackMenuHeader *)header {
    [self.output didTapProfileHeader];
}

#pragma mark - SABackMenuViewInput

- (void)updateWithMenuItems:(NSArray<SABackMenuPlainObject *> *)items {
    [self.displayManager configureDataDisplayManagerWithMenuItems:items];
    [self.contentView.tableView reloadData];
}

- (void)showAlert:(NSString *)alert {
    UIAlertController *alertController = [UIAlertController alertWithTitle:nil
                                                                   message:alert
                                                                   handler:nil];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - SABackMenuDataDisplayManagerDelegate

- (void)displayManager:(SABackMenuDataDisplayManager *)displayManager didTapMenuCellAtIndexPath:(NSIndexPath *)indexPath {
    [self.output didTapMenuCellAtIndexPath:indexPath];
}

@end

//
//  SABackMenuVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SABackMenuViewInput.h"

@class SABackMenuDataDisplayManager;
@protocol SABackMenuViewOutput;

@interface SABackMenuVC : SABaseVC <SABackMenuViewInput>

@property (nonatomic) id<SABackMenuViewOutput> output;
@property (nonatomic) SABackMenuDataDisplayManager *displayManager;

@end

//
//  SABackMenuRootVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuRootVC.h"
#import "SABackMenuVC.h"
#import "SAMenuVC.h"

NSString *const cLeftMenuRootStoryboardName = @"SABackMenu";

@interface SABackMenuRootVC () <RESideMenuDelegate>

@end

@implementation SABackMenuRootVC

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configureController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cLeftMenuRootStoryboardName;
}

#pragma mark - Private

- (void)configureController {
    [self configureContentView];
    self.contentViewController = [self loadFeatureByName:[SAMenuVC storyboardName]];
    self.leftMenuViewController = [self loadControllerByIdentifier:[SABackMenuVC identifier]];
    self.fadeMenuView = NO;
    self.panFromEdge = NO;
    self.bouncesHorizontally = NO;
    self.parallaxEnabled = NO;
    self.delegate = self;
}

- (void)configureContentView {
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewScaleValue = 1.f;
    self.contentViewInPortraitOffsetCenterX = 90;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;
    self.scaleMenuView = NO;
}

- (UIViewController *)loadFeatureByName:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    return vc;
}

- (UIViewController *)loadControllerByIdentifier:(NSString *)identifier {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[SABackMenuRootVC storyboardName] bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:identifier];
    return vc;
}

#pragma mark - RESideMenuDelegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController {
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController {
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController {
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController {
}

@end

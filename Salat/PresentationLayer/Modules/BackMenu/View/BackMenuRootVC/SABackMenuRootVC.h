//
//  SABackMenuRootVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RESideMenu/RESideMenu.h>

@interface SABackMenuRootVC : RESideMenu

+ (NSString *)storyboardName;

@end

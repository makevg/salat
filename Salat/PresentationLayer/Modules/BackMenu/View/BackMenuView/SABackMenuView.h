//
//  SABackMenuView.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseView.h"

@class SABackMenuHeader;

@interface SABackMenuView : SABaseView

@property (weak, nonatomic) IBOutlet SABackMenuHeader *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

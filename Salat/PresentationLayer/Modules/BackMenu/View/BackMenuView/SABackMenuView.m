//
//  SABackMenuView.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuView.h"
#import "SABackMenuHeader.h"

@interface SABackMenuView ()
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@end

@implementation SABackMenuView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [SAStyle clearColor];
    self.backImageView.backgroundColor = [SAStyle backgroundColor];
    self.tableView.backgroundColor = [SAStyle clearColor];
    self.tableView.tableFooterView = [UIView new];
}

@end

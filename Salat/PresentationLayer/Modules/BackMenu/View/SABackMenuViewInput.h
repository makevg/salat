//
//  SABackMenuViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SABackMenuPlainObject;

@protocol SABackMenuViewInput <NSObject>

- (void)updateWithMenuItems:(NSArray<SABackMenuPlainObject *> *)items;

- (void)showAlert:(NSString *)alert;

@end

//
//  SABackMenuViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SABackMenuViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didTapMenuCellAtIndexPath:(NSIndexPath *)indexPath;
- (void)didTapCallButton;
- (void)didTapProfileHeader;

@end

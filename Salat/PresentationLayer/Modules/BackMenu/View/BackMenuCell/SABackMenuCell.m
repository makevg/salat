//
//  SABackMenuCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuCell.h"
#import "SABackMenuPlainObject.h"

@interface SABackMenuCell ()
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation SABackMenuCell

#pragma mark - Public

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SABackMenuPlainObject class]]) {
        [self configureWithMenuObject:model];
    }
}

- (void)configureCell {
    self.backgroundColor = [UIColor clearColor];
    self.infoLabel.textColor = [SAStyle whiteColor];
    self.infoLabel.font = [SAStyle regularFontOfSize:16.f];
}

#pragma mark - Private

- (void)configureWithMenuObject:(SABackMenuPlainObject *)backMenuPlainObject {
    self.iconImageView.image = [UIImage imageNamed:backMenuPlainObject.icon];
    self.infoLabel.text = backMenuPlainObject.title;
}

@end

//
//  SABackMenuHeader.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuHeader.h"

@interface SABackMenuHeader ()
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation SABackMenuHeader

#pragma mark - init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        [self addSubview:self.mainView];
        [self configureView];
    }
    return self;
}

#pragma mark - Private

- (void)configureView {
    [self prepareConstraints];
    [self configureSubviews];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
}

- (void)configureSubviews {
    [self.mainView setBackgroundColor:[SAStyle clearColor]];
    self.infoLabel.textColor = [SAStyle whiteColor];
    self.infoLabel.font = [SAStyle regularFontOfSize:14.f];
}

#pragma mark - Actions

- (IBAction)tappedView:(id)sender {
    if (self.delegate) {
        [self.delegate didTapHeader:self];
    }
}

@end

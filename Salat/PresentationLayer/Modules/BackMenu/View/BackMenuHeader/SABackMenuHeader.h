//
//  SABackMenuHeader.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseView.h"

@protocol SABackMenuHeaderDelegate;

@interface SABackMenuHeader : SABaseView

@property (weak, nonatomic) id<SABackMenuHeaderDelegate> delegate;

@end

@protocol SABackMenuHeaderDelegate <NSObject>

- (void)didTapHeader:(SABackMenuHeader *)header;

@end

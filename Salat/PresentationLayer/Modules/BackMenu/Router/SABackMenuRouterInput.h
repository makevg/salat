//
//  SABackMenuRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SABackMenuRouterInput <NSObject>

- (void)openProfileModule;
- (void)openSignInModule;
- (void)openMenu;
- (void)openOrders;
- (void)openReviews;
- (void)openNews;
- (void)openSales;
- (void)openAboutDelivery;
- (void)openNotAuthorized;

@end

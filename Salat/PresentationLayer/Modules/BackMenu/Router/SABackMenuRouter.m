//
//  SABackMenuRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuRouter.h"
#import "SABackMenuVC.h"
#import "SAMenuVC.h"
#import "SAOrdersListScreenVC.h"
#import "SAReviewsListVC.h"
#import "SANewsListVC.h"
#import "SASalesVC.h"
#import "SAAboutDeliveryVC.h"
#import "SASignInVC.h"
#import "SAProfileVC.h"
#import "SANotAuthorizedVC.h"
#import "SANotAuthorizedPresenter.h"
#import <RESideMenu/RESideMenu.h>

@implementation SABackMenuRouter

#pragma mark - SABackMenuRouterInput

- (void)openProfileModule {
    [self animateTransition];
    [self pushViewController:[SAProfileVC storyboardName] animated:NO];
}

- (void)openSignInModule {
    [self animateTransition];
    [self pushViewController:[SASignInVC storyboardName] animated:NO];
}

- (void)openMenu {
    [self showViewControllerInSideMenu:[SAMenuVC storyboardName]];
}

- (void)openOrders {
    [self showViewControllerInSideMenu:[SAOrdersListScreenVC storyboardName]];
}

- (void)openReviews {
    [self showViewControllerInSideMenu:[SAReviewsListVC storyboardName]];
}

- (void)openNews {
    [self showViewControllerInSideMenu:[SANewsListVC storyboardName]];
}

- (void)openSales {
    [self showViewControllerInSideMenu:[SASalesVC storyboardName]];
}

- (void)openAboutDelivery {
    [self showViewControllerWithNaviInSideMenu:[SAAboutDeliveryVC storyboardName]];
}

- (void)openNotAuthorized {
    RESideMenu *sideMenuViewController = ((SABackMenuVC *)self.transitionHandler).sideMenuViewController;
    SANotAuthorizedVC *vc = (SANotAuthorizedVC *) [self controllerFromStoryboard:[SANotAuthorizedVC storyboardName]];

    id<SANotAuthorizedModuleInput> output = [vc valueForKey:@"output"];
    [output configureModuleWithInfoString:@"Список заказов доступен только зарегистрированным пользователям"
                                    title:@"Заказы"];

    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuButton"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:vc
                                                                     action:@selector(presentLeftMenuViewController:)];

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    vc.navigationItem.leftBarButtonItem = barButtonItem;

    sideMenuViewController.contentViewController = navigationController;
    [sideMenuViewController hideMenuViewController];
}


#pragma mark - Private

- (void)showViewControllerInSideMenu:(NSString *)storyboardName {
    RESideMenu *sideMenuViewController = ((SABackMenuVC *)self.transitionHandler).sideMenuViewController;
    UIViewController *vc = [self controllerFromStoryboard:storyboardName];
    sideMenuViewController.contentViewController = vc;
    [sideMenuViewController hideMenuViewController];
}

- (void)showViewControllerWithNaviInSideMenu:(NSString *)storyboardName {
    RESideMenu *sideMenuViewController = ((SABackMenuVC *)self.transitionHandler).sideMenuViewController;
    UIViewController *vc = [self controllerFromStoryboard:storyboardName];

    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuButton"]
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:vc
                                                                     action:@selector(presentLeftMenuViewController:)];

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    vc.navigationItem.leftBarButtonItem = barButtonItem;

    sideMenuViewController.contentViewController = navigationController;
    [sideMenuViewController hideMenuViewController];
}

- (void)pushViewController:(NSString *)storyboardName
                  animated:(BOOL)animated {
    UIViewController *vc = [self controllerFromStoryboard:storyboardName];
    [((SABackMenuVC *) self.transitionHandler).navigationController pushViewController:vc animated:animated];
}

- (UIViewController *)controllerFromStoryboard:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    return vc;
}

- (void)animateTransition {
    SABackMenuVC *vc = ((SABackMenuVC *)self.transitionHandler);
    [vc.sideMenuViewController hideMenuViewController];
    [UIView animateWithDuration:0.75
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                                                forView:vc.navigationController.view
                                                  cache:NO];
                     }];
}

@end

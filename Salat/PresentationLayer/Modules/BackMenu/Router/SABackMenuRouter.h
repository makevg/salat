//
//  SABackMenuRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SABackMenuRouter : NSObject <SABackMenuRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

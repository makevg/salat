//
//  SABackMenuModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuModuleAssembly.h"
#import "SABackMenuVC.h"
#import "SABackMenuPresenter.h"
#import "SABackMenuInteractor.h"
#import "SABackMenuRouter.h"
#import "SABackMenuDataDisplayManager.h"

@implementation SABackMenuModuleAssembly

- (SABackMenuVC *)viewBackMenu {
    return [TyphoonDefinition withClass:[SABackMenuVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterBackMenu]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerBackMenu]];
    }];
}

- (SABackMenuPresenter *)presenterBackMenu {
    return [TyphoonDefinition withClass:[SABackMenuPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewBackMenu]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorBackMenu]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerBackMenu]];
    }];
}

- (SABackMenuInteractor *)interactorBackMenu {
    return [TyphoonDefinition withClass:[SABackMenuInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterBackMenu]];
                              [definition injectProperty:@selector(restaurantService)
                                                    with:[self.serviceComponents restaurantService]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
    }];
}

- (SABackMenuRouter *)routerBackMenu {
    return [TyphoonDefinition withClass:[SABackMenuRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewBackMenu]];
    }];
}

- (SABackMenuDataDisplayManager *)displayManagerBackMenu {
    return [TyphoonDefinition withClass:[SABackMenuDataDisplayManager class]];
}

@end

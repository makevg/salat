//
//  SABackMenuDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SABackMenuDataDisplayManagerDelegate;
@class SABackMenuPlainObject;

@interface SABackMenuDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;
@property (weak, nonatomic) id<SABackMenuDataDisplayManagerDelegate> delegate;

- (void)configureDataDisplayManagerWithMenuItems:(NSArray<SABackMenuPlainObject *> *)items;

@end

@protocol SABackMenuDataDisplayManagerDelegate <NSObject>

- (void)displayManager:(SABackMenuDataDisplayManager *)displayManager didTapMenuCellAtIndexPath:(NSIndexPath *)indexPath;

@end

//
//  SABackMenuDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABackMenuDataDisplayManager.h"
#import "SABackMenuCell.h"
#import "SABackMenuPlainObject.h"

@interface SABackMenuDataDisplayManager ()
@property (nonatomic) NSArray *items;
@end

@implementation SABackMenuDataDisplayManager

#pragma mark - Lazy init

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - public

- (void)configureDataDisplayManagerWithMenuItems:(NSArray<SABackMenuPlainObject *> *)items {
    self.items = items;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SABackMenuCell cellIdentifier];
    SABackMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                           forIndexPath:indexPath];
    [cell setModel:self.items[(NSUInteger) indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(displayManager:didTapMenuCellAtIndexPath:)]) {
        [self.delegate displayManager:self didTapMenuCellAtIndexPath:indexPath];
    }
}

@end

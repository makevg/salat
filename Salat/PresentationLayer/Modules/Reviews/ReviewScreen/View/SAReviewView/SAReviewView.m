//
//  SAReviewView.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewView.h"
#import "SARateView.h"

@implementation SAReviewView

#pragma maark - Setup

- (void)setup {
    [self prepareRatingView];
    [self prepareSeparator];
    [self prepareReviewTextView];
}

#pragma mark - Private

- (void)prepareRatingView {
    self.rateView.backgroundColor = [SAStyle whiteColor];
    self.rateView.padding = 15;
    self.rateView.alignment = RateViewAlignmentCenter;
    self.rateView.editable = YES;
}

- (void)prepareSeparator {
    self.separator.backgroundColor = [SAStyle grayColor];
    self.separatorHeightConstraint.constant = 0.5f;
}

- (void)prepareReviewTextView {
    self.reviewTextView.textColor = [SAStyle grayColor];
    self.reviewTextView.font = [SAStyle regularFontOfSize:15.f];
    [self.reviewTextView becomeFirstResponder];
}

@end

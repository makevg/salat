//
//  SAReviewView.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@class SARateView;

@interface SAReviewView : SABaseView

@property (weak, nonatomic) IBOutlet SARateView *rateView;
@property (weak, nonatomic) IBOutlet UIView *separator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextView *reviewTextView;

@end

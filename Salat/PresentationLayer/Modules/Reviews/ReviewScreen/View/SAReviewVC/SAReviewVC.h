//
//  SAReviewVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAReviewViewInput.h"

@protocol SAReviewViewOutput;

@interface SAReviewVC : SABaseVC <SAReviewViewInput>

@property (nonatomic) id<SAReviewViewOutput> output;

@end

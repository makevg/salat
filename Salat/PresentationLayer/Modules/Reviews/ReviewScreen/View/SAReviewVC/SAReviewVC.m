//
//  SAReviewVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewVC.h"
#import "SAReviewView.h"
#import "SARateView.h"
#import "SAReviewViewOutput.h"

@interface SAReviewVC () <SARateViewDelegate, UITextViewDelegate>
@property (strong, nonatomic) IBOutlet SAReviewView *contentView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButtonItem;
@end

@implementation SAReviewVC

#pragma mark - Super

- (void)configureController {
    [super configureController];
    
    self.contentView.rateView.delegate = self;
    self.contentView.reviewTextView.text = @"";
    self.contentView.reviewTextView.delegate = self;
    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - SARateViewDelegate

- (void)rateView:(SARateView *)rateView changedToNewRate:(NSNumber *)rate {
    [self.output didChangeRate:rate];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    [self.output didChangeReviewContent:textView.text];
}

#pragma mark - Actions

- (IBAction)tappedDoneButton:(id)sender {
    [self.output didTapDoneButton];
}

#pragma mark - SAReviewViewInput

- (void)enableDoneButton:(BOOL)enable {
    self.doneButtonItem.enabled = enable;
}

@end

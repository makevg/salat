//
//  SAReviewViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SAReviewViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didChangeRate:(NSNumber *)rate;
- (void)didChangeReviewContent:(NSString *)content;
- (void)didTapDoneButton;

@end

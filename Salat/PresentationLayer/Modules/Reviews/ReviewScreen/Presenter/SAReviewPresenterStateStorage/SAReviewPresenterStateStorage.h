//
//  SAReviewPresenterStateStorage.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAReviewPresenterStateStorage : NSObject

@property (nonatomic) NSNumber *reviewRate;
@property (nonatomic) NSString *reviewContent;

@end

//
//  SAReviewPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAReviewInteractorOutput.h"
#import "SAReviewViewOutput.h"

@class SAReviewPresenterStateStorage;
@protocol SAReviewViewInput;
@protocol SAReviewInteractorInput;
@protocol SAReviewRouterInput;

@interface SAReviewPresenter : NSObject <SAReviewViewOutput, SAReviewInteractorOutput>

@property (weak, nonatomic) id<SAReviewViewInput> view;
@property (nonatomic) id<SAReviewInteractorInput> interactor;
@property (nonatomic) id<SAReviewRouterInput> router;
@property (nonatomic) SAReviewPresenterStateStorage *presenterStateStorage;

@end

//
//  SAReviewPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewPresenter.h"
#import "SAReviewViewInput.h"
#import "SAReviewInteractorInput.h"
#import "SAReviewRouterInput.h"
#import "SAReviewPresenterStateStorage.h"

@implementation SAReviewPresenter

#pragma mark - SAReviewViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view enableDoneButton:NO];
    self.presenterStateStorage.reviewRate = @(0);
}

- (void)didChangeRate:(NSNumber *)rate {
    self.presenterStateStorage.reviewRate = rate;
}

- (void)didChangeReviewContent:(NSString *)content {
    self.presenterStateStorage.reviewContent = content;
    BOOL reviewContentIsNotEmpty = [content length] > 0;
    [self.view enableDoneButton:reviewContentIsNotEmpty];
}

- (void)didTapDoneButton {
    [self.interactor storeReviewWithContent:self.presenterStateStorage.reviewContent
                                       rate:self.presenterStateStorage.reviewRate];
}

- (void)didStoreReview {
    [self.router closeCurrentModule];
}

- (void)didObtainError:(NSError *)error {
    //TODO:
}


@end

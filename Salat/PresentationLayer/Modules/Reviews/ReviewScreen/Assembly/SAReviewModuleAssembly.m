//
//  SAReviewModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewModuleAssembly.h"
#import "SAReviewVC.h"
#import "SAReviewPresenter.h"
#import "SAReviewInteractor.h"
#import "SAReviewRouter.h"
#import "SAReviewPresenterStateStorage.h"

@implementation SAReviewModuleAssembly

- (SAReviewVC *)viewReview {
    return [TyphoonDefinition withClass:[SAReviewVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterReview]];
    }];
}

- (SAReviewPresenter *)presenterReview {
    return [TyphoonDefinition withClass:[SAReviewPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewReview]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorReview]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerReview]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorageReview]];
                          }];
}

- (SAReviewInteractor *)interactorReview {
    return [TyphoonDefinition withClass:[SAReviewInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterReview]];
                              [definition injectProperty:@selector(reviewsService)
                                                    with:[self.serviceComponents reviewService]];
    }];
}

- (SAReviewRouter *)routerReview {
    return [TyphoonDefinition withClass:[SAReviewRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewReview]];
    }];
}

- (SAReviewPresenterStateStorage *)presenterStateStorageReview {
    return [TyphoonDefinition withClass:[SAReviewPresenterStateStorage class]];
}

@end

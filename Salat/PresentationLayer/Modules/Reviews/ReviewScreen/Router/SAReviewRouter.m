//
//  SAReviewRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewRouter.h"
#import "SAReviewVC.h"

@implementation SAReviewRouter

#pragma mark - SAReviewRouterInput

- (void)closeCurrentModule {
    UINavigationController *navVC = ((SAReviewVC *) self.transitionHandler).navigationController;
    [navVC popViewControllerAnimated:YES];
}

@end

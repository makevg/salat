//
//  SAReviewRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAReviewRouter : NSObject <SAReviewRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

//
//  SAReviewInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAReviewInteractorInput.h"

@protocol SAReviewInteractorOutput;
@class SAReviewsService;

@interface SAReviewInteractor : NSObject <SAReviewInteractorInput>

@property (weak, nonatomic) id<SAReviewInteractorOutput> output;
@property (nonatomic) SAReviewsService *reviewsService;

@end

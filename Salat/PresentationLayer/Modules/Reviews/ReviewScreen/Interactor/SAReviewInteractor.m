//
//  SAReviewInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewInteractor.h"
#import "SAReviewsService.h"
#import "SDWebImageCompat.h"
#import "SAReviewInteractorOutput.h"

@implementation SAReviewInteractor

#pragma mark - SAReviewInteractorInput

- (void)storeReviewWithContent:(NSString *)content rate:(NSNumber *)rate {
    __weak typeof(self) weakSelf = self;

    [self.reviewsService storeReviewWithContent:content
                                         rating:rate
                                     completion:^{
                                         dispatch_main_async_safe(^{
                                             [weakSelf.output didStoreReview];
                                         });
                                     }
                                          error:^(NSError *error) {
                                              dispatch_main_async_safe(^{
                                                  [weakSelf.output didObtainError:error];
                                              });
                                          }];
}

@end

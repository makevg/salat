//
//  SAReviewInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAReviewInteractorInput <NSObject>

- (void)storeReviewWithContent:(NSString *)content rate:(NSNumber *)rate;

@end

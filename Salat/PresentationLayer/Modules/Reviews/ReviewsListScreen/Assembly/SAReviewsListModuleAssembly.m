//
//  SAReviewsListModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListModuleAssembly.h"
#import "SAReviewsListVC.h"
#import "SAReviewsListPresenter.h"
#import "SAReviewsListInteractor.h"
#import "SAReviewsListRouter.h"
#import "SAReviewsListDataDisplayManager.h"
#import "SAReviewMapper.h"

@implementation SAReviewsListModuleAssembly

- (SAReviewsListVC *)viewReviewsList {
    return [TyphoonDefinition withClass:[SAReviewsListVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterReviewsList]];
                              [definition injectProperty:@selector(dataDisplayManager)
                                                    with:[self displayManagerReviewsList]];
    }];
}

- (SAReviewsListPresenter *)presenterReviewsList {
    return [TyphoonDefinition withClass:[SAReviewsListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewReviewsList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorReviewsList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerReviewsList]];
                          }];
}

- (SAReviewsListInteractor *)interactorReviewsList {
    return [TyphoonDefinition withClass:[SAReviewsListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterReviewsList]];
                              [definition injectProperty:@selector(reviewsService)
                                                    with:[self.serviceComponents reviewService]];
                              [definition injectProperty:@selector(reviewMapper)
                                                    with:[self.coreComponents reviewMapper]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
    }];
}

- (SAReviewsListRouter *)routerReviewsList {
    return [TyphoonDefinition withClass:[SAReviewsListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewReviewsList]];
                          }];
}

- (SAReviewsListDataDisplayManager *)displayManagerReviewsList {
    return [TyphoonDefinition withClass:[SAReviewsListDataDisplayManager class]];
}

@end

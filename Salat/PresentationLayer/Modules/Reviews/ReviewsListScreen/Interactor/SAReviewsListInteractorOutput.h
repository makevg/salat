//
//  SAReviewsListInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAReviewPlainObject;

@protocol SAReviewsListInteractorOutput <NSObject>

- (void)didObtainReviews:(NSArray<SAReviewPlainObject *> *)reviews;

- (void)didObtainError:(NSError *)error;

@end

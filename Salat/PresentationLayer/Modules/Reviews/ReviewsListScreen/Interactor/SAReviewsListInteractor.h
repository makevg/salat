//
//  SAReviewsListInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAReviewsListInteractorInput.h"

@protocol SAReviewsListInteractorOutput;
@class SAReviewsService;
@class SAReviewMapper;
@class SAUserService;

@interface SAReviewsListInteractor : NSObject <SAReviewsListInteractorInput>

@property (weak, nonatomic) id<SAReviewsListInteractorOutput> output;
@property (nonatomic) SAReviewsService *reviewsService;
@property (nonatomic) SAReviewMapper *reviewMapper;
@property (nonatomic) SAUserService *userService;

@end

//
//  SAReviewsListInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListInteractor.h"
#import "SAReviewsListInteractorOutput.h"
#import "SAReviewsService.h"
#import "SAReviewMapper.h"
#import "SAReviewPlainObject.h"
#import "Review.h"
#import "SAServiceLayer.h"
#import "SAUserService.h"

@implementation SAReviewsListInteractor

#pragma mark - SAReviewsListInteractorInput

- (void)obtainReviews {
    __weak typeof(self) weakSelf = self;

    [self.reviewsService obtainReviews:^(NSArray<Review *> *data) {
                __strong typeof(weakSelf) strongSelf = weakSelf;

                NSArray<SAReviewPlainObject *> *reviews = [strongSelf getPlainReviewsFromManagedObjects:data];

                dispatch_safe_main_async(^{
                    [strongSelf.output didObtainReviews:reviews];
                });

            }
                                 error:^(NSError *error) {
                                     dispatch_safe_main_async(^{
                                         [weakSelf.output didObtainError:error];
                                     });
                                 }];
}

- (void)obtainReviewsFromNetwork {
    weakifySelf
    
    [self.reviewsService obtainReviewsFromNetwork:^(NSArray<Review *> *data) {
        strongifySelf
        
        NSArray<SAReviewPlainObject *> *reviews = [strongSelf getPlainReviewsFromManagedObjects:data];
        
        dispatch_safe_main_async(^{
            [strongSelf.output didObtainReviews:reviews];
        });
    }
                                            error:^(NSError *error) {
                                                dispatch_safe_main_async(^{
                                                    [weakSelf.output didObtainError:error];
                                                });
                                            }];
}

- (void)obtainMyReviews {
    NSArray<Review *> *managedObjectReviews = [self.reviewsService obtainMyReviews];
    NSArray<SAReviewPlainObject *> *reviews = [self getPlainReviewsFromManagedObjects:managedObjectReviews];
    [self.output didObtainReviews:reviews];
}

- (BOOL)checkAuthorization {
    return [self.userService isAuthorized];
}


#pragma mark - Private

- (NSArray<SAReviewPlainObject *> *)getPlainReviewsFromManagedObjects:(NSArray<Review *> *)managedObjectReviews {
    NSMutableArray<SAReviewPlainObject *> *reviewPlainObjects = [NSMutableArray array];
    for (Review *managedObjectReview in managedObjectReviews) {
        SAReviewPlainObject *productPlainObject = [SAReviewPlainObject new];
        [self.reviewMapper fillObject:productPlainObject withObject:managedObjectReview];
        [reviewPlainObjects addObject:productPlainObject];
    }
    return reviewPlainObjects;
}

@end

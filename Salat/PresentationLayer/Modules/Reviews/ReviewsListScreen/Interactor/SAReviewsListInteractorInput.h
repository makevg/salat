//
//  SAReviewsListInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAReviewsListInteractorInput <NSObject>

- (void)obtainReviews;
- (void)obtainMyReviews;
- (void)obtainReviewsFromNetwork;
- (BOOL)checkAuthorization;

@end

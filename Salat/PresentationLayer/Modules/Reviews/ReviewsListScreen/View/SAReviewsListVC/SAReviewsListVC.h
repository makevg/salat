//
//  SAReviewsListVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAReviewsListViewInput.h"

@class SAReviewsListDataDisplayManager;
@protocol SAReviewsListViewOutput;

@interface SAReviewsListVC : SABaseVC <SAReviewsListViewInput>

@property (nonatomic) id<SAReviewsListViewOutput> output;
@property (nonatomic) SAReviewsListDataDisplayManager *dataDisplayManager;

@end

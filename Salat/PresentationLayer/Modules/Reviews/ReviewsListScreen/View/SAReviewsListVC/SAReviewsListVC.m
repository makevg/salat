//
//  SAReviewsListVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListVC.h"
#import "SAReviewsListView.h"
#import "SAReviewsListViewOutput.h"
#import "SAReviewsListDataDisplayManager.h"
#import "UIScrollView+EmptyDataSet.h"
#import "SAIndicatorViewHelper.h"

NSString *const cReviewsStoryboardName = @"SAReviews";

@interface SAReviewsListVC () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (strong, nonatomic) IBOutlet SAReviewsListView *contentView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *writeReviewButton;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;
@property (nonatomic) NSError *error;
@end

@implementation SAReviewsListVC {
    BOOL userIsAuthorized;
}

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }

    return _indicator;
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cReviewsStoryboardName;
}

+ (BOOL)isInitial {
    return YES;
}

- (void)configureController {
    [super configureController];

    self.contentView.tableView.emptyDataSetSource = self;
    self.contentView.tableView.emptyDataSetDelegate = self;
    self.dataDisplayManager.tableView = self.contentView.tableView;
    [self.output didTriggerViewDidLoadEvent];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    [super networkStateDidChanged:notification];
    
    [self.output networkDidChanged:[notification.object boolValue]];
}

#pragma mark - Actions

- (IBAction)segmentedControlSwitched:(UISegmentedControl *)sender {
    NSInteger selectedSegmentIndex = sender.selectedSegmentIndex;
    [self.output didSwitchedReviewsSegmentedControl:(SAReviewsSelectedSegmentIndex) selectedSegmentIndex];
}

- (IBAction)tappedNewReviewButton:(id)sender {
    [self.output didTapNewReviewButton];
}

#pragma mark - SAReviewsListViewInput

- (void)userIsAuthorized:(BOOL)isAuthorized {
    userIsAuthorized = isAuthorized;
}

- (void)updateWithReviews:(NSArray<SAReviewPlainObject *> *)reviews {
    [self.dataDisplayManager configureDataDisplayManagerWithReviews:reviews];
    [self.contentView.tableView reloadData];
}

- (void)enableWriteReviewButton:(BOOL)enable {
    self.writeReviewButton.enabled = enable;
}

- (void)showIndicator:(BOOL)show {
   show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)setErrorState:(NSError *)error {
    self.error = error;
    
    if ([self.dataDisplayManager hasData]) {
        [self showErrorMessage:error.localizedDescription];
    } else {
        [self.contentView.tableView reloadData];
    }
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Нет отзывов";
    
    if (![self.dataDisplayManager hasData]) {
        text = self.error ? self.error.localizedDescription : text;
    }
    
    if (!userIsAuthorized && self.contentView.segmentedControl.selectedSegmentIndex == 1) {
        text = @"Войдите, чтобы написать отзыв!";
    }

    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
            NSForegroundColorAttributeName: [UIColor lightGrayColor]};

    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Нам интересно знать, что вы думаете о нашем сервисе";

    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;

    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:14.f],
            NSForegroundColorAttributeName: [UIColor lightGrayColor],
            NSParagraphStyleAttributeName: paragraph};
    
    if (!userIsAuthorized && self.contentView.segmentedControl.selectedSegmentIndex == 1) {
        return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    }
    
    return nil;
}

- (UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    
    if (!userIsAuthorized && self.contentView.segmentedControl.selectedSegmentIndex == 1) {
        return [UIImage imageNamed:@"btn_auth_back"];
    }
    
    if (![self.dataDisplayManager hasData]) {
        return self.error ? [UIImage imageNamed:@"btn_auth_back"] : nil;
    }
    
    return nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
            NSForegroundColorAttributeName: [SAStyle mediumGreenColor]};
    
    if (!userIsAuthorized && self.contentView.segmentedControl.selectedSegmentIndex == 1) {
        return [[NSAttributedString alloc] initWithString:@"Войти" attributes:attributes];
    }
    
    if (![self.dataDisplayManager hasData]) {
        return self.error ? [[NSAttributedString alloc] initWithString:@"Обновить" attributes:attributes] : nil;
    }
    
    return  nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -10.f;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    
    if (!userIsAuthorized && self.contentView.segmentedControl.selectedSegmentIndex == 1) {
        [self.output didTapSignInButton];
    } else {
        [self.output didTapUpdateButton];
    }
}

@end

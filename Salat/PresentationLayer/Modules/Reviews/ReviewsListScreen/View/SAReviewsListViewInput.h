//
//  SAReviewsListViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAReviewPlainObject;

@protocol SAReviewsListViewInput <NSObject>

- (void)userIsAuthorized:(BOOL)isAuthorized;

- (void)updateWithReviews:(NSArray<SAReviewPlainObject *> *)reviews;

- (void)enableWriteReviewButton:(BOOL)enable;

- (void)showIndicator:(BOOL)show;

- (void)setErrorState:(NSError *)error;

@end

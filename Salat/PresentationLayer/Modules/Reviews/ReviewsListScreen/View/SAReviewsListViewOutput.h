//
//  SAReviewsListViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SAReviewsSelectedSegmentIndex) {
    SAReviewsSelectedSegmentIndexAll = 0,
    SAReviewsSelectedSegmentIndexMy
};

@protocol SAReviewsListViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didSwitchedReviewsSegmentedControl:(SAReviewsSelectedSegmentIndex)selectedSegmentIndex;
- (void)didTapNewReviewButton;
- (void)didTapSignInButton;
- (void)didTapUpdateButton;
- (void)networkDidChanged:(BOOL)state;

@end

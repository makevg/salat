//
//  SAReviewsListView.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListView.h"

@implementation SAReviewsListView

#pragma mark - Setup

- (void)setup {
    self.segmentedControl.tintColor = [SAStyle backgroundColor];
    [self configureTableVeiew];
}

- (void)configureTableVeiew {
    self.tableView.backgroundColor = [SAStyle whiteColor];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
}

@end

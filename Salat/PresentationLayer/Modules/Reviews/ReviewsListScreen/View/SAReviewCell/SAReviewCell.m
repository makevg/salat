//
//  SAReviewCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 18.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewCell.h"
#import "SARateView.h"
#import "SADataFormatter.h"
#import "SAReviewPlainObject.h"

@interface SAReviewCell ()
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet SARateView *rateView;
@end

@implementation SAReviewCell

#pragma mark - Configure

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAReviewPlainObject class]]) {
        [self configureByReview:model];
    }
}

- (void)configureCell {
    self.nicknameLabel.font = [SAStyle regularFontOfSize:15.f];
    
    self.dateLabel.font = [SAStyle regularFontOfSize:12.f];
    self.dateLabel.textColor = [SAStyle grayColor];
    
    self.contentLabel.font = [SAStyle regularFontOfSize:14.f];
    self.contentLabel.textColor = [SAStyle grayColor];
    
    self.rateView.backgroundColor = [SAStyle whiteColor];
    self.rateView.alignment = RateViewAlignmentRight;
}

#pragma mark - Private

- (void)configureByReview:(SAReviewPlainObject *)review {
    self.nicknameLabel.text = review.userName;
    self.dateLabel.text = [SADataFormatter stringByUnixTimeStamp:review.publishedAt];
    self.contentLabel.text = review.descr;
    self.rateView.rate = [review.rate floatValue];
}

@end

//
//  SAReviewsListDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAReviewPlainObject;

@interface SAReviewsListDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;

- (void)configureDataDisplayManagerWithReviews:(NSArray<SAReviewPlainObject *> *)reviews;

- (BOOL)hasData;

@end

//
//  SAReviewsListDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListDataDisplayManager.h"
#import "SAReviewCell.h"
#import "SAReviewPlainObject.h"
#import "SAUserService.h"
#import "SAStyle.h"

@interface SAReviewsListDataDisplayManager ()
@property (nonatomic) NSArray *reviews;
@end

@implementation SAReviewsListDataDisplayManager

#pragma mark - Lazy init

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - Public

- (void)configureDataDisplayManagerWithReviews:(NSArray<SAReviewPlainObject *> *)reviews {
    self.reviews = reviews;
}

- (BOOL)hasData {
    return self.reviews.count > 0;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.reviews count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SAReviewCell cellIdentifier];
    SAReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                forIndexPath:indexPath];
    [cell setModel:self.reviews[(NSUInteger) indexPath.row]];
    return cell;
}

@end

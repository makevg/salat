//
//  SAReviewsListPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAReviewsListPresenter.h"
#import "SAReviewsListViewInput.h"
#import "SAReviewsListInteractorInput.h"
#import "SAReviewsListRouterInput.h"

@implementation SAReviewsListPresenter

#pragma mark - Memory managment

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self addObservers];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateReviews)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateReviews {
    [self.view showIndicator:YES];
    [self.interactor obtainReviewsFromNetwork];
}

#pragma mark - SAReviewsListViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view showIndicator:YES];
    [self.view userIsAuthorized:[self.interactor checkAuthorization]];
    [self.interactor obtainReviews];
    [self.view enableWriteReviewButton:[self.interactor checkAuthorization]];
}

- (void)didSwitchedReviewsSegmentedControl:(SAReviewsSelectedSegmentIndex)selectedSegmentIndex {
    switch (selectedSegmentIndex) {
        case SAReviewsSelectedSegmentIndexAll:
            [self.interactor obtainReviews];
            break;
        case SAReviewsSelectedSegmentIndexMy:
            [self.interactor obtainMyReviews];
            break;
    }
}

- (void)didTapNewReviewButton {
    [self.router openWriteNewReviewModule];
}

- (void)didTapSignInButton {
    [self.router openSignInModule];
}

- (void)didTapUpdateButton {
    [self updateReviews];
}

- (void)networkDidChanged:(BOOL)state {
    if (state) {
        [self updateReviews];
    }
}

#pragma mark - SAReviewsListInteractorOutput

- (void)didObtainReviews:(NSArray<SAReviewPlainObject *> *)reviews {
    [self.view showIndicator:NO];
    [self.view updateWithReviews:reviews];
}

- (void)didObtainError:(NSError *)error {
    [self.view showIndicator:NO];
    [self.view setErrorState:error];
}

@end

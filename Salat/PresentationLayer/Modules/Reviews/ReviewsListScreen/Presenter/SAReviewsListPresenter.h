//
//  SAReviewsListPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAReviewsListViewOutput.h"
#import "SAReviewsListInteractorOutput.h"

@protocol SAReviewsListViewInput;
@protocol SAReviewsListInteractorInput;
@protocol SAReviewsListRouterInput;

@interface SAReviewsListPresenter : NSObject <SAReviewsListViewOutput, SAReviewsListInteractorOutput>

@property (weak, nonatomic) id<SAReviewsListViewInput> view;
@property (nonatomic) id<SAReviewsListInteractorInput> interactor;
@property (nonatomic) id<SAReviewsListRouterInput> router;

@end

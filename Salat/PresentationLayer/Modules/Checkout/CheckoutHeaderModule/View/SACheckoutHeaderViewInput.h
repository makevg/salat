//
//  SACheckoutHeaderViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SACheckoutHeaderViewInput <NSObject>

- (void)updateWithItemsCount:(NSNumber *)itemsCount;
- (void)updateWithAmount:(NSNumber *)amount;
- (void)setTotalDiscount:(NSNumber *)totalDiscount;

@end

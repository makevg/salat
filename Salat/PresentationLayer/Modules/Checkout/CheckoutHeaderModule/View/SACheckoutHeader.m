//
//  SACheckoutHeader.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutHeader.h"
#import "NSString+Currency.h"
#import "SACheckoutHeaderViewOutput.h"
#import "SADataFormatter.h"

@interface SACheckoutHeader ()
@property (weak, nonatomic) IBOutlet UILabel *itemsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumValueLabel;
@end

@implementation SACheckoutHeader

#pragma mark - Super

- (void)setup {
    [super setup];
    
    [self prepareSubviews];
    [self.output didTriggerViewLoadedEvent];
}

#pragma mark - Public

+ (SACheckoutHeader *)checkoutHeader {
    return (SACheckoutHeader *)[SABaseView loadViewFromNib:NSStringFromClass([self class])];
}

#pragma mark - Private

- (void)prepareSubviews {
    self.backgroundColor = [SAStyle lightGreenColor];
    self.itemsCountLabel.font = [SAStyle regularFontOfSize:15.f];
    self.itemsCountLabel.textColor = [SAStyle whiteColor];
    self.sumLabel.font = [SAStyle regularFontOfSize:15.f];
    self.sumLabel.textColor = [SAStyle darkGreenColor];
    self.sumValueLabel.font = [SAStyle regularFontOfSize:15.f];
    self.sumValueLabel.textColor = [SAStyle whiteColor];
}

#pragma mark - SACheckoutHeaderViewInput

- (void)updateWithItemsCount:(NSNumber *)itemsCount {
    self.itemsCountLabel.text = [SADataFormatter stringByProductsCount:[itemsCount integerValue]];
}

- (void)updateWithAmount:(NSNumber *)amount {
    self.sumValueLabel.text = [NSString priceWithCurrencySymbol:amount];
}

- (void)setTotalDiscount:(NSNumber *)totalDiscount {
    [self.output setTotalDiscount:totalDiscount];
}

@end

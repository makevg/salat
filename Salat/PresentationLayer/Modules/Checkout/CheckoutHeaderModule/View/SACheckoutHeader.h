//
//  SACheckoutHeader.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"
#import "SACheckoutHeaderViewInput.h"

@protocol SACheckoutHeaderViewOutput;

@interface SACheckoutHeader : SABaseView <SACheckoutHeaderViewInput>

@property (nonatomic) id<SACheckoutHeaderViewOutput> output;

+ (SACheckoutHeader *)checkoutHeader;

@end

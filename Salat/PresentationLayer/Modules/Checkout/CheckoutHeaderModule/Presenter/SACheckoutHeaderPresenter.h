//
//  SACheckoutHeaderPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SACheckoutHeaderViewOutput.h"
#import "SACheckoutHeaderInteractorOutput.h"

@protocol SACheckoutHeaderViewInput;
@protocol SACheckoutHeaderInteractorInput;
@protocol SACheckoutHeaderRouterInput;

@interface SACheckoutHeaderPresenter : NSObject <SACheckoutHeaderViewOutput, SACheckoutHeaderInteractorOutput>

@property (weak, nonatomic) id<SACheckoutHeaderViewInput> view;
@property (nonatomic) id<SACheckoutHeaderInteractorInput> interactor;
@property (nonatomic) id<SACheckoutHeaderRouterInput> router;

@end

//
//  SACheckoutHeaderPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutHeaderPresenter.h"
#import "SACheckoutHeaderViewInput.h"
#import "SACheckoutHeaderInteractorInput.h"

@interface SACheckoutHeaderPresenter ()

@property(nonatomic, strong) NSNumber *totalDiscount;
@property (nonatomic, strong) NSNumber *itemsSum;

@end

@implementation SACheckoutHeaderPresenter

#pragma mark - SACheckoutHeaderViewOutput

- (void)didTriggerViewLoadedEvent {
    [self.interactor obtainCartItemsCount];
    [self.interactor obtainCartItemsSum];
}

- (void)setTotalDiscount:(NSNumber *)discount {
    _totalDiscount = discount;
    [self updatePrice];
}

#pragma mark - SACheckoutHeaderInteractorOutput

- (void)didObtainCartItemsCount:(NSNumber *)itemsCount {
    [self.view updateWithItemsCount:itemsCount];
}

- (void)didObtainCartItemsSum:(NSNumber *)itemsSum {
    _itemsSum = itemsSum;
    [self updatePrice];
}

- (void)updatePrice {
    if( _itemsSum && _totalDiscount ) {
        [self.view updateWithAmount:@((int)(_itemsSum.doubleValue - (_itemsSum.doubleValue * _totalDiscount.doubleValue)/100))];
    }
}

@end

//
//  SACheckoutHeaderRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SACheckoutHeaderRouterInput.h"

@interface SACheckoutHeaderRouter : NSObject <SACheckoutHeaderRouterInput>

@end

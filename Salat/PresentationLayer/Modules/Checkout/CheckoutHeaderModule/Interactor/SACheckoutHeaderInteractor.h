//
//  SACheckoutHeaderInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SACheckoutHeaderInteractorInput.h"

@protocol SACheckoutHeaderInteractorOutput;
@class SACartService;

@interface SACheckoutHeaderInteractor : NSObject <SACheckoutHeaderInteractorInput>

@property (weak, nonatomic) id<SACheckoutHeaderInteractorOutput> output;
@property (nonatomic) SACartService *cartService;

@end

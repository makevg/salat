//
//  SACheckoutHeaderInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SACheckoutHeaderInteractorOutput <NSObject>

- (void)didObtainCartItemsCount:(NSNumber *)itemsCount;

- (void)didObtainCartItemsSum:(NSNumber *)itemsCount;

@end

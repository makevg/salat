//
//  SACheckoutHeaderInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutHeaderInteractor.h"
#import "SACheckoutHeaderInteractorOutput.h"
#import "SACartService.h"

@implementation SACheckoutHeaderInteractor

#pragma mark -

- (void)obtainCartItemsCount {
    [self.output didObtainCartItemsCount:@([[self.cartService obtainCartItems] count])];
}

- (void)obtainCartItemsSum {
    [self.output didObtainCartItemsSum:@([self.cartService obtainTotalAmount])];
}

@end

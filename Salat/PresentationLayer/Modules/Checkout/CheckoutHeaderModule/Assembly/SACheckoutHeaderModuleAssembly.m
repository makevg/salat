//
//  SACheckoutHeaderModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutHeaderModuleAssembly.h"
#import "SACheckoutHeader.h"
#import "SACheckoutHeaderPresenter.h"
#import "SACheckoutHeaderInteractor.h"
#import "SACheckoutHeaderRouter.h"

@implementation SACheckoutHeaderModuleAssembly

- (SACheckoutHeader *)viewCheckoutHeader {
    return [TyphoonDefinition withClass:[SACheckoutHeader class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(checkoutHeader)];
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCheckoutHeader]];
    }];
}

- (SACheckoutHeaderPresenter *)presenterCheckoutHeader {
    return [TyphoonDefinition withClass:[SACheckoutHeaderPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCheckoutHeader]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCheckoutHeader]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCheckoutHeader]];
                          }];
}

- (SACheckoutHeaderInteractor *)interactorCheckoutHeader {
    return [TyphoonDefinition withClass:[SACheckoutHeaderInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCheckoutHeader]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                          }];
}

- (SACheckoutHeaderRouter *)routerCheckoutHeader {
    return [TyphoonDefinition withClass:[SACheckoutHeaderRouter class]
                          configuration:^(TyphoonDefinition *definition) {
    }];
}

@end

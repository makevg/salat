//
//  SACheckoutPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Crashlytics/Crashlytics/CLSLogging.h>
#import "SACheckoutPresenter.h"
#import "SACheckoutViewInput.h"
#import "SACheckoutRouterInput.h"
#import "SACheckoutPresenterStateStorage.h"
#import "SACheckoutPlainObject.h"
#import "SAAddressPlainObject.h"
#import "STKWebKitModalViewController.h"
#import "SCLAlertView.h"
#import "SAStyle.h"
#import "SACartObject.h"
#import "SACartCalculationPO.h"

NSString *const cOnlinePayUrlMessage = @"https://api.salat.mostcreative.ru/orders/%@/payments/create";

@interface SACheckoutPresenter () <STKWebKitModalViewControllerDelegate>

@property(nonatomic, strong) NSArray <SAAddressPlainObject *> *addresses;
@property(nonatomic, strong) SAAddressPlainObject *choosenAddress;
@property(nonatomic, strong) SACheckoutPlainObject *checkingAddress;
@property(nonatomic, strong) SAProfilePlainObject *profile;
@property(nonatomic, strong) NSNumber *cartSum;
@property(nonatomic, strong) NSNumber *deliverySum;
@property(nonatomic, strong) SACartObject *cart;
@property(nonatomic, strong) NSError *error;

@end

@implementation SACheckoutPresenter

#pragma mark - SACheckoutViewOutput

- (void)networkChanged:(BOOL)hasNetwork {
    if (hasNetwork && self.error && !self.profile) {
        self.error = nil;

        [self.view setLoadingState:YES];
        [self.interactor checkSession];
    }
}

- (NSNumber *)getDiscount{
    return self.cart.discountInfo ? self.cart.discountInfo.percent : @0;
}

- (void)setDeliverySum:(NSNumber *)deliverySum {
    _deliverySum = deliverySum;

    [self.view setCartSum:self.cartSum
        withDeliveryPrice:self.deliverySum];

    [self.view enableSendButton:self.deliverySum && self.profile];
}

- (void)setProfile:(SAProfilePlainObject *)profile {
    _profile = profile;

    [self.view enableSendButton:self.deliverySum && self.profile];
}

- (void)didTriggerViewDidLoadEvent {
    self.presenterStateStorage = [[SACheckoutPresenterStateStorage alloc] init];
    [self.view setLoadingState:YES];
    [self.view enableSendButton:NO];
    [self.interactor checkSession];
    self.cartSum = @(self.cart.totalAmount);
    self.deliverySum = nil;
    self.checkingAddress = nil;
}

- (void)didTapOrderButton {
    [self.view setLoadingState:YES];
    [self.interactor sendOrder:[self.view collectData]];
}

- (void)didTapValidatePhoneButton:(NSString *)phone {
    [self.interactor validatePhone:phone];
}

- (void)didTapMapButton {
    [self.router openMapModuleWithOutput:self
                              coordinate:CLLocationCoordinate2DMake(0, 0)];
}

- (void)didChangeAddressText {
    self.deliverySum = nil;
    SEL findAddressSelector = @selector(obtainDeliverySum:);
    [NSObject cancelPreviousPerformRequestsWithTarget:self.interactor selector:findAddressSelector object:self.checkingAddress];

    SACheckoutPlainObject *plainObject = [self.view collectData];
    self.checkingAddress = plainObject;

    if ([plainObject.address.street length] < 3 || ![plainObject.address.house length]) {
        return;
    }

    [self.interactor performSelector:findAddressSelector withObject:self.checkingAddress afterDelay:1.0f];
}

- (void)didChooseAddress:(NSUInteger)index {
    self.choosenAddress = [self.addresses objectAtIndex:index];
    [self.view setAddress:self.choosenAddress];
}

- (void)handleAfterOnlinePay:(BOOL)isPaid forNumber:(NSNumber *)orderId {
    if (isPaid) {
        [self.router openSuccessScreen:[NSString stringWithFormat:@"Спасибо за ваш заказ. Его номер %@", orderId]];
    } else {
        [self.view.view setUserInteractionEnabled:NO];

        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        alert.showAnimationType = FadeIn;
        alert.hideAnimationType = FadeOut;
        alert.backgroundType = Blur;
        alert.customViewColor = [SAStyle orangeColor];
        alert.iconTintColor = [UIColor whiteColor];

        [alert setTitleFontFamily:[SAStyle regularFontName] withSize:20.f];
        [alert setBodyTextFontFamily:[SAStyle regularFontName] withSize:14.f];
        [alert setButtonsTextFontFamily:[SAStyle boldFontName] withSize:14.f];

        [alert addButton:@"Повторить" actionBlock:^(void) {
            [self runOnlinePay:orderId];
        }];

        [alert addButton:@"Отмена" actionBlock:^(void) {
        }];

        [alert addTimerToButtonIndex:0 reverse:NO];

        [self.view setLoadingState:NO];
        [alert showInfo:@"Внимание"
               subTitle:[NSString stringWithFormat:@"Вы не произвели онлайн оплату. Хотите повторить?"]
       closeButtonTitle:nil
               duration:5.0f];
    }
}

- (void)didCheckPayment:(BOOL)isPaid forOrder:(NSNumber *)orderId {
    if ([self.view.navigationController.presentedViewController isKindOfClass:[STKWebKitModalViewController class]]) {
        [self.view.navigationController.presentedViewController dismissViewControllerAnimated:YES
                                                                                   completion:^{
                                                                                       [self handleAfterOnlinePay:isPaid forNumber:orderId];
                                                                                   }];
    } else {
        [self handleAfterOnlinePay:isPaid forNumber:orderId];
    }
}

- (void)didChangeDeliveryMethod:(SACheckoutDeliveryMethodType)deliveryMethodType {
    switch (deliveryMethodType) {
        case SACheckoutDeliveryMethodTypeCourier:
            self.presenterStateStorage.isOnlinePayment = NO;
            break;
        case SACheckoutDeliveryMethodTypeOnline:
            self.presenterStateStorage.isOnlinePayment = YES;
            break;
    }
}

- (void)didObtainError:(NSError *)error {
    [self.view setLoadingState:NO];
    self.error = error;

    if (error.code == 401) {
        [self.view setProfile:nil];
    } else {
        [self.view setErrorState:error];
    }
}

- (void)didSessionChecked {
    [self.interactor obtainUserProfile];
    [self.interactor obtainAddresses];
}

- (void)didObtainProfile:(SAProfilePlainObject *)profilePlainObject {
    [self.view setLoadingState:NO];

    self.profile = profilePlainObject;
    [self.view setProfile:profilePlainObject];
}

- (void)didSendCode:(NSString *)phone {
    [self.router openValidateCodeModule:phone
                        andModuleOutput:self];
}

- (void)didObtainAddressPrice:(NSNumber *)addressPrice forAddress:(SAAddressPlainObject *)address {
    if ([self.checkingAddress.address.street isEqual:address.street] && [self.checkingAddress.address.house isEqual:address.house])
        self.deliverySum = addressPrice;
}

- (void)didObtainAddresses:(NSArray <SAAddressPlainObject *> *)addresses {
    self.addresses = addresses;
    [self.view setAddresses:addresses];
}

- (void)didObtainAddressByLocation:(SAAddressPlainObject *)address {
    [self.view setAddress:address];
}

- (void)didSendOrder:(NSNumber *)orderId {
    if (self.presenterStateStorage.isOnlinePayment) {
        [self runOnlinePay:orderId];
    } else {
        [self.router openSuccessScreen:[NSString stringWithFormat:@"Спасибо за ваш заказ. Его номер %@", orderId]];
    }
}

- (void)runOnlinePay:(NSNumber *)orderId {
    [self.view setLoadingState:YES];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:cOnlinePayUrlMessage, orderId]];
    CLS_LOG(@"online url %@", url);

    [self.interactor checkPayment:orderId];
    STKWebKitModalViewController *controller = [[STKWebKitModalViewController alloc] initWithURL:url];
    controller.closeDelegate = self;
    [self.view presentViewController:controller animated:YES completion:nil];
}

#pragma mark - SAChooseAddressModuleOutput

- (void)chooseLocation:(YMKMapCoordinate)coordinate {
    if (coordinate.latitude > 0 && coordinate.longitude > 0) {
        [self.interactor obtainAddressByLocation:coordinate];
    }
}

#pragma mark - SASecondStepPhoneConfirmationModuleOutput

- (void)configure:(NSString *)phone {
    [self.interactor obtainUserProfile];
    [self.interactor obtainAddresses];
}

- (void)setCartObject:(SACartObject *)cartObject {
    self.cart = cartObject;
}

- (void)didPressedCloseButton {
    [self.interactor stopCheck];
}

@end

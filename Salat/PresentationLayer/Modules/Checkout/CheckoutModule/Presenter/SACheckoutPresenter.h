//
//  SACheckoutPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SACheckoutViewOutput.h"
#import "SAChooseAddressModuleOutput.h"
#import "SASecondStepPhoneConfirmationModuleOutput.h"
#import "SACheckoutInteractor.h"

@protocol SACheckoutViewInput;
@protocol SACheckoutInteractorInput;
@protocol SACheckoutRouterInput;
@class SACheckoutPresenterStateStorage;
@class SACartObject;

@protocol SACheckoutModuleInput

- (void)setCartObject:(SACartObject *)cartObject;

@end

@interface SACheckoutPresenter : NSObject <SACheckoutModuleInput, SACheckoutViewOutput, SACheckoutInteractorOutput, SAChooseAddressModuleOutput, SASecondStepPhoneConfirmationModuleOutput>

@property(weak, nonatomic) UIViewController<SACheckoutViewInput> * view;
@property(nonatomic) NSObject <SACheckoutInteractorInput>* interactor;
@property(nonatomic) id <SACheckoutRouterInput> router;
@property(nonatomic) SACheckoutPresenterStateStorage *presenterStateStorage;

@end

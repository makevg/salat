//
// Created by Maximychev Evgeny on 08.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SACheckoutPresenterStateStorage : NSObject

@property (nonatomic) Boolean isOnlinePayment;

@end
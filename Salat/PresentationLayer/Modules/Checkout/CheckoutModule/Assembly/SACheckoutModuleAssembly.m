//
//  SACheckoutModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutModuleAssembly.h"
#import "SACheckoutVC.h"
#import "SACheckoutPresenter.h"
#import "SACheckoutRouter.h"

@implementation SACheckoutModuleAssembly

- (SACheckoutVC *)viewCheckout {
    return [TyphoonDefinition withClass:[SACheckoutVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCheckout]];
                          }];
}

- (SACheckoutPresenter *)presenterCheckout {
    return [TyphoonDefinition withClass:[SACheckoutPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCheckout]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCheckout]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCheckout]];
                          }];
}

- (SACheckoutInteractor *)interactorCheckout {
    return [TyphoonDefinition withClass:[SACheckoutInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCheckout]];
                              [definition injectProperty:@selector(orderService)
                                                    with:[self.serviceComponents ordersService]];
                              [definition injectProperty:@selector(addressService)
                                                    with:[self.serviceComponents addressService]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
                              [definition injectProperty:@selector(profileMapper)
                                                    with:[self.coreComponents profileMapper]];
                              [definition injectProperty:@selector(addressMapper)
                                                    with:[self.coreComponents addressMapper]];
                          }];
}

- (SACheckoutRouter *)routerCheckout {
    return [TyphoonDefinition withClass:[SACheckoutRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCheckout]];
    }];
}

@end

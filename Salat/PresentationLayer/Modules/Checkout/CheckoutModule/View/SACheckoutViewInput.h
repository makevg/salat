//
//  SACheckoutViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SACheckoutPlainObject;
@class SAProfilePlainObject;
@class SAAddressPlainObject;

@protocol SACheckoutViewInput <NSObject>

- (void)setErrorState:(NSError *)error;

- (void)setProfile:(SAProfilePlainObject *)profile;

- (void)setLoadingState:(BOOL)loading;

- (void)setCartSum:(NSNumber *)cartSum withDeliveryPrice:(NSNumber *)deliveryPrice;

- (void)setAddress:(SAAddressPlainObject *)address;

- (void)setAddresses:(NSArray <SAAddressPlainObject *> *)addresses;

- (void)enableSendButton:(BOOL)enabled;

- (SACheckoutPlainObject *)collectData;

@end

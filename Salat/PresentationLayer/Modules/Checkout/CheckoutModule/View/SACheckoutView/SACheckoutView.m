//
//  SACheckoutView.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutView.h"
#import "SABorderedPhoneField.h"
#import "SABorderedField.h"
#import "SAAddressPlainObject.h"
#import "SADataFormatter.h"
#import "DateTools.h"
#import "PCAngularActivityIndicatorView.h"
#import "SAIndicatorViewHelper.h"

@interface SACheckoutView ()

@property(strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelsCollection;
@property(weak, nonatomic) IBOutlet UILabel *paymentLabel;
@property(weak, nonatomic) IBOutlet UILabel *finalSumUpLabel;
@property(weak, nonatomic) IBOutlet UIView *contentView;

@property(nonatomic, strong) PCAngularActivityIndicatorView *indicator;

@end

@implementation SACheckoutView

#pragma mark - Setup

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self];
        _indicator.color = [SAStyle lightGreenColor];
    }

    return _indicator;
}

- (void)setup {
    [self.labelsCollection enumerateObjectsUsingBlock:^(UILabel *label, NSUInteger idx, BOOL *stop) {
        label.font = [SAStyle regularFontOfSize:15.f];
    }];

    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTimeTextField:) forControlEvents:UIControlEventValueChanged];
    datePicker.minimumDate = [NSDate new];
    datePicker.maximumDate = [[NSDate new] dateByAddingDays:1];

    [self.timeField setInputView:datePicker];

    self.paymentSegmentedControl.tintColor = [SAStyle mediumGreenColor];
//    [self.paymentSegmentedControl setEnabled:NO forSegmentAtIndex:SACheckoutDeliveryMethodTypeOnline];

    self.paymentLabel.font = [SAStyle regularFontOfSize:11.f];
    self.paymentLabel.textColor = [SAStyle grayColor];

    self.chooseAddressButton.hidden = YES;

    [self.phoneField.formatter setDefaultOutputPattern:@"(###) ###-##-##"];
    [self.phoneField.formatter setPrefix:@"+7 "];

    self.finalSumLabel.font = [SAStyle regularFontOfSize:24.f];
    self.finalSumUpLabel.font = [SAStyle regularFontOfSize:17.f];
}

- (void)showLoading {
    [self.indicator startAnimating];
    self.indicator.hidden = NO;
    self.userInteractionEnabled = NO;
}

- (void)hideLoading {
    [self.indicator stopAnimating];
    self.indicator.hidden = YES;
    self.userInteractionEnabled = YES;
}

- (void)updateTimeTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker *) self.timeField.inputView;
    self.timeField.text = [NSString stringWithFormat:@"%@", [SADataFormatter stringByDate:picker.date withFormat:@"HH:mm"]];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAAddressPlainObject class]]) {
        SAAddressPlainObject *addressPlainObject = model;
        self.streetField.text = addressPlainObject.street;
        self.houseField.text = addressPlainObject.house;
        self.apartmentField.text = addressPlainObject.flat;
        self.entranceField.text = addressPlainObject.entrance;
        self.floorField.text = addressPlainObject.floor;
        self.intercodeField.text = addressPlainObject.intercomCode;

        [self.streetField sendActionsForControlEvents:UIControlEventEditingChanged];
        [self.houseField sendActionsForControlEvents:UIControlEventEditingChanged];
    }
}

@end

//
//  SACheckoutView.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@class SABorderedField;
@class SABorderedPhoneField;
@class SABorderButton;

@interface SACheckoutView : SABaseView

@property(weak, nonatomic) IBOutlet SABorderedField *nameField;
@property(weak, nonatomic) IBOutlet SABorderedPhoneField *phoneField;
@property(weak, nonatomic) IBOutlet SABorderButton *validateButton;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *validateHeight;
@property(weak, nonatomic) IBOutlet SABorderedField *cityField;
@property(weak, nonatomic) IBOutlet SABorderedField *streetField;
@property(weak, nonatomic) IBOutlet SABorderedField *houseField;
@property(weak, nonatomic) IBOutlet SABorderedField *apartmentField;
@property(weak, nonatomic) IBOutlet SABorderedField *entranceField;
@property(weak, nonatomic) IBOutlet SABorderedField *floorField;
@property(weak, nonatomic) IBOutlet SABorderedField *intercodeField;
@property(weak, nonatomic) IBOutlet SABorderedField *timeField;
@property(weak, nonatomic) IBOutlet UISegmentedControl *paymentSegmentedControl;
@property(weak, nonatomic) IBOutlet SABorderButton *orderButton;
@property(weak, nonatomic) IBOutlet UIButton *mapButton;
@property(weak, nonatomic) IBOutlet UIButton *chooseAddressButton;
@property(weak, nonatomic) IBOutlet UILabel *finalSumLabel;

- (void)showLoading;

- (void)hideLoading;

@end

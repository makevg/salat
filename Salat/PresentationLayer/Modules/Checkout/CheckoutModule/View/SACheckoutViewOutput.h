//
//  SACheckoutViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SACheckoutDeliveryMethodType) {
    SACheckoutDeliveryMethodTypeCourier = 0,
    SACheckoutDeliveryMethodTypeOnline
};

@protocol SACheckoutViewOutput <NSObject>

- (void)networkChanged:(BOOL)hasNetwork;

- (NSNumber *)getDiscount;

- (void)didTriggerViewDidLoadEvent;

- (void)didTapOrderButton;

- (void)didTapValidatePhoneButton:(NSString *) phone;

- (void)didTapMapButton;

- (void)didChangeAddressText;

- (void)didChooseAddress:(NSUInteger)index;

- (void)didChangeDeliveryMethod:(SACheckoutDeliveryMethodType)deliveryMethodType;

@end

//
//  SACheckoutVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
#import "SACheckoutVC.h"
#import "SACheckoutView.h"
#import "SACheckoutViewOutput.h"
#import "SAAddressPlainObject.h"
#import "SACheckoutPlainObject.h"
#import "SABorderedField.h"
#import "SABorderedPhoneField.h"
#import "SAProfilePlainObject.h"
#import "SABorderButton.h"
#import "NBPhoneNumber.h"
#import "SACheckoutHeader.h"

static NSString *const cCheckoutVCStoryboardName = @"Checkout";

@interface SACheckoutVC () <UIActionSheetDelegate>

@property(strong, nonatomic) IBOutlet SACheckoutView *contentView;
@property(strong, nonatomic) IBOutlet id <SACheckoutHeaderViewInput> checkoutHeader;
@property(strong, nonatomic) NSArray<NSString *> *addressTitles;
@property(nonatomic, strong) NBPhoneNumberUtil *phoneUtil;

@end

@implementation SACheckoutVC

+ (NSString *)storyboardName {
    return cCheckoutVCStoryboardName;
}

- (void)configureController {
    [super configureController];

    [self.checkoutHeader setTotalDiscount:[self.output getDiscount]];

    __weak typeof(self) weakSelf = self;

    self.phoneUtil = [[NBPhoneNumberUtil alloc] init];
    self.contentView.validateButton.enabled = FALSE;

    self.contentView.phoneField.textDidChangeBlock = ^(UITextField *textField) {
        NSError *anError = nil;
        NBPhoneNumber *number = [weakSelf.phoneUtil parse:weakSelf.contentView.phoneField.phoneNumber
                                            defaultRegion:@"RU"
                                                    error:&anError];

        weakSelf.contentView.validateButton.enabled = [weakSelf.phoneUtil isValidNumber:number];
    };

    [self.output didTriggerViewDidLoadEvent];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    [self.output networkChanged:[notification.object boolValue]];
}

#pragma mark - Actions

- (IBAction)tappedMapButton:(id)sender {
    [self.output didTapMapButton];
}

- (IBAction)tappedValidateButton:(id)sender {
    [self.output didTapValidatePhoneButton:self.contentView.phoneField.phoneNumber];
}

- (IBAction)tappedChooseButton:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];

    for (NSString *addr in self.addressTitles) {
        [actionSheet addButtonWithTitle:addr];
    }

    [actionSheet showInView:self.contentView];
}

- (IBAction)tappedOrderButton:(id)sender {
    [self.output didTapOrderButton];
}

- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender {
    [self.output didChangeDeliveryMethod:(SACheckoutDeliveryMethodType) sender.selectedSegmentIndex];
}

- (IBAction)addressChanged {
    [self.output didChangeAddressText];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex)
        return;

    [self.output didChooseAddress:(NSUInteger) buttonIndex - 1];
}

#pragma mark - SACheckoutViewInput

- (void)setErrorState:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

- (void)setProfile:(SAProfilePlainObject *)profile {
    self.contentView.nameField.enabled = true;
    self.contentView.validateButton.hidden = profile != nil;
    self.contentView.validateHeight.constant = profile != nil ? 0 : 40.f;
    self.contentView.orderButton.enabled = profile != nil;

    if (profile) {
        self.contentView.phoneField.enabled = false;
        self.contentView.nameField.text = profile.firstName;
        self.contentView.phoneField.text = profile.phoneNumber;
    } else {
        self.contentView.phoneField.enabled = true;
    }

    [self.contentView hideLoading];
}

- (void)setLoadingState:(BOOL)loading {
    if( loading )
        [self.contentView showLoading];
    else
        [self.contentView hideLoading];
}

- (void)setCartSum:(NSNumber *)cartSum withDeliveryPrice:(NSNumber *)deliveryPrice {
    BOOL notDelivery = deliveryPrice == nil;
    NSString *cartText = [NSString stringWithFormat:@"%@ р. + ", cartSum];
    NSString *deliveryText = [NSString stringWithFormat:@"%@ р.", notDelivery ? @"?" : deliveryPrice];

    NSDictionary *greenAttributes = @{
            NSFontAttributeName : [SAStyle regularFontOfSize:24.f],
            NSForegroundColorAttributeName : [SAStyle mediumGreenColor]
    };

    NSDictionary *redAttributes = @{
            NSFontAttributeName : [SAStyle regularFontOfSize:24.f],
            NSForegroundColorAttributeName : [SAStyle redColor]
    };

    NSDictionary *orangeAttributes = @{
            NSFontAttributeName : [SAStyle regularFontOfSize:24.f],
            NSForegroundColorAttributeName : [SAStyle orangeColor]
    };

    NSNumber *totalSum = @([cartSum integerValue] + [deliveryPrice integerValue]);

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:cartText
                                                                                         attributes:greenAttributes];

    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:deliveryText
                                                                             attributes:notDelivery ? redAttributes : greenAttributes]];

    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" = "
                                                                             attributes:greenAttributes]];

    [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ р.", notDelivery ? @"?" : totalSum]
                                                                             attributes:notDelivery ? redAttributes : orangeAttributes]];

    self.contentView.finalSumLabel.attributedText = attributedString;
}


- (void)setAddress:(SAAddressPlainObject *)address {
    [self.contentView setModel:address];
}

- (void)setAddresses:(NSArray <SAAddressPlainObject *> *)addresses {
    NSMutableArray <NSString *> *array = [NSMutableArray new];

    NSString *title;

    for (SAAddressPlainObject *address in addresses) {
        if (address.isMain)
            [self setAddress:address];

        if ([address.title length])
            title = address.title;
        else
            title = [NSString stringWithFormat:@"%@, %@", address.street, address.house];

        [array addObject:title];
    }

    self.addressTitles = array;
    self.contentView.chooseAddressButton.hidden = [addresses count] == 0;
}

- (void)enableSendButton:(BOOL)enabled {
    self.contentView.orderButton.enabled = enabled;
}

- (SACheckoutPlainObject *)collectData {
    SACheckoutPlainObject *plainObject = [SACheckoutPlainObject new];

    plainObject.isOnlinePayment = self.contentView.paymentSegmentedControl.selectedSegmentIndex == 1;
    plainObject.deliverToTime = self.contentView.timeField.text;

    SAAddressPlainObject *addressPlainObject = [SAAddressPlainObject new];
    addressPlainObject.street = self.contentView.streetField.text;
    addressPlainObject.house = self.contentView.houseField.text;
    addressPlainObject.flat = self.contentView.apartmentField.text;

    plainObject.address = addressPlainObject;
    plainObject.name = self.contentView.nameField.text;

    return plainObject;
}

@end

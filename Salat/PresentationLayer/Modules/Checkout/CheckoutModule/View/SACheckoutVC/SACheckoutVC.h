//
//  SACheckoutVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SACheckoutViewInput.h"

@protocol SACheckoutViewOutput;

@interface SACheckoutVC : SABaseVC <SACheckoutViewInput>

@property (nonatomic) NSObject<SACheckoutViewOutput> * output;

@end

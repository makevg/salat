//
//  SACheckoutInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol SACheckoutInteractorOutput;
@class SAOrdersService;
@class SAUserService;
@class SACartService;
@class Profile;
@class SAProfileMapper;
@class SACheckoutPlainObject;
@class SAProfilePlainObject;
@class SAAddressPlainObject;
@class SAAddressService;
@protocol SAPrototypeMapper;


@protocol SACheckoutInteractorInput <NSObject>

- (void)sendOrder:(SACheckoutPlainObject *)plainObject;

- (void)checkSession;

- (NSNumber *)obtainCartSum;

- (void)obtainUserProfile;

- (void)obtainAddresses;

- (void)obtainAddressByLocation:(CLLocationCoordinate2D)location;

- (void)validatePhone:(NSString *)phone;

- (void)obtainDeliverySum:(SACheckoutPlainObject *)addressPlainObject;

- (void)checkPayment:(NSNumber *)orderId;

- (void)stopCheck;

@end


@protocol SACheckoutInteractorOutput <NSObject>

- (void)didCheckPayment:(BOOL)isPaid forOrder:(NSNumber *)orderId;

- (void)didObtainError:(NSError *)error;

- (void)didSessionChecked;

- (void)didObtainProfile:(SAProfilePlainObject *)profilePlainObject;

- (void)didSendOrder:(NSNumber *)orderId;

- (void)didSendCode:(NSString *)phone;

- (void)didObtainAddressPrice:(NSNumber *)addressPrice forAddress:(SAAddressPlainObject *)address;

- (void)didObtainAddresses:(NSArray <SAAddressPlainObject *> *)addresses;

- (void)didObtainAddressByLocation:(SAAddressPlainObject *)address;

@end


@interface SACheckoutInteractor : NSObject <SACheckoutInteractorInput>

@property(weak, nonatomic) id <SACheckoutInteractorOutput> output;
@property(nonatomic) SAOrdersService *orderService;
@property(nonatomic) SAUserService *userService;
@property(nonatomic) SACartService *cartService;
@property(nonatomic) SAAddressService *addressService;
@property(nonatomic, strong) SAProfileMapper *profileMapper;
@property(nonatomic, strong) id <SAPrototypeMapper> addressMapper;

@end

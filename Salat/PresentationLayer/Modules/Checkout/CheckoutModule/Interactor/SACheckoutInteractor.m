//
//  SACheckoutInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutInteractor.h"
#import "SAOrdersService.h"
#import "SACheckoutPlainObject.h"
#import "SAUserService.h"
#import "SACartService.h"
#import "CartItem.h"
#import "Profile.h"
#import "SAProfileMapper.h"
#import "SAProfilePlainObject.h"
#import "CartItemModifier.h"
#import "SAAddressService.h"
#import "SAAddressPlainObject.h"
#import "SACommon.h"
#import "SACartItemPlainObject.h"
#import "SACartItemModifierPlainObject.h"
#import "SACartCalculationPO.h"

@implementation SACheckoutInteractor {
    BOOL mNeedStopCheck;
}

- (NSString *)generateOrderToken {
    return [[NSUUID UUID] UUIDString];
}

- (void)sendOrder:(SACheckoutPlainObject *)plainObject {
    __weak typeof(self) weakSelf = self;

    plainObject.token = [self generateOrderToken];

    NSMutableArray *products = [@[] mutableCopy];
    NSArray <CartItem *> *cartItems = [self.cartService obtainCartItems];
    for (CartItem *item in cartItems) {
        SACartItemPlainObject *orderProductPlainObject = [SACartItemPlainObject new];
        orderProductPlainObject.productId = item.product_id;
        orderProductPlainObject.quantity = item.quantity;

        NSMutableArray *modifiers = [@[] mutableCopy];
        for (CartItemModifier *modifier in item.modifiers) {
            SACartItemModifierPlainObject *modifierPlainObject = [SACartItemModifierPlainObject new];
            modifierPlainObject.itemModifierId = modifier.id;
            modifierPlainObject.quantity = modifier.quantity;
            [modifiers addObject:modifierPlainObject];
        }

        orderProductPlainObject.modifiers = modifiers;
        [products addObject:orderProductPlainObject];
    }

    plainObject.products = products;

    [self.orderService sendOrderWithData:plainObject
                              completion:^(NSInteger i) {
                                  if( !plainObject.isOnlinePayment ) {
                                      [weakSelf.cartService clearCart:^{
                                          dispatch_safe_main_async(^{
                                              [weakSelf.output didSendOrder:@(i)];
                                          });
                                      }];
                                  } else {
                                      dispatch_safe_main_async(^{
                                          [weakSelf.output didSendOrder:@(i)];
                                      });
                                  }
                              }
                                   error:^(NSError *error) {
                                       dispatch_safe_main_async(^{
                                           [weakSelf.output didObtainError:error];
                                       });
                                   }];
}

- (void)checkSession {
    weakifySelf;

    [self.userService checkSession:^{
        dispatch_safe_main_async(^{
            [weakSelf.output didSessionChecked];
        })
    }
                             error:^(NSError *error) {
                                 dispatch_safe_main_async(^{
                                     [weakSelf.output didObtainError:error];
                                 })
                             }];
}

- (NSNumber *)obtainCartSum {
    return @((int)[self.cartService obtainTotalAmount]);
}

- (void)obtainUserProfile {
    if (![self.userService isAuthorized]) {
        [self.output didObtainProfile:nil];
        return;
    }

    __weak typeof(self) weakSelf = self;

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        Profile *profile = [weakSelf.userService obtainLocalProfile];
        SAProfilePlainObject *plainObject = [SAProfilePlainObject new];

        [weakSelf.profileMapper fillObject:plainObject withObject:profile];
        dispatch_safe_main_async(^{
            [weakSelf.output didObtainProfile:plainObject];
        });
    });
}

- (void)obtainAddresses {
    if (![self.userService isAuthorized])
        return;

    __weak typeof(self) weakSelf = self;

    [self.addressService obtainAddressesSuccess:^(NSArray<Address *> *dbAddresses) {
                [weakSelf handleAddresses:dbAddresses];
            }
                                          error:^(NSError *error) {
                                              dispatch_safe_main_async(^{
                                                  [weakSelf.output didObtainError:error];
                                              });
                                          }];
}

- (void)obtainAddressByLocation:(CLLocationCoordinate2D)location {
    __weak __typeof(self) weakSelf = self;
    [self.addressService obtainAddressByLocation:location success:^(SAAddressPlainObject *address) {
        dispatch_safe_main_async( ^{
            [weakSelf.output didObtainAddressByLocation:address];
        });
    }                                      error:^(NSError *error) {
        dispatch_safe_main_async( ^{
            [weakSelf.output didObtainError:error];
        });
    }];
}

- (void)validatePhone:(NSString *)phone {
    __weak typeof(self) weakSelf = self;

    [self.userService validatePhoneFirstStepWithPhone:phone
                                           completion:^{
                                               dispatch_safe_main_async(^{
                                                   [weakSelf.output didSendCode:phone];
                                               });
                                           }
                                                error:^(NSError *error) {
                                                    dispatch_safe_main_async(^{
                                                        [weakSelf.output didObtainError:error];
                                                    });
                                                }];
}

- (void)obtainDeliverySum:(SACheckoutPlainObject *)addressPlainObject {
    weakifySelf;

    [self.orderService calculateCartData:addressPlainObject
                              completion:^(SACartCalculationPO *po) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [weakSelf.output didObtainAddressPrice:po.deliveryInfo.sum
                                                                  forAddress:addressPlainObject.address];
                                  });
                              }
                                   error:^(NSError *error) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [weakSelf.output didObtainError:error];
                                       });
                                   }];
}

- (void)checkPayment:(NSNumber *)orderId {
    weakifySelf;
    
    [self.orderService checkPayOrderId:orderId
               withSuccessHandler:^(BOOL result) {
                   if (result || mNeedStopCheck) {
                       [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                                selector:@selector(checkPayment:)
                                                                  object:orderId];
                       
                       mNeedStopCheck = NO;

                       if( result ) {
                           [weakSelf.cartService clearCart:^{
                               dispatch_safe_main_async(^{
                                   [weakSelf.output didCheckPayment:result forOrder:orderId];
                               });
                           }];
                       } else {
                           dispatch_safe_main_async(^{
                               [weakSelf.output didCheckPayment:result forOrder:orderId];
                           });
                       }

                   } else {
                       [self performSelector:@selector(checkPayment:) withObject:orderId afterDelay:2.0];
                   }
               } failureHandler:^(NSError *error) {
                   [self performSelector:@selector(checkPayment:) withObject:orderId afterDelay:2.0];
               }];
}

- (void)stopCheck {
    mNeedStopCheck = YES;
}

#pragma mark - Private

- (void)handleAddresses:(NSArray<Address *> *)dbAddresses {
    weakifySelf;
    NSArray<SAAddressPlainObject *> *plainAddresses = [self getPlainAddressesFromManagedObjects:dbAddresses];
    NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey:@"createdAt"
                                                             ascending:NO];
    NSSortDescriptor *mainSort = [[NSSortDescriptor alloc] initWithKey:@"isMain"
                                                             ascending:NO];
    NSSortDescriptor *nameSort = [[NSSortDescriptor alloc] initWithKey:@"title"
                                                             ascending:YES];

    plainAddresses = [plainAddresses sortedArrayUsingDescriptors:@[mainSort, dateSort, nameSort]];

    dispatch_safe_main_async(^{
        [weakSelf.output didObtainAddresses:plainAddresses];
    });
}

- (NSArray<SAAddressPlainObject *> *)getPlainAddressesFromManagedObjects:(NSArray<Address *> *)managedObjectAddresses {
    NSMutableArray<SAAddressPlainObject *> *addressesPlainObjects = [NSMutableArray array];
    for (Address *managedObjectAddress in managedObjectAddresses) {
        SAAddressPlainObject *addressPlainObject = [SAAddressPlainObject new];
        [self.addressMapper fillObject:addressPlainObject withObject:managedObjectAddress];
        [addressesPlainObjects addObject:addressPlainObject];
    }
    return addressesPlainObjects;
}

@end

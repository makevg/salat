//
//  SACheckoutRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACheckoutRouter.h"
#import "SASuccessVC.h"
#import "SAChooseAddressModuleInput.h"
#import "SAAuthModuleInputStorage.h"
#import "SASecondStepPhoneConfirmationModuleInput.h"
#import "SASecondStepPhoneConfirmationModuleOutput.h"

@implementation SACheckoutRouter

- (void)openSuccessScreen:(NSString *)content {
    SASuccessVC *vc = (SASuccessVC *) [self controllerFromStoryboard:[SASuccessVC storyboardName]];
    [vc configureModuleWithContent:content];
    [self setRootFeature:vc];
}

- (void)openMapModuleWithOutput:(id <RamblerViperModuleOutput>)moduleOutput coordinate:(CLLocationCoordinate2D)coordinate {
    [[self.transitionHandler openModuleUsingSegue:@"kChooseLocation"]
            thenChainUsingBlock:(RamblerViperModuleLinkBlock) ^id <RamblerViperModuleOutput>(id <SAChooseAddressModuleInput> moduleInput) {
                [moduleInput configureModuleWithCoordinate:coordinate];
                return moduleOutput;
            }];
}

- (void)openValidateCodeModule:(NSString *)phone andModuleOutput:(id <SASecondStepPhoneConfirmationModuleOutput>)moduleOutput {
    [[self.transitionHandler openModuleUsingSegue:@"openValidateSegue"]
            thenChainUsingBlock:(RamblerViperModuleLinkBlock) ^id <RamblerViperModuleOutput>(id <SASecondStepPhoneConfirmationModuleInput> moduleInput) {
                SAAuthModuleInputStorage *inputStorage = [SAAuthModuleInputStorage new];
                inputStorage.phone = phone;
                inputStorage.closeModule = YES;
                [moduleInput configureModuleWithAuthStorage:inputStorage];

                return moduleOutput;
            }];
}


#pragma mark - Private

- (UIViewController *)controllerFromStoryboard:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [sb instantiateInitialViewController];
}

- (void)setRootFeature:(UIViewController *)vc {
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}

@end

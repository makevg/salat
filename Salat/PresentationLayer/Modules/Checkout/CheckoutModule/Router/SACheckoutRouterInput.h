//
//  SACheckoutRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 07.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SASecondStepPhoneConfirmationModuleOutput;

@protocol SACheckoutRouterInput <NSObject>

- (void)openSuccessScreen:(NSString *)content;

- (void)openMapModuleWithOutput:(id<RamblerViperModuleOutput>)moduleOutput coordinate:(CLLocationCoordinate2D)coordinate;

- (void)openValidateCodeModule:(NSString *)phone
               andModuleOutput:(id <SASecondStepPhoneConfirmationModuleOutput>)moduleOutput;

@end

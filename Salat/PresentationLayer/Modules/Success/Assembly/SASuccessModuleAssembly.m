//
//  SASuccessModuleAssembly.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASuccessModuleAssembly.h"
#import "SASuccessVC.h"
#import "SASuccessPresenter.h"
#import "SASuccessInteractor.h"
#import "SASuccessRouter.h"
#import "SASuccessPresenterStateStorage.h"

@implementation SASuccessModuleAssembly

- (SASuccessVC *)viewSuccess {
    return [TyphoonDefinition withClass:[SASuccessVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSuccess]];
    }];
}

- (SASuccessPresenter *)presenterSuccess {
    return [TyphoonDefinition withClass:[SASuccessPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSuccess]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSuccess]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSuccess]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorageSuccess]];
    }];
}

- (SASuccessInteractor *)interactorSuccess {
    return [TyphoonDefinition withClass:[SASuccessInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSuccess]];
    }];
}

- (SASuccessRouter *)routerSuccess {
    return [TyphoonDefinition withClass:[SASuccessRouter class]
                          configuration:^(TyphoonDefinition *definition) {
    }];
}

- (SASuccessPresenterStateStorage *)presenterStateStorageSuccess {
    return [TyphoonDefinition withClass:[SASuccessPresenterStateStorage class]];
}

@end

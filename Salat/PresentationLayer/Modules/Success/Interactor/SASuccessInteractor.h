//
//  SASuccessInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASuccessInteractorInput.h"

@protocol SASuccessInteractorOutput;

@interface SASuccessInteractor : NSObject <SASuccessInteractorInput>

@property (weak, nonatomic) id<SASuccessInteractorOutput> output;

@end

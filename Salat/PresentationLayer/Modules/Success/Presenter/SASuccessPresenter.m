//
//  SASuccessPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASuccessPresenter.h"
#import "SASuccessViewInput.h"
#import "SASuccessInteractorInput.h"
#import "SASuccessRouterInput.h"
#import "SASuccessPresenterStateStorage.h"

@implementation SASuccessPresenter

#pragma mark - SASuccessViewOutput

- (void)configureWithContent:(NSString *)content {
    self.presenterStateStorage.content = content;
    [self.router openMainMenu];
}

- (void)didTriggerViewDidLoadEvent {
    [self.view updateWithContent:self.presenterStateStorage.content];
}

@end

//
// Created by Maximychev Evgeny on 05.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SASuccessPresenterStateStorage : NSObject

@property (nonatomic) NSString *content;

@end
//
//  SASuccessPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASuccessViewOutput.h"
#import "SASuccessInteractorOutput.h"
#import "SASuccessModuleInput.h"

@protocol SASuccessViewInput;
@protocol SASuccessInteractorInput;
@protocol SASuccessRouterInput;
@class SASuccessPresenterStateStorage;

@interface SASuccessPresenter : NSObject <SASuccessViewOutput, SASuccessInteractorOutput>

@property (weak, nonatomic) id<SASuccessViewInput> view;
@property (nonatomic) id<SASuccessInteractorInput> interactor;
@property (nonatomic) id<SASuccessRouterInput> router;
@property (nonatomic) SASuccessPresenterStateStorage *presenterStateStorage;

@end

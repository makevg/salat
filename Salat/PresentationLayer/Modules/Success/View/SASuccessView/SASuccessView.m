//
//  SASuccessView.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASuccessView.h"

@interface SASuccessView ()
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@end

@implementation SASuccessView

#pragma mark - Setup

- (void)setup {
    self.contentLabel.font = [SAStyle regularFontOfSize:18.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSString class]]) {
        self.contentLabel.text = model;
    }
}

@end

//
//  SASuccessViewOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASuccessViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)configureWithContent:(NSString *)content;

@end

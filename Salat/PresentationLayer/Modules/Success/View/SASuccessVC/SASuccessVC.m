//
//  SASuccessVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASuccessVC.h"
#import "SASuccessView.h"
#import "SASuccessViewOutput.h"

static NSString *cSASuccessStoryboardName = @"Success";

@interface SASuccessVC ()
@property (strong, nonatomic) IBOutlet SASuccessView *contentView;
@end

@implementation SASuccessVC

#pragma mark - Super

+ (NSString *)storyboardName {
    return cSASuccessStoryboardName;
}

- (void)configureController {
    [super configureController];

    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - SASuccessModuleInput

- (void)configureModuleWithContent:(NSString *)content {
    [self.output configureWithContent:content];
}

- (void)updateWithContent:(NSString *)content {
    [self.contentView setModel:content];
}


@end

//
//  SASuccessVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SASuccessViewInput.h"
#import "SASuccessModuleInput.h"

@protocol SASuccessViewOutput;

@interface SASuccessVC : SABaseVC <SASuccessModuleInput, SASuccessViewInput>

@property (nonatomic) id<SASuccessViewOutput> output;

@end

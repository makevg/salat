//
//  SASuccessRouter.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASuccessRouter.h"
#import "SABackMenuRootVC.h"

@implementation SASuccessRouter

#pragma mark - SASuccessRouterInput

- (void)openMainMenu {
    UIViewController *vc = [self controllerFromStoryboard:[SABackMenuRootVC storyboardName]];
    [self performSelector:@selector(setRootFeature:) withObject:vc afterDelay:2.f];
}

#pragma mark - Private

- (UIViewController *)controllerFromStoryboard:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [sb instantiateInitialViewController];
}

- (void)setRootFeature:(UIViewController *)vc {
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}

@end

//
//  SASuccessRouter.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASuccessRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SASuccessRouter : NSObject <SASuccessRouterInput>

@property (weak, nonatomic) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

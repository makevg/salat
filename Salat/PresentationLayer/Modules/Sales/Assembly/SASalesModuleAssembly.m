//
//  SASalesModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASalesModuleAssembly.h"
#import "SASalesVC.h"
#import "SASalesPresenter.h"
#import "SASalesInteractor.h"
#import "SASalesDataDisplayManager.h"

@implementation SASalesModuleAssembly

- (SASalesVC *)viewSales {
    return [TyphoonDefinition withClass:[SASalesVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSales]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerSales]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
    }];
}

- (SASalesPresenter *)presenterSales {
    return [TyphoonDefinition withClass:[SASalesPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSales]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSales]];
    }];
}

- (SASalesInteractor *)interactorSales {
    return [TyphoonDefinition withClass:[SASalesInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSales]];
                              [definition injectProperty:@selector(salesService)
                                                    with:[self.serviceComponents saleService]];
                              [definition injectProperty:@selector(saleMapper)
                                                    with:[self.coreComponents saleMapper]];
    }];
}

- (SASalesDataDisplayManager *)displayManagerSales {
    return [TyphoonDefinition withClass:[SASalesDataDisplayManager class]];
}

@end

//
//  SASalesDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SASalePlainObject;

@interface SASalesDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;

- (void)configureDataDisplayManagerWithSales:(NSArray<SASalePlainObject *> *)sales;

- (BOOL)hasData;

@end

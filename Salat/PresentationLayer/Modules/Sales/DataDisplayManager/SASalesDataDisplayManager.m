//
//  SASalesDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASalesDataDisplayManager.h"
#import "SASaleCell.h"
#import "SASalePlainObject.h"

@interface SASalesDataDisplayManager ()
@property NSArray *sales;
@end

@implementation SASalesDataDisplayManager

#pragma mark - Lazy init

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - Public

- (void)configureDataDisplayManagerWithSales:(NSArray<SASalePlainObject *> *)sales {
    self.sales = sales;
}

- (BOOL)hasData {
    return self.sales.count > 0;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.sales count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SASaleCell cellIdentifier];
    SASaleCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                       forIndexPath:indexPath];
    [cell setModel:self.sales[indexPath.row]];
    return cell;
}

@end

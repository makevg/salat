//
//  SASalesInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASalesInteractorInput.h"

@protocol SASalesInteractorOutput;
@class SASalesService;
@class SASaleMapper;

@interface SASalesInteractor : NSObject <SASalesInteractorInput>

@property (weak, nonatomic) id<SASalesInteractorOutput> output;
@property (nonatomic) SASalesService *salesService;
@property (nonatomic) SASaleMapper *saleMapper;

@end

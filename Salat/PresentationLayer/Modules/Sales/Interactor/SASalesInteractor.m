//
//  SASalesInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASalesInteractor.h"
#import "SASalesInteractorOutput.h"
#import "SASalesService.h"
#import "SASaleMapper.h"
#import "SASalePlainObject.h"
#import "Sale.h"
#import "SAServiceLayer.h"
#import "SACommon.h"

@implementation SASalesInteractor

#pragma mark - SASalesInteractorInput

- (void)obtainSales {
    weakifySelf;

    [self.salesService obtainSales:^(NSArray<Sale *> *data) {
                strongifySelf;

                NSArray<SASalePlainObject *> *sales = [strongSelf getPlainSalesFromManagedObjects:data];

                dispatch_safe_main_async(^{
                    [strongSelf.output didObtainSales:sales];
                });
            }
                             error:^(NSError *error) {
                                 dispatch_safe_main_async(^{
                                     [weakSelf.output didObtainError:error];
                                 });
                             }];
}

- (void)obtainSalesFromNetwork {
    weakifySelf;
    
    [self.salesService obtainSalesFromNetwork:^(NSArray<Sale *> *data) {
        strongifySelf;
        
        NSArray<SASalePlainObject *> *sales = [strongSelf getPlainSalesFromManagedObjects:data];
        
        dispatch_safe_main_async(^{
            [strongSelf.output didObtainSales:sales];
        });
    }
                                        error:^(NSError *error) {
                                            dispatch_safe_main_async(^{
                                                [weakSelf.output didObtainError:error];
                                            });
                                        }];
}

#pragma mark - Private

- (NSArray<SASalePlainObject *> *)getPlainSalesFromManagedObjects:(NSArray<Sale *> *)managedObjectSales {
    NSMutableArray *salePlainObjects = [NSMutableArray array];
    for (Sale *managedObjectSale in managedObjectSales) {
        SASalePlainObject *productPlainObject = [SASalePlainObject new];
        [self.saleMapper fillObject:productPlainObject withObject:managedObjectSale];
        [salePlainObjects addObject:productPlainObject];
    }
    return salePlainObjects;
}

@end

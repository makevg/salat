//
//  SASalesInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SASalePlainObject;

@protocol SASalesInteractorOutput <NSObject>

- (void)didObtainSales:(NSArray<SASalePlainObject *> *)sales;

- (void)didObtainError:(NSError *)error;

@end

//
//  SASalesVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SASalesViewInput.h"

@class SASalesDataDisplayManager;
@protocol SASalesViewOutput;

@interface SASalesVC : SABaseVC <SASalesViewInput>

@property (nonatomic) id<SASalesViewOutput> output;
@property (nonatomic) SASalesDataDisplayManager *displayManager;

@end

//
//  SASalesViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SASalePlainObject;

@protocol SASalesViewInput <NSObject>

- (void)updateWithSales:(NSArray<SASalePlainObject *> *)sales;

- (void)showIndicator:(BOOL)show;

- (void)setErrorState:(NSError *)error;

@end

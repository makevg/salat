//
//  SASalesViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SASalesViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didTapUpdateButton;
- (void)networkDidChanged:(BOOL)state;

@end

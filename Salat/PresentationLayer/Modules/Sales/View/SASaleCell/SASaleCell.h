//
//  SASaleCell.h
//  Salat
//
//  Created by Maximychev Evgeny on 09.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseTableViewCell.h"

@interface SASaleCell : SABaseTableViewCell

@end

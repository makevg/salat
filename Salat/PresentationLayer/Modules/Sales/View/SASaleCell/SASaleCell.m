//
//  SASaleCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 09.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASaleCell.h"
#import "SASalePlainObject.h"

@interface SASaleCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *validityLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *validityDescrLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@end

@implementation SASaleCell

#pragma mark - Public

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SASalePlainObject class]]) {
        [self configureCellBySale:model];
    }
}

- (void)configureCell {
    self.titleLabel.font = [SAStyle regularFontOfSize:16.f];
    self.validityLabel.textColor = [SAStyle grayColor];
    self.validityLabel.font = [SAStyle regularFontOfSize:12.f];
    self.contentLabel.font = [SAStyle regularFontOfSize:15.f];
    self.contentLabel.textColor = [SAStyle grayColor];
    self.validityDescrLabel.font = [SAStyle regularFontOfSize:15.f];
    self.validityDescrLabel.textColor = [SAStyle grayColor];
    self.validityDescrLabel.hidden = YES;
    self.separatorView.backgroundColor = [SAStyle grayColor];
}

#pragma mark - Private

- (void)configureCellBySale:(SASalePlainObject *)sale {
    self.titleLabel.text = sale.title;
    self.contentLabel.text = sale.content;
    NSString *permanentString = [sale.isPermanent boolValue] ? @"постоянно" : @"временно";
    self.validityLabel.text = permanentString;
}

@end

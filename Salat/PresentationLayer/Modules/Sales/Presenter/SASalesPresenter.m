//
//  SASalesPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASalesPresenter.h"
#import "SASalesViewInput.h"
#import "SASalesInteractorInput.h"

@implementation SASalesPresenter

#pragma mark - Memory managment

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self addObservers];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateSales)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateSales {
    [self.view showIndicator:YES];
    [self.interactor obtainSalesFromNetwork];
}

#pragma mark - SASalesViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view showIndicator:YES];
    [self.interactor obtainSales];
}

- (void)didTapUpdateButton {
    [self updateSales];
}

- (void)networkDidChanged:(BOOL)state {
    if (state) {
        [self updateSales];
    }
}

#pragma mark - SASalesInteractorOutput

- (void)didObtainSales:(NSArray<SASalePlainObject *> *)sales {
    [self.view updateWithSales:sales];
    [self.view showIndicator:NO];
}

- (void)didObtainError:(NSError *)error {
    [self.view setErrorState:error];
    [self.view showIndicator:NO];
}


@end

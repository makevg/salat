//
//  SASalesPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 19.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASalesViewOutput.h"
#import "SASalesInteractorOutput.h"

@protocol SASalesViewInput;
@protocol SASalesInteractorInput;

@interface SASalesPresenter : NSObject <SASalesViewOutput, SASalesInteractorOutput>

@property (weak, nonatomic) id<SASalesViewInput> view;
@property (nonatomic) id<SASalesInteractorInput> interactor;

@end

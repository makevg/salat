//
// Created by mtx on 08.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAUserService.h"


@interface SAAuthModuleInputStorage : NSObject

@property(nonatomic, copy) NSString *phone;
@property(nonatomic, assign) BOOL closeModule;
@property(nonatomic, copy) NSString *token;
@property(nonatomic, assign) SocialRegisterType provider;

@end
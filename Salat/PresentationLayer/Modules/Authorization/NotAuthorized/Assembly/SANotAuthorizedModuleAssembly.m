//
//  SANotAuthorizedModuleAssembly.m
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANotAuthorizedModuleAssembly.h"
#import "SANotAuthorizedVC.h"
#import "SANotAuthorizedPresenter.h"
#import "SANotAuthorizedInteractor.h"
#import "SANotAuthorizedRouter.h"
#import "SANotAuthorizedPresenterStateStorage.h"

@implementation SANotAuthorizedModuleAssembly

- (SANotAuthorizedVC *)viewNotAuthorized {
    return [TyphoonDefinition withClass:[SANotAuthorizedVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNotAuthorized]];
    }];
}

- (SANotAuthorizedPresenter *)presenterNotAuthorized {
    return [TyphoonDefinition withClass:[SANotAuthorizedPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewNotAuthorized]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorNotAuthorized]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerNotAuthorized]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorageNotAuthorized]];
    }];
}

- (SANotAuthorizedInteractor *)interactorNotAuthorized {
    return [TyphoonDefinition withClass:[SANotAuthorizedInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNotAuthorized]];
    }];
}

- (SANotAuthorizedRouter *)routerNotAuthorized {
    return [TyphoonDefinition withClass:[SANotAuthorizedRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewNotAuthorized]];
    }];
}

- (SANotAuthorizedPresenterStateStorage *)presenterStateStorageNotAuthorized {
    return [TyphoonDefinition withClass:[SANotAuthorizedPresenterStateStorage class]];
}

@end

//
//  SANotAuthorizedRouter.m
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANotAuthorizedRouter.h"

static NSString *const kOpenSignInModuleInNotAuthSegue = @"openSignInModuleInNotAuthSegue";

@implementation SANotAuthorizedRouter

#pragma mark - SANotAuthorizedRouterInput

- (void)openSignInModule {
    [self.transitionHandler openModuleUsingSegue:kOpenSignInModuleInNotAuthSegue];
}

@end

//
//  SANotAuthorizedRouter.h
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANotAuthorizedRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SANotAuthorizedRouter : NSObject<SANotAuthorizedRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

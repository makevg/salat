//
//  SANotAuthorizedInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SANotAuthorizedInteractorInput.h"

@protocol SANotAuthorizedInteractorOutput;

@interface SANotAuthorizedInteractor : NSObject<SANotAuthorizedInteractorInput>

@property (nonatomic) id<SANotAuthorizedInteractorOutput> output;

@end

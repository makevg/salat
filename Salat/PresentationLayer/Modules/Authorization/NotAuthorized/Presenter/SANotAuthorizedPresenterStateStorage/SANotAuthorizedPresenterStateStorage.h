//
// Created by Maximychev Evgeny on 17.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SANotAuthorizedPresenterStateStorage : NSObject

@property (nonatomic) NSString *info;
@property (nonatomic) NSString *title;

@end
//
//  SANotAuthorizedPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SANotAuthorizedViewOutput.h"
#import "SANotAuthorizedInteractorOutput.h"
#import "SANotAuthorizedModuleInput.h"

@protocol SANotAuthorizedViewInput;
@protocol SANotAuthorizedInteractorInput;
@protocol SANotAuthorizedRouterInput;
@class SANotAuthorizedPresenterStateStorage;

@interface SANotAuthorizedPresenter : NSObject<SANotAuthorizedModuleInput, SANotAuthorizedViewOutput, SANotAuthorizedInteractorOutput>

@property (weak, nonatomic) id<SANotAuthorizedViewInput> view;
@property (nonatomic) id<SANotAuthorizedInteractorInput> interactor;
@property (nonatomic) id<SANotAuthorizedRouterInput> router;
@property (nonatomic) SANotAuthorizedPresenterStateStorage *presenterStateStorage;

@end

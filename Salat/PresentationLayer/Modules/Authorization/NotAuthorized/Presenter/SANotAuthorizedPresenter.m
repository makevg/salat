//
//  SANotAuthorizedPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANotAuthorizedPresenter.h"
#import "SANotAuthorizedViewInput.h"
#import "SANotAuthorizedInteractorInput.h"
#import "SANotAuthorizedRouterInput.h"
#import "SANotAuthorizedPresenterStateStorage.h"

@implementation SANotAuthorizedPresenter

#pragma mark - SANotAuthorizedModuleInput

- (void)configureModuleWithInfoString:(NSString *)infoString
                                title:(NSString *)title {
    self.presenterStateStorage.info = infoString;
    self.presenterStateStorage.title = title;
}

#pragma mark - SANotAuthorizedViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view updateWithInfoString:self.presenterStateStorage.info];
    [self.view setNavBarTitle:self.presenterStateStorage.title];
}

- (void)didTapSignInButton {
    [self.router openSignInModule];
}

@end

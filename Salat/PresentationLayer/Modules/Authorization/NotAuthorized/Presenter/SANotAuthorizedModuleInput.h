//
//  SANotAuthorizedModuleInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 17.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SANotAuthorizedModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithInfoString:(NSString *)infoString
                                title:(NSString *)title;

@end
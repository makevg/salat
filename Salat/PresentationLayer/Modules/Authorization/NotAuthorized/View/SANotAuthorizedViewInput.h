//
//  SANotAuthorizedViewInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SANotAuthorizedViewInput <NSObject>

- (void)updateWithInfoString:(NSString *)infoString;
- (void)setNavBarTitle:(NSString *)navBarTitle;

@end

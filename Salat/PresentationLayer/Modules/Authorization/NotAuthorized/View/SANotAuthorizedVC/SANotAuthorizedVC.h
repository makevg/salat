//
//  SANotAuthorizedVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SANotAuthorizedViewInput.h"

@protocol SANotAuthorizedViewOutput;

@interface SANotAuthorizedVC : SABaseVC <SANotAuthorizedViewInput>

@property (nonatomic) id<SANotAuthorizedViewOutput> output;

@end

//
//  SANotAuthorizedVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANotAuthorizedVC.h"
#import "SANotAuthorizedView.h"
#import "SANotAuthorizedViewOutput.h"

static NSString *const cNotAuthorizedStoryboardName = @"NotAuthorized";

@interface SANotAuthorizedVC ()
@property (strong, nonatomic) IBOutlet SANotAuthorizedView *contentView;
@end

@implementation SANotAuthorizedVC

#pragma mark - Super

+ (NSString *)storyboardName {
    return cNotAuthorizedStoryboardName;
}

- (void)configureController {
    [super configureController];
    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self prepareNavBar];
}

#pragma mark - Private

- (void)prepareNavBar {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

#pragma mark - Actions

- (IBAction)tappedSignInButton:(id)sender {
    [self.output didTapSignInButton];
}

#pragma mark - SANotAuthorizedViewInput

- (void)updateWithInfoString:(NSString *)infoString {
    [self.contentView setModel:infoString];
}

- (void)setNavBarTitle:(NSString *)navBarTitle {
    [self setTitle:navBarTitle];
}

@end

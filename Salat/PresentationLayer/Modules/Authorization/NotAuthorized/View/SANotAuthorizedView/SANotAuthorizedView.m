//
//  SANotAuthorizedView.m
//  Salat
//
//  Created by Maximychev Evgeny on 16.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANotAuthorizedView.h"

@interface SANotAuthorizedView ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation SANotAuthorizedView

#pragma mark - Setup

- (void)setup {
    [super setup];

    self.backgroundColor = [SAStyle lightGrayColor];
    self.infoLabel.font = [SAStyle regularFontOfSize:17.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSString class]]) {
        self.infoLabel.text = model;
    }
}

@end

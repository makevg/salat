//
//  SASecondStepPhoneConfirmationRouterInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASecondStepPhoneConfirmationRouterInput <NSObject>

- (void)openSuccessModuleWithContent:(NSString *)content;

- (void)closeCurrentModule;

@end

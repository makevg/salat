//
//  SASecondStepPhoneConfirmationRouter.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASecondStepPhoneConfirmationRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SASecondStepPhoneConfirmationRouter : NSObject <SASecondStepPhoneConfirmationRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

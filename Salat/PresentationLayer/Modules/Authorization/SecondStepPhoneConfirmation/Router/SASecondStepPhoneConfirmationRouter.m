//
//  SASecondStepPhoneConfirmationRouter.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASecondStepPhoneConfirmationRouter.h"
#import "SASuccessVC.h"

@implementation SASecondStepPhoneConfirmationRouter

#pragma mark - SASecondStepPhoneConfirmationRouterInput

- (void)openSuccessModuleWithContent:(NSString *)content {
    SASuccessVC *vc = (SASuccessVC *) [self controllerFromStoryboard:[SASuccessVC storyboardName]];
    [vc configureModuleWithContent:content];
    [self setRootFeature:vc];
}

- (void)closeCurrentModule {
    [self.transitionHandler closeCurrentModule:YES];
}

#pragma mark - Private

- (UIViewController *)controllerFromStoryboard:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [sb instantiateInitialViewController];
}

- (void)setRootFeature:(UIViewController *)vc {
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}

@end

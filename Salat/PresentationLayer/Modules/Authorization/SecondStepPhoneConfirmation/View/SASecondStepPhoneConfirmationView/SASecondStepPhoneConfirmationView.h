//
//  SASecondStepPhoneConfirmationView.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseView.h"

@class SAAuthField;
@class SAPinView;

@interface SASecondStepPhoneConfirmationView : SABaseView

@property (strong, nonatomic) IBOutlet SAPinView *pinCode;

@end

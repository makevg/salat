//
//  SASecondStepPhoneConfirmationView.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASecondStepPhoneConfirmationView.h"

@interface SASecondStepPhoneConfirmationView ()
@property(weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation SASecondStepPhoneConfirmationView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [SAStyle backgroundColor];
    self.infoLabel.textColor = [SAStyle lightGreenColor];
}

@end

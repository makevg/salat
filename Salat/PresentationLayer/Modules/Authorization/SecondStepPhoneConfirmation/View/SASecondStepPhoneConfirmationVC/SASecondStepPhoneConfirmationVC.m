//
//  SASecondStepPhoneConfirmationVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASecondStepPhoneConfirmationVC.h"
#import "SASecondStepPhoneConfirmationView.h"
#import "SASecondStepPhoneConfirmationViewOutput.h"
#import "SAPinView.h"
#import "SAIndicatorViewHelper.h"
#import "UINavigationController+TransparentNavigationController.h"

@interface SASecondStepPhoneConfirmationVC () <SAPinViewDelegate>
@property(strong, nonatomic) IBOutlet SASecondStepPhoneConfirmationView *contentView;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;

@end

@implementation SASecondStepPhoneConfirmationVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }
    
    return _indicator;
}

#pragma mark - Public

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController presentTransparentNavigationBar];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)configureController {
    [super configureController];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
            initWithTarget:self
                    action:@selector(hideKeyBoard)];

    [self.view addGestureRecognizer:tapGesture];

    [self.contentView.pinCode becomeFirstResponder];
    [self.output didTriggerViewDidLoadEvent];
}

- (void)hideKeyBoard {
    [self.view endEditing:YES];
}

#pragma mark - SASecondStepPhoneConfirmationViewInput

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)showErrorState:(NSError *)error {
    [self.contentView.pinCode resetPinCode];
    [self showErrorMessage:error.localizedDescription];
}

#pragma mark - SAPinViewDelegate

- (void)pinCodeView:(SAPinView *)view didEnterPin:(NSString *)pinCode {
    [self.output didChangeCode:pinCode];
}


@end

//
//  SASecondStepPhoneConfirmationVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SASecondStepPhoneConfirmationViewInput.h"

@protocol SASecondStepPhoneConfirmationViewOutput;

@interface SASecondStepPhoneConfirmationVC : SABaseVC <SASecondStepPhoneConfirmationViewInput>

@property (nonatomic) id<SASecondStepPhoneConfirmationViewOutput> output;

@end

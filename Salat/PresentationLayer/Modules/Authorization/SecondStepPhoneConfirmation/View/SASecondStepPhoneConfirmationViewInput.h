//
//  SASecondStepPhoneConfirmationViewInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASecondStepPhoneConfirmationViewInput <NSObject>

- (void)showIndicator:(BOOL)show;

- (void)showErrorState:(NSError *)error;

@end

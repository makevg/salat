//
//  SASecondStepPhoneConfirmationViewOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASecondStepPhoneConfirmationViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didChangeCode:(NSString *)code;

@end

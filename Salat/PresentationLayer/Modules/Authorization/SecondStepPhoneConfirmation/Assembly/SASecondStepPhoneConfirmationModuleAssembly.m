//
//  SASecondStepPhoneConfirmationModuleAssembly.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASecondStepPhoneConfirmationModuleAssembly.h"
#import "SASecondStepPhoneConfirmationVC.h"
#import "SASecondStepPhoneConfirmationPresenter.h"
#import "SASecondStepPhoneConfirmationInteractor.h"
#import "SASecondStepPhoneConfirmationRouter.h"

@implementation SASecondStepPhoneConfirmationModuleAssembly

- (SASecondStepPhoneConfirmationVC *)viewSecondStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SASecondStepPhoneConfirmationVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSecondStepPhoneConfirmation]];
                          }];
}

- (SASecondStepPhoneConfirmationPresenter *)presenterSecondStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SASecondStepPhoneConfirmationPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSecondStepPhoneConfirmation]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSecondStepPhoneConfirmation]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSecondStepPhoneConfirmation]];
                          }];
}

- (SASecondStepPhoneConfirmationInteractor *)interactorSecondStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SASecondStepPhoneConfirmationInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSecondStepPhoneConfirmation]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
                          }];
}

- (SASecondStepPhoneConfirmationRouter *)routerSecondStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SASecondStepPhoneConfirmationRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSecondStepPhoneConfirmation]];
                          }];
}

@end

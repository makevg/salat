//
//  SASecondStepPhoneConfirmationModuleInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 21.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@class SAAuthModuleInputStorage;

@protocol SASecondStepPhoneConfirmationModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithAuthStorage:(SAAuthModuleInputStorage *)storage;

@end

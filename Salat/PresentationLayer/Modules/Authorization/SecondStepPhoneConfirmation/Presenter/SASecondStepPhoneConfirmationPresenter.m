//
//  SASecondStepPhoneConfirmationPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASecondStepPhoneConfirmationPresenter.h"
#import "SASecondStepPhoneConfirmationInteractorInput.h"
#import "SASecondStepPhoneConfirmationRouterInput.h"
#import "SASecondStepPhoneConfirmationViewInput.h"
#import "SAAuthModuleInputStorage.h"
#import "SASecondStepPhoneConfirmationModuleOutput.h"

static NSString *const kWelcomeString = @"Добро пожаловать!";

@interface SASecondStepPhoneConfirmationPresenter ()
@property (nonatomic, weak) id<SASecondStepPhoneConfirmationModuleOutput> output;
@end

@implementation SASecondStepPhoneConfirmationPresenter

#pragma mark - SASecondStepPhoneConfirmationModuleInput

- (void)configureModuleWithAuthStorage:(SAAuthModuleInputStorage *)storage {
    self.storage = storage;
}

#pragma mark - SASecondStepPhoneConfirmationViewOutput

- (void)setModuleOutput:(id<RamblerViperModuleOutput>)moduleOutput {
    self.output = (id <SASecondStepPhoneConfirmationModuleOutput>) moduleOutput;
}

#pragma mark - SASecondStepPhoneConfirmationViewOutput

- (void)didTriggerViewDidLoadEvent {

}

- (void)didChangeCode:(NSString *)code {
    if ([code length] > 3) {
        if (self.storage.token) {
            [self.view showIndicator:YES];
            [self.interactor registerWithAuthStorage:self.storage
                                                code:code];
        } else {
            if ([self.interactor isAuthenticated]) {
                [self.interactor updatePhone:self.storage.phone
                                        code:code];
            } else {
                [self.interactor validatePhone:self.storage.phone
                                          code:code];
            }
        }
    }
}

#pragma mark - SASecondStepPhoneConfirmationInteractorOutput

- (void)didConfirmCode:(NSString *)phone {
    [self.output configure:phone];
    [self.view showIndicator:NO];
    
    if (self.storage.closeModule) {
        [self.router closeCurrentModule];
    } else {
        [self.router openSuccessModuleWithContent:kWelcomeString];
    }
}

- (void)didUpdatePhone:(NSString *)phone {
    [self.view showIndicator:NO];
    [self.output configure:phone];
    [self.router closeCurrentModule];
}

- (void)didObtainError:(NSError *)error {
    [self.view showIndicator:NO];
    [self.view showErrorState:error];
}

@end

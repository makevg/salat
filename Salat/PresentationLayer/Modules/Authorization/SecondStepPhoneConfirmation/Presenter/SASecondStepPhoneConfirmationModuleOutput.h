//
// Created by mtx on 11.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SASecondStepPhoneConfirmationModuleOutput <RamblerViperModuleOutput>

- (void)configure:(NSString *)phone;

@end
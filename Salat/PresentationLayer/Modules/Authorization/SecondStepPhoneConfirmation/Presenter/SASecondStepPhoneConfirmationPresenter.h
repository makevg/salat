//
//  SASecondStepPhoneConfirmationPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASecondStepPhoneConfirmationViewOutput.h"
#import "SASecondStepPhoneConfirmationInteractorOutput.h"
#import "SASecondStepPhoneConfirmationModuleInput.h"

@protocol SASecondStepPhoneConfirmationViewInput;
@protocol SASecondStepPhoneConfirmationInteractorInput;
@protocol SASecondStepPhoneConfirmationRouterInput;

@interface SASecondStepPhoneConfirmationPresenter : NSObject <
        SASecondStepPhoneConfirmationModuleInput,
        SASecondStepPhoneConfirmationViewOutput,
        SASecondStepPhoneConfirmationInteractorOutput>

@property(weak, nonatomic) id <SASecondStepPhoneConfirmationViewInput> view;
@property(nonatomic) id <SASecondStepPhoneConfirmationInteractorInput> interactor;
@property(nonatomic) id <SASecondStepPhoneConfirmationRouterInput> router;
@property(nonatomic) SAAuthModuleInputStorage *storage;

@end
//
//  SASecondStepPhoneConfirmationInteractor.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASecondStepPhoneConfirmationInteractor.h"
#import "SAUserService.h"
#import "SASecondStepPhoneConfirmationInteractorOutput.h"
#import "SAAuthModuleInputStorage.h"

@implementation SASecondStepPhoneConfirmationInteractor

#pragma mark - SASecondStepPhoneConfirmationInteractorInput

- (void)validatePhone:(NSString *)phone
                 code:(NSString *)code {
    __weak __typeof(self) weakSelf = self;

    [self.userService loginWithPhone:phone
                             smsCode:code
                          completion:^{
                              dispatch_safe_main_async( ^{
                                  [weakSelf.output didConfirmCode:phone];
                              });
                          }
                               error:^(NSError *error) {
                                   dispatch_safe_main_async( ^{
                                       [weakSelf.output didObtainError:error];
                                   });
                               }];
}

- (void)updatePhone:(NSString *)phone code:(NSString *)code {
    __weak __typeof(self) weakSelf = self;

    [self.userService updatePhone:phone
                          smsCode:code
                       completion:^(NSString *newPhone) {
                           dispatch_safe_main_async( ^{
                               [weakSelf.output didUpdatePhone:newPhone];
                           });
                       }
                            error:^(NSError *error) {
                                dispatch_safe_main_async( ^{
                                    [weakSelf.output didObtainError:error];
                                });
                            }];
}

- (void)registerWithAuthStorage:(SAAuthModuleInputStorage *)storage
                           code:(NSString *)code {
    __weak __typeof(self) weakSelf = self;

    [self.userService registerWithProvider:storage.provider
                                     token:storage.token
                                      hone:storage.phone
                                   smsCode:code
                                completion:^{
                                    dispatch_safe_main_async(^{
                                        [weakSelf.output didConfirmCode:storage.phone];
                                    });
                                }
                                     error:^(NSError *error) {
                                         dispatch_safe_main_async(^{
                                             [weakSelf.output didObtainError:error];
                                         });
                                     }];
}

- (BOOL)isAuthenticated {
    return [self.userService isAuthorized];
}


@end

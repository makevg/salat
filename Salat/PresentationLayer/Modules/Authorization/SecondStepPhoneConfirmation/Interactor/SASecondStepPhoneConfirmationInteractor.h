//
//  SASecondStepPhoneConfirmationInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASecondStepPhoneConfirmationInteractorInput.h"

@protocol SASecondStepPhoneConfirmationInteractorOutput;
@class SAUserService;

@interface SASecondStepPhoneConfirmationInteractor : NSObject <SASecondStepPhoneConfirmationInteractorInput>

@property (weak, nonatomic) id<SASecondStepPhoneConfirmationInteractorOutput> output;
@property (nonatomic) SAUserService *userService;

@end

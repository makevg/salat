//
//  SASecondStepPhoneConfirmationInteractorOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASecondStepPhoneConfirmationInteractorOutput <NSObject>

- (void)didConfirmCode:(NSString *)phone;

- (void)didUpdatePhone:(NSString *)phone;

- (void)didObtainError:(NSError *)error;

@end

//
//  SASecondStepPhoneConfirmationInteractorInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAuthModuleInputStorage;

@protocol SASecondStepPhoneConfirmationInteractorInput <NSObject>

- (void)validatePhone:(NSString *)phone
                 code:(NSString *)code;

- (void)updatePhone:(NSString *)phone
               code:(NSString *)code;

- (void)registerWithAuthStorage:(SAAuthModuleInputStorage *)storage
                           code:(NSString *)code;

- (BOOL)isAuthenticated;

@end

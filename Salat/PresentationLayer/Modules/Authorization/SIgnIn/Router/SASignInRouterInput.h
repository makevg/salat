//
//  SASignInRouterInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAuthModuleInputStorage;

@protocol SASignInRouterInput <NSObject>

- (void)openValidateCodeModuleWithPhone:(SAAuthModuleInputStorage *)storage;

- (void)openValidatePhoneModuleWithToken:(SAAuthModuleInputStorage *)storage;

- (void)openSuccessModuleWithContent:(NSString *)content;

@end

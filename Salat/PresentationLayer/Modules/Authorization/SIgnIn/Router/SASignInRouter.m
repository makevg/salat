//
//  SASignInRouter.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASignInRouter.h"
#import "SAFirstStepPhoneConfirmationModuleInput.h"
#import "SASuccessVC.h"
#import "SASecondStepPhoneConfirmationModuleInput.h"

static NSString *const kOpenSecondStepFromSignIn = @"kOpenSecondStepFromSignIn";
static NSString *const kOpenFirstStepFromSignIn = @"kOpenFirstStepFromSignIn";

@implementation SASignInRouter

//SASecondStepPhoneConfirmationModuleInput

#pragma mark - SASignInRouterInput

- (void)openValidateCodeModuleWithPhone:(SAAuthModuleInputStorage *)storage {
    [[self.transitionHandler openModuleUsingSegue:kOpenSecondStepFromSignIn]
            thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <SASecondStepPhoneConfirmationModuleInput> moduleInput) {
                [moduleInput configureModuleWithAuthStorage:storage];
                return nil;
            }];
}

- (void)openValidatePhoneModuleWithToken:(SAAuthModuleInputStorage *)storage {
    [[self.transitionHandler openModuleUsingSegue:kOpenFirstStepFromSignIn]
            thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <SAFirstStepPhoneConfirmationModuleInput> moduleInput) {
                [moduleInput configureModuleWithAuthStorage:storage];
                return nil;
            }];
}

- (void)openSuccessModuleWithContent:(NSString *)content {
    SASuccessVC *vc = (SASuccessVC *) [self controllerFromStoryboard:[SASuccessVC storyboardName]];
    [vc configureModuleWithContent:content];
    [self setRootFeature:vc];
}

- (UIViewController *)controllerFromStoryboard:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    return [sb instantiateInitialViewController];
}

- (void)setRootFeature:(UIViewController *)vc {
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}


@end

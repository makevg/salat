//
//  SASignInRouter.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASignInRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SASignInRouter : NSObject <SASignInRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

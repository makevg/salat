//
//  SASignInPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
#import "SASignInViewInput.h"
#import "SASignInInteractorInput.h"
#import "SASignInRouterInput.h"
#import "SASignInPresenter.h"
#import "SAAuthModuleInputStorage.h"

static NSString *const kSignInWelcomeString = @"Добро пожаловать!";

@interface SASignInPresenter ()
@property (nonatomic, strong) NBPhoneNumberUtil *phoneUtil;
@end

@implementation SASignInPresenter

#pragma mark - SASignInViewOutput

- (instancetype)init {
    if (self = [super init]) {
        self.phoneUtil = [[NBPhoneNumberUtil alloc] init];
    }

    return self;
}

- (void)didTriggerViewDidLoadEvent {
    [self.view enableGetCodeButton:NO];
}

- (void)didTapGetCodeButtonWithPhone:(NSString *)phone {
    [self.view showIndicator:YES];
    [self.view enableGetCodeButton:NO];
    self.signInPresenterStateStorage.phone = phone;
    self.signInPresenterStateStorage.token = nil;
    self.signInPresenterStateStorage.provider = eSRTNONE;
    [self.interactor confirmPhone:phone];
}

- (void)didChangePhone:(NSString *)phone {
    NSError *anError = nil;
    NBPhoneNumber *number = [self.phoneUtil parse:phone
                                      defaultRegion:@"RU"
                                              error:&anError];

    [self.view enableGetCodeButton:[self.phoneUtil isValidNumber:number]];
}

- (void)didTapVKAuth:(NSString *)token {
    [self.view showIndicator:YES];
    self.signInPresenterStateStorage.token = token;
    self.signInPresenterStateStorage.provider = eSRTVK;
    [self.interactor authByVK:token];
}

- (void)didTapFBAuth:(NSString *)token {
    [self.view showIndicator:YES];
    self.signInPresenterStateStorage.token = token;
    self.signInPresenterStateStorage.provider = eSRTFB;
    [self.interactor authByFB:token];
}

#pragma mark - SASignInInteractorOutput

- (void)didConfirmPhone {
    [self.view showIndicator:NO];
    [self.view enableGetCodeButton:YES];
    [self.router openValidateCodeModuleWithPhone:self.signInPresenterStateStorage];
}

- (void)didAuthByProvider {
    [self.view showIndicator:NO];
    [self.router openSuccessModuleWithContent:kSignInWelcomeString];
}

- (void)didErrorOnProvider:(NSError *)error {
    [self.view showIndicator:NO];
    [self.view showErrorState:error];
}

- (void)needAuth {
    [self.view showIndicator:NO];
    [self.router openValidatePhoneModuleWithToken:self.signInPresenterStateStorage];
}

- (void)didErrorOnConfirmPhone:(NSError *)error {
    [self.view showIndicator:NO];
    [self.view enableGetCodeButton:YES];
    [self.view showErrorState:error];
}

@end
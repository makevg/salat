//
//  SASignInPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASignInViewOutput.h"
#import "SASignInInteractorOutput.h"

@protocol SASignInViewInput;
@protocol SASignInInteractorInput;
@protocol SASignInRouterInput;
@class SAAuthModuleInputStorage;

@interface SASignInPresenter : NSObject <SASignInViewOutput, SASignInInteractorOutput>

@property(weak, nonatomic) id <SASignInViewInput> view;
@property(nonatomic) id <SASignInInteractorInput> interactor;
@property(nonatomic) id <SASignInRouterInput> router;
@property(nonatomic) SAAuthModuleInputStorage *signInPresenterStateStorage;

@end

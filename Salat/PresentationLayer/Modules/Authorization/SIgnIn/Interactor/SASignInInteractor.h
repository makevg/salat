//
//  SASignInInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SASignInInteractorInput.h"

@protocol SASignInInteractorOutput;
@class SAUserService;

@interface SASignInInteractor : NSObject <SASignInInteractorInput>

@property (weak, nonatomic) id<SASignInInteractorOutput> output;
@property (nonatomic) SAUserService *userService;

@end

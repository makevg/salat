//
//  SASignInInteractorOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASignInInteractorOutput <NSObject>

- (void)didConfirmPhone;

- (void)didAuthByProvider;

- (void)didErrorOnConfirmPhone:(NSError *) error;

- (void)didErrorOnProvider:(NSError *) error;

- (void)needAuth;

@end

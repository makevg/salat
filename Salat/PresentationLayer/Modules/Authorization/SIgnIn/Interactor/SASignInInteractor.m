//
//  SASignInInteractor.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASignInInteractor.h"
#import "SAUserService.h"
#import "SASignInInteractorOutput.h"

@implementation SASignInInteractor

#pragma mark - SASignInInteractorInput

- (void)confirmPhone:(NSString *)phone {
    __weak typeof(self) weakSelf = self;

    [self.userService validatePhoneFirstStepWithPhone:phone
                                           completion:^{
                                               dispatch_safe_main_async(^{
                                                   [weakSelf.output didConfirmPhone];
                                               });
                                           }
                                                error:^(NSError *error) {
                                                    dispatch_safe_main_async(^{
                                                        [weakSelf.output didErrorOnConfirmPhone:error];
                                                    });
                                                }];
}

- (void)authByVK:(NSString *)token {
    [self authByProvider:token type:eSRTVK];
}

- (void)authByFB:(NSString *)token {
    [self authByProvider:token type:eSRTFB];
}

- (void)authByProvider:(NSString *)token type:(SocialRegisterType)provider {
    __weak typeof(self) weakSelf = self;

    [self.userService loginWithProvider:provider
                                  token:token
                             completion:^(BOOL i) {
                                 dispatch_safe_main_async(^{
                                     if (i) {
                                         [weakSelf.output didAuthByProvider];
                                     } else {
                                         [weakSelf.output needAuth];
                                     }
                                 });
                             }
                                  error:^(NSError *error) {
                                      dispatch_safe_main_async(^{
                                          [weakSelf.output didErrorOnProvider:error];
                                      });
                                  }];
}

@end

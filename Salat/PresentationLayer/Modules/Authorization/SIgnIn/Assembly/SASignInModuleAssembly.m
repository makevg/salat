//
//  SASignInModuleAssembly.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASignInModuleAssembly.h"
#import "SASignInVC.h"
#import "SASignInPresenter.h"
#import "SASignInInteractor.h"
#import "SASignInRouter.h"
#import "SAAuthModuleInputStorage.h"

@implementation SASignInModuleAssembly

- (SASignInVC *)viewSignIn {
    return [TyphoonDefinition withClass:[SASignInVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSignIn]];
                          }];
}

- (SASignInPresenter *)presenterSignIn {
    return [TyphoonDefinition withClass:[SASignInPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSignIn]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSignIn]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSignIn]];
                              [definition injectProperty:@selector(signInPresenterStateStorage)
                                                    with:[self signInPresenterStateStorage]];
                          }];
}

- (SASignInInteractor *)interactorSignIn {
    return [TyphoonDefinition withClass:[SASignInInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSignIn]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
    }];
}

- (SASignInRouter *)routerSignIn {
    return [TyphoonDefinition withClass:[SASignInRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSignIn]];
    }];
}

- (SAAuthModuleInputStorage *)signInPresenterStateStorage {
    return [TyphoonDefinition withClass:[SAAuthModuleInputStorage class]];
}

@end

//
//  SASignInViewOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol SASignInViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;

- (void)didTapGetCodeButtonWithPhone:(NSString *)phone;

- (void)didChangePhone:(NSString *)phone;

- (void)didTapVKAuth:(NSString *)token;

- (void)didTapFBAuth:(NSString *)token;

@end

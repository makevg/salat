//
//  SASignInView.m
//  Salat
//
//  Created by Maximychev Evgeny on 03.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASignInView.h"

@implementation SASignInView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [SAStyle backgroundColor];
}

@end

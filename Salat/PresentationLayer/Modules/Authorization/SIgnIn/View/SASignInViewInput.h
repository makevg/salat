//
//  SASignInViewInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASignInViewInput <NSObject>

- (void)enableGetCodeButton:(BOOL)enable;

- (void)showIndicator:(BOOL)show;

- (void)showErrorState:(NSError *)error;

@end

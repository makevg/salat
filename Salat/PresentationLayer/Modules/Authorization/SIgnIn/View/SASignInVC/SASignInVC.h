//
//  SASignInVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 03.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SASignInViewInput.h"

@protocol SASignInViewOutput;

@interface SASignInVC : SABaseVC <SASignInViewInput>

@property (nonatomic) id<SASignInViewOutput> output;

@end

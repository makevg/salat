//
//  SASignInVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 03.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SASignInVC.h"
#import "SASignInView.h"
#import "SAAuthField.h"
#import "SASignInViewOutput.h"
#import "SABorderButton.h"
#import "VKAuthorizationResult.h"
#import "VKSdk.h"
#import "SASocialHelper.h"
#import "SAIndicatorViewHelper.h"
#import "UINavigationController+TransparentNavigationController.h"

NSString *const cAuthorizationStoryboardName = @"SAAuthorization";

@interface SASignInVC () <UITextFieldDelegate, SASocialHelperDelegate>
@property(strong, nonatomic) IBOutlet SASignInView *contentView;
@property(strong, nonatomic) SASocialHelper *socialHelper;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;
@end

@implementation SASignInVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }
    
    return _indicator;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self prepareNavBar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self removeObservers];
}

#pragma mark - keyboard movements

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[notification userInfo][UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = MIN(-(self.contentView.getCodeButton.frame.origin.y +
                self.contentView.getCodeButton.frame.size.height - self.contentView.frame.size.height + keyboardSize.height), 0);
        self.view.frame = f;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

#pragma mark - Public

+ (NSString *)storyboardName {
    return cAuthorizationStoryboardName;
}

+ (BOOL)isInitial {
    return YES;
}

- (void)configureController {
    [super configureController];

    self.socialHelper = [[SASocialHelper alloc] initWithDelegate:self];

    __weak typeof(self) weakSelf = self;

    self.contentView.phoneField.textDidChangeBlock = ^(UITextField *textField) {
        [weakSelf.output didChangePhone:weakSelf.contentView.phoneField.phoneNumber];
    };

    [self.output didTriggerViewDidLoadEvent];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
            initWithTarget:self
                    action:@selector(hideKeyBoard)];

    [self.view addGestureRecognizer:tapGesture];
}

- (void)hideKeyBoard {
    [self.view endEditing:YES];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)prepareNavBar {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    [self.navigationController presentTransparentNavigationBar];
}

- (void)performSignInAction {
    NSString *phone = self.contentView.phoneField.text;
    [self.output didTapGetCodeButtonWithPhone:phone];
}

#pragma mark - Actions

- (IBAction)tappedGetCodeButton:(id)sender {
    [self performSignInAction];
}

- (IBAction)tappedFbButton:(id)sender {
    [self.socialHelper authFB];
}

- (IBAction)tappedVkButton:(id)sender {
    [self.socialHelper authVK];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self performSignInAction];
    }
    return YES;
}

#pragma mark - SASignInViewInput

- (void)enableGetCodeButton:(BOOL)enable {
    self.contentView.getCodeButton.enabled = enable;
}

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)showErrorState:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

#pragma mark - SASocialHelperDelegate

- (void)vkAuthResultError:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

- (void)vkAuthResultSuccess:(NSString *)token {
    [self.output didTapVKAuth:token];
}

- (void)fbAuthResultError:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

- (void)fbAuthResultSuccess:(NSString *)token {
    [self.output didTapFBAuth:token];
}

@end

//
//  SAFirstStepPhoneConfirmationModuleAssembly.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAFirstStepPhoneConfirmationModuleAssembly.h"
#import "SAFirstStepPhoneConfirmationVC.h"
#import "SAFirstStepPhoneConfirmationPresenter.h"
#import "SAFirstStepPhoneConfirmationInteractor.h"
#import "SAFirstStepPhoneConfirmationRouter.h"
#import "SAFirstStepPhoneConfirmationPresenterStateStorage.h"

@implementation SAFirstStepPhoneConfirmationModuleAssembly

- (SAFirstStepPhoneConfirmationVC *)viewFirstStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SAFirstStepPhoneConfirmationVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFirstStepPhoneConfirmation]];
    }];
}

- (SAFirstStepPhoneConfirmationPresenter *)presenterFirstStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SAFirstStepPhoneConfirmationPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewFirstStepPhoneConfirmation]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorFirstStepPhoneConfirmation]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerFirstStepPhoneConfirmation]];
                              [definition injectProperty:@selector(inputStorage)
                                                    with:[self presenterStateStorageFirstStepPhoneConfirmation]];
                          }];
}

- (SAFirstStepPhoneConfirmationInteractor *)interactorFirstStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SAFirstStepPhoneConfirmationInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterFirstStepPhoneConfirmation]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
                          }];
}

- (SAFirstStepPhoneConfirmationRouter *)routerFirstStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SAFirstStepPhoneConfirmationRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewFirstStepPhoneConfirmation]];
                          }];
}

- (SAFirstStepPhoneConfirmationPresenterStateStorage *)presenterStateStorageFirstStepPhoneConfirmation {
    return [TyphoonDefinition withClass:[SAFirstStepPhoneConfirmationPresenterStateStorage class]];
}

@end

//
//  SAFirstStepPhoneConfirmationInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAFirstStepPhoneConfirmationInteractorInput.h"

@protocol SAFirstStepPhoneConfirmationInteractorOutput;
@class SAUserService;

@interface SAFirstStepPhoneConfirmationInteractor : NSObject <SAFirstStepPhoneConfirmationInteractorInput>

@property (weak, nonatomic) id<SAFirstStepPhoneConfirmationInteractorOutput> output;
@property (nonatomic) SAUserService *userService;

@end

//
//  SAFirstStepPhoneConfirmationInteractor.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAFirstStepPhoneConfirmationInteractor.h"
#import "SAUserService.h"
#import "SAFirstStepPhoneConfirmationInteractorOutput.h"

@implementation SAFirstStepPhoneConfirmationInteractor

#pragma mark - SAFirstStepPhoneConfirmationInteractorInput

- (void)validatePhone:(NSString *)phone {
    __weak __typeof(self) weakSelf = self;

    [self.userService validatePhoneFirstStepWithPhone:phone
                                           completion:^{
                                               dispatch_safe_main_async( ^{
                                                   [weakSelf.output didSendCode];
                                               });
                                           }
                                                error:^(NSError *error) {
                                                    dispatch_safe_main_async( ^{
                                                        [weakSelf.output didObtainError:error];
                                                    });
                                                }];
}

@end

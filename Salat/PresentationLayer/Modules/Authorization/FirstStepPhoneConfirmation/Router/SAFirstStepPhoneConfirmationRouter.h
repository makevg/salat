//
//  SAFirstStepPhoneConfirmationRouter.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAFirstStepPhoneConfirmationRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAFirstStepPhoneConfirmationRouter : NSObject <SAFirstStepPhoneConfirmationRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

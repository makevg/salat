//
//  SAFirstStepPhoneConfirmationRouter.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAFirstStepPhoneConfirmationRouter.h"
#import "SABackMenuRootVC.h"
#import "SASuccessVC.h"
#import "SAAuthModuleInputStorage.h"
#import "SASecondStepPhoneConfirmationModuleInput.h"

static NSString *const kOpenFirstStepFromFirstStep = @"kOpenSecondStepFromFirstStep";

@implementation SAFirstStepPhoneConfirmationRouter

#pragma mark - SAFirstStepPhoneConfirmationRouterInput

- (void)openSecondStepWithStorage:(SAAuthModuleInputStorage *)storage {
    [[self.transitionHandler openModuleUsingSegue:kOpenFirstStepFromFirstStep]
            thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <SASecondStepPhoneConfirmationModuleInput> moduleInput) {
                [moduleInput configureModuleWithAuthStorage:storage];
                return nil;
            }];
}


@end

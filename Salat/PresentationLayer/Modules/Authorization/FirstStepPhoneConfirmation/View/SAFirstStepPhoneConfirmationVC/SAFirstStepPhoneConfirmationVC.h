//
//  SAFirstStepPhoneConfirmationVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseVC.h"
#import "SAFirstStepPhoneConfirmationViewInput.h"

@protocol SAFirstStepPhoneConfirmationViewOutput;

@interface SAFirstStepPhoneConfirmationVC : SABaseVC <SAFirstStepPhoneConfirmationViewInput>

@property (nonatomic) id<SAFirstStepPhoneConfirmationViewOutput> output;

@end

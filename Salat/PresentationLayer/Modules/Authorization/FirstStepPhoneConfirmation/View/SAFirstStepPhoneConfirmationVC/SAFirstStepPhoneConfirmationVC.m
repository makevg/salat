//
//  SAFirstStepPhoneConfirmationVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 05.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAFirstStepPhoneConfirmationViewInput.h"
#import "SAFirstStepPhoneConfirmationVC.h"
#import "SAFirstStepPhoneConfirmationView.h"
#import "SABorderButton.h"
#import "SAFirstStepPhoneConfirmationViewOutput.h"
#import "SAAuthField.h"
#import "UINavigationController+TransparentNavigationController.h"
#import "SACommon.h"

NSString *const cPhoneConfirmationStoryboardName = @"SAPhoneConfirmation";

@interface SAFirstStepPhoneConfirmationVC () <UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet SAFirstStepPhoneConfirmationView *contentView;
@end

@implementation SAFirstStepPhoneConfirmationVC

#pragma mark - Public

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[notification userInfo][UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = MIN(-(
                self.contentView.getCodeButton.superview.frame.origin.y +
                        self.contentView.getCodeButton.frame.origin.y +
                        self.contentView.getCodeButton.frame.size.height -
                        self.contentView.frame.size.height +
                        keyboardSize.height), 0);
        self.view.frame = f;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

+ (NSString *)storyboardName {
    return cPhoneConfirmationStoryboardName;
}

- (void)configureController {
    [super configureController];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
            initWithTarget:self
                    action:@selector(hideKeyBoard)];

    [self.view addGestureRecognizer:tapGesture];

    weakifySelf;

    self.contentView.phoneField.textDidChangeBlock = ^(UITextField *textField) {
        [weakSelf.output didChangePhone:weakSelf.contentView.phoneField.phoneNumber];
    };

    self.contentView.getCodeButton.enabled = NO;

    [self.navigationController presentTransparentNavigationBar];
    [self.output didTriggerViewDidLoadEvent];
}

- (void)hideKeyBoard {
    [self.view endEditing:YES];
}

#pragma mark - Private

- (void)performSignInAction {
    NSString *phone = self.contentView.phoneField.text;
    [self.output didTapGetCodeButtonWithPhone:phone];
}

#pragma mark - Actions

- (IBAction)tappedSendButton:(id)sender {
    [self performSignInAction];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self performSignInAction];
    }
    return YES;
}

- (void)enableGetCodeButton:(BOOL)enable {
    self.contentView.getCodeButton.enabled = enable;
}

- (void)showErrorState:(NSError *)error {
    [self showMessage:error.localizedDescription];
}


@end

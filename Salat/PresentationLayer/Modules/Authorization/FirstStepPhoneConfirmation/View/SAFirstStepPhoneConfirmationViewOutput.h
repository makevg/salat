//
//  SAFirstStepPhoneConfirmationViewOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAFirstStepPhoneConfirmationViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;

- (void)didChangePhone:(NSString *)phone;

- (void)didTapGetCodeButtonWithPhone:(NSString *)phone;

@end

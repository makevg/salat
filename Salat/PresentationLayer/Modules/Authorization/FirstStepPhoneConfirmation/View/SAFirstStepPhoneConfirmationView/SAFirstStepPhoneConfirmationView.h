//
//  SAFirstStepPhoneConfirmationView.h
//  Salat
//
//  Created by Maximychev Evgeny on 05.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseView.h"

@class SAAuthField;
@class SABorderButton;

@interface SAFirstStepPhoneConfirmationView : SABaseView

@property(weak, nonatomic) IBOutlet SAAuthField *phoneField;
@property(weak, nonatomic) IBOutlet SABorderButton *getCodeButton;

@end

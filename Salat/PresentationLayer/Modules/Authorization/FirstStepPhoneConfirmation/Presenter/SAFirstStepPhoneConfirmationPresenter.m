//
//  SAFirstStepPhoneConfirmationPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <libPhoneNumber-iOS/NBPhoneNumber.h>
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
#import "SAFirstStepPhoneConfirmationPresenter.h"
#import "SAFirstStepPhoneConfirmationInteractorInput.h"
#import "SAFirstStepPhoneConfirmationRouterInput.h"
#import "SAFirstStepPhoneConfirmationViewInput.h"
#import "SAAuthModuleInputStorage.h"

@interface SAFirstStepPhoneConfirmationPresenter ()

@property(nonatomic, strong) NBPhoneNumberUtil *phoneUtil;

@end

@implementation SAFirstStepPhoneConfirmationPresenter

- (instancetype)init {
    if (self = [super init]) {
        self.phoneUtil = [[NBPhoneNumberUtil alloc] init];
    }

    return self;
}

#pragma mark - SAFirstStepPhoneConfirmationModuleInput

- (void)configureModuleWithAuthStorage:(SAAuthModuleInputStorage *)storage {
    self.inputStorage = storage;
}

#pragma mark - SAFirstStepPhoneConfirmationViewOutput

- (void)didTriggerViewDidLoadEvent {

}

- (void)didSendCode {
    [self.router openSecondStepWithStorage:self.inputStorage];
}

- (void)didObtainError:(NSError *)error {
    [self.view showErrorState:error];
}

- (void)didChangePhone:(NSString *)phone {
    NSError *anError = nil;
    NBPhoneNumber *number = [self.phoneUtil parse:phone
                                    defaultRegion:@"RU"
                                            error:&anError];

    [self.view enableGetCodeButton:[self.phoneUtil isValidNumber:number]];
}

- (void)didTapGetCodeButtonWithPhone:(NSString *)phone {
    self.inputStorage.phone = phone;
    [self.interactor validatePhone:phone];
}


@end

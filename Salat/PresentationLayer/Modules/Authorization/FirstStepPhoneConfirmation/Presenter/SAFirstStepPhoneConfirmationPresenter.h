//
//  SAFirstStepPhoneConfirmationPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 20.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAFirstStepPhoneConfirmationViewOutput.h"
#import "SAFirstStepPhoneConfirmationInteractorOutput.h"
#import "SAFirstStepPhoneConfirmationModuleInput.h"

@protocol SAFirstStepPhoneConfirmationViewInput;
@protocol SAFirstStepPhoneConfirmationInteractorInput;
@protocol SAFirstStepPhoneConfirmationRouterInput;
@class SAFirstStepPhoneConfirmationPresenterStateStorage;

@interface SAFirstStepPhoneConfirmationPresenter : NSObject <
        SAFirstStepPhoneConfirmationModuleInput,
        SAFirstStepPhoneConfirmationViewOutput,
        SAFirstStepPhoneConfirmationInteractorOutput>

@property(weak, nonatomic) id <SAFirstStepPhoneConfirmationViewInput> view;
@property(nonatomic) id <SAFirstStepPhoneConfirmationInteractorInput> interactor;
@property(nonatomic) id <SAFirstStepPhoneConfirmationRouterInput> router;
@property (nonatomic) SAAuthModuleInputStorage *inputStorage;

@end
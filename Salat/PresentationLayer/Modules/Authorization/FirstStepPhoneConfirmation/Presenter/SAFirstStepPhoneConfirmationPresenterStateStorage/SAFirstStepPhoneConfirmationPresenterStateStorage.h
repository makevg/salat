//
//  SAFirstStepPhoneConfirmationPresenterStateStorage.h
//  Salat
//
//  Created by Максимычев Е.О. on 21.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAUserService.h"

@interface SAFirstStepPhoneConfirmationPresenterStateStorage : NSObject

@property (nonatomic, copy) NSString *token;
@property (nonatomic, assign) SocialRegisterType provider;

@end

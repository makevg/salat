//
//  SAAboutDeliveryPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryPresenter.h"
#import "SAAboutDeliveryInteractorInput.h"
#import "SAAboutDeliveryViewInput.h"
#import "SADeliveryTimePlainObject.h"
#import "SAServiceLayer.h"
#import "SACommon.h"
#import "SADeliveryTimeDayPlainObject.h"

@implementation SAAboutDeliveryPresenter

#pragma mark - Memory managment

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self addObservers];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateRestaurantInfo)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateRestaurantInfo {
    [self.view showIndicator:YES];
    [self.interactor obtainRestaurantInfoFromNetwork];
}

#pragma mark - SAAboutDeliveryViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view showIndicator:YES];
    [self.interactor obtainRestaurantInfo];
}

- (void)didTapUpdateButton {
    [self updateRestaurantInfo];
}

- (void)networkDidChanged:(BOOL)state {
    if (state) {
        [self updateRestaurantInfo];
    }
}

#pragma mark - SAAboutDeliveryInteractorOutput

- (void)didObtainRestaurantInfo:(NSArray *)restaurantInfo {
    [self.interactor convertWorkingTimes:restaurantInfo];
}

- (void)didObtainTransformInfo:(NSArray *)restaurantInfo {
    [self.view updateWithRestaurantInfo:restaurantInfo];
    [self.view showIndicator:NO];
}

- (void)didObtainError:(NSError *)error {
    [self.view setErrorState:error];
    [self.view showIndicator:NO];
}

@end

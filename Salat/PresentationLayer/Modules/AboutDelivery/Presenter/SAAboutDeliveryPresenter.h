//
//  SAAboutDeliveryPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAAboutDeliveryInteractorOutput.h"
#import "SAAboutDeliveryViewOutput.h"
#import "SAAboutDeliveryInteractorOutput.h"

@protocol SAAboutDeliveryViewInput;
@protocol SAAboutDeliveryInteractorInput;

@interface SAAboutDeliveryPresenter : NSObject <SAAboutDeliveryViewOutput, SAAboutDeliveryInteractorOutput>

@property (weak, nonatomic) id<SAAboutDeliveryViewInput> view;
@property (nonatomic) id<SAAboutDeliveryInteractorInput> interactor;

@end

//
//  SAAboutDeliveryFooter.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@interface SAAboutDeliveryFooter : SABaseView

+ (CGFloat)footerHeight;

@end

//
//  SAAboutDeliveryVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAAboutDeliveryViewInput.h"

@protocol SAAboutDeliveryViewOutput;
@class SAAboutDeliveryDataDisplayManager;

@interface SAAboutDeliveryVC : SABaseVC <SAAboutDeliveryViewInput>

@property (nonatomic) id<SAAboutDeliveryViewOutput> output;
@property (nonatomic) SAAboutDeliveryDataDisplayManager *displayManager;

@end

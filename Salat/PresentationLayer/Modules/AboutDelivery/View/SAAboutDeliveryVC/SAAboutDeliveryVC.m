//
//  SAAboutDeliveryVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryVC.h"
#import "SAAboutDeliveryView.h"
#import "SAAboutDeliveryViewOutput.h"
#import "SARestaurantPlainObject.h"
#import "SAAboutDeliveryDataDisplayManager.h"
#import "SAIndicatorViewHelper.h"
#import "UIScrollView+EmptyDataSet.h"

NSString *const cAboutDeliveryStoryboardName = @"SAAboutDelivery";

@interface SAAboutDeliveryVC () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (strong, nonatomic) IBOutlet SAAboutDeliveryView *contentView;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;
@property (nonatomic) NSError *error;
@end

@implementation SAAboutDeliveryVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }
    
    return _indicator;
}

#pragma mark - Super

+ (NSString *)storyboardName {
    return cAboutDeliveryStoryboardName;
}

+ (BOOL)isInitial {
    return YES;
}

- (void)configureController {
    [super configureController];

    self.displayManager.tableView = self.contentView.tableView;
    self.contentView.tableView.emptyDataSetSource = self;
    self.contentView.tableView.emptyDataSetDelegate = self;
    [self.output didTriggerViewDidLoadEvent];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    [super networkStateDidChanged:notification];
    
    [self.output networkDidChanged:[notification.object boolValue]];
}

#pragma mark - SAAboutDeliveryViewInput

- (void)updateWithRestaurantInfo:(NSArray *)restaurantInfo {
    [self.displayManager configureDisplayManagerWithRestaurantInfo:restaurantInfo];
    [self.contentView.tableView reloadData];
}

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)setErrorState:(NSError *)error {
    self.error = error;
    
    if ([self.displayManager hasData]) {
        [self showErrorMessage:error.localizedDescription];
    } else {
        [self.contentView.tableView reloadData];
    }
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Нет информации о ресторане";
    
    if (![self.displayManager hasData]) {
        text = self.error ? self.error.localizedDescription : @"";
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    
    if (![self.displayManager hasData]) {
        return [UIImage imageNamed:@"btn_auth_back"];
    }
    
    return nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    
    if (![self.displayManager hasData]) {
        NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                     NSForegroundColorAttributeName: [SAStyle mediumGreenColor]};
        
        return [[NSAttributedString alloc] initWithString:@"Обновить" attributes:attributes];
    }
    
    return nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -10.f;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self.output didTapUpdateButton];
}

@end

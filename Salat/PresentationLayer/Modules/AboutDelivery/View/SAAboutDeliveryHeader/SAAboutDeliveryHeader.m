//
//  SAAboutDeliveryHeader.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryHeader.h"

static CGFloat const cAboutDeliveryHeaderHeight = 40.f;

@interface SAAboutDeliveryHeader ()
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@end

@implementation SAAboutDeliveryHeader

#pragma mark - Lazy init

- (void)setTitle:(NSString *)title {
    _title = title;
    self.headerTitleLabel.text = title;
}

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

#pragma mark - Public

+ (CGFloat)headerHeight {
    return cAboutDeliveryHeaderHeight;
}

#pragma mark - Private

- (void)commonInit {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.mainView];
    [self configureView];
}

- (void)configureView {
    [self prepareConstraints];
    [self prepareSubviews];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.mainView}]];
}

- (void)prepareSubviews {
    self.headerTitleLabel.font = [SAStyle regularFontOfSize:15.f];
}

@end

//
//  SAAboutDeliveryViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SARestaurantPlainObject;

@protocol SAAboutDeliveryViewInput <NSObject>

- (void)updateWithRestaurantInfo:(NSArray *)restaurantInfo;

- (void)showIndicator:(BOOL)show;

- (void)setErrorState:(NSError *)error;

@end

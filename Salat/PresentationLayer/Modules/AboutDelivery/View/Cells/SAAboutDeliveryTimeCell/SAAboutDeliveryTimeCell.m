//
//  SAAboutDeliveryTimeCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryTimeCell.h"
#import "SADeliveryTimePlainObject.h"
#import "SADeliveryTimeHelper.h"
#import "SADeliveryTimeDayPlainObject.h"

@interface SAAboutDeliveryTimeCell ()
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation SAAboutDeliveryTimeCell

#pragma mark - Configure

- (void)configureCell {
    self.dayLabel.font = [SAStyle regularFontOfSize:11.f];
    self.timeLabel.font = [SAStyle regularFontOfSize:11.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SADeliveryTimeDayPlainObject class]]) {
        [self configureWithDeliveryTime:model];
    }
}

#pragma mark - Private

- (void)configureWithDeliveryTime:(SADeliveryTimeDayPlainObject *)deliveryTime {
    NSNumber *dayNumber = deliveryTime.dow;

    self.dayLabel.text = [SADeliveryTimeHelper dayDescriptionByDayNumber:dayNumber];
    self.timeLabel.text = deliveryTime.workTimes;

    UIColor *dayColor = [SADeliveryTimeHelper dayColorByDayNumber:dayNumber andWorkTimes:deliveryTime.workTimes];
    self.dayLabel.textColor = dayColor;
    self.timeLabel.textColor = dayColor;

}

@end

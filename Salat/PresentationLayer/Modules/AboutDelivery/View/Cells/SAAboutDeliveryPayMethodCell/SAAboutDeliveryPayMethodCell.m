//
//  SAAboutDeliveryPayMethodCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryPayMethodCell.h"
#import "SAPaymentMethodPlainObject.h"

@interface SAAboutDeliveryPayMethodCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descrLabel;
@end

@implementation SAAboutDeliveryPayMethodCell

#pragma mark - Configure

- (void)configureCell {
    self.titleLabel.textColor = [SAStyle lightGreenColor];
    self.titleLabel.font = [SAStyle regularFontOfSize:11.f];
    self.descrLabel.textColor = [SAStyle grayColor];
    self.descrLabel.font = [SAStyle regularFontOfSize:11.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAPaymentMethodPlainObject class]]) {
        [self configureWithPayMethod:model];
    }
}

#pragma mark - Private

- (void)configureWithPayMethod:(SAPaymentMethodPlainObject *)payMethod {
    self.titleLabel.text = payMethod.title;
    self.descrLabel.text = payMethod.descr;
}

@end

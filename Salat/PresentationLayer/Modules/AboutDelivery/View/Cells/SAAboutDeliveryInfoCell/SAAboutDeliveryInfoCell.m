//
//  SAAboutDeliveryInfoCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryInfoCell.h"

@interface SAAboutDeliveryInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end

@implementation SAAboutDeliveryInfoCell

#pragma mark - Configure

- (void)configureCell {
    self.infoLabel.textColor = [SAStyle grayColor];
    self.infoLabel.font = [SAStyle regularFontOfSize:12.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSString class]]) {
        self.infoLabel.text = model;
    }
}

@end

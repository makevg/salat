//
//  SAAboutDeliveryAreaCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryAreaCell.h"
#import "SADeliveryAreaPlainObject.h"
#import "NSString+Currency.h"

@interface SAAboutDeliveryAreaCell ()
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@end

@implementation SAAboutDeliveryAreaCell

#pragma mark - Configure

- (void)configureCell {
    self.priceLabel.textColor = [SAStyle lightGreenColor];
    self.priceLabel.font = [SAStyle regularFontOfSize:11.f];
    self.areaLabel.textColor = [SAStyle grayColor];
    self.areaLabel.font = [SAStyle regularFontOfSize:11.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SADeliveryAreaPlainObject class]]) {
        [self configureWithDeliveryArea:model];
    }
}

#pragma mark - Private

- (void)configureWithDeliveryArea:(SADeliveryAreaPlainObject *)deliveryArea {
    self.priceLabel.text = [NSString priceWithCurrencySymbol:deliveryArea.price];
    self.areaLabel.text = deliveryArea.title;
}

@end

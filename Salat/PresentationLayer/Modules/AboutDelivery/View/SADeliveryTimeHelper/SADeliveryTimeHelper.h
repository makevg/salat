//
//  SADeliveryTimeHelper.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SADeliveryTimeHelper : NSObject

+ (NSString *)dayDescriptionByDayNumber:(NSNumber *)dayNumber;
+ (UIColor *)dayColorByDayNumber:(NSNumber *)dayNumber andWorkTimes:(NSString *)workTimes;

@end

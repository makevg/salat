//
//  SADeliveryTimeHelper.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SADeliveryTimeHelper.h"
#import "SAStyle.h"

@implementation SADeliveryTimeHelper

#pragma mark - Public

+ (NSString *)dayDescriptionByDayNumber:(NSNumber *)dayNumber {
    switch ([dayNumber integerValue]) {
        case 1:
            return @"понедельник";
        case 2:
            return @"вторник";
        case 3:
            return @"среда";
        case 4:
            return @"четверг";
        case 5:
            return @"пятница";
        case 6:
            return @"суббота";
        case 7:
            return @"воскресенье";
        default:
            return nil;
    }
}

+ (UIColor *)dayColorByDayNumber:(NSNumber *)dayNumber andWorkTimes:(NSString *)workTimes {
    if([workTimes isEqualToString:@"закрыто"])
        return [SAStyle redColor];

    switch ([dayNumber integerValue]) {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            return [SAStyle grayColor];
        case 6:
        case 7:
            return [SAStyle lightGreenColor];
        default:
            return nil;
    }
}

@end

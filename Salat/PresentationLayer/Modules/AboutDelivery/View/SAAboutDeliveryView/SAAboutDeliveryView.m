//
//  SAAboutDeliveryView.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryView.h"

@implementation SAAboutDeliveryView

#pragma mark - Setup

- (void)setup {
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 58.f;
    self.tableView.backgroundColor = [SAStyle whiteColor];
}

@end

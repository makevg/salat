//
//  SAAboutDeliveryModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryModuleAssembly.h"
#import "SAAboutDeliveryVC.h"
#import "SAAboutDeliveryPresenter.h"
#import "SAAboutDeliveryInteractor.h"
#import "SAAboutDeliveryDataDisplayManager.h"

@implementation SAAboutDeliveryModuleAssembly

- (SAAboutDeliveryVC *)viewAboutDelivery {
    return [TyphoonDefinition withClass:[SAAboutDeliveryVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAboutDelivery]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerAboutDelivery]];
    }];
}

- (SAAboutDeliveryPresenter *)presenterAboutDelivery {
    return [TyphoonDefinition withClass:[SAAboutDeliveryPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAboutDelivery]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAboutDelivery]];
    }];
}

- (SAAboutDeliveryInteractor *)interactorAboutDelivery {
    return [TyphoonDefinition withClass:[SAAboutDeliveryInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAboutDelivery]];
                              [definition injectProperty:@selector(restaurantService)
                                                    with:[self.serviceComponents restaurantService]];
                              [definition injectProperty:@selector(restaurantMapper)
                                                    with:[self.coreComponents restaurantMapper]];
                              [definition injectProperty:@selector(deliveryTimeMapper)
                                                    with:[self.coreComponents deliveryTimeMapper]];
                              [definition injectProperty:@selector(deliveryAreaMapper)
                                                    with:[self.coreComponents deliveryAreaMapper]];
                              [definition injectProperty:@selector(paymentMethodMapper)
                                                    with:[self.coreComponents paymentMethodMapper]];
    }];
}

- (SAAboutDeliveryDataDisplayManager *)displayManagerAboutDelivery {
    return [TyphoonDefinition withClass:[SAAboutDeliveryDataDisplayManager class]];
}

@end

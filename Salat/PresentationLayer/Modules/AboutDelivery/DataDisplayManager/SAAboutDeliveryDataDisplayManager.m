//
//  SAAboutDeliveryDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryDataDisplayManager.h"
#import "SAAboutDeliveryTimeCell.h"
#import "SAAboutDeliveryAreaCell.h"
#import "SAAboutDeliveryPayMethodCell.h"
#import "SAAboutDeliveryHeader.h"
#import "SAAboutDeliveryFooter.h"
#import "SAAboutDeliveryInfoCell.h"

typedef NS_ENUM(NSUInteger, SAAboutDeliverySectionType) {
    SAAboutDeliverySectionTypeTimes = 0,
    SAAboutDeliverySectionTypeAreas,
    SAAboutDeliverySectionTypePayMethods
};

@interface SAAboutDeliveryDataDisplayManager ()
@property(nonatomic) NSArray *restaurantInfo;
@property(nonatomic) NSArray *headers;
@end

@implementation SAAboutDeliveryDataDisplayManager

#pragma mark - Lazy init

- (NSArray *)headers {
    return @[@"Время работы доставки", @"Стоимость доставки", @"Способы оплаты"];
}

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - Public

- (void)configureDisplayManagerWithRestaurantInfo:(NSArray *)restaurantInfo {
    self.restaurantInfo = restaurantInfo;
}

- (BOOL)hasData {
    return self.restaurantInfo.count > 0;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.restaurantInfo count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case SAAboutDeliverySectionTypeTimes:
            return [self.restaurantInfo[(NSUInteger) section] count] + 1;
        case SAAboutDeliverySectionTypeAreas:
        case SAAboutDeliverySectionTypePayMethods:
            return [self.restaurantInfo[(NSUInteger) section] count];
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSInteger timesCount = [self.restaurantInfo[SAAboutDeliverySectionTypeTimes] count];
    NSString *identifier = [self cellIdentifierByIndexPath:indexPath];
    SABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                forIndexPath:indexPath];

    switch (indexPath.section) {
        case SAAboutDeliverySectionTypeTimes: {
            if (row == timesCount)
                [cell setModel:@"В праздничные и выходные дни возможны изменения в графике работ, следите за новостями."];
            else
                [cell setModel:self.restaurantInfo[(NSUInteger) indexPath.section][(NSUInteger) indexPath.row]];
            break;
        }
        case SAAboutDeliverySectionTypeAreas:
        case SAAboutDeliverySectionTypePayMethods:
        default:
            [cell setModel:self.restaurantInfo[(NSUInteger) indexPath.section][(NSUInteger) indexPath.row]];
    }

    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, [SAAboutDeliveryHeader headerHeight]);
    SAAboutDeliveryHeader *header = [[SAAboutDeliveryHeader alloc] initWithFrame:frame];
    header.title = self.headers[(NSUInteger) section];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [SAAboutDeliveryHeader headerHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, [SAAboutDeliveryFooter footerHeight]);
    SAAboutDeliveryFooter *footer = [[SAAboutDeliveryFooter alloc] initWithFrame:frame];

    switch (section) {
        case SAAboutDeliverySectionTypeTimes:
        case SAAboutDeliverySectionTypeAreas:
            return footer;
        case SAAboutDeliverySectionTypePayMethods:
        default:
            return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
        case SAAboutDeliverySectionTypeTimes:
        case SAAboutDeliverySectionTypeAreas:
            return [SAAboutDeliveryFooter footerHeight];
        case SAAboutDeliverySectionTypePayMethods:
        default:
            return 1.f;
    }
}

#pragma mark - Private

- (NSString *)cellIdentifierByIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSInteger timesCount = [self.restaurantInfo[SAAboutDeliverySectionTypeTimes] count];
    switch (indexPath.section) {
        case SAAboutDeliverySectionTypeTimes:
            return row == timesCount ? [SAAboutDeliveryInfoCell cellIdentifier] : [SAAboutDeliveryTimeCell cellIdentifier];
        case SAAboutDeliverySectionTypeAreas:
            return row == 0 ? [SAAboutDeliveryInfoCell cellIdentifier] : [SAAboutDeliveryAreaCell cellIdentifier];
        case SAAboutDeliverySectionTypePayMethods:
            return [SAAboutDeliveryPayMethodCell cellIdentifier];
        default:
            return @"";
    }
}

@end

//
//  SAAboutDeliveryDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SARestaurantPlainObject;

@interface SAAboutDeliveryDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;

- (void)configureDisplayManagerWithRestaurantInfo:(NSArray *)restaurantInfo;

- (BOOL)hasData;

@end

//
//  SAAboutDeliveryInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SARestaurantPlainObject;

@protocol SAAboutDeliveryInteractorOutput <NSObject>

- (void)didObtainRestaurantInfo:(NSArray *)restaurantInfo;

- (void)didObtainTransformInfo:(NSArray *)restaurantInfo;

- (void)didObtainError:(NSError *)error;

@end

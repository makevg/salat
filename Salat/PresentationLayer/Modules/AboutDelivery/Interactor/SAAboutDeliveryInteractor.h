//
//  SAAboutDeliveryInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryInteractorInput.h"

@protocol SAAboutDeliveryInteractorOutput;
@class SARestaurantService;
@class SARestaurantMapper;
@class SADeliveryTimeMapper;
@class SADeliveryAreaMapper;
@class SAPaymentMethodMapper;

@interface SAAboutDeliveryInteractor : NSObject <SAAboutDeliveryInteractorInput>

@property (weak, nonatomic) id<SAAboutDeliveryInteractorOutput> output;
@property (nonatomic) SARestaurantService *restaurantService;
@property (nonatomic) SARestaurantMapper *restaurantMapper;
@property (nonatomic) SADeliveryTimeMapper *deliveryTimeMapper;
@property (nonatomic) SADeliveryAreaMapper *deliveryAreaMapper;
@property (nonatomic) SAPaymentMethodMapper *paymentMethodMapper;

@end

//
//  SAAboutDeliveryInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 23.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAboutDeliveryInteractor.h"
#import "SAAboutDeliveryInteractorOutput.h"
#import "SARestaurantService.h"
#import "SARestaurantMapper.h"
#import "SADeliveryTimeMapper.h"
#import "SADeliveryAreaMapper.h"
#import "SAPaymentMethodMapper.h"
#import "SADeliveryTimePlainObject.h"
#import "SADeliveryAreaPlainObject.h"
#import "SAPaymentMethodPlainObject.h"
#import "Restaurant.h"
#import "DeliveryTime.h"
#import "SAServiceLayer.h"
#import "SADeliveryTimeDayPlainObject.h"
#import "SACommon.h"

@implementation SAAboutDeliveryInteractor

#pragma mark - SAAboutDeliveryInteractorInput

- (void)obtainRestaurantInfo {
    __weak typeof(self) weakSelf = self;

    [self.restaurantService obtainRestaurant:^(Restaurant *restaurant) {
                dispatch_safe_main_async(^{
                    [weakSelf processRestaurant:restaurant];
                })
            }
                                       error:^(NSError *error) {
                                           dispatch_safe_main_async(^{
                                               [weakSelf.output didObtainError:error];
                                           });
                                       }];
}

- (void)obtainRestaurantInfoFromNetwork {
    __weak typeof(self) weakSelf = self;
    
    [self.restaurantService obtainRestaurantInfoFromNetwork:^(Restaurant *restaurant) {
        dispatch_safe_main_async(^{
            [weakSelf processRestaurant:restaurant];
        });
    }
                                                      error:^(NSError *error) {
                                                          dispatch_safe_main_async(^{
                                                              [weakSelf.output didObtainError:error];
                                                          });
                                                      }];
}

- (void)convertWorkingTimes:(NSArray *)restaurantInfo {
    weakifySelf;

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        strongifySelf;

        NSDictionary *interDays = [strongSelf transformPlainWorkTimesToDaysDict:restaurantInfo[0]];
        NSArray *sortedKeys = [[interDays allKeys] sortedArrayUsingSelector:@selector(compare:)];

        NSMutableArray *days = [NSMutableArray new];

        for (NSNumber *dow in sortedKeys) {
            NSArray *sortedTimes = [interDays[dow] sortedArrayUsingComparator:^NSComparisonResult(NSArray <NSNumber *> *obj1, NSArray <NSNumber *> *obj2) {
                return [obj1[0] compare:obj2[0]];
            }];

            [days addObject:[strongSelf handleDay:sortedTimes dow:dow source:interDays]];
        }

        NSArray *viewOutput = @[days, restaurantInfo[1], restaurantInfo[2]];

        dispatch_safe_main_async(^{
            [weakSelf.output didObtainTransformInfo:viewOutput];
        });
    });
}

#pragma mark - Private

- (void)processRestaurant:(Restaurant *)restaurant {
    NSSortDescriptor *timeDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"day_number"
                                                                     ascending:YES];

    NSMutableArray *deliveryTimes = [[self getPlainTimesFromManagedObjects:[restaurant.delivery_times sortedArrayUsingDescriptors:@[timeDescriptor]]] mutableCopy];
    deliveryTimes = [[deliveryTimes sortedArrayUsingComparator:^NSComparisonResult(SADeliveryTimePlainObject *obj1, SADeliveryTimePlainObject *obj2) {
        return [obj1.dayNumber compare:obj2.dayNumber];
    }] mutableCopy];

    NSMutableArray *deliveryAreas = [@[@"Сумма доставки зависит от вашего адреса доставки"] mutableCopy];

    NSSortDescriptor *areaDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"price"
                                                                     ascending:YES];

    [deliveryAreas addObjectsFromArray:[self getPlainAreasFromManagedObjects:[restaurant.delivery_areas sortedArrayUsingDescriptors:@[areaDescriptor]]]];

    NSSortDescriptor *payDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"id"
                                                                    ascending:YES];

    NSArray *payMethods = [self getPlainPayMethodsFromManagedObjects:[restaurant.payment_methods sortedArrayUsingDescriptors:@[payDescriptor]]];

    NSArray *restaurantInfo = @[deliveryTimes, deliveryAreas, payMethods];
    [self.output didObtainRestaurantInfo:restaurantInfo];
}

- (NSArray<SADeliveryTimePlainObject *> *)getPlainTimesFromManagedObjects:(NSArray<DeliveryTime *> *)managedObjectTimes {
    NSMutableArray<SADeliveryTimePlainObject *> *timePlainObjects = [NSMutableArray<SADeliveryTimePlainObject *> array];
    for (DeliveryTime *managedObjectDeliveryTime in managedObjectTimes) {
        if ([managedObjectDeliveryTime.day_number integerValue] > 0) {
            SADeliveryTimePlainObject *deliveryTimePlainObject = [SADeliveryTimePlainObject new];
            [self.deliveryTimeMapper fillObject:deliveryTimePlainObject withObject:managedObjectDeliveryTime];
            [timePlainObjects addObject:deliveryTimePlainObject];
        }
    }
    return timePlainObjects;
}

- (NSArray<SADeliveryAreaPlainObject *> *)getPlainAreasFromManagedObjects:(NSArray<DeliveryArea *> *)managedObjectAreas {
    NSMutableArray<SADeliveryAreaPlainObject *> *areaPlainObjects = [NSMutableArray<SADeliveryAreaPlainObject *> array];
    for (DeliveryArea *managedObjectArea in managedObjectAreas) {
        SADeliveryAreaPlainObject *deliveryAreaPlainObject = [SADeliveryAreaPlainObject new];
        [self.deliveryAreaMapper fillObject:deliveryAreaPlainObject withObject:managedObjectArea];
        [areaPlainObjects addObject:deliveryAreaPlainObject];
    }
    return areaPlainObjects;
}

- (NSArray<SAPaymentMethodPlainObject *> *)getPlainPayMethodsFromManagedObjects:(NSArray<PaymentMethod *> *)managedObjectPayMethods {
    NSMutableArray<SAPaymentMethodPlainObject *> *payMethodObjects = [NSMutableArray<SAPaymentMethodPlainObject *> array];
    for (PaymentMethod *paymentMethod in managedObjectPayMethods) {
        SAPaymentMethodPlainObject *payMethodPlainObject = [SAPaymentMethodPlainObject new];
        [self.paymentMethodMapper fillObject:payMethodPlainObject withObject:paymentMethod];
        [payMethodObjects addObject:payMethodPlainObject];
    }
    return payMethodObjects;
}

#pragma mark - Working times

- (NSDictionary *)transformPlainWorkTimesToDaysDict:(NSArray <SADeliveryTimePlainObject *> *)deliveryTimes {
    NSMutableDictionary *interDays = [@{
            @1 : [@[] mutableCopy],
            @2 : [@[] mutableCopy],
            @3 : [@[] mutableCopy],
            @4 : [@[] mutableCopy],
            @5 : [@[] mutableCopy],
            @6 : [@[] mutableCopy],
            @7 : [@[] mutableCopy],
    } mutableCopy];

    for (SADeliveryTimePlainObject *deliveryTimePlainObject in deliveryTimes) {
        if (!deliveryTimePlainObject.isOpened)
            continue;

        [interDays[deliveryTimePlainObject.dayNumber] addObject:@[deliveryTimePlainObject.timeStart, deliveryTimePlainObject.timeEnd]];
    }

    return interDays;
}

- (NSNumber *)findEndOfCurrentDayInNextDay:(NSDictionary *)source dayKey:(NSNumber *)dayKey {
    NSArray *nextSortedTimes = [source[dayKey] sortedArrayUsingComparator:^NSComparisonResult(NSArray <NSNumber *> *obj1, NSArray <NSNumber *> *obj2) {
        return [obj1[0] compare:obj2[0]];
    }];

    for (NSArray *times in nextSortedTimes) {
        if ([times[0] integerValue] == 0) {
            return times[1];
        }
    }

    return nil;
}

- (NSNumber *)findStartOfCurrentDayInPrevDay:(NSDictionary *)source dayKey:(NSNumber *)dayKey {
    NSArray *nextSortedTimes = [source[dayKey] sortedArrayUsingComparator:^NSComparisonResult(NSArray <NSNumber *> *obj1, NSArray <NSNumber *> *obj2) {
        return [obj1[0] compare:obj2[0]];
    }];

    for (NSArray *times in nextSortedTimes) {
        if ([times[1] integerValue] == 86400) {
            return times[0];
        }
    }

    return nil;
}

- (SADeliveryTimeDayPlainObject *)handleDay:(NSArray *)sortedTimes dow:(NSNumber *)dow source:(NSDictionary *)source {
    NSMutableString *workTimes = [NSMutableString new];
    for (NSArray *workPeriod in sortedTimes) {
        if ([workTimes length])
            [workTimes appendString:@"\r\n"];

        long timeFromInSeconds = [workPeriod[0] integerValue];
        long timeToInSeconds = [workPeriod[1] integerValue];

        if (timeFromInSeconds == 0) {
            //look previous day
            long prevDay = [dow integerValue];
            prevDay = ((prevDay + 5) % 7) + 1;

            NSNumber *startOfDay = [self findStartOfCurrentDayInPrevDay:source dayKey:@(prevDay)];
            if (startOfDay && [startOfDay integerValue]) {
                continue;
            }
        }

        if (timeToInSeconds == 86400) {
            //look next day
            long nextDay = [dow integerValue];
            nextDay = (nextDay % 7) + 1;

            NSNumber *endOfDay = [self findEndOfCurrentDayInNextDay:source dayKey:@(nextDay)];
            if (endOfDay)
                timeToInSeconds = [endOfDay integerValue];
        }

        [workTimes appendString:[NSString stringWithFormat:@"с %ld:%02ld до %ld:%02ld",
                                 (timeFromInSeconds / 3600) % 24,
                                 (timeFromInSeconds / 60) % 60,
                                 timeToInSeconds == 86400 ? 24 : (timeToInSeconds / 3600) % 24,
                                 (timeToInSeconds / 60) % 60]];
    }

    if (![workTimes length])
        [workTimes appendString:@"закрыто"];

    return [SADeliveryTimeDayPlainObject objectWithWorkTimes:workTimes dow:dow];
}

@end

//
//  SAOrderView.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@interface SAOrderView : SABaseView

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

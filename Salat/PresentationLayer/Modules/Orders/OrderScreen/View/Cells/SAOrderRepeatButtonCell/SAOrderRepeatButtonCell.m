//
//  SAOrderRepeatButtonCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderRepeatButtonCell.h"
#import "SABorderButton.h"

static CGFloat const cOrderRepeatButtonCellHeight = 90.f;

@interface SAOrderRepeatButtonCell ()
@property (weak, nonatomic) IBOutlet SABorderButton *repeatButton;
@end

@implementation SAOrderRepeatButtonCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cOrderRepeatButtonCellHeight;
}

- (void)configureCell {
    [super configureCell];

    self.repeatButton.titleLabel.font = [SAStyle regularFontOfSize:16.f];
}

@end

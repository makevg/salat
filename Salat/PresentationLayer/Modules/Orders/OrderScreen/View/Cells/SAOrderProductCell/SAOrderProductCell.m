//
//  SAOrderProductCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderProductCell.h"
#import "NSString+Currency.h"
#import "UIImageView+AsyncLoad.h"
#import "SAOrderProductPlainObject.h"
#import "SAOrderProductModifierPlainObject.h"

static CGFloat cOrderProductCellHeight = 85.f;

@interface SAOrderProductCell ()
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescrLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCostLabel;
@end

@implementation SAOrderProductCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cOrderProductCellHeight;
}

- (void)configureCell {
    [super configureCell];

    self.productTitleLabel.font = [SAStyle regularFontOfSize:14.f];
    self.productDescrLabel.font = [SAStyle regularFontOfSize:12.f];
    self.productCostLabel.font = [SAStyle regularFontOfSize:12.f];
    self.productImageView.layer.cornerRadius = CGRectGetWidth(self.productImageView.frame)/2;
    self.productImageView.clipsToBounds = YES;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAOrderProductPlainObject class]]) {
        [self configureWithProduct:model];
    }
}

#pragma mark - Private

- (void)configureWithProduct:(SAOrderProductPlainObject *)product {
    double totalPrice = 0;
    if ([product.modifiers count] > 0) {
        for (SAOrderProductModifierPlainObject *modifier in product.modifiers) {
            totalPrice += [modifier.price doubleValue];
        }
    } else {
        totalPrice = [product.price doubleValue];
    }
    self.productTitleLabel.text = product.title;
    self.productDescrLabel.text = product.descr;
    self.productCostLabel.text = [NSString stringWithFormat:@"%@ x %@", product.quantity,
                    [NSString priceWithCurrencySymbol:@(totalPrice)]];
    [self.productImageView loadAsyncFromUrl:product.imageUrl];
}

@end

//
//  SAOrderSumCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderSumCell.h"
#import "NSString+Currency.h"

static CGFloat const cOrderSumCellHeight = 40.f;

@interface SAOrderSumCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@end

@implementation SAOrderSumCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cOrderSumCellHeight;
}

- (void)configureCell {
    [super configureCell];

    self.titleLabel.font = [SAStyle regularFontOfSize:16.f];
    self.valueLabel.font = [SAStyle regularFontOfSize:16.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSNumber class]]) {
        self.valueLabel.text = [NSString priceWithCurrencySymbol:model];
    }
}

@end

//
// Created by mtx on 09.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAOrderBaseCell.h"


@implementation SAOrderBaseCell

- (void)configureCell {
    [super configureCell];

    self.backgroundColor = [UIColor clearColor];
    self.backgroundView = [UIView new];
    self.selectedBackgroundView = [UIView new];
}

@end
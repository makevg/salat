//
//  SAOrderAddressCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderAddressCell.h"

@interface SAOrderAddressCell ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@end

static CGFloat const cOrderAddressCellHeight = 30.f;

@implementation SAOrderAddressCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cOrderAddressCellHeight;
}

- (void)configureCell {
    [super configureCell];

    self.addressLabel.font = [SAStyle regularFontOfSize:13.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSString class]]) {
        self.addressLabel.text = model;
    }
}

@end

//
//  SAOrderStatusCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderStatusCell.h"
#import "SAOrderHelper.h"

static CGFloat const cOrderStatusCellHeight = 40.f;

@interface SAOrderStatusCell ()
@property (weak, nonatomic) IBOutlet UIView *leftLine;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *rightLine;
@end

@implementation SAOrderStatusCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cOrderStatusCellHeight;
}

- (void)configureCell {
    [super configureCell];

    self.statusLabel.font = [SAStyle regularFontOfSize:14.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSNumber class]]) {
        self.statusLabel.text = [SAOrderHelper stringByOrderStatus:model];
        self.statusLabel.textColor = [SAOrderHelper colorByOrderStatus:model];
        self.leftLine.backgroundColor = [SAOrderHelper colorByOrderStatus:model];
        self.rightLine.backgroundColor = [SAOrderHelper colorByOrderStatus:model];
    }
}

@end

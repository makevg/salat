//
//  SAOrderDateCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderDateCell.h"
#import "SADataFormatter.h"
#import "SAOrderPlainObject.h"

static CGFloat const cOrderDateCellHeight = 30.f;

@interface SAOrderDateCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@end

@implementation SAOrderDateCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cOrderDateCellHeight;
}

- (void)configureCell {
    [super configureCell];

    self.dateLabel.font = [SAStyle regularFontOfSize:12.f];
    self.orderNumberLabel.font = [SAStyle regularFontOfSize:12.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAOrderPlainObject class]]) {
        [self configureWithOrder:model];
    }
}

#pragma mark - Private

- (void)configureWithOrder:(SAOrderPlainObject *)order {
    self.dateLabel.text = [SADataFormatter fullDateTimeStringByUnixTimeStamp:order.createdAt];
    self.orderNumberLabel.text = [NSString stringWithFormat:@"№ %@", order.orderId];
}

@end

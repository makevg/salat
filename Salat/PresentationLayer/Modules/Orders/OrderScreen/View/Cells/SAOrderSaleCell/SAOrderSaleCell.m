//
//  SAOrderSaleCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderSaleCell.h"

static CGFloat const cOrderSaleCellHeight = 50.f;

@interface SAOrderSaleCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end

@implementation SAOrderSaleCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cOrderSaleCellHeight;
}

- (void)configureCell {
    [super configureCell];

    self.titleLabel.font = [SAStyle regularFontOfSize:14.f];
    self.valueLabel.font = [SAStyle regularFontOfSize:14.f];
    self.descriptionLabel.font = [SAStyle regularFontOfSize:11.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSDictionary class]]) {
        self.valueLabel.text = [NSString stringWithFormat:@"%@ %%", model[@"discountPercent"]];
        self.descriptionLabel.text = model[@"discountTitle"];
    }
}

@end

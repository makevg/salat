//
//  SAOrderVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderVC.h"
#import "SAOrderView.h"
#import "SAOrderViewOutput.h"
#import "SAOrdersListDisplayManager.h"
#import "SAOrderDisplayManager.h"

static NSString *const cOrderVCStoryboardName = @"SAOrders";

@interface SAOrderVC ()
@property (strong, nonatomic) IBOutlet SAOrderView *contentView;
@property (strong, nonatomic) IBOutlet UIImageView *backView;
@end

@implementation SAOrderVC

#pragma mark - Super

+ (NSString *)storyboardName {
    return cOrderVCStoryboardName;
}

- (void)configureController {
    [super configureController];

//    self.contentView.tableView.backgroundView = self.backView;
    self.displayManager.tableView = self.contentView.tableView;
    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Actions

- (IBAction)tappedRepeatButton:(id)sender {
    [self.output didTapRepeatOrderButton];
}

#pragma mark - SAOrderViewInput

- (void)updateWithOrderData:(NSArray *)orderData {
    [self.displayManager configureWithOrderData:orderData];
    [self.contentView.tableView reloadData];
}

@end

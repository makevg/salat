//
//  SAOrderVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAOrderViewInput.h"

@protocol SAOrderViewOutput;
@class SAOrderDisplayManager;

@interface SAOrderVC : SABaseVC <SAOrderViewInput>

@property (nonatomic) id<SAOrderViewOutput> output;
@property (nonatomic) SAOrderDisplayManager *displayManager;

@end

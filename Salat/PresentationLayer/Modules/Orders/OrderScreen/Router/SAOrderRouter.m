//
//  SAOrderRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderRouter.h"
#import "SACartVC.h"

@implementation SAOrderRouter

- (void)openCartModule {
    [((UIViewController *) self.transitionHandler).navigationController pushViewController:[self getCartVC] animated:YES];
}

#pragma mark - Private

- (UIViewController *)getCartVC {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[SACartVC storyboardName] bundle:nil];
    return [sb instantiateInitialViewController];
}

@end

//
//  SAOrderRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAOrderRouter : NSObject <SAOrderRouterInput>

@property (weak, nonatomic) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

//
//  SAOrderPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAOrderModuleInput.h"
#import "SAOrderViewOutput.h"
#import "SAOrderInteractorOutput.h"
#import "SAOrderPresenterStateStorage.h"

@protocol SAOrderViewInput;
@protocol SAOrderInteractorInput;
@protocol SAOrderRouterInput;

@interface SAOrderPresenter : NSObject <SAOrderModuleInput, SAOrderViewOutput, SAOrderInteractorOutput>

@property (weak, nonatomic) id<SAOrderViewInput> view;
@property (nonatomic) id<SAOrderInteractorInput> interactor;
@property (nonatomic) id<SAOrderRouterInput> router;
@property (nonatomic) SAOrderPresenterStateStorage *presenterStateStorage;

@end

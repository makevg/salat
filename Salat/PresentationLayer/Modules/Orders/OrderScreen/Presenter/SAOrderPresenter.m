//
//  SAOrderPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderPresenter.h"
#import "SAOrderViewInput.h"
#import "SAOrderInteractorInput.h"
#import "SAOrderRouterInput.h"

@implementation SAOrderPresenter

#pragma mark - SAOrderModuleInput

- (void)configureModuleWithOrderId:(NSNumber *)orderId {
    self.presenterStateStorage.orderId = orderId;
}

- (void)didTriggerViewDidLoadEvent {
    [self.interactor obtainOrderInfoByOrderId:self.presenterStateStorage.orderId];
}

- (void)didTapRepeatOrderButton {
    [self.interactor repeatOrder:self.presenterStateStorage.orderId];
    [self.router openCartModule];
}

#pragma mark - SAOrderInteractorOutput

- (void)didObtainOrderData:(NSArray *)orderData {
    [self.view updateWithOrderData:orderData];
}

- (void)didObtainError:(NSError *)error {
    //TODO: write
}

@end

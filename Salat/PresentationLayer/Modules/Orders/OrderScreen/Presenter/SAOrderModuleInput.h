//
//  SAOrderModuleInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SAOrderModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithOrderId:(NSNumber *)orderId;

@end

//
//  SAOrderPresenterStateStorage.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAOrderPresenterStateStorage : NSObject

@property (nonatomic) NSNumber *orderId;

@end

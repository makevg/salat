//
//  SAOrderDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderDisplayManager.h"
#import "SAOrderDateCell.h"
#import "SAOrderAddressCell.h"
#import "SAOrderStatusCell.h"
#import "SAOrderProductCell.h"
#import "SAOrderSaleCell.h"
#import "SAOrderSumCell.h"
#import "SAOrderRepeatButtonCell.h"

typedef NS_ENUM(NSUInteger, SAOrderSectionType) {
    SAOrderSectionTypeDate = 0,
    SAOrderSectionTypeAddress,
    SAOrderSectionTypeStatus,
    SAOrderSectionTypeProducts,
    SAOrderSectionTypeSale,
    SAOrderSectionTypeSum,
    SAOrderSectionTypeRepeatButton
};

@interface SAOrderDisplayManager ()
@property (nonatomic) NSArray *orderData;
@end

@implementation SAOrderDisplayManager

#pragma mark - Lazy init

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - Public

- (void)configureWithOrderData:(NSArray *)orderData {
    self.orderData = orderData;
}

#pragma mark - Private

- (NSString *)cellIdentifierAtIndexPath:(NSIndexPath *)indexPath {
    SAOrderSectionType type = (SAOrderSectionType) indexPath.section;
    switch (type) {
        case SAOrderSectionTypeDate:
            return [SAOrderDateCell cellIdentifier];
        case SAOrderSectionTypeAddress:
            return [SAOrderAddressCell cellIdentifier];
        case SAOrderSectionTypeStatus:
            return [SAOrderStatusCell cellIdentifier];
        case SAOrderSectionTypeProducts:
            return [SAOrderProductCell cellIdentifier];
        case SAOrderSectionTypeSale:
            return [SAOrderSaleCell cellIdentifier];
        case SAOrderSectionTypeSum:
            return [SAOrderSumCell cellIdentifier];
        case SAOrderSectionTypeRepeatButton:
            return [SAOrderRepeatButtonCell cellIdentifier];
        default:
            return nil;
    }
}

- (CGFloat)cellHeightAtIndexPath:(NSIndexPath *)indexPath {
    SAOrderSectionType type = (SAOrderSectionType) indexPath.section;
    switch (type) {
        case SAOrderSectionTypeDate:
            return [SAOrderDateCell cellHeight];
        case SAOrderSectionTypeAddress:
            return [SAOrderAddressCell cellHeight];
        case SAOrderSectionTypeStatus:
            return [SAOrderStatusCell cellHeight];
        case SAOrderSectionTypeProducts:
            return [SAOrderProductCell cellHeight];
        case SAOrderSectionTypeSale:
            return [SAOrderSaleCell cellHeight];
        case SAOrderSectionTypeSum:
            return [SAOrderSumCell cellHeight];
        case SAOrderSectionTypeRepeatButton:
            return [SAOrderRepeatButtonCell cellHeight];
        default:
            return [SABaseTableViewCell cellHeight];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.orderData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.orderData[(NSUInteger) section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [self cellIdentifierAtIndexPath:indexPath];
    SABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                forIndexPath:indexPath];
    [cell setModel:self.orderData[(NSUInteger) indexPath.section][(NSUInteger) indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellHeightAtIndexPath:indexPath];
}

@end

//
//  SAOrderModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderModuleAssembly.h"
#import "SAOrderVC.h"
#import "SAOrderPresenter.h"
#import "SAOrderInteractor.h"
#import "SAOrderRouter.h"
#import "SAOrderDisplayManager.h"

@implementation SAOrderModuleAssembly

- (SAOrderVC *)viewOrder {
    return [TyphoonDefinition withClass:[SAOrderVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterOrder]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerOrder]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
    }];
}

- (SAOrderPresenter *)presenterOrder {
    return [TyphoonDefinition withClass:[SAOrderPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewOrder]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorOrder]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerOrder]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self orderPresenterStateStorage]];
                          }];
}

- (SAOrderInteractor *)interactorOrder {
    return [TyphoonDefinition withClass:[SAOrderInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterOrder]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                              [definition injectProperty:@selector(ordersService)
                                                    with:[self.serviceComponents ordersService]];
                              [definition injectProperty:@selector(orderMapper)
                                                    with:[self.coreComponents orderMapper]];
                              [definition injectProperty:@selector(orderProductMapper)
                                                    with:[self.coreComponents orderProductMapper]];
                              [definition injectProperty:@selector(itemModifierMapper)
                                                    with:[self.coreComponents cartItemModifierMapper]];
                              [definition injectProperty:@selector(orderProductModifierMapper)
                                                    with:[self.coreComponents orderProductModifierMapper]];
                          }];
}

- (SAOrderRouter *)routerOrder {
    return [TyphoonDefinition withClass:[SAOrderRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewOrder]];
    }];
}

- (SAOrderDisplayManager *)displayManagerOrder {
    return [TyphoonDefinition withClass:[SAOrderDisplayManager class]];
}

- (SAOrderPresenterStateStorage *)orderPresenterStateStorage {
    return [TyphoonDefinition withClass:[SAOrderPresenterStateStorage class]];
}

@end

//
//  SAOrderInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SAOrderInteractorInput <NSObject>

- (void)obtainOrderInfoByOrderId:(NSNumber *)orderId;
- (void)repeatOrder:(NSNumber *)orderId;

@end

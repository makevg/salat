//
//  SAOrderInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderInteractor.h"
#import "SAOrderInteractorOutput.h"
#import "SAOrderPlainObject.h"
#import "SAPrototypeMapper.h"
#import "SAOrdersService.h"
#import "Order.h"
#import "SAOrderProductPlainObject.h"
#import "SACartService.h"
#import "SAOrderProductModifierPlainObject.h"
#import "OrderProductModifier.h"
#import "OrderProduct.h"

@implementation SAOrderInteractor

#pragma mark - SAOrderInteractorInput

- (void)obtainOrderInfoByOrderId:(NSNumber *)orderId {
    SAOrderPlainObject *orderPlainObject = [self getOrderPlainObject:orderId];
    NSString *address = [NSString stringWithFormat:@"%@, д. %@, кв. %@", orderPlainObject.addressStreet,
                                                   orderPlainObject.addressHouse, orderPlainObject.addressFlat];
    NSMutableArray *discount = [@[] mutableCopy];
    if([orderPlainObject.discountPercent integerValue])
        [discount addObject:@{
                @"discountPercent": orderPlainObject.discountPercent,
                @"discountTitle": orderPlainObject.discountTitle
        }];

    NSArray *orderData = @[@[orderPlainObject], @[address],
            @[orderPlainObject.status], orderPlainObject.products,
            discount, @[@((int)([orderPlainObject.cartSum doubleValue] - [orderPlainObject.discountSum doubleValue]))], @[@""]];
    [self.output didObtainOrderData:orderData];
}

- (void)repeatOrder:(NSNumber *)orderId {
    SAOrderPlainObject *orderPlainObject = [self getOrderPlainObject:orderId];

    [self.cartService repeatOrder:orderPlainObject mapper:self.itemModifierMapper];
}

#pragma mark - Private

- (SAOrderPlainObject *)getOrderPlainObject:(NSNumber *)orderId {
    Order *order = [self.ordersService obtainOrderById:orderId];
    SAOrderPlainObject *orderPlainObject = [SAOrderPlainObject new];
    [self.orderMapper fillObject:orderPlainObject withObject:order];
    orderPlainObject.products = [self getPlainProductsFromManagedObjects:[order.products allObjects]];
    return orderPlainObject;
}

- (NSArray<SAOrderProductPlainObject *> *)getPlainProductsFromManagedObjects:(NSArray<OrderProduct *> *)managedObjectProducts {
    NSMutableArray<SAOrderProductPlainObject *> *productsPlainObjects = [NSMutableArray array];
    for (OrderProduct *managedObjectProduct in managedObjectProducts) {
        SAOrderProductPlainObject *orderProductPlainObject = [SAOrderProductPlainObject new];
        [self.orderProductMapper fillObject:orderProductPlainObject withObject:managedObjectProduct];
        orderProductPlainObject.modifiers = [self getPlainModifierFromManagedObjects:[managedObjectProduct.modifiers allObjects]];
        [productsPlainObjects addObject:orderProductPlainObject];
    }
    return productsPlainObjects;
}

- (NSArray<SAOrderProductModifierPlainObject *> *)getPlainModifierFromManagedObjects:(NSArray<OrderProductModifier *> *)managedObjectModifiers {
    NSMutableArray<SAOrderProductModifierPlainObject *> *modifierPlainObjects
            = [NSMutableArray<SAOrderProductModifierPlainObject *> array];
    for (OrderProductModifier *managedObjectModifier in managedObjectModifiers) {
        SAOrderProductModifierPlainObject *orderProductModifierPlainObject = [SAOrderProductModifierPlainObject new];
        [self.orderProductModifierMapper fillObject:orderProductModifierPlainObject withObject:managedObjectModifier];
        [modifierPlainObjects addObject:orderProductModifierPlainObject];
    }
    return modifierPlainObjects;
}

@end

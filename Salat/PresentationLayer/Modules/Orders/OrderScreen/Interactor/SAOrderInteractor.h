//
//  SAOrderInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAOrderInteractorInput.h"

@protocol SAOrderInteractorOutput;
@class SACartService;
@protocol SAPrototypeMapper;
@class SAOrdersService;

@interface SAOrderInteractor : NSObject <SAOrderInteractorInput>

@property (weak, nonatomic) id<SAOrderInteractorOutput> output;
@property (nonatomic) SAOrdersService *ordersService;
@property (nonatomic) SACartService *cartService;
@property (nonatomic) id<SAPrototypeMapper> orderMapper;
@property (nonatomic) id<SAPrototypeMapper> orderProductMapper;
@property (nonatomic) id<SAPrototypeMapper> itemModifierMapper;
@property (nonatomic) id<SAPrototypeMapper> orderProductModifierMapper;

@end

//
//  SAOrderInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SAOrderInteractorOutput <NSObject>

- (void)didObtainOrderData:(NSArray *)orderData;

- (void)didObtainError:(NSError *)error;

@end

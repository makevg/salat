//
// Created by Maximychev Evgeny on 07.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAOrderHelper : NSObject

+ (NSString *)stringByOrderStatus:(NSNumber *)status;
+ (UIColor *)colorByOrderStatus:(NSNumber *)status;
+ (CGFloat)backgroundAlphaByOrderStatus:(NSNumber *)status;

@end
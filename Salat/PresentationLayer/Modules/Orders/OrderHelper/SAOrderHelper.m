//
// Created by Maximychev Evgeny on 07.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAOrderHelper.h"
#import "SAStyle.h"

@implementation SAOrderHelper

+ (NSString *)stringByOrderStatus:(NSNumber *)status {
    switch ([status integerValue]) {
        case 1:
            return @"В обработке";
        case 2:
            return @"Принят";
        case 3:
            return @"Доставлен";
        case 4:
            return @"Отменен";
        default:
            return nil;
    }
}

+ (UIColor *)colorByOrderStatus:(NSNumber *)status {
    switch ([status integerValue]) {
        case 1:
            return [UIColor grayColor];
        case 2:
            return [UIColor orangeColor];
        case 3:
            return [SAStyle mediumGreenColor];
        case 4:
            return [UIColor redColor];
        default:
            return nil;
    }
}

+ (CGFloat)backgroundAlphaByOrderStatus:(NSNumber *)status {
    return [status integerValue] == 1 ? 0.f : 0.7f;
}

@end
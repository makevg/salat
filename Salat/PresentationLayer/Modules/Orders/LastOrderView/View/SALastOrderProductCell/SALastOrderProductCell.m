//
//  SALastOrderProductCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALastOrderProductCell.h"
#import "UIImageView+AsyncLoad.h"

@interface SALastOrderProductCell ()
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@end

@implementation SALastOrderProductCell

#pragma mark - Configure

- (void)configureCell {
    self.backgroundColor = [SAStyle clearColor];
    self.productImageView.layer.cornerRadius = CGRectGetWidth(self.productImageView.frame)/2;
    self.productImageView.clipsToBounds = YES;
    self.productImageView.image = [UIImage imageNamed:@"noproduct"];
}

- (void)setModel:(id)model {
//    if ([model isKindOfClass:[NSString class]]) {
        [self.productImageView loadAsyncFromUrl:model];
//    }
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

@end

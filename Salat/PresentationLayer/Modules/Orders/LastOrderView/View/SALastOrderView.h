//
//  SALastOrderView.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"
#import "SALastOrderViewInput.h"

@protocol SALastOrderViewOutput;

@interface SALastOrderView : SABaseView <SALastOrderViewInput>

@property (nonatomic) id<SALastOrderViewOutput> output;

@end

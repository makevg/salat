//
//  SALastOrderViewInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAOrderPlainObject;

@protocol SALastOrderViewInput <NSObject>

- (void)updateWithOrder:(SAOrderPlainObject *)order;

@end

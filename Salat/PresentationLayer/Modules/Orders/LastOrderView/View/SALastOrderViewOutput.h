//
//  SALastOrderViewOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SALastOrderViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didTapRepeatOrderButton:(NSNumber *)orderId;

@end

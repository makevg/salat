//
//  SALastOrderView.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALastOrderView.h"
#import "SAOrderPlainObject.h"
#import "SADataFormatter.h"
#import "NSString+Currency.h"
#import "SALastOrderProductCell.h"
#import "SALastOrderViewOutput.h"
#import "SAOrderProductPlainObject.h"
#import "SALastOrderPresenter.h"
#import "SALastOrderInteractor.h"
#import "SALastOrderRouter.h"

@interface SALastOrderView () <UICollectionViewDataSource>
@property(strong, nonatomic) IBOutlet UIView *mainView;
@property(weak, nonatomic) IBOutlet UICollectionView *productsCollectionView;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *productsCountLabel;
@property(weak, nonatomic) IBOutlet UILabel *sumTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *sumValueLabel;
@property(weak, nonatomic) IBOutlet UILabel *addressLabel;
@property(nonatomic) NSArray<SAOrderProductPlainObject *> *products;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *productsCollectionViewHeightConstraint;
@end

@implementation SALastOrderView {
    NSNumber *orderId;
}

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

#pragma mark - Private

- (void)commonInit {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.mainView];
    [self configureView];
}

- (void)configureView {
    [self prepareConstraints];
    [self prepareSubviews];
    [self configureModule];
    [self.output didTriggerViewDidLoadEvent];
}

- (void)configureModule {
    SALastOrderPresenter *presenter = [SALastOrderPresenter new];
    SALastOrderInteractor *interactor = [SALastOrderInteractor new];
    SALastOrderRouter *router = [SALastOrderRouter new];
    presenter.view = self;
    interactor.output = presenter;
    presenter.interactor = interactor;
    presenter.router = router;
    self.output = presenter;
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:(NSLayoutFormatOptions) 0 metrics:nil views:@{@"view" : self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:(NSLayoutFormatOptions) 0 metrics:nil views:@{@"view" : self.mainView}]];
}

- (void)prepareSubviews {
    self.backgroundColor = [SAStyle clearColor];
    self.mainView.backgroundColor = [SAStyle clearColor];
    self.titleLabel.font = [SAStyle regularFontOfSize:14.f];
    self.productsCountLabel.font = [SAStyle regularFontOfSize:14.f];
    self.productsCountLabel.textColor = [SAStyle lightGreenColor];
    self.sumTitleLabel.font = [SAStyle regularFontOfSize:14.f];
    self.sumTitleLabel.textColor = [SAStyle lightGreenColor];
    self.sumValueLabel.font = [SAStyle boldFontOfSize:14.f];
    self.addressLabel.font = [SAStyle regularFontOfSize:12.f];
    self.addressLabel.textColor = [SAStyle grayColor];

    [self.productsCollectionView registerNib:[UINib nibWithNibName:[SALastOrderProductCell cellIdentifier] bundle:nil]
                  forCellWithReuseIdentifier:[SALastOrderProductCell cellIdentifier]];
    self.productsCollectionView.backgroundColor = [SAStyle whiteColor];
    self.productsCollectionView.userInteractionEnabled = NO;
    self.productsCollectionView.dataSource = self;
    self.productsCollectionView.backgroundColor = [SAStyle clearColor];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SALastOrderProductCell cellIdentifier];
    SALastOrderProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                             forIndexPath:indexPath];
    [cell setModel:(self.products[(NSUInteger) indexPath.row]).imageUrl];
    return cell;
}

#pragma mrk - SALastOrderViewInput

- (void)updateWithOrder:(SAOrderPlainObject *)order {
    orderId = order.orderId;
    self.products = order.products;
    self.productsCollectionViewHeightConstraint.constant = [self.products count] > 5 ? 82.f : 41.f;
    [self.productsCollectionView reloadData];
    self.productsCountLabel.text = [SADataFormatter stringByProductsCount:[order.products count]];
    self.sumValueLabel.text = [NSString stringWithFormat:@"/ %@", [NSString priceWithCurrencySymbol:order.price]];
    self.addressLabel.text = [NSString stringWithFormat:@"%@, д. %@, кв. %@", order.addressStreet,
                                                        order.addressHouse, order.addressFlat];
}

#pragma mark - Actions

- (IBAction)tappedRepeatButton:(id)sender {
    [self.output didTapRepeatOrderButton:orderId];
}

@end

//
//  SALastOrderInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SALastOrderInteractorInput.h"

@protocol SALastOrderInteractorOutput;
@class SAOrdersService;
@class SACartService;
@protocol SAPrototypeMapper;

@interface SALastOrderInteractor : NSObject <SALastOrderInteractorInput>

@property (weak, nonatomic) id<SALastOrderInteractorOutput> output;
@property (nonatomic) SAOrdersService *ordersService;
@property (nonatomic) SACartService *cartService;
@property (nonatomic) id<SAPrototypeMapper> orderMapper;
@property (nonatomic) id<SAPrototypeMapper> orderProductMapper;
@property (nonatomic) id<SAPrototypeMapper> itemModifierMapper;
@property (nonatomic) id<SAPrototypeMapper> orderProductModifierMapper;

@end

//
//  SALastOrderInteractor.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SALastOrderInteractor.h"
#import "SAOrdersService.h"
#import "SACartService.h"
#import "SAPrototypeMapper.h"
#import "SAOrderPlainObject.h"
#import "Order.h"
#import "OrderProduct.h"
#import "SAOrderProductModifierPlainObject.h"
#import "SAOrderProductPlainObject.h"
#import "SAOrderMapper.h"
#import "SAOrderProductMapper.h"
#import "SAOrderProductModifierMapper.h"
#import "SACartItemModifierMapper.h"
#import "SAOrderParser.h"
#import "SAOrderProductParser.h"
#import "SAOrderProductModifierParser.h"

@implementation SALastOrderInteractor

#pragma mark - Init

- (instancetype)init {
    self = [super init];
    self.ordersService = [SAOrdersService new];
    self.ordersService.orderParser = [SAOrderParser new];
    self.ordersService.orderProductParser = [SAOrderProductParser new];
    self.ordersService.orderProductModifierParser = [SAOrderProductModifierParser new];
    self.cartService = [SACartService sharedInstance];
    self.orderMapper = [SAOrderMapper new];
    self.orderProductMapper = [SAOrderProductMapper new];
    self.itemModifierMapper = [SACartItemModifierMapper new];
    self.orderProductModifierMapper = [SAOrderProductModifierMapper new];
    return self;
}

#pragma mark - SALastOrderInteractorInput

- (void)repeatOrder:(NSNumber *)orderId {
    SAOrderPlainObject *orderPlainObject = [self getOrderPlainObject:orderId];
    [self.cartService repeatOrder:orderPlainObject mapper:self.itemModifierMapper];
}

#pragma mark - Private

- (SAOrderPlainObject *)getLastOrder {
    Order *order = [self.ordersService obtainLastOrder];
    SAOrderPlainObject *orderPlainObject = [SAOrderPlainObject new];
    [self.orderMapper fillObject:orderPlainObject withObject:order];
    orderPlainObject.products = [self getPlainProductsFromManagedObjects:[order.products allObjects]];
    return orderPlainObject;
}

- (SAOrderPlainObject *)getOrderPlainObject:(NSNumber *)orderId {
    Order *order = [self.ordersService obtainOrderById:orderId];
    SAOrderPlainObject *orderPlainObject = [SAOrderPlainObject new];
    [self.orderMapper fillObject:orderPlainObject withObject:order];
    orderPlainObject.products = [self getPlainProductsFromManagedObjects:[order.products allObjects]];
    return orderPlainObject;
}

- (NSArray<SAOrderProductPlainObject *> *)getPlainProductsFromManagedObjects:(NSArray<OrderProduct *> *)managedObjectProducts {
    NSMutableArray<SAOrderProductPlainObject *> *productsPlainObjects = [NSMutableArray array];
    for (OrderProduct *managedObjectProduct in managedObjectProducts) {
        SAOrderProductPlainObject *orderProductPlainObject = [SAOrderProductPlainObject new];
        [self.orderProductMapper fillObject:orderProductPlainObject withObject:managedObjectProduct];
        orderProductPlainObject.modifiers = [self getPlainModifierFromManagedObjects:[managedObjectProduct.modifiers allObjects]];
        [productsPlainObjects addObject:orderProductPlainObject];
    }
    return productsPlainObjects;
}

- (NSArray<SAOrderProductModifierPlainObject *> *)getPlainModifierFromManagedObjects:(NSArray<OrderProductModifier *> *)managedObjectModifiers {
    NSMutableArray<SAOrderProductModifierPlainObject *> *modifierPlainObjects
            = [NSMutableArray<SAOrderProductModifierPlainObject *> array];
    for (OrderProductModifier *managedObjectModifier in managedObjectModifiers) {
        SAOrderProductModifierPlainObject *orderProductModifierPlainObject = [SAOrderProductModifierPlainObject new];
        [self.orderProductModifierMapper fillObject:orderProductModifierPlainObject withObject:managedObjectModifier];
        [modifierPlainObjects addObject:orderProductModifierPlainObject];
    }
    return modifierPlainObjects;
}

@end

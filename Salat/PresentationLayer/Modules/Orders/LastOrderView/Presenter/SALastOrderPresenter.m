//
//  SALastOrderPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALastOrderPresenter.h"
#import "SALastOrderViewInput.h"
#import "SALastOrderRouterInput.h"
#import "SALastOrderInteractorInput.h"
#import "SAOrderPlainObject.h"

@implementation SALastOrderPresenter

#pragma mark - SALastOrderViewOutput

- (void)didTriggerViewDidLoadEvent {
//    [self.interactor obtainLastOrder];
}

- (void)didTapRepeatOrderButton:(NSNumber *)orderId {
    [self.interactor repeatOrder:orderId];
}


@end

//
//  SALastOrderPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SALastOrderViewOutput.h"
#import "SALastOrderInteractorOutput.h"

@protocol SALastOrderViewInput;
@protocol SALastOrderInteractorInput;
@protocol SALastOrderRouterInput;

@interface SALastOrderPresenter : NSObject <SALastOrderViewOutput, SALastOrderInteractorOutput>

@property (weak, nonatomic) id<SALastOrderViewInput> view;
@property (nonatomic) id<SALastOrderInteractorInput> interactor;
@property (nonatomic) id<SALastOrderRouterInput> router;

@end

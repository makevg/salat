//
//  SAOrdersListScreenView.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@interface SAOrdersListScreenView : SABaseView
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

//
//  SAOrdersListScreenView.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListScreenView.h"

@implementation SAOrdersListScreenView

#pragma mark - Setup

- (void)setup {
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 145.f;
}

@end

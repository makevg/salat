//
//  SAOrdersListScreenVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListScreenVC.h"
#import "SAOrdersListScreenView.h"
#import "SAOrdersListViewOutput.h"
#import "SAIndicatorViewHelper.h"
#import "UIScrollView+EmptyDataSet.h"

NSString *const cOrdersStoryboardName = @"SAOrders";

@interface SAOrdersListScreenVC () <SAOrdersListDisplayManagerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (strong, nonatomic) IBOutlet SAOrdersListScreenView *contentView;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;
@property (nonatomic) NSError *error;
@end

@implementation SAOrdersListScreenVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }

    return _indicator;
}

#pragma mark - Super

+ (NSString *)storyboardName {
    return cOrdersStoryboardName;
}

+ (BOOL)isInitial {
    return YES;
}

- (void)configureController {
    [super configureController];

    self.displayManager.tableView = self.contentView.tableView;
    self.displayManager.delegate = self;
    self.contentView.tableView.emptyDataSetSource = self;
    self.contentView.tableView.emptyDataSetDelegate = self;
    [self.output didTriggerViewDidLoadEvent];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    [super networkStateDidChanged:notification];
    
    [self.output networkDidChanged:[notification.object boolValue]];
}

#pragma mark - SAOrdersListDisplayManagerDelegate

- (void)displayManager:(SAOrdersListDisplayManager *)displayManager didTapOrder:(NSNumber *)orderId {
    [self.output didTapOrder:orderId];
}

#pragma mark - SAOrdersListViewInput

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)updateWithOrders:(NSArray<SAOrderPlainObject *> *)orders {
    [self.displayManager configureWithOrders:orders];
    [self.contentView.tableView reloadData];
}

- (void)setErrorState:(NSError *)error {
    self.error = error;
    
    if ([self.displayManager hasData]) {
        [self showErrorMessage:error.localizedDescription];
    } else {
        [self.contentView.tableView reloadData];
    }
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Нет Заказов";
    
    if (![self.displayManager hasData]) {
        text = self.error ? self.error.localizedDescription : text;
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    
    if (![self.displayManager hasData]) {
        return self.error ? [UIImage imageNamed:@"btn_auth_back"] : nil;
    }
    
    return nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    
    if (![self.displayManager hasData]) {
        NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                     NSForegroundColorAttributeName: [SAStyle mediumGreenColor]};
        
        return self.error ? [[NSAttributedString alloc] initWithString:@"Обновить" attributes:attributes] : nil;
    }
    
    return nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -10.f;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self.output didTapUpdateButton];
}

@end

//
//  SAOrdersListScreenVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAOrdersListViewInput.h"
#import "SAOrdersListDisplayManager.h"

@protocol SAOrdersListViewOutput;

@interface SAOrdersListScreenVC : SABaseVC <SAOrdersListViewInput>

@property (nonatomic) id<SAOrdersListViewOutput> output;
@property (nonatomic) SAOrdersListDisplayManager *displayManager;

@end

//
//  SAOrdersListViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAOrderPlainObject;

@protocol SAOrdersListViewInput <NSObject>

- (void)showIndicator:(BOOL)show;

- (void)updateWithOrders:(NSArray<SAOrderPlainObject *> *)orders;

- (void)setErrorState:(NSError *)error;

@end

//
//  SAOrderProductCollectionCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderProductCollectionCell.h"
#import "UIImageView+AsyncLoad.h"

@interface SAOrderProductCollectionCell ()
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@end

@implementation SAOrderProductCollectionCell

#pragma mark - Configure

- (void)configureCell {
    self.productImageView.layer.cornerRadius = CGRectGetWidth(self.productImageView.frame)/2;
    self.productImageView.clipsToBounds = YES;
}

- (void)setModel:(id)model {
//    if ([model isKindOfClass:[NSString class]]) {
        [self.productImageView loadAsyncFromUrl:model];
//    }
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

@end

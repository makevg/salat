//
//  SAOrderListCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrderListCell.h"
#import "SAOrderProductCollectionCell.h"
#import "SAOrderPlainObject.h"
#import "NSString+Currency.h"
#import "SADataFormatter.h"
#import "SAOrderProductPlainObject.h"
#import "SAOrderHelper.h"

@interface SAOrderListCell () <UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *itemsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *productsCollectionView;
@property (nonatomic) NSArray<SAOrderProductPlainObject *> *products;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productsCollectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@end

@implementation SAOrderListCell

#pragma mark - Configure

- (void)configureCell {
    self.itemsCountLabel.font = [SAStyle regularFontOfSize:14.f];
    self.priceLabel.font = [SAStyle regularFontOfSize:14.f];
    self.statusLabel.font = [SAStyle regularFontOfSize:14.f];
    self.dateLabel.font = [SAStyle regularFontOfSize:12.f];
    self.locationLabel.font = [SAStyle regularFontOfSize:12.f];
    self.productsCollectionView.backgroundColor = [SAStyle whiteColor];
    self.productsCollectionView.userInteractionEnabled = NO;
    self.productsCollectionView.dataSource = self;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAOrderPlainObject class]]) {
        [self configureWithOrder:model];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SAOrderProductCollectionCell cellIdentifier];
    SAOrderProductCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                   forIndexPath:indexPath];
    [cell setModel:(self.products[(NSUInteger) indexPath.row]).imageUrl];
    return cell;
}

#pragma mark - Private

- (void)configureWithOrder:(SAOrderPlainObject *)order {
    self.products = order.products;
    self.productsCollectionViewHeightConstraint.constant = [self.products count] > 5 ? 82.f : 41.f;
    [self.productsCollectionView reloadData];
    self.itemsCountLabel.text = [SADataFormatter stringByProductsCount:[order.products count]];
    self.priceLabel.text = [NSString stringWithFormat:@"/ %@", [NSString priceWithCurrencySymbol:order.price]];
    self.dateLabel.text = [SADataFormatter fullDateTimeStringByUnixTimeStamp:order.createdAt];
    self.locationLabel.text = [NSString stringWithFormat:@"%@, д. %@, кв. %@", order.addressStreet,
                    order.addressHouse, order.addressFlat];
    self.statusLabel.text = [SAOrderHelper stringByOrderStatus:order.status];
    self.statusLabel.textColor = [SAOrderHelper colorByOrderStatus:order.status];
    self.blurView.alpha = [SAOrderHelper backgroundAlphaByOrderStatus:order.status];
}

@end

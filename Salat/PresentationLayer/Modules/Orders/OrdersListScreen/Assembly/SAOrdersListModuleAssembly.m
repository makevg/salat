//
//  SAOrdersListModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListModuleAssembly.h"
#import "SAOrdersListScreenVC.h"
#import "SAOrdersListPresenter.h"
#import "SAOrdersListInteractor.h"
#import "SAOrdersListRouter.h"

@implementation SAOrdersListModuleAssembly

- (SAOrdersListScreenVC *)viewOrdersList {
    return [TyphoonDefinition withClass:[SAOrdersListScreenVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterOrdersList]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerOrdersList]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
                          }];
}

- (SAOrdersListPresenter *)presenterOrdersList {
    return [TyphoonDefinition withClass:[SAOrdersListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewOrdersList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorOrdersList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerOrdersList]];
                          }];
}

- (SAOrdersListInteractor *)interactorOrdersList {
    return [TyphoonDefinition withClass:[SAOrdersListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterOrdersList]];
                              [definition injectProperty:@selector(ordersService)
                                                    with:[self.serviceComponents ordersService]];
                              [definition injectProperty:@selector(orderMapper)
                                                    with:[self.coreComponents orderMapper]];
                              [definition injectProperty:@selector(orderProductMapper)
                                                    with:[self.coreComponents orderProductMapper]];
                              [definition injectProperty:@selector(orderProductModifierMapper)
                                                    with:[self.coreComponents orderProductModifierMapper]];
    }];
}

- (SAOrdersListRouter *)routerOrdersList {
    return [TyphoonDefinition withClass:[SAOrdersListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewOrdersList]];
                          }];
}

- (SAOrdersListDisplayManager *)displayManagerOrdersList {
    return [TyphoonDefinition withClass:[SAOrdersListDisplayManager class]];
}

@end

//
//  SAOrdersListDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListDisplayManager.h"
#import "SAOrderListCell.h"
#import "SAOrderPlainObject.h"

@interface SAOrdersListDisplayManager ()
@property (nonatomic) NSArray *orders;
@end

@implementation SAOrdersListDisplayManager

#pragma mark - Lazy init

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - Public

- (void)configureWithOrders:(NSArray<SAOrderPlainObject *> *)orders {
    self.orders = orders;
}

- (BOOL)hasData {
    return self.orders.count > 0;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.orders count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SAOrderListCell cellIdentifier];
    SABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                       forIndexPath:indexPath];
    [cell setModel:self.orders[(NSUInteger) indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(displayManager:didTapOrder:)]) {
        SAOrderPlainObject *orderPlainObject = self.orders[(NSUInteger) indexPath.row];
        [self.delegate displayManager:self didTapOrder:orderPlainObject.orderId];
    }
}

@end

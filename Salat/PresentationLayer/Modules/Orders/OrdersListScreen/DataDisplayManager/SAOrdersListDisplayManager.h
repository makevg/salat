//
//  SAOrdersListDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SAOrdersListDisplayManagerDelegate;
@class SAOrderPlainObject;

@interface SAOrdersListDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;
@property (weak, nonatomic) id<SAOrdersListDisplayManagerDelegate> delegate;

- (void)configureWithOrders:(NSArray<SAOrderPlainObject *> *)orders;

- (BOOL)hasData;

@end

@protocol SAOrdersListDisplayManagerDelegate <NSObject>

- (void)displayManager:(SAOrdersListDisplayManager *)displayManager didTapOrder:(NSNumber *)orderId;

@end

//
//  SAOrdersListRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAOrdersListRouter : NSObject <SAOrdersListRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

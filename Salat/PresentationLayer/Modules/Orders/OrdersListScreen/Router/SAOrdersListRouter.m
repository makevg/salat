//
//  SAOrdersListRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListRouter.h"
#import "SAOrderModuleInput.h"

static NSString *const kOpenOrderModuleInOrdersListModuleSegue = @"openOrderModuleInOrdersListModuleSegue";

@implementation SAOrdersListRouter

#pragma mark - SAOrdersListRouterInput

- (void)openOrderModuleWithOrderId:(NSNumber *)orderId {
    [[self.transitionHandler openModuleUsingSegue:kOpenOrderModuleInOrdersListModuleSegue] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SAOrderModuleInput> moduleInput) {
        [moduleInput configureModuleWithOrderId:orderId];
        return nil;
    }];
}

@end

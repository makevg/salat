//
//  SAOrdersListPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAOrdersListViewOutput.h"
#import "SAOrdersListInteractorOutput.h"

@protocol SAOrdersListViewInput;
@protocol SAOrdersListInteractorInput;
@protocol SAOrdersListRouterInput;

@interface SAOrdersListPresenter : NSObject <SAOrdersListViewOutput, SAOrdersListInteractorOutput>

@property (weak, nonatomic) id<SAOrdersListViewInput> view;
@property (nonatomic) id<SAOrdersListInteractorInput> interactor;
@property (nonatomic) id<SAOrdersListRouterInput> router;

@end

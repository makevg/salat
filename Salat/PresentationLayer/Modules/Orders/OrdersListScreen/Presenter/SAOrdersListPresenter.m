//
//  SAOrdersListPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListPresenter.h"
#import "SAOrdersListViewInput.h"
#import "SAOrdersListInteractorInput.h"
#import "SAOrdersListRouterInput.h"

@implementation SAOrdersListPresenter

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self addObservers];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateOrders)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateOrders {
    [self.view showIndicator:YES];
    [self.interactor obtainOrdersFromNetwork];
}

#pragma mark - SAOrdersListViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view showIndicator:YES];
    [self.interactor obtainOrders];
}

- (void)didTapOrder:(NSNumber *)orderId {
    [self.router openOrderModuleWithOrderId:orderId];
}

- (void)didTapUpdateButton {
    [self updateOrders];
}

- (void)networkDidChanged:(BOOL)state {
    if (state) {
        [self updateOrders];
    }
}

#pragma mark - SAOrdersListInteractorOutput

- (void)didObtainOrders:(NSArray<SAOrderPlainObject *> *)orders {
    [self.view updateWithOrders:orders];
    [self.view showIndicator:NO];
}

- (void)didObtainError:(NSError *)error {
    [self.view setErrorState:error];
    [self.view showIndicator:NO];
}

@end

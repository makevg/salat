//
//  SAOrdersListInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAOrdersListInteractor.h"
#import "SAOrdersListInteractorOutput.h"
#import "SAOrdersService.h"
#import "SAOrderPlainObject.h"
#import "SAPrototypeMapper.h"
#import "OrderProduct.h"
#import "SAOrderProductPlainObject.h"
#import "Order.h"
#import "SAOrderProductModifierPlainObject.h"
#import "SAServiceLayer.h"

@implementation SAOrdersListInteractor

#pragma mark - SAOrdersListInteractorInput

- (void)obtainOrders {
    __weak __typeof(self) weakSelf = self;
    
    [self.ordersService obtainOrders:^(NSArray<Order *> *data) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        NSArray<SAOrderPlainObject *> *orders = [strongSelf getPlainOrdersFromManagedObjects:data];
        dispatch_safe_main_sync(^{
            [strongSelf.output didObtainOrders:orders];
        });
    }
                               error:^(NSError *error) {
                                   dispatch_safe_main_sync(^{
                                       [weakSelf.output didObtainError:error];
                                   });
                               }];
}

- (void)obtainOrdersFromNetwork {
    __weak __typeof(self) weakSelf = self;
    
    [self.ordersService obtainOrdersOnlyNetwork:^(NSArray<Order *> *data) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        NSArray<SAOrderPlainObject *> *orders = [strongSelf getPlainOrdersFromManagedObjects:data];
        dispatch_safe_main_async(^{
            [strongSelf.output didObtainOrders:orders];
        });
    }
                                          error:^(NSError *error) {
                                              dispatch_safe_main_sync(^{
                                                  [weakSelf.output didObtainError:error];
                                              });
                                          }];
}

#pragma mark - Private

- (NSArray<SAOrderPlainObject *> *)getPlainOrdersFromManagedObjects:(NSArray<Order *> *)managedObjectOrders {
    NSMutableArray<SAOrderPlainObject *> *ordersPlainObjects = [NSMutableArray array];
    for (Order *managedObjectOrder in managedObjectOrders) {
        SAOrderPlainObject *orderPlainObject = [SAOrderPlainObject new];
        [self.orderMapper fillObject:orderPlainObject withObject:managedObjectOrder];
        orderPlainObject.products = [self getPlainProductsFromManagedObjects:[managedObjectOrder.products allObjects]];
        [ordersPlainObjects addObject:orderPlainObject];
    }
    return ordersPlainObjects;
}

- (NSArray<SAOrderProductPlainObject *> *)getPlainProductsFromManagedObjects:(NSArray<OrderProduct *> *)managedObjectProducts {
    NSMutableArray<SAOrderProductPlainObject *> *productsPlainObjects = [NSMutableArray array];
    for (OrderProduct *managedObjectProduct in managedObjectProducts) {
        SAOrderProductPlainObject *orderProductPlainObject = [SAOrderProductPlainObject new];
        [self.orderProductMapper fillObject:orderProductPlainObject withObject:managedObjectProduct];
        orderProductPlainObject.modifiers = [self getPlainModifierFromManagedObjects:[managedObjectProduct.modifiers allObjects]];
        [productsPlainObjects addObject:orderProductPlainObject];
    }
    return productsPlainObjects;
}

- (NSArray<SAOrderProductModifierPlainObject *> *)getPlainModifierFromManagedObjects:(NSArray<OrderProductModifier *> *)managedObjectModifiers {
    NSMutableArray<SAOrderProductModifierPlainObject *> *modifierPlainObjects
            = [NSMutableArray<SAOrderProductModifierPlainObject *> array];
    for (OrderProductModifier *managedObjectModifier in managedObjectModifiers) {
        SAOrderProductModifierPlainObject *orderProductModifierPlainObject = [SAOrderProductModifierPlainObject new];
        [self.orderProductModifierMapper fillObject:orderProductModifierPlainObject withObject:managedObjectModifier];
        [modifierPlainObjects addObject:orderProductModifierPlainObject];
    }
    return modifierPlainObjects;
}

@end

//
//  SAOrdersListInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAOrderPlainObject;

@protocol SAOrdersListInteractorOutput <NSObject>

- (void)didObtainOrders:(NSArray<SAOrderPlainObject *> *)orders;

- (void)didObtainError:(NSError *)error;

@end

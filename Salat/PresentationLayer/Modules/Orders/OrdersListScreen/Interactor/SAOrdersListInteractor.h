//
//  SAOrdersListInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 10.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAOrdersListInteractorInput.h"

@protocol SAOrdersListInteractorOutput;
@class SAOrdersService;
@protocol SAPrototypeMapper;

@interface SAOrdersListInteractor : NSObject <SAOrdersListInteractorInput>

@property(weak, nonatomic) id <SAOrdersListInteractorOutput> output;
@property(nonatomic) SAOrdersService *ordersService;
@property(nonatomic) id <SAPrototypeMapper> orderMapper;
@property(nonatomic) id <SAPrototypeMapper> orderProductMapper;
@property(nonatomic) id <SAPrototypeMapper> orderProductModifierMapper;

@end

//
//  SALaunchModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALaunchModuleAssembly.h"
#import "SALaunchVC.h"
#import "SALaunchPresenter.h"
#import "SALaunchInteractor.h"
#import "SALaunchRouter.h"

@implementation SALaunchModuleAssembly

- (SALaunchVC *)viewLaunch {
    return [TyphoonDefinition withClass:[SALaunchVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterLaunch]];
    }];
}

- (SALaunchPresenter *)presenterLaunch {
    return [TyphoonDefinition withClass:[SALaunchPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewLaunch]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorLaunch]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerLaunch]];
    }];
}

- (SALaunchInteractor *)interactorLaunch {
    return [TyphoonDefinition withClass:[SALaunchInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterLaunch]];
                              [definition injectProperty:@selector(restaurantService)
                                                    with:[self.serviceComponents restaurantService]];
                              [definition injectProperty:@selector(productService)
                                                    with:[self.serviceComponents productService]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
    }];
}

- (SALaunchRouter *)routerLaunch {
    return [TyphoonDefinition withClass:[SALaunchRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewLaunch]];
    }];
}

@end

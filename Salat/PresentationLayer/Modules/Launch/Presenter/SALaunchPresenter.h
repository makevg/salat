//
//  SALaunchPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SALaunchViewOutput.h"
#import "SALaunchInteractorOutput.h"

@protocol SALaunchViewInput;
@protocol SALaunchInteractorInput;
@protocol SALaunchRouterInput;

@interface SALaunchPresenter : NSObject <SALaunchViewOutput, SALaunchInteractorOutput>

@property (weak, nonatomic) id<SALaunchViewInput> view;
@property (nonatomic) id<SALaunchInteractorInput> interactor;
@property (nonatomic) id<SALaunchRouterInput> router;

@end

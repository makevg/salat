//
//  SALaunchPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALaunchPresenter.h"
#import "SALaunchViewInput.h"
#import "SALaunchInteractorInput.h"
#import "SALaunchRouterInput.h"

@implementation SALaunchPresenter

#pragma mark - SALaunchViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view showLoadingIndicator:YES];
    [self.interactor doingSmthng];
}

#pragma mark - SALaunchInteractorOutput

- (void)goNext {
    [self.view showLoadingIndicator:NO];
    [self.router openMainMenu];
}

- (void)didObtainError:(NSError *)error {
    [self.view showErrorState:error];
}


@end

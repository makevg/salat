//
//  SALaunchInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SALaunchInteractorInput.h"

@protocol SALaunchInteractorOutput;
@class SARestaurantService;
@class SAProductService;
@class SAUserService;

@interface SALaunchInteractor : NSObject <SALaunchInteractorInput>

@property (weak, nonatomic) id<SALaunchInteractorOutput> output;
@property (nonatomic) SARestaurantService *restaurantService;
@property (nonatomic) SAProductService *productService;
@property (nonatomic) SAUserService *userService;

@end

//
//  SALaunchInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALaunchInteractor.h"
#import "SALaunchInteractorOutput.h"
#import "SARestaurantService.h"
#import "SAUserService.h"
#import "SACommon.h"

@implementation SALaunchInteractor

#pragma mark - SALaunchInteractorInput

- (void)doingSmthng {
    [self performSelector:@selector(goNext) withObject:nil afterDelay:2.f];
}

- (void)goNext {
    weakifySelf;

    dispatch_safe_main_async(^{
        [weakSelf.output goNext];
    });
}

@end

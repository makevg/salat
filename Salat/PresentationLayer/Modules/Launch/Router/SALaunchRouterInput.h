//
//  SALaunchRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SALaunchRouterInput <NSObject>

- (void)openMainMenu;

@end

//
//  SALaunchRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALaunchRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SALaunchRouter : NSObject <SALaunchRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

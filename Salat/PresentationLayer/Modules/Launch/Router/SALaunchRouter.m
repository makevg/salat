//
//  SALaunchRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALaunchRouter.h"
#import "SABackMenuRootVC.h"

@implementation SALaunchRouter

#pragma mark - SALaunchRouterInput

- (void)openMainMenu {
    [self setRootFeatureByName:[SABackMenuRootVC storyboardName]];
}

#pragma mark - Private

- (void)setRootFeatureByName:(NSString *)storyboardName {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *vc = [sb instantiateInitialViewController];
    [[UIApplication sharedApplication] keyWindow].rootViewController = vc;
}

@end

//
//  SALaunchViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SALaunchViewInput <NSObject>

- (void)showLoadingIndicator:(BOOL)show;

- (void)showErrorState:(NSError *)error;

@end
//
//  SALaunchView.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@class PCAngularActivityIndicatorView;

@interface SALaunchView : SABaseView

@property(strong, nonatomic) IBOutlet PCAngularActivityIndicatorView *activityIndicatorView;

@end

//
//  SALaunchVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SALaunchVC.h"
#import "SALaunchView.h"
#import "UIAlertController+Helper.h"
#import "SALaunchViewOutput.h"

@interface SALaunchVC ()

@property(strong, nonatomic) IBOutlet SALaunchView *contentView;

@end

@implementation SALaunchVC

#pragma mark - Super

- (void)configureController {
    [super configureController];

    self.contentView.activityIndicatorView.color = [SAStyle whiteColor];

    [self.output didTriggerViewDidLoadEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.contentView.activityIndicatorView startAnimating];
}

#pragma mark - SALaunchViewInput

- (void)showLoadingIndicator:(BOOL)show {
    if (show) {
        [self.contentView.activityIndicatorView startAnimating];
    } else {
        [self.contentView.activityIndicatorView stopAnimating];
    }
}

- (void)showErrorState:(NSError *)error {
    UIAlertController *controller = [UIAlertController alertWithTitle:@"Внимание" message:error.localizedDescription handler:nil];
    [self presentViewController:controller animated:YES completion:nil];
}


@end

//
//  SALaunchVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 24.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SALaunchViewInput.h"

@protocol SALaunchViewOutput;

@interface SALaunchVC : SABaseVC <SALaunchViewInput>

@property (nonatomic) id<SALaunchViewOutput> output;

@end

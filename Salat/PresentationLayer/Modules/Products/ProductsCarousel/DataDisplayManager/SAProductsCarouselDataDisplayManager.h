//
//  SAProductsCarouselDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAProductPlainObject;
@protocol SAProductsCarouselDataDisplayManagerDelegate;

@interface SAProductsCarouselDataDisplayManager : NSObject <UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic) UICollectionView *collectionView;
@property(weak, nonatomic) id <SAProductsCarouselDataDisplayManagerDelegate> delegate;

- (void)configureDataDisplayManagerWithProducts:(NSArray<SAProductPlainObject *> *)products;

@end

@protocol SAProductsCarouselDataDisplayManagerDelegate <NSObject>

- (void)displayManager:(SAProductsCarouselDataDisplayManager *)displayManager didPlusProduct:(SAProductPlainObject *)product;

- (void)displayManager:(SAProductsCarouselDataDisplayManager *)displayManager didMinusProduct:(SAProductPlainObject *)product;

@end
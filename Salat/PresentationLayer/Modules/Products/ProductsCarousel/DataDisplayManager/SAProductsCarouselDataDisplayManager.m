//
//  SAProductsCarouselDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsCarouselDataDisplayManager.h"
#import "SAHorizontalLinearFlowLayout.h"
#import "SAProductsCarouselCell.h"

@interface SAProductsCarouselDataDisplayManager () <SAProductsCarouselCellDelegate>
@property(nonatomic) NSArray *products;
@property(nonatomic) SAHorizontalLinearFlowLayout *collectionViewLayout;
@end

@implementation SAProductsCarouselDataDisplayManager

#pragma mark - Lazy load

- (void)setCollectionView:(UICollectionView *)collectionView {
    _collectionView = collectionView;
    [self prepareCollectionView];
}

#pragma mark - Private

- (void)prepareCollectionView {
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionViewLayout = [SAHorizontalLinearFlowLayout layoutConfiguredWithCollectionView:self.collectionView
                                                                                        itemSize:[self itemSize]
                                                                              minimumLineSpacing:0];
    self.collectionViewLayout.minimumScaleFactor = 0.9f;
}

- (CGSize)itemSize {
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = CGRectGetWidth(bounds);
//    CGFloat screenHeight = CGRectGetHeight(bounds);
    return CGSizeMake(screenWidth - 70.f, 380.f);
}

#pragma mark - Public

- (void)configureDataDisplayManagerWithProducts:(NSArray<SAProductPlainObject *> *)products {
    self.products = products;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SAProductsCarouselCell cellIdentifier];
    SAProductsCarouselCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                             forIndexPath:indexPath];
    [cell setModel:self.products[(NSUInteger) indexPath.row]];
    cell.delegate = self;
    return cell;
}

#pragma mark - SAProductsCarouselCellDelegate

- (void)productCell:(SAProductsCarouselCell *)cell didTapPlusProduct:(SAProductPlainObject *)product {
    [self.delegate displayManager:self didPlusProduct:product];
}

- (void)productCell:(SAProductsCarouselCell *)cell didTapMinusProduct:(SAProductPlainObject *)product {
    [self.delegate displayManager:self didMinusProduct:product];
}


@end

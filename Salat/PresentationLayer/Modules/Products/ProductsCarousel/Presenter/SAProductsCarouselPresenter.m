//
//  SAProductsCarouselPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsCarouselPresenter.h"
#import "SAProductsCarouselViewInput.h"
#import "SAProductsCarouselInteractorInput.h"
#import "SAProductsCarouselPresenterStateStorage.h"

@implementation SAProductsCarouselPresenter

#pragma mark - SAProductsCarouselModuleInput

- (void)configureModuleWithCategoryId:(NSNumber *)categoryId productIndexPath:(NSIndexPath *)indexPath {
    self.presenterStateStorage.categoryId = categoryId;
    self.presenterStateStorage.productIndexPath = indexPath;
}

#pragma mark - SAProductsCarouselViewOutput

- (void)didTriggerViewWillAppearEvent {
    [self.interactor obtainProductsByCategoryId:self.presenterStateStorage.categoryId];
}

- (void)didTapPlusProduct:(SAProductPlainObject *)product {
    [self.interactor plusProduct:product];
}

- (void)didTapMinusProduct:(SAProductPlainObject *)product {
    [self.interactor minusProduct:product];
}

#pragma mark - SAProductsCarouselInteractorOutput

- (void)didObtainProducts:(NSArray<SAProductPlainObject *> *)products {
    [self.view updateWithProducts:products];
    [self.view scrollToProductAtIndexPath:self.presenterStateStorage.productIndexPath];
}

- (void)didObtainError:(NSError *)error {
    //TODO: write
}


@end

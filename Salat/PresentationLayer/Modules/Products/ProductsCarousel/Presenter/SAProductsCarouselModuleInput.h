//
//  SAProductsCarouselModuleInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SAProductsCarouselModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithCategoryId:(NSNumber *)categoryId productIndexPath:(NSIndexPath *)indexPath;

@end

//
//  SAProductsCarouselPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAProductsCarouselModuleInput.h"
#import "SAProductsCarouselViewOutput.h"
#import "SAProductsCarouselInteractorOutput.h"

@protocol SAProductsCarouselViewInput;
@protocol SAProductsCarouselInteractorInput;
@class SAProductsCarouselPresenterStateStorage;

@interface SAProductsCarouselPresenter : NSObject <SAProductsCarouselModuleInput, SAProductsCarouselViewOutput, SAProductsCarouselInteractorOutput>

@property(weak, nonatomic) id <SAProductsCarouselViewInput> view;
@property(nonatomic) id <SAProductsCarouselInteractorInput> interactor;
@property(nonatomic) SAProductsCarouselPresenterStateStorage *presenterStateStorage;

@end

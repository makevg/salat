//
//  SAProductsCarouselPresenterStateStorage.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAProductsCarouselPresenterStateStorage : NSObject

@property(nonatomic) NSNumber *categoryId;
@property(nonatomic) NSIndexPath *productIndexPath;

@end

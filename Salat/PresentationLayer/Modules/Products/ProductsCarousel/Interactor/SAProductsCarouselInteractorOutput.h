//
//  SAProductsCarouselInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductPlainObject;

@protocol SAProductsCarouselInteractorOutput <NSObject>

- (void)didObtainProducts:(NSArray<SAProductPlainObject *> *)products;

- (void)didObtainError:(NSError *)error;

@end

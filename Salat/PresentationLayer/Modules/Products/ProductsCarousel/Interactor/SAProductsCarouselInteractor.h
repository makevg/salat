//
//  SAProductsCarouselInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAProductsCarouselInteractorInput.h"

@protocol SAProductsCarouselInteractorOutput;
@class SAProductService;
@class SAProductMapper;
@class SACartService;
@class SAProductModifierMapper;
@protocol SAPrototypeMapper;

@interface SAProductsCarouselInteractor : NSObject <SAProductsCarouselInteractorInput>

@property(weak, nonatomic) id <SAProductsCarouselInteractorOutput> output;
@property(nonatomic) SAProductService *productService;
@property(nonatomic) SAProductMapper *productMapper;
@property(nonatomic) SAProductModifierMapper *productModifierMapper;
@property(nonatomic) SACartService *cartService;
@property(nonatomic) id <SAPrototypeMapper> itemModifierMapper;

@end

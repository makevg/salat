//
//  SAProductsCarouselInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductPlainObject;

@protocol SAProductsCarouselInteractorInput <NSObject>

- (void)obtainProductsByCategoryId:(NSNumber *)categoryId;

- (void)plusProduct:(SAProductPlainObject *)product;

- (void)minusProduct:(SAProductPlainObject *)product;

@end

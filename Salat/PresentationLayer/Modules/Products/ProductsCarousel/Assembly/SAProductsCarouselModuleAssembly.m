//
//  SAProductsCarouselModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsCarouselModuleAssembly.h"
#import "SAProductsCarouselVC.h"
#import "SAProductsCarouselInteractor.h"
#import "SAProductsCarouselPresenter.h"
#import "SAProductsCarouselDataDisplayManager.h"
#import "SAProductsCarouselPresenterStateStorage.h"

@implementation SAProductsCarouselModuleAssembly

- (SAProductsCarouselVC *)viewProductsCarousel {
    return [TyphoonDefinition withClass:[SAProductsCarouselVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProductsCarousel]];
                              [definition injectProperty:@selector(dataDisplayManager)
                                                    with:[self dataDisplayManagerProductsCarousel]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
                          }];
}

- (SAProductsCarouselPresenter *)presenterProductsCarousel {
    return [TyphoonDefinition withClass:[SAProductsCarouselPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewProductsCarousel]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorProductsCarousel]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorageProductsCarousel]];
                          }];
}

- (SAProductsCarouselInteractor *)interactorProductsCarousel {
    return [TyphoonDefinition withClass:[SAProductsCarouselInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProductsCarousel]];
                              [definition injectProperty:@selector(productService)
                                                    with:[self.serviceComponents productService]];
                              [definition injectProperty:@selector(productMapper)
                                                    with:[self.coreComponents productMapper]];
                              [definition injectProperty:@selector(productModifierMapper)
                                                    with:[self.coreComponents productModifierMapper]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                              [definition injectProperty:@selector(itemModifierMapper)
                                                    with:[self.coreComponents cartItemModifierMapper]];
                          }];
}

- (SAProductsCarouselDataDisplayManager *)dataDisplayManagerProductsCarousel {
    return [TyphoonDefinition withClass:[SAProductsCarouselDataDisplayManager class]];
}

- (SAProductsCarouselPresenterStateStorage *)presenterStateStorageProductsCarousel {
    return [TyphoonDefinition withClass:[SAProductsCarouselPresenterStateStorage class]];
}

@end

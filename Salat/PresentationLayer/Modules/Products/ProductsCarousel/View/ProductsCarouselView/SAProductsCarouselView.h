//
//  SAProductsCarouselView.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.03.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseView.h"

@interface SAProductsCarouselView : SABaseView

@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (void)setCurrentIndexPath:(NSIndexPath *)indexPath;

@end

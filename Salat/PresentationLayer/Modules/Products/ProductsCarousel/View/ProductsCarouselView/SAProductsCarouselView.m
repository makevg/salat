//
//  SAProductsCarouselView.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.03.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsCarouselView.h"
#import "SAHorizontalLinearFlowLayout.h"
#import "SACommon.h"

@interface SAProductsCarouselView ()

@property(weak, nonatomic) IBOutlet UIImageView *backImageView;

@end

@implementation SAProductsCarouselView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [SAStyle lightGreenColor];
    self.backImageView.backgroundColor = [SAStyle backgroundColor];
    self.collectionView.backgroundColor = [UIColor clearColor];
}

- (void)setCurrentIndexPath:(NSIndexPath *)indexPath {
    weakifySelf;

    dispatch_async(dispatch_get_main_queue(), ^{
        strongifySelf;

        SAHorizontalLinearFlowLayout *flowLayout = (SAHorizontalLinearFlowLayout *) strongSelf.collectionView.collectionViewLayout;
        CGPoint offset = [strongSelf.collectionView.collectionViewLayout targetContentOffsetForProposedContentOffset:CGPointMake([flowLayout pageWidth] * indexPath.row, 0)
                                                                                               withScrollingVelocity:CGPointMake(0, 0)];
        [strongSelf.collectionView setContentOffset:offset animated:NO];
    });
}


@end

//
//  SAProductsCarouselCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.03.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsCarouselCell.h"
#import "SAAddToCartView.h"
#import "SAProductPlainObject.h"
#import "UIImageView+AsyncLoad.h"
#import "NSString+Currency.h"

@interface SAProductsCarouselCell () <SAAddToCartViewDelegate>
@property(weak, nonatomic) IBOutlet UIView *innerView;
@property(weak, nonatomic) IBOutlet UIImageView *productImageView;
@property(weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *productDescrLabel;
@property(weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property(weak, nonatomic) IBOutlet SAAddToCartView *addToCartView;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *separatorHeightConstraint;
@end

@implementation SAProductsCarouselCell {
    SAProductPlainObject *productPlainObject;
}

#pragma mark - Public

- (void)configureCell {
    [self setRounded];
    [self configureSubviews];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProductPlainObject class]]) {
        [self configureWithProduct:model];
    }
}

#pragma mark - Private

- (void)setRounded {
    self.innerView.layer.cornerRadius = 10.f;
    self.innerView.layer.masksToBounds = YES;

    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.5;
}

- (void)configureSubviews {
    self.separatorHeightConstraint.constant = 0.5f;
    self.productTitleLabel.font = [SAStyle regularFontOfSize:15.f];
    self.productDescrLabel.textColor = [UIColor grayColor];
    self.productDescrLabel.font = [SAStyle regularFontOfSize:12.f];
    self.productPriceLabel.textColor = [UIColor grayColor];
    self.productPriceLabel.font = [SAStyle regularFontOfSize:15.f];
    self.addToCartView.delegate = self;
}

- (void)configureWithProduct:(SAProductPlainObject *)product {
    productPlainObject = product;
    self.productTitleLabel.text = product.title;
    self.productDescrLabel.text = product.descr;
    self.productPriceLabel.text = [NSString priceWithCurrencySymbol:product.price];
    [self.productImageView loadAsyncFromUrl:product.imageUrl];
    [self.addToCartView updateWithQuantity:product.quantity];
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

#pragma mark - SAAddToCartViewDelegate

- (void)addToCartViewDidTapPlus:(SAAddToCartView *)view {
    [self.delegate productCell:self didTapPlusProduct:productPlainObject];
}

- (void)addToCartViewDidTapMinus:(SAAddToCartView *)view {
    [self.delegate productCell:self didTapMinusProduct:productPlainObject];
}


@end

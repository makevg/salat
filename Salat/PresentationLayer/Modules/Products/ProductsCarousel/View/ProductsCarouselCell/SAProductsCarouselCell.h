//
//  SAProductsCarouselCell.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.03.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseCollectionViewCell.h"

@class SAProductPlainObject;
@protocol SAProductsCarouselCellDelegate;

@interface SAProductsCarouselCell : SABaseCollectionViewCell

@property(weak, nonatomic) id <SAProductsCarouselCellDelegate> delegate;

@end

@protocol SAProductsCarouselCellDelegate <NSObject>

- (void)productCell:(SAProductsCarouselCell *)cell didTapPlusProduct:(SAProductPlainObject *)product;

- (void)productCell:(SAProductsCarouselCell *)cell didTapMinusProduct:(SAProductPlainObject *)product;

@end

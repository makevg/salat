//
//  SAProductsCarouselViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAProductPlainObject;

@protocol SAProductsCarouselViewOutput <NSObject>

- (void)didTriggerViewWillAppearEvent;

- (void)didTapPlusProduct:(SAProductPlainObject *)product;

- (void)didTapMinusProduct:(SAProductPlainObject *)product;

@end

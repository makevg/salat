//
//  SAHorizontalLinearFlowLayout.h
//  Salat
//
//  Created by Maximychev Evgeny on 24.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAHorizontalLinearFlowLayout : UICollectionViewFlowLayout

@property(assign, nonatomic) CGFloat scalingOffset;
@property(assign, nonatomic) CGFloat minimumScaleFactor;
@property(assign, nonatomic) BOOL scaleItems;

+ (SAHorizontalLinearFlowLayout *)layoutConfiguredWithCollectionView:(UICollectionView *)collectionView
                                                            itemSize:(CGSize)itemSize
                                                  minimumLineSpacing:(CGFloat)minimumLineSpacing;

- (CGFloat)pageWidth;

@end

//
//  SAProductsCarouselViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 18.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductPlainObject;

@protocol SAProductsCarouselViewInput <NSObject>

- (void)updateWithProducts:(NSArray<SAProductPlainObject *> *)products;

- (void)setupTitle:(NSString *)title;

- (void)scrollToProductAtIndexPath:(NSIndexPath *)indexPath;

@end

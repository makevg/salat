//
//  SAProductsCarouselVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.03.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsCarouselVC.h"
#import "SAProductsCarouselView.h"
#import "SAProductsCarouselViewOutput.h"
#import "SAProductsCarouselDataDisplayManager.h"
#import "UINavigationController+TransparentNavigationController.h"

@interface SAProductsCarouselVC () <SAProductsCarouselDataDisplayManagerDelegate>
@property(strong, nonatomic) IBOutlet SAProductsCarouselView *contentView;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *topBack;
@end

@implementation SAProductsCarouselVC

#pragma mark - Super

- (BOOL)showCartButton {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.output didTriggerViewWillAppearEvent];
    [self.navigationController presentTransparentNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController hideTransparentNavigationBar];
}

- (void)configureController {
    [super configureController];

    self.view.backgroundColor = [SAStyle backgroundColor];
    self.dataDisplayManager.collectionView = self.contentView.collectionView;
    self.dataDisplayManager.delegate = self;

    self.topBack.constant = -(self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height);
}

#pragma mark -SAProductsCarouselViewInput

- (void)updateWithProducts:(NSArray<SAProductPlainObject *> *)products {
    [self.dataDisplayManager configureDataDisplayManagerWithProducts:products];
    [self.contentView.collectionView reloadData];
}

- (void)setupTitle:(NSString *)title {
    [self setTitle:title];
}

- (void)scrollToProductAtIndexPath:(NSIndexPath *)indexPath {
    [self.contentView setCurrentIndexPath:indexPath];
}

#pragma mark - SAProductsCarouselDataDisplayManagerDelegate

- (void)displayManager:(SAProductsCarouselDataDisplayManager *)displayManager didPlusProduct:(SAProductPlainObject *)product {
    [self.output didTapPlusProduct:product];
}

- (void)displayManager:(SAProductsCarouselDataDisplayManager *)displayManager didMinusProduct:(SAProductPlainObject *)product {
    [self.output didTapMinusProduct:product];
}


@end

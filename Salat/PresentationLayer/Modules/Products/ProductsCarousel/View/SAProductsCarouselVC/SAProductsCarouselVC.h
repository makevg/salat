//
//  SAProductsCarouselVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.03.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAProductsCarouselViewInput.h"

@class SAProductsCarouselDataDisplayManager;
@protocol SAProductsCarouselViewOutput;

@interface SAProductsCarouselVC : SABaseVC <SAProductsCarouselViewInput>

@property(nonatomic) id <SAProductsCarouselViewOutput> output;
@property(nonatomic) SAProductsCarouselDataDisplayManager *dataDisplayManager;

@end

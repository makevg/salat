//
//  SAHitProductsHeader.m
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAHitProductsHeader.h"
#import "SAHitProductsView.h"
#import "SALastOrderView.h"
#import "SAUserService.h"
#import "SAHitProductsPresenter.h"
#import "PCAngularActivityIndicatorView.h"
#import "SAIndicatorViewHelper.h"
#import "SACommon.h"
#import "SABorderButton.h"
#import "SAMenuVC.h"

@interface SAHitProductsHeader () <UIScrollViewDelegate, SAHitProductsPresenterOutput>

@property(strong, nonatomic) IBOutlet UIView *mainView;

@property(strong, nonatomic) IBOutlet UILabel *errorTitle;

@property(strong, nonatomic) IBOutlet SABorderButton *errorButton;

@property(weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property(weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property(strong, nonatomic) SAHitProductsView *hitProductsView;

@property(strong, nonatomic) SALastOrderView *lastOrderView;

@property(nonatomic, strong) PCAngularActivityIndicatorView *indicator;

@end

@implementation SAHitProductsHeader {
    CGFloat screenWidth;
    SAHitProductsPresenter *presenter;
    bool obtainHits;
    bool hasHits;
    
    bool obtainFavs;
    bool hasFavs;
    
    bool obtainOrder;
    bool hasOrder;
    
    NSError *_error;
}

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self];
        _indicator.color = [SAStyle mediumGreenColor];
    }

    return _indicator;
}

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)setTransitionHandler:(UIViewController *)transitionHandler {
    _transitionHandler = transitionHandler;
    self.hitProductsView.transitionHandler = transitionHandler;
}

#pragma mark - Private

- (void)commonInit {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    presenter = [SAHitProductsPresenter new];
    presenter.delegate = self;

    [self addSubview:self.mainView];
    [self addSubview:self.indicator];
    [self configureView];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginLogoutHandler:)
                                                 name:kLogoutNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginLogoutHandler:)
                                                 name:kLogoutNotification
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loginLogoutHandler:(id)sender {
    weakifySelf;

    dispatch_safe_main_async(^{
        [weakSelf loadData];
    })
}

- (void)configureView {
    [self prepareConstraints];
    [self prepareSubviews];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                 options:(NSLayoutFormatOptions) 0
                                                                 metrics:nil
                                                                   views:@{@"view" : self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                                 options:(NSLayoutFormatOptions) 0
                                                                 metrics:nil
                                                                   views:@{@"view" : self.mainView}]];
}

- (void)prepareSubviews {
    self.backgroundColor = [SAStyle clearColor];
    self.mainView.backgroundColor = [SAStyle clearColor];
    self.scrollView.backgroundColor = [SAStyle clearColor];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    CGFloat scrollViewHeight = CGRectGetHeight(self.mainView.frame);
    self.scrollView.frame = CGRectMake(0, 0, screenWidth, scrollViewHeight);

    self.hitProductsView = [[SAHitProductsView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, scrollViewHeight)];
    self.lastOrderView = [[SALastOrderView alloc] initWithFrame:CGRectMake(screenWidth, 0, screenWidth, scrollViewHeight)];
    
    self.hitProductsView.hidden = YES;
    self.lastOrderView.hidden = YES;

    [self.scrollView addSubview:self.hitProductsView];
    [self.scrollView addSubview:self.lastOrderView];

    self.pageControl.currentPage = 0;
    self.scrollView.delegate = self;
    
    self.errorTitle.font = [SAStyle regularFontOfSize:16.f];
    self.errorTitle.textColor = [SAStyle grayColor];
}

- (void)loadData {
    obtainFavs = NO;
    obtainHits = NO;
    obtainOrder = NO;
    
    hasFavs = NO;
    hasHits = NO;
    hasOrder = NO;
    
    _error = nil;
    
    self.hitProductsView.hidden = YES;
    self.lastOrderView.hidden = YES;
    
    self.errorTitle.hidden = YES;
    self.errorButton.hidden = YES;
    
    self.indicator.hidden = NO;
    [self.indicator startAnimating];

    self.pageControl.currentPage = 0;
    self.scrollView.contentOffset = CGPointMake(0, 0);

    [presenter obtainData];
}

- (IBAction)reload:(id)sender {
    [self loadData];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
    CGFloat currentPage = (CGFloat) (floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1);
    self.pageControl.currentPage = (NSInteger) currentPage;
}

#pragma mark - SAHitProductsPresenterOutput

- (void)didObtainLastOrder:(SAOrderPlainObject *)order {
    if (order) {
        hasOrder = YES;
        [self.lastOrderView updateWithOrder:order];
    } else {
        hasOrder = NO;
    }
    
    obtainOrder = YES;
    [self checkFinish];
}

- (void)didObtainHits:(NSArray <SAProductPlainObject *> *)hits {
    if (![hits count]) {
        [self.hitProductsView hideHits];
        hasHits = NO;
    } else {
        [self.hitProductsView setFirstHit:[hits objectAtIndex:0]];
        
        if ([hits count] > 1)
            [self.hitProductsView setSecondHit:[hits objectAtIndex:1]];
        else
            [self.hitProductsView setSecondHit:[hits objectAtIndex:0]];

        hasHits = YES;
    }
    
    obtainHits = YES;
    [self checkFinish];
}

- (void)didObtainFav:(SAProductPlainObject *)fav {
    if (fav) {
        [self.hitProductsView setMyFavorite:fav];
        hasFavs = YES;
    } else {
        hasFavs = NO;
    }
    
    obtainFavs = YES;
    [self checkFinish];
}

- (void)didObtainError:(NSError *)error {
    //TODO: qwe
    _error = error;
    [self didFinish];
}

- (void)checkFinish {
    if( obtainOrder && obtainHits && obtainFavs ) {
        [self didFinish];
    }
}

- (void)didFinish {
    self.pageControl.currentPage = 0;
    
    if( _error && _error.code != 401 ) {
        [self setPageNumbers:1];
        self.errorTitle.hidden = NO;
        self.errorButton.hidden = NO;
        
        self.errorTitle.text = _error.localizedDescription;
        [(SAMenuVC *)_transitionHandler showHits];
    } else {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        screenWidth = screenRect.size.width;
        int pages = 1;
        if( hasHits ) {
            pages = (hasOrder || hasFavs) ? 2 : 1;
        } else {
            pages = (hasOrder && hasFavs) ? 2 : 1;
        }

        self.lastOrderView.frame = CGRectMake(pages == 2 ? screenWidth : 0, 0, screenWidth, self.lastOrderView.frame.size.height);
        [self setPageNumbers:pages];
        
        self.lastOrderView.hidden = !hasOrder;
        self.hitProductsView.hidden = NO;

        if( !hasOrder && !hasFavs && !hasHits ) {
            [(SAMenuVC *)_transitionHandler hideHits];
        } else {
            [(SAMenuVC *)_transitionHandler showHits];
        }
    }
    
    self.indicator.hidden = YES;
    [self.indicator stopAnimating];
}

- (void)setPageNumbers:(int) pageNumber  {
    self.pageControl.numberOfPages = pageNumber;
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.scrollView.frame) * self.pageControl.numberOfPages, CGRectGetHeight(self.scrollView.frame));
}

@end

//
//  SAHitProductsHeader.h
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@interface SAHitProductsHeader : SABaseView

@property(nonatomic, weak) UIViewController *transitionHandler;

- (void)loadData;

@end

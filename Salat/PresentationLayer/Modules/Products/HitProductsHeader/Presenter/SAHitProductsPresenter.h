//
// Created by mtx on 14.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAOrderPlainObject;
@class SAProductPlainObject;

@protocol SAHitProductsPresenterOutput <NSObject>

- (void)didObtainLastOrder:(SAOrderPlainObject *)order;

- (void)didObtainHits:(NSArray <SAProductPlainObject *> *)hits;

- (void)didObtainFav:(SAProductPlainObject *)fav;

- (void)didObtainError:(NSError *)error;

- (void)didFinish;

@end

@interface SAHitProductsPresenter : NSObject

@property(nonatomic, weak) id <SAHitProductsPresenterOutput> delegate;

- (void)obtainData;

@end
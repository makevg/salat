//
// Created by mtx on 14.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAHitProductsPresenter.h"
#import "SAOrderPlainObject.h"
#import "SAHitProductsInteractor.h"
#import "SAUserService.h"
#import "SAProductPlainObject.h"

@interface SAHitProductsPresenter () <SAHitProductsInteractorOutput>

@property(nonatomic, strong) SAHitProductsInteractor *interactor;
@property(nonatomic, strong) SAUserService *userService;

@end

@implementation SAHitProductsPresenter

- (instancetype)init {
    self = [super init];

    self.interactor = [SAHitProductsInteractor new];
    self.interactor.delegate = self;
    self.userService = [SAUserService new];

    return self;
}

- (void)obtainData {
    [self.interactor obtainHits];
}

- (void)didObtainHits:(NSArray <SAProductPlainObject *> *)hits {
    weakifySelf;
    
    [self.delegate didObtainHits:hits];

    [self.userService checkSession:^{
        [weakSelf.interactor obtainFavs];
        [weakSelf.interactor obtainLastOrder];
    } error:^(NSError *error) {
        [weakSelf didObtainError:error];
    }];
}

- (void)didObtainFav:(SAProductPlainObject *)fav {
    [self.delegate didObtainFav:fav];
}

- (void)didObtainError:(NSError *)error {
    [self.delegate didObtainError:error];
}

- (void)didObtainLastOrder:(SAOrderPlainObject *)orderPlainObject {
    [self.delegate didObtainLastOrder:orderPlainObject];
}

@end
//
// Created by mtx on 14.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAHitProductsInteractor.h"
#import "SAServiceLayer.h"
#import "Order.h"
#import "SAOrderProductModifierPlainObject.h"
#import "SAOrderPlainObject.h"
#import "OrderProductModifier.h"
#import "OrderProduct.h"
#import "SAOrderProductPlainObject.h"
#import "SAPrototypeMapper.h"
#import "SACartService.h"
#import "SAOrdersService.h"
#import "SAOrderProductModifierParser.h"
#import "SAOrderProductParser.h"
#import "SAOrderParser.h"
#import "SAOrderMapper.h"
#import "SAOrderProductMapper.h"
#import "SACartItemModifierMapper.h"
#import "SAOrderProductModifierMapper.h"
#import "SAProductService.h"
#import "SAProductParser.h"
#import "SAProductCategoryParser.h"
#import "SAProductModifierParser.h"
#import "Product.h"
#import "SAProductPlainObject.h"
#import "SAProductMapper.h"

@interface SAHitProductsInteractor ()

@property(nonatomic) SAOrdersService *ordersService;
@property(nonatomic) SAProductService *productService;

@property(nonatomic) id <SAPrototypeMapper> productMapper;
@property(nonatomic) id <SAPrototypeMapper> orderMapper;
@property(nonatomic) id <SAPrototypeMapper> orderProductMapper;
@property(nonatomic) id <SAPrototypeMapper> itemModifierMapper;
@property(nonatomic) id <SAPrototypeMapper> orderProductModifierMapper;

@end

@implementation SAHitProductsInteractor

- (instancetype)init {
    if (self = [super init]) {
        self.productService = [SAProductService new];
        self.productService.productParser = [SAProductParser new];
        self.productService.productCategoryParser = [SAProductCategoryParser new];
        self.productService.productModifierParser = [SAProductModifierParser new];

        self.ordersService = [SAOrdersService new];
        self.ordersService.orderParser = [SAOrderParser new];
        self.ordersService.orderProductParser = [SAOrderProductParser new];
        self.ordersService.orderProductModifierParser = [SAOrderProductModifierParser new];

        self.productMapper = [SAProductMapper new];
        self.orderMapper = [SAOrderMapper new];
        self.orderProductMapper = [SAOrderProductMapper new];
        self.itemModifierMapper = [SACartItemModifierMapper new];
        self.orderProductModifierMapper = [SAOrderProductModifierMapper new];
    }

    return self;
}

- (void)obtainLastOrder {
    weakifySelf;
    
    [self.ordersService obtainOrders:^(NSArray<Order *> *data) {
                dispatch_safe_main_async(^{
                    [weakSelf.delegate didObtainLastOrder:[weakSelf getLastOrder]];
                })
            }
                                          error:^(NSError *error) {
                                              dispatch_safe_main_async(^{
                                                  [weakSelf.delegate didObtainError:error];
                                              })
                                          }];
}

- (void)obtainHits {
    weakifySelf;

    [self.productService obtainHits:^(NSArray<NSNumber *> *hits) {
                unsigned long maxHits = 2;
                NSMutableArray <SAProductPlainObject *> *plainHits = [NSMutableArray new];

                for (unsigned long i = 0, size = MIN([hits count], maxHits); i != size; ++i) {
                    SAProductPlainObject *plainProduct = [weakSelf productById:[hits objectAtIndex:i]];

                    if (plainProduct)
                        [plainHits addObject:plainProduct];
                }

                dispatch_safe_main_async(^{
                    [weakSelf.delegate didObtainHits:plainHits];
                })
            }
                              error:^(NSError *error) {
                                  dispatch_safe_main_async(^{
                                      [weakSelf.delegate didObtainError:error];
                                  })
                              }];
}

- (void)obtainFavs {
    weakifySelf;

    [self.productService obtainMyFavId:^(NSNumber *fav) {
                SAProductPlainObject *favProd = [weakSelf productById:fav];

                dispatch_safe_main_async(^{
                    [weakSelf.delegate didObtainFav:favProd];
                })
            }
                                 error:^(NSError *error) {
                                     dispatch_safe_main_async(^{
                                         [weakSelf.delegate didObtainError:error];
                                     })
                                 }];
}


- (SAOrderPlainObject *)getLastOrder {
    Order *order = [self.ordersService obtainLastOrder];
    if (!order)
        return nil;

    SAOrderPlainObject *orderPlainObject = [SAOrderPlainObject new];
    [self.orderMapper fillObject:orderPlainObject withObject:order];
    orderPlainObject.products = [self getPlainProductsFromManagedObjects:[order.products allObjects]];
    return orderPlainObject;
}

- (NSArray<SAOrderProductPlainObject *> *)getPlainProductsFromManagedObjects:(NSArray<OrderProduct *> *)managedObjectProducts {
    NSMutableArray<SAOrderProductPlainObject *> *productsPlainObjects = [NSMutableArray array];
    for (OrderProduct *managedObjectProduct in managedObjectProducts) {
        SAOrderProductPlainObject *orderProductPlainObject = [SAOrderProductPlainObject new];
        [self.orderProductMapper fillObject:orderProductPlainObject withObject:managedObjectProduct];
        orderProductPlainObject.modifiers = [self getPlainModifierFromManagedObjects:[managedObjectProduct.modifiers allObjects]];
        [productsPlainObjects addObject:orderProductPlainObject];
    }
    return productsPlainObjects;
}

- (NSArray<SAOrderProductModifierPlainObject *> *)getPlainModifierFromManagedObjects:(NSArray<OrderProductModifier *> *)managedObjectModifiers {
    NSMutableArray<SAOrderProductModifierPlainObject *> *modifierPlainObjects
            = [NSMutableArray<SAOrderProductModifierPlainObject *> array];
    for (OrderProductModifier *managedObjectModifier in managedObjectModifiers) {
        SAOrderProductModifierPlainObject *orderProductModifierPlainObject = [SAOrderProductModifierPlainObject new];
        [self.orderProductModifierMapper fillObject:orderProductModifierPlainObject withObject:managedObjectModifier];
        [modifierPlainObjects addObject:orderProductModifierPlainObject];
    }
    return modifierPlainObjects;
}

- (SAProductPlainObject *)productById:(NSNumber *)productId {
    Product *product = [self.productService obtainProductById:productId];
    if (!product)
        return nil;

    SAProductPlainObject *productPlainObject = [SAProductPlainObject new];
    [self.productMapper fillObject:productPlainObject withObject:product];
    return productPlainObject;
}

@end
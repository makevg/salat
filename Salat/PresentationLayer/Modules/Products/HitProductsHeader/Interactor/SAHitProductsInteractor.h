//
// Created by mtx on 14.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAOrderPlainObject;
@class SAProductPlainObject;

@protocol SAHitProductsInteractorOutput <NSObject>

- (void)didObtainLastOrder:(SAOrderPlainObject *)orderPlainObject;

- (void)didObtainHits:(NSArray <SAProductPlainObject *> *)hits;

- (void)didObtainFav:(SAProductPlainObject *)fav;

- (void)didObtainError:(NSError *)error;

@end

@interface SAHitProductsInteractor : NSObject

@property(nonatomic, weak) id <SAHitProductsInteractorOutput> delegate;

- (void)obtainLastOrder;

- (void)obtainHits;

- (void)obtainFavs;

@end
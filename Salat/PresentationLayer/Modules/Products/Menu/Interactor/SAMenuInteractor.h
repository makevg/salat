//
//  SAMenuInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAMenuInteractorInput.h"

@protocol SAMenuInteractorOutput;
@class SAProductService;
@class SAProductCategoryMapper;

@interface SAMenuInteractor : NSObject <SAMenuInteractorInput>

@property(weak, nonatomic) id <SAMenuInteractorOutput> output;
@property(nonatomic) SAProductService *productService;
@property(nonatomic) SAProductCategoryMapper *productCategoryMapper;

@end

//
//  SAMenuInteractorInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAMenuInteractorInput <NSObject>

- (void)obtainMenuItems;

@end

//
//  SAMenuInteractorOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductCategoryPlainObject;

@protocol SAMenuInteractorOutput <NSObject>

- (void)didObtainMenuItems:(NSArray<SAProductCategoryPlainObject *> *)items;

- (void)didObtainError:(NSError *)error;

@end

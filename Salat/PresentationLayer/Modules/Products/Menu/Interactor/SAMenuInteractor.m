//
//  SAMenuInteractor.m
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuInteractor.h"
#import "SAMenuInteractorOutput.h"
#import "SAProductService.h"
#import "ProductCategory.h"
#import "SAProductCategoryPlainObject.h"
#import "SAProductCategoryMapper.h"
#import "SAServiceLayer.h"
#import "SACommon.h"

@implementation SAMenuInteractor

#pragma mark - SAMenuInteractorInput

- (void)obtainMenuItems {
    weakifySelf;

    [self.productService obtainProductCategories:^(NSArray<ProductCategory *> *result) {
        strongifySelf;
        
        if ([result count]) {
            NSArray<SAProductCategoryPlainObject *> *categories = [strongSelf getPlainCategoriesFromManagedObjects:result];
            
            dispatch_safe_main_async(^{
                [strongSelf.output didObtainMenuItems:categories];
            });
        }
    } error:^(NSError *error) {
        dispatch_safe_main_async(^{
            [weakSelf.output didObtainError:error];
        });
    }];
}

#pragma mark - Private methods

- (NSArray<SAProductCategoryPlainObject *> *)getPlainCategoriesFromManagedObjects:(NSArray<ProductCategory *> *)managedObjectCategories {
    NSMutableArray<SAProductCategoryPlainObject *> *categoryPlainObjects = [NSMutableArray array];
    for (ProductCategory *managedObjectProductCategory in managedObjectCategories) {
        SAProductCategoryPlainObject *productCategoryPlainObject = [SAProductCategoryPlainObject new];
        [self.productCategoryMapper fillObject:productCategoryPlainObject withObject:managedObjectProductCategory];
        [categoryPlainObjects addObject:productCategoryPlainObject];
    }
    return categoryPlainObjects;
}

@end

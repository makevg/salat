//
//  SAMenuModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 12.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuModuleAssembly.h"

#import "SAMenuVC.h"
#import "SAMenuPresenter.h"
#import "SAMenuInteractor.h"
#import "SAMenuRouter.h"
#import "SAMenuDataDisplayManager.h"
#import "SAProductService.h"

@implementation SAMenuModuleAssembly

- (SAMenuVC *)viewMenu {
    return [TyphoonDefinition withClass:[SAMenuVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMenu]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self dataDisplayManagerMenu]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
                          }];
}

- (SAMenuPresenter *)presenterMenu {
    return [TyphoonDefinition withClass:[SAMenuPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewMenu]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorMenu]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerMenu]];
                          }];
}

- (SAMenuInteractor *)interactorMenu {
    return [TyphoonDefinition withClass:[SAMenuInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterMenu]];
                              [definition injectProperty:@selector(productService)
                                                    with:[self.serviceComponents productService]];
                              [definition injectProperty:@selector(productCategoryMapper)
                                                    with:[self.coreComponents productCategoryMapper]];
                          }];
}

- (SAMenuRouter *)routerMenu {
    return [TyphoonDefinition withClass:[SAMenuRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewMenu]];
                          }];
}

- (SAMenuDataDisplayManager *)dataDisplayManagerMenu {
    return [TyphoonDefinition withClass:[SAMenuDataDisplayManager class]];
}

@end

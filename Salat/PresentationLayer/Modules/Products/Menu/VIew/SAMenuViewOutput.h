//
//  SAMenuViewOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAProductCategoryPlainObject;

@protocol SAMenuViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;

- (void)didTapCategoryCell:(SAProductCategoryPlainObject *)productCategory;

- (void)didTapUpdateButton;

- (void)networkChanged:(BOOL)connected hasData:(BOOL)hasData forError:(NSError*)error;

@end

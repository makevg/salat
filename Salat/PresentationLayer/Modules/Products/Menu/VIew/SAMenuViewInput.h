//
//  SAMenuViewInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductCategoryPlainObject;

@protocol SAMenuViewInput <NSObject>

- (void)updateWithMenuItems:(NSArray<SAProductCategoryPlainObject *> *)items;

- (void)setErrorState:(NSError *)error;

- (void)showHits;

- (void)loadHeader;

- (void)setLoadingState;

@end

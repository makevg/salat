//
//  SAMenuCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuCell.h"
#import "SAProductCategoryPlainObject.h"
#import "UIImageView+AsyncLoad.h"

static CGFloat const cMenuCellHeight = 90.f;

@interface SAMenuCell ()
@property(weak, nonatomic) IBOutlet UIImageView *backImageView;
@property(weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property(weak, nonatomic) IBOutlet UILabel *captionLabel;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *backHeight;
@end

@implementation SAMenuCell

#pragma mark - Public

+ (CGFloat)cellHeight {
    return cMenuCellHeight;
}

#pragma mark - Configure

- (void)configureCell {
    self.backgroundColor = [SAStyle lightGreenColor];
    self.captionLabel.font = [SAStyle regularFontOfSize:15.f];
    self.captionLabel.textColor = [SAStyle whiteColor];
    self.backHeight.constant = .5f;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProductCategoryPlainObject class]]) {
        [self configureWithModel:model];
    }
}

#pragma mark - Private

- (void)configureWithModel:(SAProductCategoryPlainObject *)menuItem {
    self.captionLabel.text = menuItem.title;
    [self.backImageView loadAsyncFromUrl:menuItem.backgroundImageUrl];
    [self.iconImageView loadAsyncFromUrlNoPlace:menuItem.icon];
}

@end

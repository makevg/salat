//
//  SAMenuVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAMenuViewInput.h"

@protocol SAMenuViewOutput;
@class SAMenuDataDisplayManager;

@interface SAMenuVC : SABaseVC <SAMenuViewInput>

@property(nonatomic) id <SAMenuViewOutput> output;
@property(nonatomic) SAMenuDataDisplayManager *displayManager;

- (void)hideHits;

@end

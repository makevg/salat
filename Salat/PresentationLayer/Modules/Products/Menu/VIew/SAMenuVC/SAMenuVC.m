//
//  SAMenuVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuVC.h"
#import "SAMenuView.h"
#import "SAMenuViewOutput.h"
#import "SAMenuDataDisplayManager.h"
#import "SARestaurantWorkingHeader.h"
#import "SAHitProductsHeader.h"
#import "UIScrollView+EmptyDataSet.h"

NSString *const cMenuStoryboardName = @"Products";
NSUInteger const cMenuHitsHeight = 160;

@interface SAMenuVC () <SAMenuDataDisplayManagerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property(strong, nonatomic) IBOutlet SAMenuView *contentView;

@property(weak, nonatomic) IBOutlet UIView <SARestaurantWorkingHeaderProtocol> *workingHeader;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *workingHeaderHeight;
@property(weak, nonatomic) IBOutlet SAHitProductsHeader *productsHeader;
@property (nonatomic) NSError *error;

@end

@implementation SAMenuVC

#pragma mark - Super

+ (NSString *)storyboardName {
    return cMenuStoryboardName;
}

+ (BOOL)isInitial {
    return NO;
}

- (BOOL)showCartButton {
    return YES;
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    [self.output networkChanged:[notification.object boolValue]
                        hasData:[self.displayManager.tableView numberOfRowsInSection:0]
                       forError:self.error];
}

- (void)configureController {
    [super configureController];

    self.displayManager.tableView = self.contentView.tableView;
    self.displayManager.delegate = self;
    self.contentView.tableView.emptyDataSetSource = self;
    self.contentView.tableView.emptyDataSetDelegate = self;

    [self.workingHeader setHeightConstraint:self.workingHeaderHeight];
    self.productsHeader.transitionHandler = self;

    [self hideHits];
    [self.output didTriggerViewDidLoadEvent];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.workingHeader willAppeared];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [self.workingHeader willDisappeared];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return NO;
}

#pragma mark - SAMenuViewInput

- (void)updateWithMenuItems:(NSArray<SAProductCategoryPlainObject *> *)items {
    self.error = nil;
    [self.contentView hideLoading];

    [self.displayManager configureDataDisplayManagerWithMenuItems:items];
    [self.contentView.tableView reloadData];
}

- (void)setErrorState:(NSError *)error {
    self.error = error;
    [self.contentView hideLoading];
    
    [self.contentView.tableView reloadData];
}

- (void)setLoadingState {
    self.error = nil;
    [self.contentView.tableView reloadEmptyDataSet];
    [self.contentView showLoading];
}


#pragma mark - SAMenuDataDisplayManagerDelegate

- (void)displayManager:(SAMenuDataDisplayManager *)displayManager didTapCategoryCell:(SAProductCategoryPlainObject *)category {
    [self.output didTapCategoryCell:category];
}

#pragma mark - Private

- (void)hideHits {
    self.productsHeader.frame = CGRectMake(
            self.productsHeader.frame.origin.x,
            self.productsHeader.frame.origin.x,
            self.productsHeader.frame.size.width,
            0
    );

    self.contentView.tableView.tableHeaderView = self.productsHeader;
}

- (void)showHits {
    self.productsHeader.frame = CGRectMake(
            self.productsHeader.frame.origin.x,
            self.productsHeader.frame.origin.x,
            self.productsHeader.frame.size.width,
            cMenuHitsHeight
    );

    self.contentView.tableView.tableHeaderView = self.productsHeader;
}

- (void)loadHeader {
    [self.productsHeader loadData];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = self.error ? self.error.localizedDescription : @"Нет меню";
    
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return self.error ? [UIImage imageNamed:@"btn_auth_back"] : nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                 NSForegroundColorAttributeName: [SAStyle mediumGreenColor]};
    return self.error ? [[NSAttributedString alloc] initWithString:@"Повторить"
                                                        attributes:attributes] : nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -10.f;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self.output didTapUpdateButton];
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return self.error;
}

@end

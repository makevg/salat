//
//  SAMenuView.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuView.h"
#import "PCAngularActivityIndicatorView.h"
#import "SAIndicatorViewHelper.h"

@interface SAMenuView ()

@property(nonatomic, strong) PCAngularActivityIndicatorView *indicator;

@end

@implementation SAMenuView

#pragma mark - Setup

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self];
        _indicator.color = [SAStyle mediumGreenColor];
    }

    return _indicator;
}

- (void)setup {
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [SAStyle clearColor];

    [self addSubview:self.indicator];
}

- (void)showLoading {
    [self.indicator startAnimating];
    self.indicator.hidden = NO;
}

- (void)hideLoading {
    [self.indicator stopAnimating];
    self.indicator.hidden = YES;
}

@end
//
//  SAMenuPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuPresenter.h"
#import "SAMenuViewInput.h"
#import "SAMenuInteractorInput.h"
#import "SAMenuRouterInput.h"
#import "SAProductCategoryPlainObject.h"

//static NSString *const kWokCategory = @"Вок";

@interface SAMenuPresenter ()

@property (nonatomic, assign) BOOL loadHeader;

@end

@implementation SAMenuPresenter

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateMenu)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateMenu {
    [self.view setLoadingState];
    [self.interactor obtainMenuItems];
}

#pragma mark - SAMenuViewOutput

- (void)didTriggerViewDidLoadEvent {
    self.loadHeader = NO;
    [self.view setLoadingState];
    [self.interactor obtainMenuItems];
}

- (void)didTapCategoryCell:(SAProductCategoryPlainObject *)productCategory {
    if ([productCategory.isWok boolValue]) {
        [self.router openWokModuleWithCategoryId:productCategory.objectId];
    } else {
        [self.router openProductsWithCategoryId:productCategory.objectId];
    }
}

#pragma mark - SAMenuInteractorOutput

- (void)didObtainMenuItems:(NSArray<SAProductCategoryPlainObject *> *)items {
    [self.view updateWithMenuItems:items];
    if( !self.loadHeader ) {
        [self.view showHits];
        [self.view loadHeader];
        self.loadHeader = YES;
    }
}

- (void)didObtainError:(NSError *)error {
    [self.view setErrorState:error];
}

- (void)didTapUpdateButton {
    [self updateMenu];
}

- (void)networkChanged:(BOOL)connected hasData:(BOOL)hasData forError:(NSError *)error {
    if( !connected )
        return;
    
    if( error ) {
        if( !hasData ) {
            [self.view setLoadingState];
        } else {
            [self.view loadHeader];
        }
    
        [self.interactor obtainMenuItems];
    }
}

@end

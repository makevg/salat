//
//  SAMenuPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAMenuViewOutput.h"
#import "SAMenuInteractorOutput.h"

@protocol SAMenuViewInput;
@protocol SAMenuInteractorInput;
@protocol SAMenuRouterInput;

@interface SAMenuPresenter : NSObject <SAMenuViewOutput, SAMenuInteractorOutput>

@property(weak, nonatomic) id <SAMenuViewInput> view;
@property(nonatomic) id <SAMenuInteractorInput> interactor;
@property(nonatomic) id <SAMenuRouterInput> router;

@end

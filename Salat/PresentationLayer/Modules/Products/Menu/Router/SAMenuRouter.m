//
//  SAMenuRouter.m
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuRouter.h"
#import "SAProductsModuleInput.h"
#import "SAWokPresenter.h"

static NSString *const kMenuModuleToProductsModuleSegue = @"kMenuModuleToProductsModuleSegue";
static NSString *const kMenuModuleToWokModuleSegue = @"kMenuModuleToWokModuleSegue";

@implementation SAMenuRouter

#pragma mark - SAMenuRouterInput

- (void)openProductsWithCategoryId:(NSNumber *)productId {
    [[self.transitionHandler openModuleUsingSegue:kMenuModuleToProductsModuleSegue] thenChainUsingBlock:(RamblerViperModuleLinkBlock) ^id <RamblerViperModuleOutput>(id <SAProductsModuleInput> moduleInput) {
        [moduleInput configureModuleWithCategoryId:productId];
        return nil;
    }];
}

- (void)openWokModuleWithCategoryId:(NSNumber *)productId {
    [[self.transitionHandler openModuleUsingSegue:kMenuModuleToWokModuleSegue] thenChainUsingBlock:(RamblerViperModuleLinkBlock) ^id <RamblerViperModuleOutput>(id <SAWokModuleInput> moduleInput) {
        [moduleInput configureModuleWithCategoryId:productId];
        return nil;
    }];
}

@end

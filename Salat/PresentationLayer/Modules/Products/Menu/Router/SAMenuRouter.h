//
//  SAMenuRouter.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAMenuRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAMenuRouter : NSObject <SAMenuRouterInput>

@property(weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

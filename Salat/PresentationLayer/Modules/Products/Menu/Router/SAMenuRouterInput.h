//
//  SAMenuRouterInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SAMenuRouterInput <NSObject>

- (void)openProductsWithCategoryId:(NSNumber *)productId;

- (void)openWokModuleWithCategoryId:(NSNumber *)productId;

@end

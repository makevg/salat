//
//  SAMenuDataDisplayManager.h
//  Salat
//
//  Created by Maximychev Evgeny on 23.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAProductCategoryPlainObject;
@protocol SAMenuDataDisplayManagerDelegate;

@interface SAMenuDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic) UITableView *tableView;
@property(weak, nonatomic) id <SAMenuDataDisplayManagerDelegate> delegate;

- (void)configureDataDisplayManagerWithMenuItems:(NSArray<SAProductCategoryPlainObject *> *)items;

@end

@protocol SAMenuDataDisplayManagerDelegate <NSObject>

- (void)displayManager:(SAMenuDataDisplayManager *)displayManager didTapCategoryCell:(SAProductCategoryPlainObject *)category;

@end
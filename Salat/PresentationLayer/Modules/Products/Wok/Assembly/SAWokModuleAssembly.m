//
//  SAWokModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAWokModuleAssembly.h"
#import "SAWokVC.h"
#import "SAWokPresenter.h"
#import "SAWokInteractor.h"
#import "SAWokRouter.h"

@implementation SAWokModuleAssembly

- (SAWokVC *)viewWok {
    return [TyphoonDefinition withClass:[SAWokVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterWok]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
                          }];
}

- (SAWokPresenter *)presenterWok {
    return [TyphoonDefinition withClass:[SAWokPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewWok]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorWok]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerWok]];
                          }];
}

- (SAWokInteractor *)interactorWok {
    return [TyphoonDefinition withClass:[SAWokInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterWok]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                              [definition injectProperty:@selector(productService)
                                                    with:[self.serviceComponents productService]];

                              [definition injectProperty:@selector(productModifierMapper)
                                                    with:[self.coreComponents productModifierMapper]];
                              [definition injectProperty:@selector(productMapper)
                                                    with:[self.coreComponents productMapper]];
                          }];
}

- (SAWokRouter *)routerWok {
    return [TyphoonDefinition withClass:[SAWokRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewWok]];
                          }];
}

@end

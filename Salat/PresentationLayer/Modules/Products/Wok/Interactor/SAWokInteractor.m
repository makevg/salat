//
//  SAWokInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAWokInteractor.h"
#import "SACartService.h"
#import "SACartItemModifierPlainObject.h"
#import "SAProductPlainObject.h"
#import "Product.h"
#import "SAProductModifierPlainObject.h"
#import "SAProductService.h"
#import "SAPrototypeMapper.h"

@implementation SAWokInteractor

#pragma mark - SAWokInteractorInput

- (void)addWok:(SAProductPlainObject *)product andModifiers:(NSArray <SAProductModifierPlainObject *> *)modifiers {
    NSMutableArray<SACartItemModifierPlainObject *> *cartModifiers = [NSMutableArray<SACartItemModifierPlainObject *> new];

    SAProductModifierPlainObject *firstModifier = [modifiers objectAtIndex:0];
    SACartItemModifierPlainObject *firstCartModifier = [SACartItemModifierPlainObject new];
    firstCartModifier.itemModifierId = firstModifier.modifierId;
    firstCartModifier.title = firstModifier.title;
    firstCartModifier.price = firstModifier.price;
    firstCartModifier.quantity = @1;
    [cartModifiers addObject:firstCartModifier];

    SAProductModifierPlainObject *secondModifier = [modifiers objectAtIndex:1];
    SACartItemModifierPlainObject *secondCartModifier = [SACartItemModifierPlainObject new];
    secondCartModifier.itemModifierId = secondModifier.modifierId;
    secondCartModifier.title = secondModifier.title;
    secondCartModifier.price = secondModifier.price;
    secondCartModifier.quantity = @1;
    [cartModifiers addObject:secondCartModifier];

    SAProductModifierPlainObject *thirdModifier = [modifiers objectAtIndex:2];
    SACartItemModifierPlainObject *thirdCartModifier = [SACartItemModifierPlainObject new];
    thirdCartModifier.itemModifierId = thirdModifier.modifierId;
    thirdCartModifier.title = thirdModifier.title;
    thirdCartModifier.price = thirdModifier.price;
    thirdCartModifier.quantity = @1;
    [cartModifiers addObject:thirdCartModifier];

    [self.cartService plusItemWithProductId:product.objectId
                                      title:product.title
                                      descr:nil
                                   imageUrl:product.imageUrl
                                       cost:product.price
                                  modifiers:cartModifiers];
}

- (void)obtainProductByCategoryId:(NSNumber *)categoryId {
    Product *managedObjectProducts = [self.productService obtainProductByCategoryId:categoryId];
    if (!managedObjectProducts) {
        [self.output didObtainProduct:nil];
    }

    SAProductPlainObject *products = [self getPlainProductsFromManagedObject:managedObjectProducts];
    [self.output didObtainProduct:products];
}

#pragma mark - Private methods

- (SAProductPlainObject *)getPlainProductsFromManagedObject:(Product *)managedObjectProduct {
    SAProductPlainObject *productPlainObject = [SAProductPlainObject new];
    [self.productMapper fillObject:productPlainObject withObject:managedObjectProduct];

    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"position"
                                                         ascending:YES];

    NSArray *rawModif = [managedObjectProduct.modifiers sortedArrayUsingDescriptors:@[sort]];
    NSArray<SAProductModifierPlainObject *> *modifiers = [self getPlainModifiersFromManagedObjects:rawModif];
    productPlainObject.modifiers = modifiers;

    return productPlainObject;
}

- (NSArray *)getPlainModifiersFromManagedObjects:(NSArray *)managedObjectModifiers {
    NSMutableArray *modifiersPlainObjects = [NSMutableArray array];
    for (ProductModifier *modifier in managedObjectModifiers) {
        SAProductModifierPlainObject *productModifier = [SAProductModifierPlainObject new];
        [self.productModifierMapper fillObject:productModifier withObject:modifier];
        [modifiersPlainObjects addObject:productModifier];
    }
    return modifiersPlainObjects;
}


@end

//
//  SAWokInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SACartService;
@class SAProductPlainObject;
@class SAProductService;
@protocol SAPrototypeMapper;
@class SAProductModifierPlainObject;

@protocol SAWokInteractorOutput <NSObject>

- (void)didObtainError:(NSError *)error;

- (void)didObtainProduct:(SAProductPlainObject *)product;

@end

@protocol SAWokInteractorInput <NSObject>

- (void)addWok:(SAProductPlainObject *)product andModifiers:(NSArray <SAProductModifierPlainObject *> *)modifiers;

- (void)obtainProductByCategoryId:(NSNumber *)categoryId;

@end

@interface SAWokInteractor : NSObject <SAWokInteractorInput>

@property(weak, nonatomic) id <SAWokInteractorOutput> output;
@property(nonatomic) SACartService *cartService;
@property(nonatomic) SAProductService *productService;

@property(nonatomic, strong) id <SAPrototypeMapper> productModifierMapper;
@property(nonatomic, strong) id <SAPrototypeMapper> productMapper;

@end

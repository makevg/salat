//
//  SAWokVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAWokVC.h"
#import "SAWokView.h"
#import "SAWokViewOutput.h"
#import "UINavigationController+TransparentNavigationController.h"
#import "SAWokProductCell.h"
#import "SAProductModifierPlainObject.h"
#import "SAWokProductSouseCell.h"
#import "NSString+Currency.h"

static NSString *const cWokVCStoryboardName = @"Wok";

@interface SAWokVC () <UICollectionViewDelegate, UICollectionViewDataSource>

@property(strong, nonatomic) IBOutlet SAWokView *contentView;

@property(strong, nonatomic) NSArray <SAProductModifierPlainObject *> *firstGroupArray;
@property(strong, nonatomic) NSArray <SAProductModifierPlainObject *> *secondGroupArray;
@property(strong, nonatomic) NSArray <SAProductModifierPlainObject *> *souseGroupArray;

@property(strong, nonatomic) SAProductModifierPlainObject *firstProduct;
@property(strong, nonatomic) SAProductModifierPlainObject *secondProduct;
@property(strong, nonatomic) SAProductModifierPlainObject *souseProduct;
@property(strong, nonatomic) NSNumber *price;

@end

@implementation SAWokVC

#pragma mark - Super

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self prepareCollectionView];
    [self.navigationController presentTransparentNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self.navigationController hideTransparentNavigationBar];
}

+ (NSString *)storyboardName {
    return cWokVCStoryboardName;
}

- (void)configureController {
    [super configureController];

    self.firstGroupArray = @[];
    self.secondGroupArray = @[];
    self.souseGroupArray = @[];

    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Actions

- (IBAction)tappedOrderButton:(id)sender {
    [self.output didTapOrderButton];
}

- (IBAction)tappedChangeButton:(id)sender {
    [self.contentView flipViews];
}

- (void)prepareCollectionView {
}

#pragma mark - Public

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.contentView.firstCollectionView)
        return [self.firstGroupArray count];

    if (collectionView == self.contentView.secondCollectionView)
        return [self.secondGroupArray count];

    if (collectionView == self.contentView.souseCollectionView)
        return [self.souseGroupArray count];

    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.contentView.souseCollectionView) {
        return CGSizeMake(self.view.frame.size.width, 40);
    }

    return CGSizeMake(self.view.frame.size.width, 120);
}


//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
////    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
////    NSInteger numberOfCells = self.view.frame.size.width / flowLayout.itemSize.width;
////    NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * flowLayout.itemSize.width)) / (numberOfCells + 1);
////    return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
//}
//
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float currentPage = scrollView.contentOffset.x / pageWidth;

    if (0.0f != fmodf(currentPage, 1.0f)) {
        currentPage = currentPage + 1;
    }

    SAProductModifierPlainObject *modifierPlainObject;

    if (scrollView == self.contentView.firstCollectionView) {
        currentPage = MIN( [self.firstGroupArray count] - 1, currentPage );
        modifierPlainObject = [self.firstGroupArray objectAtIndex:(NSUInteger) currentPage];
        self.firstProduct = modifierPlainObject;

        self.contentView.firstLeftArrow.hidden = currentPage == 0;
        self.contentView.firstRightArrow.hidden = currentPage == [self.firstGroupArray count] - 1;
    }

    if (scrollView == self.contentView.secondCollectionView) {
        currentPage = MIN( [self.secondGroupArray count] - 1, currentPage );
        modifierPlainObject = [self.secondGroupArray objectAtIndex:(NSUInteger) currentPage];
        self.secondProduct = modifierPlainObject;

        self.contentView.secondLeftArrow.hidden = currentPage == 0;
        self.contentView.secondRightArrow.hidden = currentPage == [self.secondGroupArray count] - 1;
    }

    if (scrollView == self.contentView.souseCollectionView) {
        currentPage = MIN( [self.souseGroupArray count] - 1, currentPage );
        modifierPlainObject = [self.souseGroupArray objectAtIndex:(NSUInteger) currentPage];
        self.souseProduct = modifierPlainObject;

        self.contentView.souseLeftArrow.hidden = currentPage == 0;
        self.contentView.souseRightArrow.hidden = currentPage == [self.souseGroupArray count] - 1;
    }

    [self calc];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SAWokProductCell *cell;
    SAProductModifierPlainObject *modifierPlainObject;

    if (collectionView == self.contentView.souseCollectionView) {
        NSString *identifier = [SAWokProductSouseCell cellIdentifier];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                         forIndexPath:indexPath];

        modifierPlainObject = [self.souseGroupArray objectAtIndex:(NSUInteger) indexPath.row];
        self.souseProduct = modifierPlainObject;

    } else {
        NSString *identifier = [SAWokProductCell cellIdentifier];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                         forIndexPath:indexPath];

        if (collectionView == self.contentView.secondCollectionView) {
            [cell setUp:NO];
            modifierPlainObject = [self.secondGroupArray objectAtIndex:(NSUInteger) indexPath.row];
            self.secondProduct = modifierPlainObject;
        } else {
            [cell setUp:YES];
            modifierPlainObject = [self.firstGroupArray objectAtIndex:(NSUInteger) indexPath.row];
            self.firstProduct = modifierPlainObject;
        }
    }

    [cell setModel:modifierPlainObject];

    [self calc];
    return cell;
}

#pragma mark - SAWokViewInput

- (void)setBasePrice:(NSNumber *)price {
self.price = price;
}

- (void)setFirstGroup:(NSArray <SAProductModifierPlainObject *> *)array {
    self.firstGroupArray = array;

    if ([self.firstGroupArray count])
        self.firstProduct = [self.firstGroupArray objectAtIndex:0];

    [self calc];
}

- (void)setSecondGroup:(NSArray <SAProductModifierPlainObject *> *)array {
    self.secondGroupArray = array;

    if ([self.secondGroupArray count])
        self.secondProduct = [self.secondGroupArray objectAtIndex:0];

    [self calc];
}

- (void)setSouseGroup:(NSArray <SAProductModifierPlainObject *> *)array {
    self.souseGroupArray = array;

    if ([self.souseGroupArray count])
        self.souseProduct = [self.souseGroupArray objectAtIndex:0];

    [self calc];

}

- (NSArray <SAProductModifierPlainObject *> *)obtainData {
    return @[
            self.firstProduct,
            self.secondProduct,
            self.souseProduct
    ];
}


- (void)calc {
    double price = [self.price doubleValue] +
            [self.firstProduct.price doubleValue] +
            [self.secondProduct.price doubleValue] +
            [self.souseProduct.price doubleValue];

    self.contentView.priceLabel.text = [NSString priceWithCurrencySymbol:@(price)];

    self.contentView.firstProductLabel.text = self.firstProduct.title;
    self.contentView.secondProductLabel.text = self.secondProduct.title;
    self.contentView.souseLabel.text = self.souseProduct.title;
}

@end

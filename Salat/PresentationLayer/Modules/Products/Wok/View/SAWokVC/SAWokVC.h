//
//  SAWokVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAWokViewInput.h"

@protocol SAWokViewOutput;

@interface SAWokVC : SABaseVC <SAWokViewInput>

@property(nonatomic) id <SAWokViewOutput> output;

@end

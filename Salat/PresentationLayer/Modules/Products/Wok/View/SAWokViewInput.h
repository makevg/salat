//
//  SAWokViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductModifierPlainObject;

@protocol SAWokViewInput <NSObject>

- (void)setBasePrice:(NSNumber*)price;

- (void)setFirstGroup:(NSArray <SAProductModifierPlainObject *> *)array;

- (void)setSecondGroup:(NSArray <SAProductModifierPlainObject *> *)array;

- (void)setSouseGroup:(NSArray <SAProductModifierPlainObject *> *)array;

- (NSArray <SAProductModifierPlainObject *> *)obtainData;

@end

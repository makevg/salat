//
//  SAWokView.m
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAWokView.h"
#import "SABorderButton.h"
//#import "SAHorizontalLinearFlowLayout.h"
//#import "UIImage+ImageEffects.h"

@interface SAWokView ()
@property(weak, nonatomic) IBOutlet UILabel *wokLabel;
@property(weak, nonatomic) IBOutlet UIImageView *pan;
@property(weak, nonatomic) IBOutlet UIImageView *bg;
@property(weak, nonatomic) IBOutlet SABorderButton *orderButton;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *topImageConstraint;

@property(weak, nonatomic) IBOutlet NSLayoutConstraint *firstCenterConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *secondCenterConstraint;

@property(weak, nonatomic) IBOutlet NSLayoutConstraint *firstLabelConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *secondLabelConstraint;

//@property(strong, nonatomic) SAHorizontalLinearFlowLayout *firstCollectionViewLayout;
//@property(strong, nonatomic) SAHorizontalLinearFlowLayout *secondCollectionViewLayout;
//@property(strong, nonatomic) SAHorizontalLinearFlowLayout *souseCollectionViewLayout;
@end

@implementation SAWokView {
    BOOL flipped;
}

#pragma mark - Setup

- (void)setup {
    flipped = NO;
    self.firstCollectionView.backgroundColor = [UIColor clearColor];
    self.secondCollectionView.backgroundColor = [UIColor clearColor];
    self.souseCollectionView.backgroundColor = [UIColor clearColor];

    self.pan.layer.shadowOffset = CGSizeMake(5, 5);
    self.pan.layer.shadowRadius = 5.0;
    self.pan.layer.shadowOpacity = 0.6;
    self.pan.layer.masksToBounds = NO;

    self.backgroundColor = [SAStyle backgroundColor];
    self.wokLabel.textColor = [SAStyle whiteColor];
    self.wokLabel.font = [SAStyle regularFontOfSize:15.f];
    self.souseLabel.font = [SAStyle regularFontOfSize:12.f];
    self.firstProductLabel.font = [SAStyle regularFontOfSize:16.f];
    self.secondProductLabel.font = [SAStyle regularFontOfSize:16.f];
    self.priceLabel.font = [SAStyle regularFontOfSize:16.f];

    self.orderButton.titleLabel.font = [SAStyle regularFontOfSize:17.f];
    self.orderButton.layer.borderWidth = 1.f;

    self.firstLeftArrow.hidden = YES;
    self.secondLeftArrow.hidden = YES;
    self.souseLeftArrow.hidden = YES;

    [self prepareCollections];
}

- (void)flipViews {
    self.secondCenterConstraint.constant = -self.secondCenterConstraint.constant;
    self.firstCenterConstraint.constant = -self.firstCenterConstraint.constant;
    self.firstLabelConstraint.constant = -self.firstLabelConstraint.constant;
    self.secondLabelConstraint.constant = -self.secondLabelConstraint.constant;

    if( flipped ) {
        self.secondCollectionView.transform = CGAffineTransformIdentity;
        self.firstCollectionView.transform = CGAffineTransformIdentity;
    } else {
        self.secondCollectionView.transform = CGAffineTransformMakeScale(1, -1);
        self.firstCollectionView.transform = CGAffineTransformMakeScale(1, -1);
    }

    flipped = !flipped;
}

//- (CGSize)productItemSize {
//    return ((UICollectionViewFlowLayout *) self.firstCollectionView.collectionViewLayout).itemSize;
//}
//
//- (CGSize)souseItemSize {
//    return ((UICollectionViewFlowLayout *) self.souseCollectionView.collectionViewLayout).itemSize;
//}

- (void)prepareCollections {
//    self.firstCollectionViewLayout = [SAHorizontalLinearFlowLayout layoutConfiguredWithCollectionView:self.firstCollectionView
//                                                                                             itemSize:[self productItemSize]
//                                                                                   minimumLineSpacing:10];
//    self.firstCollectionViewLayout.minimumScaleFactor = 1.f;
//    self.firstCollectionViewLayout.scaleItems = NO;
//    self.firstCollectionView.collectionViewLayout = self.firstCollectionViewLayout;

//    self.secondCollectionViewLayout = [SAHorizontalLinearFlowLayout layoutConfiguredWithCollectionView:self.secondCollectionView
//                                                                                              itemSize:[self productItemSize]
//                                                                                    minimumLineSpacing:10];
//    self.secondCollectionViewLayout.minimumScaleFactor = 1.f;
//    self.secondCollectionViewLayout.scaleItems = NO;
//    self.secondCollectionView.collectionViewLayout = self.secondCollectionViewLayout;

//    self.souseCollectionViewLayout = [SAHorizontalLinearFlowLayout layoutConfiguredWithCollectionView:self.souseCollectionView
//                                                                                             itemSize:[self souseItemSize]
//                                                                                   minimumLineSpacing:50];
//    self.souseCollectionViewLayout.minimumScaleFactor = 1.f;
//    self.souseCollectionViewLayout.scaleItems = NO;
//    self.souseCollectionView.collectionViewLayout = self.souseCollectionViewLayout;
}


@end

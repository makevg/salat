//
//  SAWokView.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@interface SAWokView : SABaseView

@property(weak, nonatomic) IBOutlet UICollectionView *firstCollectionView;
@property(weak, nonatomic) IBOutlet UICollectionView *secondCollectionView;
@property(weak, nonatomic) IBOutlet UICollectionView *souseCollectionView;

@property(weak, nonatomic) IBOutlet UILabel *priceLabel;
@property(weak, nonatomic) IBOutlet UILabel *firstProductLabel;
@property(weak, nonatomic) IBOutlet UILabel *secondProductLabel;
@property(weak, nonatomic) IBOutlet UILabel *souseLabel;

@property(weak, nonatomic) IBOutlet UIView *firstLeftArrow;
@property(weak, nonatomic) IBOutlet UIView *secondLeftArrow;
@property(weak, nonatomic) IBOutlet UIView *souseLeftArrow;

@property(weak, nonatomic) IBOutlet UIView *firstRightArrow;
@property(weak, nonatomic) IBOutlet UIView *secondRightArrow;
@property(weak, nonatomic) IBOutlet UIView *souseRightArrow;

- (void)flipViews;

@end

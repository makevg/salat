//
// Created by mtx on 15.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAWokProductSouseCell.h"
#import "SAProductModifierPlainObject.h"
#import "UIImageView+AsyncLoad.h"


@interface SAWokProductSouseCell()

@property (nonatomic, weak) IBOutlet UIView *insideView;

@end

@implementation SAWokProductSouseCell

- (void)configureCell {
    [self configureSubviews];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProductModifierPlainObject class]]) {
        [self configureWithProduct:model];
    }
}

#pragma mark - Private

- (void)configureSubviews {
    self.productImage.layer.cornerRadius = self.frame.size.height/2;
    self.productImage.layer.masksToBounds = YES;

    self.layer.shadowOffset = CGSizeMake(3, 3);
    self.layer.shadowRadius = 3.0;
    self.layer.shadowOpacity = 0.6;
    self.layer.masksToBounds = NO;
}

- (void)configureWithProduct:(SAProductModifierPlainObject *)product {
    [self.productImage loadAsyncFromUrl:product.imageUrl placeHolder:@"noproduct"];
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

@end
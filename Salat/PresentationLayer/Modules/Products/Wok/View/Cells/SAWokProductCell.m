//
// Created by mtx on 15.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAWokProductCell.h"
#import "SAProductModifierPlainObject.h"
#import "UIImageView+AsyncLoad.h"


@implementation SAWokProductCell

#pragma mark - Public

- (void)configureCell {
    [self setRounded];
    [self configureSubviews];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProductModifierPlainObject class]]) {
        [self configureWithProduct:model];
    }
}

#pragma mark - Private

- (UIBezierPath *)createPath:(CGRect)ovalRect {
    UIBezierPath *ovalPath = UIBezierPath.bezierPath;
    [ovalPath addArcWithCenter:CGPointMake(0, 0)
                        radius:CGRectGetWidth(ovalRect) / 2
                    startAngle:(CGFloat) -M_PI
                      endAngle:0
                     clockwise:YES];

    [ovalPath addLineToPoint:CGPointMake(0, 0)];
    [ovalPath closePath];

    CGAffineTransform ovalTransform = CGAffineTransformMakeTranslation(CGRectGetMidX(ovalRect), CGRectGetMidY(ovalRect));
    ovalTransform = CGAffineTransformScale(ovalTransform, 1, CGRectGetHeight(ovalRect) / CGRectGetWidth(ovalRect));
    [ovalPath applyTransform:ovalTransform];

    return ovalPath;
}

- (void)setRounded {
    CAShapeLayer *shapeView = [[CAShapeLayer alloc] init];
    [shapeView setPath:[self createPath:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height * 2)].CGPath];
    self.productImage.layer.mask = shapeView;
}

- (void)configureSubviews {

}

- (void)setUp:(BOOL)up {
    _up = up;

    if( !_up ) {
       self.productImage.transform = CGAffineTransformMakeRotation((CGFloat) M_PI);
    }
}

- (void)configureWithProduct:(SAProductModifierPlainObject *)product {
    [self.productImage loadAsyncFromUrl:product.imageUrl placeHolder:@"noproduct"];
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

@end
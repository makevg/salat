//
// Created by mtx on 15.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SABaseCollectionViewCell.h"


@interface SAWokProductCell : SABaseCollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *productImage;
@property (nonatomic, assign) BOOL up;

@end
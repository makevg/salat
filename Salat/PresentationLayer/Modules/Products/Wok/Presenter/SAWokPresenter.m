//
//  SAWokPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAWokPresenter.h"
#import "SAProductModifierPlainObject.h"
#import "SAProductPlainObject.h"
#import "SAWokViewInput.h"

@interface SAWokPresenter ()

@property(nonatomic, strong) NSNumber *categoryId;
@property(nonatomic, strong) SAProductPlainObject *wokProduct;

@end

@implementation SAWokPresenter

#pragma mark - SAWokViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.interactor obtainProductByCategoryId:self.categoryId];
}

- (void)didTapOrderButton {
    [self.interactor addWok:self.wokProduct andModifiers:[self.view obtainData]];
}

- (void)didObtainError:(NSError *)error {
    //TODO: write
}

- (void)didObtainProduct:(SAProductPlainObject *)product {
    self.wokProduct = product;

    NSMutableArray *firstGroup = [NSMutableArray new];
    NSMutableArray *secondGroup = [NSMutableArray new];
    NSMutableArray *souseGroup = [NSMutableArray new];

    for (SAProductModifierPlainObject *modifierPlainObject in product.modifiers) {
        if ([modifierPlainObject.modifierCategoryId isEqualToNumber:@1]) {
            [firstGroup addObject:modifierPlainObject];
            continue;
        }

        if ([modifierPlainObject.modifierCategoryId isEqualToNumber:@2]) {
            [secondGroup addObject:modifierPlainObject];
            continue;
        }

        if ([modifierPlainObject.modifierCategoryId isEqualToNumber:@3]) {
            [souseGroup addObject:modifierPlainObject];
            continue;
        }
    }

    [self.view setBasePrice:self.wokProduct.price];
    [self.view setFirstGroup:firstGroup];
    [self.view setSecondGroup:secondGroup];
    [self.view setSouseGroup:souseGroup];
}


- (void)configureModuleWithCategoryId:(NSNumber *)categoryId {
    self.categoryId = categoryId;
}

@end

//
//  SAWokPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAWokViewOutput.h"
#import "SAWokInteractor.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SAWokViewInput;
@protocol SAWokInteractorInput;
@protocol SAWokRouterInput;

@protocol SAWokModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithCategoryId:(NSNumber *)categoryId;

@end


@interface SAWokPresenter : NSObject <SAWokViewOutput, SAWokInteractorOutput, SAWokModuleInput>

@property(weak, nonatomic) id <SAWokViewInput> view;
@property(nonatomic) id <SAWokInteractorInput> interactor;
@property(nonatomic) id <SAWokRouterInput> router;

@end

//
//  SAWokRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 15.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAWokRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAWokRouter : NSObject <SAWokRouterInput>

@property(weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

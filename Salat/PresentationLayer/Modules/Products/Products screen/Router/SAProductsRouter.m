//
//  SAProductsRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsRouter.h"
#import "SAProductsCarouselModuleInput.h"

static NSString *const kProductsModuleToProductsCarouselModuleSegue = @"kProductsModuleToProductsCarouselModuleSegue";

@implementation SAProductsRouter

#pragma mark - SAProductsRouterInput

- (void)openProductsCarouselWithCategoryId:(NSNumber *)categoryId productIndexPath:(NSIndexPath *)indexPath {
    [[self.transitionHandler openModuleUsingSegue:kProductsModuleToProductsCarouselModuleSegue] thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <SAProductsCarouselModuleInput> moduleInput) {
        [moduleInput configureModuleWithCategoryId:categoryId productIndexPath:indexPath];
        return nil;
    }];
}

@end

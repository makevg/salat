//
//  SAProductsRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsRouterInput.h"
#import "ViperMcFlurry.h"

@interface SAProductsRouter : NSObject <SAProductsRouterInput>

@property(weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

//
//  SAProductsRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAProductsRouterInput <NSObject>

- (void)openProductsCarouselWithCategoryId:(NSNumber *)categoryId productIndexPath:(NSIndexPath *)indexPath;

@end

//
//  SAProductsDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsDataDisplayManager.h"
#import "SAProductCollectionCell.h"
#import "SAProductPlainObject.h"

@interface SAProductsDataDisplayManager () <SAProductCollectionCellDelegate>
@property(nonatomic) NSArray *products;
@end

@implementation SAProductsDataDisplayManager {
    CGFloat screenWidth;
}

#pragma mark - Lazy init

- (void)setCollectionView:(UICollectionView *)collectionView {
    _collectionView = collectionView;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
}

#pragma mark - Init

- (instancetype)init {
    self = [super init];
    if (self) {
        screenWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    }
    return self;
}

#pragma mark - Public

- (void)configureDataDisplayManagerWithProducts:(NSArray<SAProductPlainObject *> *)products {
    self.products = products;
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SAProductCollectionCell cellIdentifier];
    SAProductCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                              forIndexPath:indexPath];
    [cell setModel:self.products[(NSUInteger) indexPath.row]];
    cell.delegate = self;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate displayManager:self didTapProductCellAtIndexPath:indexPath];
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellWidth = screenWidth / 2 - 21.f;
    CGFloat cellHeight = 200.f;
    return CGSizeMake(cellWidth, cellHeight);
}

#pragma mark - SAProductCollectionCellDelegate

- (void)productCell:(SAProductCollectionCell *)cell didPlusProduct:(SAProductPlainObject *)product {
    [self.delegate displayManager:self didPlusProduct:product];
}

- (void)productCell:(SAProductCollectionCell *)cell didMinusProduct:(SAProductPlainObject *)product {
    [self.delegate displayManager:self didMinusProduct:product];
}


@end

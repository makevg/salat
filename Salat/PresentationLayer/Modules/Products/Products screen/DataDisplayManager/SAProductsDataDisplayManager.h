//
//  SAProductsDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAProductPlainObject;
@protocol SAProductsDataDisplayManagerDelegate;

@interface SAProductsDataDisplayManager : NSObject <UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic) UICollectionView *collectionView;
@property(weak, nonatomic) id <SAProductsDataDisplayManagerDelegate> delegate;

- (void)configureDataDisplayManagerWithProducts:(NSArray<SAProductPlainObject *> *)products;

@end

@protocol SAProductsDataDisplayManagerDelegate <NSObject>

- (void)displayManager:(SAProductsDataDisplayManager *)displayManager didTapProductCellAtIndexPath:(NSIndexPath *)indexPath;

- (void)displayManager:(SAProductsDataDisplayManager *)displayManager didPlusProduct:(SAProductPlainObject *)product;

- (void)displayManager:(SAProductsDataDisplayManager *)displayManager didMinusProduct:(SAProductPlainObject *)product;

@end

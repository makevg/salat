//
//  SAProductsModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsModuleAssembly.h"
#import "SAProductsVC.h"
#import "SAProductsPresenter.h"
#import "SAProductsInteractor.h"
#import "SAProductsRouter.h"
#import "SAProductsDataDisplayManager.h"
#import "SAProductsPresenterStateStorage.h"

@implementation SAProductsModuleAssembly

- (SAProductsVC *)viewProducts {
    return [TyphoonDefinition withClass:[SAProductsVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProducts]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self dataDisplayManagerProducts]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
                          }];
}

- (SAProductsPresenter *)presenterProducts {
    return [TyphoonDefinition withClass:[SAProductsPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewProducts]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorProducts]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerProducts]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorage]];
                          }];
}

- (SAProductsInteractor *)interactorProducts {
    return [TyphoonDefinition withClass:[SAProductsInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProducts]];
                              [definition injectProperty:@selector(productService)
                                                    with:[self.serviceComponents productService]];
                              [definition injectProperty:@selector(productMapper)
                                                    with:[self.coreComponents productMapper]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                              [definition injectProperty:@selector(itemModifierMapper)
                                                    with:[self.coreComponents cartItemModifierMapper]];
                          }];
}

- (SAProductsRouter *)routerProducts {
    return [TyphoonDefinition withClass:[SAProductsRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewProducts]];
                          }];
}

- (SAProductsDataDisplayManager *)dataDisplayManagerProducts {
    return [TyphoonDefinition withClass:[SAProductsDataDisplayManager class]];
}

- (SAProductsPresenterStateStorage *)presenterStateStorage {
    return [TyphoonDefinition withClass:[SAProductsPresenterStateStorage class]];
}

@end

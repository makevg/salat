//
//  SAProductsInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsInteractor.h"
#import "SAProductsInteractorOutput.h"
#import "SAProductService.h"
#import "Product.h"
#import "SAProductPlainObject.h"
#import "SAProductMapper.h"
#import "ProductCategory.h"
#import "SACartService.h"
#import "SAProductModifierPlainObject.h"
#import "SAProductModifierMapper.h"
#import "SACommon.h"

@implementation SAProductsInteractor

#pragma mark - SAProductsInteractorInput

- (void)obtainProductsByCategoryId:(NSNumber *)categoryId {
    weakifySelf;
    
    [self.productService obtainProductsByCategoryId:categoryId
                                          copletion:^(NSArray<Product *> *result) {
                                              strongifySelf;
                                              NSArray *products = [strongSelf getPlainProductsFromManagedObjects:result];
                                              
                                              dispatch_safe_main_async(^{
                                                  [strongSelf.output didObtainProducts:products];
                                              });
                                          } error:^(NSError *error) {
                                              dispatch_safe_main_async(^{
                                                  [weakSelf.output didObtainError:error];
                                              });
                                          }];
}

- (void)obtainCategoryNameById:(NSNumber *)categoryId {
    NSString *categoryName = [self.productService obtainProductCategoryById:categoryId].title;
    [self.output didObtainCategoryName:categoryName];
}

- (void)plusProduct:(SAProductPlainObject *)product {
    [self.cartService plusItemWithProductId:product.objectId
                                      title:product.title
                                      descr:product.descr
                                   imageUrl:product.imageUrl
                                       cost:product.price
                                  modifiers:@[]];
}

- (void)minusProduct:(SAProductPlainObject *)product {
    [self.cartService minusItemWithProductId:product.objectId
                                   modifiers:@[]];
}

#pragma mark - Private methods

- (NSArray *)getPlainProductsFromManagedObjects:(NSArray *)managedObjectProducts {
    NSMutableArray *productPlainObjects = [NSMutableArray array];
    for (Product *managedObjectProduct in managedObjectProducts) {
        SAProductPlainObject *productPlainObject = [SAProductPlainObject new];
        [self.productMapper fillObject:productPlainObject withObject:managedObjectProduct];
        productPlainObject.quantity = [self.cartService obtainItemQuantityByProductId:productPlainObject.objectId];
        [productPlainObjects addObject:productPlainObject];
        NSArray<SAProductModifierPlainObject *> *modifiers = [self getPlainModifiersFromManagedObjects:[managedObjectProduct.modifiers allObjects]];
        productPlainObject.modifiers = modifiers;
    }
    return productPlainObjects;
}

- (NSArray *)getPlainModifiersFromManagedObjects:(NSArray *)managedObjectModifiers {
    NSMutableArray *modifiersPlainObjects = [NSMutableArray array];
    for (ProductModifier *modifier in managedObjectModifiers) {
        SAProductModifierPlainObject *productModifier = [SAProductModifierPlainObject new];
        [self.productModifierMapper fillObject:productModifier withObject:modifier];
        [modifiersPlainObjects addObject:productModifier];
    }
    return modifiersPlainObjects;
}

@end

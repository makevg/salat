//
//  SAProductsInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductPlainObject;

@protocol SAProductsInteractorInput <NSObject>

- (void)obtainProductsByCategoryId:(NSNumber *)categoryId;

- (void)obtainCategoryNameById:(NSNumber *)categoryId;

- (void)plusProduct:(SAProductPlainObject *)product;

- (void)minusProduct:(SAProductPlainObject *)product;

@end

//
//  SAProductsInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductPlainObject;

@protocol SAProductsInteractorOutput <NSObject>

- (void)didObtainProducts:(NSArray<SAProductPlainObject *> *)products;

- (void)didObtainCategoryName:(NSString *)categoryName;

- (void)didObtainError:(NSError *)error;

@end

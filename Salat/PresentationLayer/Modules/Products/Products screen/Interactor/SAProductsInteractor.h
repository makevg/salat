//
//  SAProductsInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsInteractorInput.h"

@protocol SAProductsInteractorOutput;
@class SAProductService;
@class SAProductMapper;
@class SACartService;
@class SAProductModifierMapper;
@protocol SAPrototypeMapper;

@interface SAProductsInteractor : NSObject <SAProductsInteractorInput>

@property(weak, nonatomic) id <SAProductsInteractorOutput> output;
@property(nonatomic) SAProductService *productService;
@property(nonatomic) SAProductMapper *productMapper;
@property(nonatomic) SAProductModifierMapper *productModifierMapper;
@property(nonatomic) SACartService *cartService;
@property(nonatomic) id <SAPrototypeMapper> itemModifierMapper;

@end

//
//  SAProductsContentView.m
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsContentView.h"

@implementation SAProductsContentView

#pragma mark - Setup

- (void)setup {
    self.collectionView.backgroundColor = [SAStyle clearColor];
}

@end

//
//  SAProductsViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAProductPlainObject;

@protocol SAProductsViewOutput <NSObject>

- (void)didTriggerViewWillAppearEvent;

- (void)didTapProductCellAtIndexPath:(NSIndexPath *)indexPath;

- (void)didTapPlusProduct:(SAProductPlainObject *)product;

- (void)didTapMinusProduct:(SAProductPlainObject *)product;

- (void)didTapUpdateButton;

@end

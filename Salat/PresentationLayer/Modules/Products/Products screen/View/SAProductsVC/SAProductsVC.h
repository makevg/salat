//
//  SAProductsVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAProductsViewInput.h"

@class SAProductsDataDisplayManager;
@protocol SAProductsViewOutput;

@interface SAProductsVC : SABaseVC <SAProductsViewInput>

@property(nonatomic) id <SAProductsViewOutput> output;
@property(nonatomic) SAProductsDataDisplayManager *displayManager;

@end

//
//  SAProductsVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsVC.h"
#import "SAProductsContentView.h"
#import "SAProductsDataDisplayManager.h"
#import "SAProductsViewOutput.h"
#import "SARestaurantWorkingHeader.h"
#import "SAIndicatorViewHelper.h"
#import "UIScrollView+EmptyDataSet.h"

static NSString *const cProductsVCStoryboardName = @"Products";

@interface SAProductsVC () <SAProductsDataDisplayManagerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (strong, nonatomic) IBOutlet SAProductsContentView *contentView;
@property (weak, nonatomic) IBOutlet UIView <SARestaurantWorkingHeaderProtocol> *workingHeader;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workingHeaderHeight;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;
@property (nonatomic) NSError *error;
@end

@implementation SAProductsVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }
    
    return _indicator;
}

#pragma mark - Super

+ (NSString *)storyboardName {
    return cProductsVCStoryboardName;
}

- (BOOL)showCartButton {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.output didTriggerViewWillAppearEvent];
    [self.workingHeader willAppeared];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self.workingHeader willDisappeared];
}

- (void)configureController {
    [super configureController];

    self.displayManager.collectionView = self.contentView.collectionView;
    self.displayManager.delegate = self;
    self.contentView.collectionView.emptyDataSetSource = self;
    self.contentView.collectionView.emptyDataSetDelegate = self;
    [self.workingHeader setHeightConstraint:self.workingHeaderHeight];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return NO;
}

#pragma mark - SAProductsViewInput

- (void)updateWithProducts:(NSArray<SAProductPlainObject *> *)products {
    self.error = nil;
    [self.displayManager configureDataDisplayManagerWithProducts:products];
    [self.contentView.collectionView reloadData];
}

- (void)setupTitle:(NSString *)title {
    [self setTitle:title];
}

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)setErrorState:(NSError *)error {
    self.error = error;
    [self.displayManager configureDataDisplayManagerWithProducts:nil];
    [self.contentView.collectionView reloadData];
}

#pragma mark - SAProductsDataDisplayManagerDelegate

- (void)displayManager:(SAProductsDataDisplayManager *)displayManager didTapProductCellAtIndexPath:(NSIndexPath *)indexPath {
    [self.output didTapProductCellAtIndexPath:indexPath];
}

- (void)displayManager:(SAProductsDataDisplayManager *)displayManager didPlusProduct:(SAProductPlainObject *)product {
    [self.output didTapPlusProduct:product];
}

- (void)displayManager:(SAProductsDataDisplayManager *)displayManager didMinusProduct:(SAProductPlainObject *)product {
    [self.output didTapMinusProduct:product];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = self.error ? self.error.localizedDescription : @"Нет продуктов";
    
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    return self.error ? [UIImage imageNamed:@"btn_auth_back"] : nil;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                 NSForegroundColorAttributeName: [SAStyle mediumGreenColor]};
    return self.error ? [[NSAttributedString alloc] initWithString:@"Повторить"
                                                        attributes:attributes] : nil;
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -10.f;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self.output didTapUpdateButton];
}

@end

//
//  SAProductCollectionCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductCollectionCell.h"
#import "SAAddToCartView.h"
#import "SAProductPlainObject.h"
#import "UIImageView+AsyncLoad.h"
#import "NSString+Currency.h"

@interface SAProductCollectionCell () <SAAddToCartViewDelegate>
@property(weak, nonatomic) IBOutlet UIImageView *productImageView;
@property(weak, nonatomic) IBOutlet UILabel *productTitle;
@property(weak, nonatomic) IBOutlet UILabel *productPrice;
@property(weak, nonatomic) IBOutlet SAAddToCartView *addToCartView;
@end

@implementation SAProductCollectionCell {
    SAProductPlainObject *productPlainObject;
}

#pragma mark - Configure

- (void)configureCell {
    self.backgroundColor = [SAStyle whiteColor];
    self.layer.borderColor = [SAStyle grayColor].CGColor;
    self.layer.borderWidth = 0.5f;
    self.layer.cornerRadius = 5.f;
    self.clipsToBounds = YES;
    [self configureSubviews];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProductPlainObject class]]) {
        [self configureWithProduct:model];
    }
}

#pragma mark - Private

- (void)configureSubviews {
    self.productTitle.textColor = [UIColor grayColor];
    self.productTitle.font = [SAStyle defaultFontOfSize:14.f];
    self.productPrice.textColor = [UIColor grayColor];
    self.productPrice.font = [SAStyle boldFontOfSize:15.f];
    self.addToCartView.delegate = self;
}

- (void)configureWithProduct:(SAProductPlainObject *)product {
    productPlainObject = product;
    self.productTitle.text = product.title;
    self.productPrice.text = [NSString priceWithCurrencySymbol:product.price];
    [self.productImageView loadAsyncFromUrl:product.imageUrl placeHolder:@"noproduct"];
    [self.addToCartView updateWithQuantity:product.quantity];
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

#pragma mark - SAAddToCartViewDelegate

- (void)addToCartViewDidTapPlus:(SAAddToCartView *)view {
    [self.delegate productCell:self didPlusProduct:productPlainObject];
}

- (void)addToCartViewDidTapMinus:(SAAddToCartView *)view {
    [self.delegate productCell:self didMinusProduct:productPlainObject];
}


@end

//
//  SAProductCollectionCell.h
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseCollectionViewCell.h"

@class SAProductPlainObject;
@protocol SAProductCollectionCellDelegate;

@interface SAProductCollectionCell : SABaseCollectionViewCell

@property(weak, nonatomic) id <SAProductCollectionCellDelegate> delegate;

@end

@protocol SAProductCollectionCellDelegate <NSObject>

- (void)productCell:(SAProductCollectionCell *)cell didPlusProduct:(SAProductPlainObject *)product;

- (void)productCell:(SAProductCollectionCell *)cell didMinusProduct:(SAProductPlainObject *)product;

@end

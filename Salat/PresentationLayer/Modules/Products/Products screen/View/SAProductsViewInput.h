//
//  SAProductsViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductPlainObject;

@protocol SAProductsViewInput <NSObject>

- (void)updateWithProducts:(NSArray<SAProductPlainObject *> *)products;

- (void)setupTitle:(NSString *)title;

- (void)showIndicator:(BOOL)show;

- (void)setErrorState:(NSError *)error;

@end

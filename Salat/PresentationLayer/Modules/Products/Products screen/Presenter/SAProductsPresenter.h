//
//  SAProductsPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAProductsModuleInput.h"
#import "SAProductsViewOutput.h"
#import "SAProductsInteractorOutput.h"

@protocol SAProductsViewInput;
@protocol SAProductsInteractorInput;
@protocol SAProductsRouterInput;
@class SAProductsPresenterStateStorage;

@interface SAProductsPresenter : NSObject <SAProductsModuleInput, SAProductsViewOutput, SAProductsInteractorOutput>

@property(weak, nonatomic) id <SAProductsViewInput> view;
@property(nonatomic) id <SAProductsInteractorInput> interactor;
@property(nonatomic) id <SAProductsRouterInput> router;
@property(nonatomic) SAProductsPresenterStateStorage *presenterStateStorage;

@end

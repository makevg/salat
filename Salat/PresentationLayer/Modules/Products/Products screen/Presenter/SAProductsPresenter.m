//
//  SAProductsPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductsPresenter.h"
#import "SAProductsViewInput.h"
#import "SAProductsInteractorInput.h"
#import "SAProductsRouterInput.h"
#import "SAProductsPresenterStateStorage.h"

@implementation SAProductsPresenter

#pragma mark - Memory managment

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self addObservers];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProducts)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateProducts {
    [self.view showIndicator:YES];
    [self.interactor obtainProductsByCategoryId:self.presenterStateStorage.categoryId];
}

#pragma mark - SAProductsModuleInput

- (void)configureModuleWithCategoryId:(NSNumber *)categoryId {
    self.presenterStateStorage.categoryId = categoryId;
}

#pragma mark - SAProductsViewOutput

- (void)didTriggerViewWillAppearEvent {
    [self.view showIndicator:YES];
    [self.interactor obtainCategoryNameById:self.presenterStateStorage.categoryId];
    [self.interactor obtainProductsByCategoryId:self.presenterStateStorage.categoryId];
}

- (void)didTapProductCellAtIndexPath:(NSIndexPath *)indexPath {
    [self.router openProductsCarouselWithCategoryId:self.presenterStateStorage.categoryId
                                   productIndexPath:indexPath];
}

- (void)didTapPlusProduct:(SAProductPlainObject *)product {
    [self.interactor plusProduct:product];
}

- (void)didTapMinusProduct:(SAProductPlainObject *)product {
    [self.interactor minusProduct:product];
}

- (void)didTapUpdateButton {
    [self updateProducts];
}

#pragma mark - SAProductsInteractorOutput

- (void)didObtainProducts:(NSArray *)products {
    [self.view updateWithProducts:products];
    [self.view showIndicator:NO];
}

- (void)didObtainCategoryName:(NSString *)categoryName {
    [self.view setupTitle:categoryName];
}

- (void)didObtainError:(NSError *)error {
    [self.view setErrorState:error];
    [self.view showIndicator:NO];
}


@end

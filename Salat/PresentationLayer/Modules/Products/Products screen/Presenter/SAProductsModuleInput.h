//
//  SAProductsModuleInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 17.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViperMcFlurry.h"

@protocol SAProductsModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithCategoryId:(NSNumber *)categoryId;

@end

//
//  SAProductInteractorInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAProductInteractorInput <NSObject>

- (void)obtainProductWithId:(NSNumber *)productId;

- (void)addProductToCart:(NSNumber *)productId withType:(NSString *)productType;

@end

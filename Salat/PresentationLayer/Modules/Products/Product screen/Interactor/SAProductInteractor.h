//
//  SAProductInteractor.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductInteractorInput.h"

@protocol SAProductInteractorOutput;
@class SAProductService;
@class SACartService;
@protocol SAPrototypeMapper;

@interface SAProductInteractor : NSObject <SAProductInteractorInput>

@property(weak, nonatomic) id <SAProductInteractorOutput> output;
@property(nonatomic) SAProductService *productService;
@property(nonatomic) SACartService *cartService;
@property(nonatomic) id <SAPrototypeMapper> productMapper;
@property(nonatomic) id <SAPrototypeMapper> productModifierMapper;
@property(nonatomic) id <SAPrototypeMapper> itemModifierMapper;

@end

//
//  SAProductInteractor.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductInteractor.h"
#import "SAProductInteractorOutput.h"
#import "SAProductService.h"
#import "SACartService.h"
#import "SAPrototypeMapper.h"
#import "SAProductPlainObject.h"
#import "SAProductModifierPlainObject.h"
#import "ProductModifier.h"
#import "Product.h"

@implementation SAProductInteractor

#pragma mark - SAProductInteractorInput

- (void)obtainProductWithId:(NSNumber *)productId {
    [self.output didObtainProduct:[self productById:productId]];
}

- (void)addProductToCart:(NSNumber *)productId withType:(NSString *)productType {
    SAProductPlainObject *product = [self productById:productId];

    [self.cartService plusItemWithProductId:product.objectId
                                      title:product.title
                                      descr:product.descr
                                   imageUrl:product.imageUrl
                                       cost:product.price
                                productType:productType
                                  modifiers:@[]];
}

#pragma mark - Private

- (SAProductPlainObject *)productById:(NSNumber *)productId {
    Product *product = [self.productService obtainProductById:productId];
    SAProductPlainObject *productPlainObject = [SAProductPlainObject new];
    [self.productMapper fillObject:productPlainObject withObject:product];
    productPlainObject.modifiers = [self getPlainModifiersFromManagedObjects:[product.modifiers allObjects]];
    return productPlainObject;
}

- (NSArray *)getPlainModifiersFromManagedObjects:(NSArray *)managedObjectModifiers {
    NSMutableArray *modifiersPlainObjects = [NSMutableArray array];
    for (ProductModifier *modifier in managedObjectModifiers) {
        SAProductModifierPlainObject *productModifier = [SAProductModifierPlainObject new];
        [self.productModifierMapper fillObject:productModifier withObject:modifier];
        [modifiersPlainObjects addObject:productModifier];
    }
    return modifiersPlainObjects;
}

@end

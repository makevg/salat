//
//  SAProductInteractorOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProductPlainObject;

@protocol SAProductInteractorOutput <NSObject>

- (void)didObtainProduct:(SAProductPlainObject *)product;

@end

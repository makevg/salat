//
//  SAProductVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductVC.h"
#import "SAProductView.h"
#import "SAProductViewOutput.h"

static NSString *const cProductStoryboardName = @"Product";

@interface SAProductVC ()
@property(strong, nonatomic) IBOutlet SAProductView *contentView;
@end

@implementation SAProductVC

#pragma mark - Super

+ (NSString *)storyboardName {
    return cProductStoryboardName;
}

- (void)configureController {
    [super configureController];

    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Actions

- (IBAction)tappedAddToCartButton:(id)sender {
    [self.output didTapAddToCartButton];
}

- (IBAction)tappedCloseButton:(id)sender {
    [self.output didTapCloseButton];
}

#pragma mark  - SAProductViewInput

- (void)updateWithProduct:(SAProductPlainObject *)product {
    [self.contentView setModel:product];
}

@end

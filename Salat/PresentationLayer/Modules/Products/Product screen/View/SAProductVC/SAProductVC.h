//
//  SAProductVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAProductViewInput.h"

@protocol SAProductViewOutput;

@interface SAProductVC : SABaseVC <SAProductViewInput>

@property(nonatomic) id <SAProductViewOutput> output;

@end

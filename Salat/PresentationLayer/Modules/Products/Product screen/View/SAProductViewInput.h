//
//  SAProductViewInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SAProductPlainObject;

@protocol SAProductViewInput <NSObject>

- (void)updateWithProduct:(SAProductPlainObject *)product;

@end

//
//  SAProductViewOutput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SAProductViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;

- (void)didTapAddToCartButton;

- (void)didTapCloseButton;

@end

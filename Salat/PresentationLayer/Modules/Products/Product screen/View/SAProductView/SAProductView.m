//
//  SAProductView.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductView.h"
#import "SABaseButton.h"
#import "UIImageView+AsyncLoad.h"
#import "SAProductPlainObject.h"
#import "NSString+Currency.h"

@interface SAProductView ()
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIView *productView;
@property(weak, nonatomic) IBOutlet UIView *productInnerView;
@property(weak, nonatomic) IBOutlet UIImageView *productImageView;
@property(weak, nonatomic) IBOutlet UILabel *productTitle;
@property(weak, nonatomic) IBOutlet UILabel *productDescrLabel;
@property(weak, nonatomic) IBOutlet UILabel *priceLabel;
@property(weak, nonatomic) IBOutlet SABaseButton *closeButton;
@property(weak, nonatomic) IBOutlet SABaseButton *addToCartButton;
@end

@implementation SAProductView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [SAStyle backgroundColor];
    self.titleLabel.font = [SAStyle regularFontOfSize:14.f];
    [self.closeButton setTitleColor:[SAStyle whiteColor]
                           forState:UIControlStateNormal];

    self.productInnerView.layer.cornerRadius = 10.f;
    self.productInnerView.layer.masksToBounds = YES;

    self.productView.backgroundColor = [SAStyle clearColor];
    self.productView.layer.shadowOffset = CGSizeMake(0, 0);
    self.productView.layer.shadowRadius = 5;
    self.productView.layer.shadowOpacity = 0.5;

    self.productTitle.font = [SAStyle regularFontOfSize:15.f];
    self.productDescrLabel.font = [SAStyle regularFontOfSize:14.f];
    self.productDescrLabel.textColor = [SAStyle grayColor];
    self.priceLabel.font = [SAStyle regularFontOfSize:16.f];
    self.priceLabel.textColor = [SAStyle grayColor];
    [self.addToCartButton setTitleColor:[SAStyle lightGreenColor]
                               forState:UIControlStateNormal];

    [self.productImageView loadAsyncFromUrl:@""];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProductPlainObject class]]) {
        [self configureWithProduct:model];
    }
}

#pragma mark - Private

- (void)configureWithProduct:(SAProductPlainObject *)product {
    self.productTitle.text = product.title;
    self.productDescrLabel.text = product.descr;
    self.priceLabel.text = [NSString priceWithCurrencySymbol:product.price];
    [self.productImageView loadAsyncFromUrl:product.imageUrl];
}

@end

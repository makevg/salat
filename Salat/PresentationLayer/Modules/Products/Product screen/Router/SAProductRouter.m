//
//  SAProductRouter.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductRouter.h"

@implementation SAProductRouter

#pragma mark - SAProductRouterInput

- (void)closeModule:(void (^)())completion {
    [(UIViewController *) self.transitionHandler dismissViewControllerAnimated:YES completion:completion];
}

@end

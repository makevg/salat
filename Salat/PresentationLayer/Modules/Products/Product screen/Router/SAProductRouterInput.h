//
//  SAProductRouterInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAProductRouterInput <NSObject>

- (void)closeModule:(void (^)())completion;

@end

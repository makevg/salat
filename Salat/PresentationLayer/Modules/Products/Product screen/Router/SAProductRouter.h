//
//  SAProductRouter.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAProductRouter : NSObject <SAProductRouterInput>

@property(weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

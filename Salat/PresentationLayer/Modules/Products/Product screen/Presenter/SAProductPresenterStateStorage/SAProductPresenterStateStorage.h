//
// Created by Maximychev Evgeny on 11.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAProductPresenterStateStorage : NSObject

@property(nonatomic) NSNumber *productId;
@property(copy, nonatomic) NSString * productType;

@end
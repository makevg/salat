//
//  SAProductPresenter.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductPresenter.h"
#import "SAProductViewInput.h"
#import "SAProductInteractorInput.h"
#import "SAProductRouterInput.h"
#import "SAProductPresenterStateStorage.h"
#import "SAProductModuleOutput.h"

@interface SAProductPresenter ()
@property(weak, nonatomic) id <SAProductModuleOutput> delegate;
@end

@implementation SAProductPresenter

#pragma mark - SAProductModuleInput

- (void)configureWithProductId:(NSNumber *)productId
                   productType:(NSString *)productType
                      delegate:(__weak id <SAProductModuleOutput>)delegate {
    self.presenterStateStorage.productId = productId;
    self.presenterStateStorage.productType = productType;
    self.delegate = delegate;
}

#pragma mark - SAProductViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.interactor obtainProductWithId:self.presenterStateStorage.productId];
}

- (void)didTapAddToCartButton {
    [self.interactor addProductToCart:self.presenterStateStorage.productId
                             withType:self.presenterStateStorage.productType];

    [self.delegate productSelected];
    [self didTapCloseButton];
}

- (void)didTapCloseButton {
    __weak __typeof(self) weakSelf = self;
    [self.router closeModule:^{
        [weakSelf.delegate productModuleIsClosed];
    }];
}

#pragma mark - SAProductInteractorOutput

- (void)didObtainProduct:(SAProductPlainObject *)product {
    [self.view updateWithProduct:product];
}

@end

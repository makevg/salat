//
//  SAProductModuleInput.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAProductModuleOutput;

@protocol SAProductModuleInput <NSObject>

- (void)configureWithProductId:(NSNumber *)productId
                   productType:(NSString *)productType
                      delegate:(__weak id <SAProductModuleOutput>)delegate;

@end

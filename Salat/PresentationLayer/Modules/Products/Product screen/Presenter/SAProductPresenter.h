//
//  SAProductPresenter.h
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAProductInteractorOutput.h"
#import "SAProductViewOutput.h"
#import "SAProductModuleInput.h"

@protocol SAProductViewInput;
@protocol SAProductInteractorInput;
@protocol SAProductRouterInput;
@class SAProductPresenterStateStorage;

@interface SAProductPresenter : NSObject <SAProductModuleInput, SAProductViewOutput, SAProductInteractorOutput>

@property(weak, nonatomic) id <SAProductViewInput> view;
@property(nonatomic) id <SAProductInteractorInput> interactor;
@property(nonatomic) id <SAProductRouterInput> router;
@property(nonatomic) SAProductPresenterStateStorage *presenterStateStorage;

@end

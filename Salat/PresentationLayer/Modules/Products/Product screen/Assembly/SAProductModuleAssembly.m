//
//  SAProductModuleAssembly.m
//  Salat
//
//  Created by Maximychev Evgeny on 11.07.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProductModuleAssembly.h"
#import "SAProductVC.h"
#import "SAProductPresenter.h"
#import "SAProductInteractor.h"
#import "SAProductRouter.h"
#import "SAProductPresenterStateStorage.h"

@implementation SAProductModuleAssembly

- (SAProductVC *)viewProduct {
    return [TyphoonDefinition withClass:[SAProductVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProduct]];
                          }];
}

- (SAProductPresenter *)presenterProduct {
    return [TyphoonDefinition withClass:[SAProductPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewProduct]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorProduct]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerProduct]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorageProduct]];
                          }];
}

- (SAProductInteractor *)interactorProduct {
    return [TyphoonDefinition withClass:[SAProductInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProduct]];
                              [definition injectProperty:@selector(productService)
                                                    with:[self.serviceComponents productService]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                              [definition injectProperty:@selector(productMapper)
                                                    with:[self.coreComponents productMapper]];
                              [definition injectProperty:@selector(productModifierMapper)
                                                    with:[self.coreComponents productModifierMapper]];
                              [definition injectProperty:@selector(itemModifierMapper)
                                                    with:[self.coreComponents cartItemModifierMapper]];
                          }];
}

- (SAProductRouter *)routerProduct {
    return [TyphoonDefinition withClass:[SAProductRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewProduct]];
                          }];
}

- (SAProductPresenterStateStorage *)presenterStateStorageProduct {
    return [TyphoonDefinition withClass:[SAProductPresenterStateStorage class]];
}

@end

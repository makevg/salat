//
//  SAHitProductCell.h
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@protocol SAHitProductCellDelegate;

@interface SAHitProductCell : SABaseView

@property(weak, nonatomic) id <SAHitProductCellDelegate> delegate;

@property(copy, nonatomic) NSString *productType;

@end


@protocol SAHitProductCellDelegate <NSObject>

- (void)hitProductCell:(SAHitProductCell *)cell tappedAtProduct:(NSNumber *)productId;

@end
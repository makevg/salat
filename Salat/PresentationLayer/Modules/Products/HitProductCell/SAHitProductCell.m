//
//  SAHitProductCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAHitProductCell.h"
#import "SAProductPlainObject.h"
#import "NSString+Currency.h"
#import "UIImageView+AsyncLoad.h"

@interface SAHitProductCell ()
@property(strong, nonatomic) IBOutlet UIView *mainView;

@property(weak, nonatomic) IBOutlet UIImageView *productImageView;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *costLabel;

@end

@implementation SAHitProductCell {
    NSNumber *productId;
}

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

#pragma mark - Public

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProductPlainObject class]]) {
        [self configureWithProduct:model];
    }
}

#pragma mark - Private

- (void)commonInit {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.mainView];
    [self configureView];
}

- (void)configureView {
    [self prepareConstraints];
    [self prepareSubviews];
    [self prepareTapRecognizer];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view" : self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view" : self.mainView}]];
}

- (void)prepareSubviews {
    self.layer.cornerRadius = 5.f;
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [SAStyle grayColor].CGColor;
    self.mainView.backgroundColor = [SAStyle whiteColor];
    self.titleLabel.font = [SAStyle regularFontOfSize:12.f];
    self.titleLabel.textColor = [SAStyle mediumGrayColor];
    self.costLabel.font = [SAStyle boldFontOfSize:12.f];
    self.costLabel.textColor = [SAStyle mediumGrayColor];
}

- (void)prepareTapRecognizer {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(performTapProductAction)];
    [self addGestureRecognizer:tapGestureRecognizer];
}

- (void)performTapProductAction {
    if (self.delegate && [self.delegate respondsToSelector:@selector(hitProductCell:tappedAtProduct:)]) {
        [self.delegate hitProductCell:self tappedAtProduct:productId];
    }
}

- (void)configureWithProduct:(SAProductPlainObject *)product {
    productId = product.objectId;
    self.titleLabel.text = product.title;
    self.costLabel.text = [NSString priceWithCurrencySymbol:product.price];

    [self.productImageView loadAsyncFromUrl:product.imageUrl
                                placeHolder:@"noproduct"];
}

@end

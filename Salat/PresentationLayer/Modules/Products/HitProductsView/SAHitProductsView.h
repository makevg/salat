//
//  SAHitProductsView.h
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@class SAProductPlainObject;

@interface SAHitProductsView : SABaseView

@property(nonatomic, weak) UIViewController *transitionHandler;

- (void)hideHits;

- (void)setFirstHit:(SAProductPlainObject *)product;

- (void)setSecondHit:(SAProductPlainObject *)product;

- (void)setMyFavorite:(SAProductPlainObject *)product;

@end

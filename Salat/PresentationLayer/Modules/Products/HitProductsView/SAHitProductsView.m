//
//  SAHitProductsView.m
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAHitProductsView.h"
#import "SAHitProductCell.h"
#import "SAProductModuleInput.h"
#import "SAProductPlainObject.h"
#import "SACartService.h"

@interface SAHitProductsView () <SAHitProductCellDelegate>
@property(strong, nonatomic) IBOutlet UIView *mainView;
@property(weak, nonatomic) IBOutlet SAHitProductCell *firstProductView;
@property(weak, nonatomic) IBOutlet SAHitProductCell *secondProductView;
@property(weak, nonatomic) IBOutlet UILabel *firstLabel;
@property(weak, nonatomic) IBOutlet UILabel *secondLabel;
@end

@implementation SAHitProductsView

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

#pragma mark - Private

- (void)commonInit {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.mainView];
    [self configureView];
}

- (void)configureView {
    [self prepareConstraints];
    [self prepareSubviews];
    
    [self hideHits];
}

- (void)prepareConstraints {
    [self.mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view" : self.mainView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view" : self.mainView}]];
}

- (void)prepareSubviews {
    self.backgroundColor = [SAStyle clearColor];
    self.mainView.backgroundColor = [SAStyle clearColor];
    self.firstLabel.font = [SAStyle regularFontOfSize:11.f];
    self.secondLabel.font = [SAStyle regularFontOfSize:11.f];
    self.secondLabel.textColor = [SAStyle mediumGreenColor];
    self.firstProductView.delegate = self;
    self.secondProductView.delegate = self;
}

#pragma mark - SAHitProductCellDelegate

- (void)hitProductCell:(SAHitProductCell *)cell tappedAtProduct:(NSNumber *)productId {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Product"
                                                         bundle:nil];
    UIViewController *vc = [storyboard instantiateInitialViewController];
    id <SAProductModuleInput> input = [vc valueForKey:@"output"];
    [input configureWithProductId:productId
                      productType:cell.productType
                         delegate:nil];

    [self.transitionHandler presentViewController:vc
                                         animated:YES
                                       completion:^{

                                       }];
}

- (void)hideHits {
    self.firstProductView.hidden = YES;
    self.secondProductView.hidden = YES;
    self.secondLabel.hidden = YES;
    self.firstLabel.hidden = YES;
}

- (void)setFirstHit:(SAProductPlainObject *)product {
    [self.firstProductView setModel:product];
    self.firstProductView.productType = SACartServiceProductTypeHit;
    
    self.firstProductView.hidden = NO;
    self.firstLabel.hidden = NO;
}

- (void)setSecondHit:(SAProductPlainObject *)product {
    self.secondLabel.text = @"Хит дня в доставке";
    self.secondLabel.textColor = [UIColor blackColor];
    [self.secondProductView setModel:product];
    self.secondProductView.productType = SACartServiceProductTypeHit;
    
    self.secondProductView.hidden = NO;
    self.secondLabel.hidden = NO;
}

- (void)setMyFavorite:(SAProductPlainObject *)product {
    self.secondLabel.text = @"Ваше любимое блюдо";
    self.secondLabel.textColor = [SAStyle mediumGreenColor];
    [self.secondProductView setModel:product];
    self.secondProductView.productType = SACartServiceProductTypeRecommendation;
    
    self.secondProductView.hidden = NO;
    self.secondLabel.hidden = NO;
}


@end

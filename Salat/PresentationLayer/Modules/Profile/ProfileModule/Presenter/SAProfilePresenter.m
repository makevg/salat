//
//  SAProfilePresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfilePresenter.h"
#import "SAProfileViewInput.h"
#import "SAProfileInteractorInput.h"
#import "SAProfileRouterInput.h"
#import "SAAddressPlainObject.h"

@implementation SAProfilePresenter

#pragma mark - Memory managment

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self addObservers];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProfileData)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateProfileData {
    [self.view showIndicator:YES];
    [self.interactor obtainProfile];
    [self.interactor obtainAddresses];
}

#pragma mark - SAProfileViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self updateProfileData];
}

- (void)didTapEditButton {
    [self.router openProfileEditModuleWith:self];
}

- (void)didTapAddAddressButton {
    [self.router openAddressModuleWithDelegate:self];
}

- (void)didTapAddress:(SAAddressPlainObject *)address {
    [self.router openAddressModuleWithAddressId:address.addressId andWithDelegate:self];
}

- (void)didTapLogoutButton {
    [self.interactor makeLogout];
    [self.router closeCurrentModule];
}

- (void)didTapDeleteAddress:(NSNumber *)addressId {
    [self.interactor deleteAddress:addressId];
}

- (void)didTapSetMainAddress:(NSNumber *)addressId {
    [self.interactor setMainAddress:addressId];
}

- (void)networkDidChanged:(BOOL)state {
    if (state) {
        [self updateProfileData];
    }
}

#pragma mark - SAProfileInteractorOutput

- (void)didObtainProfile:(SAProfilePlainObject *)profile {
    [self.view updateWithProfile:profile];
    [self.view showIndicator:NO];
}

- (void)didObtainAddresses:(NSArray<SAAddressPlainObject *> *)addresses {
    [self.view updateWithAddresses:addresses];
}

- (void)didObtainError:(NSError *)error {
    [self.view showIndicator:NO];
    
    if (error.code == 401) {
        [self.router openSignInModule];
        return;
    }
    
    [self.view setErrorState:error];
}

#pragma mark - SAEditProfileModuleOutput

- (void)changeProfile:(SAProfilePlainObject *)profile {
    [self.view updateWithProfile:profile];
    [self.view showIndicator:NO];
}

#pragma mark - SAAddressModuleOutput

- (void)didAddressSaved {
    [self.interactor obtainLocalAddresses];
}


@end

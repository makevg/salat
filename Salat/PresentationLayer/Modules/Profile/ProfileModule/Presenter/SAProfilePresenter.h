//
//  SAProfilePresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAProfileViewOutput.h"
#import "SAProfileInteractorOutput.h"
#import "SAEditProfileModuleOutput.h"
#import "SAAddressModuleOutput.h"

@protocol SAProfileViewInput;
@protocol SAProfileInteractorInput;
@protocol SAProfileRouterInput;

@interface SAProfilePresenter : NSObject <SAProfileViewOutput, SAProfileInteractorOutput, SAEditProfileModuleOutput, SAAddressModuleOutput>

@property (weak, nonatomic) id<SAProfileViewInput> view;
@property (nonatomic) id<SAProfileInteractorInput> interactor;
@property (nonatomic) id<SAProfileRouterInput> router;

@end

//
//  SAProfileViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAddressPlainObject;

@protocol SAProfileViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;

- (void)didTapEditButton;

- (void)didTapAddAddressButton;

- (void)didTapAddress:(SAAddressPlainObject *)address;

- (void)didTapLogoutButton;

- (void)didTapDeleteAddress:(NSNumber *)addressId;

- (void)didTapSetMainAddress:(NSNumber *)addressId;

- (void)networkDidChanged:(BOOL)state;

@end

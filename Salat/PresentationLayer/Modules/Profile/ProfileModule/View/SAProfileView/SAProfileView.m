//
//  SAProfileView.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileView.h"
#import "SABaseButton.h"
#import "SABorderButton.h"
#import "SAProfileHeaderModuleInput.h"

@interface SAProfileView ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *minOrderLabel;
@property (weak, nonatomic) IBOutlet SABaseButton *addAddressButton;
@property (weak, nonatomic) IBOutlet SABorderButton *logoutButton;
@end

@implementation SAProfileView

#pragma mark - Setup

- (void)setup {
    self.addressLabel.font = [SAStyle regularFontOfSize:15.f];
    self.minOrderLabel.font = [SAStyle regularFontOfSize:13.f];
    [self.addAddressButton setTitleColor:[SAStyle mediumGreenColor]
                                forState:UIControlStateNormal];
}

@end

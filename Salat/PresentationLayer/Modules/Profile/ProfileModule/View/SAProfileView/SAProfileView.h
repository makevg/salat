//
//  SAProfileView.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@protocol SAProfileHeaderModuleInput;

@interface SAProfileView : SABaseView

@property (weak, nonatomic) IBOutlet UIView<SAProfileHeaderModuleInput> *profileHeader;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

//
//  SAProfileViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAddressPlainObject;
@class SAProfilePlainObject;

@protocol SAProfileViewInput <NSObject>

- (void)updateWithProfile:(SAProfilePlainObject *)profile;

- (void)updateWithAddresses:(NSArray<SAAddressPlainObject *> *)addresses;

- (void)showIndicator:(BOOL)show;

- (void)setErrorState:(NSError *)error;

@end

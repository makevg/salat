//
//  SAProfileVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAProfileViewInput.h"

@protocol SAProfileViewOutput;
@class SAProfileDataDisplayManager;

@interface SAProfileVC : SABaseVC <SAProfileViewInput>

@property (nonatomic) id<SAProfileViewOutput> output;
@property (nonatomic) SAProfileDataDisplayManager *displayManager;

@end

//
//  SAProfileVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <MGSwipeTableCell/MGSwipeTableCell.h>
#import "SAProfileVC.h"
#import "SAProfileView.h"
#import "SAProfileDataDisplayManager.h"
#import "SAProfileViewOutput.h"
#import "SAProfileHeaderModuleInput.h"
#import "SAIndicatorViewHelper.h"
#import "UIScrollView+EmptyDataSet.h"

static NSString *const cProfileVCStoryboardName = @"Profile";

@interface SAProfileVC () <SAProfileDataDisplayManagerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (strong, nonatomic) IBOutlet SAProfileView *contentView;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;
@end

@implementation SAProfileVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }
    
    return _indicator;
}

#pragma mark - Super

+ (NSString *)storyboardName {
    return cProfileVCStoryboardName;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self prepareNavBar];
}

- (void)configureController {
    [super configureController];

    self.displayManager.tableView = self.contentView.tableView;
    self.displayManager.delegate = self;
    self.contentView.tableView.emptyDataSetSource = self;
    self.contentView.tableView.emptyDataSetDelegate = self;
    [self.output didTriggerViewDidLoadEvent];
}

- (void)networkStateDidChanged:(NSNotification *)notification {
    [super networkStateDidChanged:notification];
    
    [self.output networkDidChanged:[notification.object boolValue]];
}

#pragma mark - Private

- (void)prepareNavBar {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
}

#pragma mark - Actions

- (IBAction)tappedEditButton:(id)sender {
    [self.output didTapEditButton];
}

- (IBAction)tappedAddAddressButton:(id)sender {
    [self.output didTapAddAddressButton];
}

- (IBAction)tappedLogoutButton:(id)sender {
    [self.output didTapLogoutButton];
}

#pragma mark - SAProfileViewInput

- (void)updateWithProfile:(SAProfilePlainObject *)profile {
    [self.contentView.profileHeader configureModuleWithProfile:profile];
}

- (void)updateWithAddresses:(NSArray *)addresses {
    [self.displayManager configureWithAddresses:addresses];
    [self.contentView.tableView reloadData];
}

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)setErrorState:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

#pragma mark - SAProfileDataDisplayManagerDelegate

- (void)displayManager:(SAProfileDataDisplayManager *)displayManager didTapAddress:(SAAddressPlainObject *)address {
    [self.output didTapAddress:address];
}

- (void)didTapDeleteAddress:(NSNumber *)addressId {
    [self.output didTapDeleteAddress:addressId];
}

- (void)didTapSetMainAddress:(NSNumber *)addressId {
    [self.output didTapSetMainAddress:addressId];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Нет адресов";
    
    NSDictionary *attributes = @{NSFontAttributeName: [SAStyle regularFontOfSize:17.f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -10.f;
}

@end

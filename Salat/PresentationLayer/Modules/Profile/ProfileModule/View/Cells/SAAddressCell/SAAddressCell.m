//
//  SAAddressCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddressCell.h"
#import "SAAddressPlainObject.h"
#import "SAStyle.h"
#import "NSString+Currency.h"

@interface SAAddressCell ()
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *descrLabel;
@property(weak, nonatomic) IBOutlet UILabel *deliveryCostLabel;
@property(weak, nonatomic) IBOutlet UILabel *mainLabel;
@end

@implementation SAAddressCell

#pragma mark - Configure

- (void)configureCell {
    self.mainLabel.font = [SAStyle regularFontOfSize:10.f];
    self.titleLabel.textColor = [SAStyle mediumGreenColor];
    self.titleLabel.font = [SAStyle regularFontOfSize:13.f];
    self.titleLabel.textColor = [SAStyle mediumGreenColor];
    self.descrLabel.font = [SAStyle regularFontOfSize:13.f];
    self.descrLabel.textColor = [SAStyle mediumGrayColor];
    self.deliveryCostLabel.font = [SAStyle regularFontOfSize:10.f];
    self.deliveryCostLabel.textColor = [SAStyle mediumGrayColor];

    self.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Удалить" backgroundColor:[SAStyle redColor]],
            [MGSwipeButton buttonWithTitle:@"Как основной" backgroundColor:[SAStyle mediumGreenColor]]];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAAddressPlainObject class]]) {
        [self configureWithAddress:model];
    }
}

#pragma mark - Private

- (void)configureWithAddress:(SAAddressPlainObject *)address {
    self.titleLabel.text = address.title;
    self.descrLabel.text = [NSString stringWithFormat:@"%@, д.%@%@",
                                                      address.street,
                                                      address.house,
                                                      [address.flat length] ?
                                                              [NSString stringWithFormat:@", кв. %@", address.flat] :
                                                              @""];
    self.mainLabel.hidden = ![address.isMain boolValue];
    NSString *part = [address.isMain boolValue] ? @", " : @"";
    if ([address.deliverySum isEqualToNumber:@-1]) {
        self.deliveryCostLabel.text = [NSString stringWithFormat:@"%@%@", part, @"Не доставляем :("];
        self.deliveryCostLabel.textColor = [SAStyle redColor];
    } else {
        self.deliveryCostLabel.text = [NSString stringWithFormat:@"%@%@", part, [NSString priceWithCurrencySymbol:address.deliverySum]];
        self.deliveryCostLabel.textColor = [SAStyle mediumGrayColor];
    }
}

@end

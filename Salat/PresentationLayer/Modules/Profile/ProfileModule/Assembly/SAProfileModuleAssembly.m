//
//  SAProfileModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileModuleAssembly.h"
#import "SAProfileVC.h"
#import "SAProfilePresenter.h"
#import "SAProfileInteractor.h"
#import "SAProfileRouter.h"
#import "SAProfileDataDisplayManager.h"

@implementation SAProfileModuleAssembly

- (SAProfileVC *)viewProfile {
    return [TyphoonDefinition withClass:[SAProfileVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProfile]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerProfile]];
    }];
}

- (SAProfilePresenter *)presenterProfile {
    return [TyphoonDefinition withClass:[SAProfilePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewProfile]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorProfile]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerProfile]];
                          }];
}

- (SAProfileInteractor *)interactorProfile {
    return [TyphoonDefinition withClass:[SAProfileInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterProfile]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
                              [definition injectProperty:@selector(addressService)
                                                    with:[self.serviceComponents addressService]];
                              [definition injectProperty:@selector(profileMapper)
                                                    with:[self.coreComponents profileMapper]];
                              [definition injectProperty:@selector(addressMapper)
                                                    with:[self.coreComponents addressMapper]];
    }];
}

- (SAProfileRouter *)routerProfile {
    return [TyphoonDefinition withClass:[SAProfileRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewProfile]];
    }];
}

- (SAProfileDataDisplayManager *)displayManagerProfile {
    return [TyphoonDefinition withClass:[SAProfileDataDisplayManager class]];
}

@end

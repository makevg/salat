//
//  SAProfileDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileDataDisplayManager.h"
#import "SAAddressCell.h"
#import "SAAddressPlainObject.h"

@interface SAProfileDataDisplayManager () <MGSwipeTableCellDelegate>
@property(nonatomic) NSMutableArray<SAAddressPlainObject *> *addresses;
@end

@implementation SAProfileDataDisplayManager

#pragma mark - Lazy init

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - Public

#pragma mark - Public

- (void)configureWithAddresses:(NSArray<SAAddressPlainObject *> *)addresses {
    self.addresses = [addresses mutableCopy];
}

- (void)removeCellByIndex:(NSIndexPath *)indexPath {
    [self.addresses removeObjectAtIndex:(NSUInteger) indexPath.row];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.addresses count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [SAAddressCell cellIdentifier];
    SAAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                          forIndexPath:indexPath];
    [cell setModel:self.addresses[(NSUInteger) indexPath.row]];
    cell.delegate = self;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (self.delegate && [self.delegate respondsToSelector:@selector(displayManager:didTapAddress:)]) {
        [self.delegate displayManager:self didTapAddress:[self getObjectByIndexPath:indexPath]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.tableView.rowHeight;
}

#pragma mark - MGSwipeTableCellDelegate

- (BOOL)swipeTableCell:(MGSwipeTableCell *)cell
   tappedButtonAtIndex:(NSInteger)index
             direction:(MGSwipeDirection)direction
         fromExpansion:(BOOL)fromExpansion {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    switch (index) {
        case 0: {
            [self.delegate didTapDeleteAddress:[self getObjectByIndexPath:indexPath].addressId];
            [self removeCellByIndex:indexPath];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case 1: {
            [self.delegate didTapSetMainAddress:[self getObjectByIndexPath:indexPath].addressId];
            break;
        }
        default:
            break;
    }
    return YES;
}

- (SAAddressPlainObject *)getObjectByIndexPath:(NSIndexPath *)indexPath {
    return self.addresses[(NSUInteger) indexPath.row];
}

@end

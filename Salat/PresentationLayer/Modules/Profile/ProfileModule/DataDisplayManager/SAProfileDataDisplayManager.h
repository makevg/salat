//
//  SAProfileDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SAProfileDataDisplayManagerDelegate;
@class SAAddressPlainObject;

@interface SAProfileDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic) UITableView *tableView;
@property(weak, nonatomic) id <SAProfileDataDisplayManagerDelegate> delegate;

- (void)configureWithAddresses:(NSArray<SAAddressPlainObject *> *)addresses;

@end


@protocol SAProfileDataDisplayManagerDelegate <NSObject>

- (void)displayManager:(SAProfileDataDisplayManager *)displayManager didTapAddress:(SAAddressPlainObject *)address;

- (void)didTapDeleteAddress:(NSNumber *)addressId;

- (void)didTapSetMainAddress:(NSNumber *)addressId;

@end
//
//  SAProfileRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SAProfileRouter : NSObject <SAProfileRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

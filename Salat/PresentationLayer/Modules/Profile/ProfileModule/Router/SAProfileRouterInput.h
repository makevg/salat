//
//  SAProfileRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RamblerViperModuleOutput;

@protocol SAProfileRouterInput <NSObject>

- (void)openProfileEditModuleWith:(id <RamblerViperModuleOutput>)output;

- (void)openSignInModule;

- (void)openAddressModuleWithDelegate:(id <RamblerViperModuleOutput>)output;

- (void)openAddressModuleWithAddressId:(NSNumber *)addressId andWithDelegate:(id <RamblerViperModuleOutput>)output;

- (void)closeCurrentModule;

@end

//
//  SAProfileRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileRouter.h"
#import "SAAddressModuleInput.h"
#import "SASignInVC.h"

static NSString *const cOpenProfileEditInProfileSegue = @"openProfileEditInProfileSegue";
static NSString *const cOpenAddressInProfileSegue = @"openAddressInProfileSegue";

@implementation SAProfileRouter

#pragma mark - SAProfileRouterInput

- (void)openProfileEditModuleWith:(id <RamblerViperModuleOutput>)output {
    [[self.transitionHandler openModuleUsingSegue:cOpenProfileEditInProfileSegue]
            thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <RamblerViperModuleInput> moduleInput) {
                return output;
            }];
}

- (void)openSignInModule {
    UIViewController *vc = (UIViewController *) self.transitionHandler;
    NSMutableArray *controllerStack = [NSMutableArray arrayWithArray:vc.navigationController.viewControllers];

    if (![controllerStack count])
        return;

    UIStoryboard *sb = [UIStoryboard storyboardWithName:[SASignInVC storyboardName] bundle:nil];
    UIViewController *toVc = [sb instantiateInitialViewController];

    controllerStack[[controllerStack count] - 1] = toVc;
    [vc.navigationController setViewControllers:controllerStack animated:YES];
}

- (void)openAddressModuleWithDelegate:(id <RamblerViperModuleOutput>)output {
    [[self.transitionHandler openModuleUsingSegue:cOpenAddressInProfileSegue]
            thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <RamblerViperModuleInput> moduleInput) {
                return output;
            }];
}

- (void)openAddressModuleWithAddressId:(NSNumber *)addressId andWithDelegate:(id <RamblerViperModuleOutput>)output {
    [[self.transitionHandler openModuleUsingSegue:cOpenAddressInProfileSegue]
            thenChainUsingBlock:(RamblerViperModuleLinkBlock) ^id <RamblerViperModuleOutput>(id <SAAddressModuleInput> moduleInput) {
                [moduleInput configureModuleWithAddressId:addressId];
                return output;
            }];
}

- (void)closeCurrentModule {
    [((UIViewController *) self.transitionHandler).navigationController popViewControllerAnimated:YES];
}

@end

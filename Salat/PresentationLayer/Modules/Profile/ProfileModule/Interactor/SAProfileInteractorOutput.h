//
//  SAProfileInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAddressPlainObject;
@class SAProfilePlainObject;

@protocol SAProfileInteractorOutput <NSObject>

- (void)didObtainProfile:(SAProfilePlainObject *)profile;
- (void)didObtainAddresses:(NSArray<SAAddressPlainObject *> *)addresses;

- (void)didObtainError:(NSError *)error;

@end

//
//  SAProfileInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAProfileInteractorInput.h"

@protocol SAProfileInteractorOutput;
@protocol SAPrototypeMapper;
@class SAUserService;
@class SAAddressService;

@interface SAProfileInteractor : NSObject <SAProfileInteractorInput>

@property(weak, nonatomic) id <SAProfileInteractorOutput> output;
@property(nonatomic) SAUserService *userService;
@property(nonatomic) SAAddressService *addressService;
@property(nonatomic) id <SAPrototypeMapper> profileMapper;
@property(nonatomic) id <SAPrototypeMapper> addressMapper;

@end

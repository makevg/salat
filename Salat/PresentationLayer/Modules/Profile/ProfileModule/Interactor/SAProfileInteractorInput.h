//
//  SAProfileInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAProfileInteractorInput <NSObject>

- (void)obtainAddresses;

- (void)obtainLocalAddresses;

- (void)setMainAddress:(NSNumber *)addressId;

- (void)deleteAddress:(NSNumber *)addressId;

- (void)obtainProfile;

- (void)makeLogout;

@end

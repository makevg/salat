//
//  SAProfileInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileInteractor.h"
#import "SAProfileInteractorOutput.h"
#import "SAUserService.h"
#import "Profile.h"
#import "SAProfilePlainObject.h"
#import "SAPrototypeMapper.h"
#import "SAAddressPlainObject.h"
#import "SAAddressService.h"
#import "SACommon.h"

@implementation SAProfileInteractor

#pragma mark - SAProfileInteractorInput

//NSArray<SAAddressPlainObject *> *addresses =
//        [weakSelf getPlainAddressesFromManagedObjects:[profile.addresses allObjects]];
//
//addresses = [addresses sortedArrayUsingComparator:^NSComparisonResult(SAAddressPlainObject *obj1, SAAddressPlainObject *obj2) {
//    return [obj1.createdAt compare:obj2.createdAt];
//}];

- (void)obtainAddresses {
    __weak typeof(self) weakSelf = self;

    [self.addressService obtainAddressesSuccess:^(NSArray<Address *> *dbAddresses) {
                [weakSelf handleAddresses:dbAddresses];
            }
                                          error:^(NSError *error) {
                                              dispatch_safe_main_async(^{
                                                  [weakSelf.output didObtainError:error];
                                              });
                                          }];
}

- (void)obtainLocalAddresses {
    [self handleAddresses:[self.addressService obtainLocalAddresses]];
}


- (void)setMainAddress:(NSNumber *)addressId {
    __weak typeof(self) weakSelf = self;

    [self.addressService setMain:addressId
                         success:^{
                             [weakSelf handleAddresses:[weakSelf.addressService obtainLocalAddresses]];
                         }
                           error:^(NSError *error) {
                               dispatch_safe_main_async(^{
                                   [weakSelf.output didObtainError:error];
                               })
                           }];
}

- (void)deleteAddress:(NSNumber *)addressId {
    __weak typeof(self) weakSelf = self;

    [self.addressService destroyAddress:addressId
                                success:^{
                                    [weakSelf handleAddresses:[weakSelf.addressService obtainLocalAddresses]];
                                }
                                  error:^(NSError *error) {
                                      dispatch_safe_main_async(^{
                                          [weakSelf.output didObtainError:error];
                                      })
                                  }];
}

- (void)handleAddresses:(NSArray<Address *> *)dbAddresses {
    weakifySelf;
    NSArray<SAAddressPlainObject *> *plainAddresses = [self getPlainAddressesFromManagedObjects:dbAddresses];
    NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey:@"createdAt"
                                                             ascending:NO];
    NSSortDescriptor *mainSort = [[NSSortDescriptor alloc] initWithKey:@"isMain"
                                                             ascending:NO];
    NSSortDescriptor *nameSort = [[NSSortDescriptor alloc] initWithKey:@"title"
                                                             ascending:YES];

    plainAddresses = [plainAddresses sortedArrayUsingDescriptors:@[mainSort, dateSort, nameSort]];

    dispatch_safe_main_async(^{
        [weakSelf.output didObtainAddresses:plainAddresses];
    });
}


- (void)obtainProfile {
    __weak typeof(self) weakSelf = self;
    [self.userService obtainProfile:^(Profile *profile) {
                SAProfilePlainObject *profilePlainObject = [SAProfilePlainObject new];
                [weakSelf.profileMapper fillObject:profilePlainObject withObject:profile];
                dispatch_safe_main_async(^{
                    [weakSelf.output didObtainProfile:profilePlainObject];
                })
            }
                              error:^(NSError *error) {
                                  dispatch_safe_main_async(^{
                                      [weakSelf.output didObtainError:error];
                                  })
                              }];
}

- (void)makeLogout {
    [self.userService logout:nil];
}

#pragma mark - Private

- (NSArray<SAAddressPlainObject *> *)getPlainAddressesFromManagedObjects:(NSArray<Address *> *)managedObjectAddresses {
    NSMutableArray<SAAddressPlainObject *> *addressesPlainObjects = [NSMutableArray array];
    for (Address *managedObjectAddress in managedObjectAddresses) {
        SAAddressPlainObject *addressPlainObject = [SAAddressPlainObject new];
        [self.addressMapper fillObject:addressPlainObject withObject:managedObjectAddress];
        [addressesPlainObjects addObject:addressPlainObject];
    }
    return addressesPlainObjects;
}

@end

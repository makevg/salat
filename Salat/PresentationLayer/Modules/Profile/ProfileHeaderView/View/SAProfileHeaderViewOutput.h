//
//  SAProfileHeaderViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAProfileHeaderViewOutput <NSObject>

- (void)didTriggerViewLoadedEvent;

@end

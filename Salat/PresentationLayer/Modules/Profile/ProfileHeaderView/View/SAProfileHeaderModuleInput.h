//
//  SAProfileHeaderModuleInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 27.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProfilePlainObject;

@protocol SAProfileHeaderModuleInput <NSObject>

- (void)configureModuleWithProfile:(SAProfilePlainObject *)profile;

@end

//
//  SAProfileHeaderView.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileHeaderView.h"
#import "SAProfileHeaderViewOutput.h"
#import "SAProfilePlainObject.h"

@interface SAProfileHeaderView ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIView *photoView;

@property (weak, nonatomic) IBOutlet UIView *fbView;
@property (weak, nonatomic) IBOutlet UIView *vkView;
@end

@implementation SAProfileHeaderView

#pragma mark - Setup

- (void)setup {
    [super setup];
    
    [self prepareSubviews];
    [self.output didTriggerViewLoadedEvent];
}

#pragma mark - Public

+ (SAProfileHeaderView *)profileHeaderView {
    return (SAProfileHeaderView *)[SABaseView loadViewFromNib:NSStringFromClass([self class])];
}

#pragma mark - Private

- (void)prepareSubviews {
    self.backgroundColor = [SAStyle mediumGreenColor];
    self.nameLabel.font = [SAStyle regularFontOfSize:15.f];
    self.phoneLabel.font = [SAStyle regularFontOfSize:13.f];
    self.phoneLabel.textColor = [SAStyle lightGreenColor];
    self.photoView.layer.cornerRadius = CGRectGetWidth(self.photoView.frame)/2;
    self.photoView.clipsToBounds = YES;
}

#pragma mark - SAProfileHeaderModuleInput

- (void)configureModuleWithProfile:(SAProfilePlainObject *)profile {
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", profile.firstName, profile.lastName];
    self.phoneLabel.text = profile.phoneNumber;
    self.fbView.alpha = profile.fbName ? 1.0f : 0.5f;
    self.vkView.alpha = profile.vkName ? 1.0f : 0.5f;
}

@end

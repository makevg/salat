//
//  SAProfileHeaderView.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"
#import "SAProfileHeaderViewInput.h"
#import "SAProfileHeaderModuleInput.h"

@protocol SAProfileHeaderViewOutput;

@interface SAProfileHeaderView : SABaseView <SAProfileHeaderModuleInput, SAProfileHeaderViewInput>

@property (nonatomic) id<SAProfileHeaderViewOutput> output;

+ (SAProfileHeaderView *)profileHeaderView;

@end

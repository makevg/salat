//
//  SAProfileHeaderModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAProfileHeaderModuleAssembly.h"
#import "SAProfileHeaderView.h"

@implementation SAProfileHeaderModuleAssembly

- (SAProfileHeaderView *)viewProfileHeader {
    return [TyphoonDefinition withClass:[SAProfileHeaderView class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(profileHeaderView)];
                          }];
}

@end

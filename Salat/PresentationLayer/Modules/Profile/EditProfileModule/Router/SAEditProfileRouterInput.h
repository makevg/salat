//
//  SAEditProfileRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SASecondStepPhoneConfirmationModuleOutput;

@protocol SAEditProfileRouterInput <NSObject>

- (void)closeCurrentModule;

- (void)openValidateCodeModule:(NSString *)phone
               andModuleOutput:(id <SASecondStepPhoneConfirmationModuleOutput>)moduleOutput;

@end

//
//  SAEditProfileRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAEditProfileRouter.h"
#import "SASecondStepPhoneConfirmationModuleInput.h"
#import "SAAuthModuleInputStorage.h"
#import "SASecondStepPhoneConfirmationModuleOutput.h"

@implementation SAEditProfileRouter

- (void)closeCurrentModule {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openValidateCodeModule:(NSString *)phone andModuleOutput:(id <SASecondStepPhoneConfirmationModuleOutput>)moduleOutput {
    [[self.transitionHandler openModuleUsingSegue:@"openValidateSegue"]
            thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <SASecondStepPhoneConfirmationModuleInput> moduleInput) {
                SAAuthModuleInputStorage *inputStorage = [SAAuthModuleInputStorage new];
                inputStorage.phone = phone;
                [moduleInput configureModuleWithAuthStorage:inputStorage];

                return moduleOutput;
            }];
}


@end

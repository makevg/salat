//
//  SAEditProfileRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAEditProfileRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface SAEditProfileRouter : NSObject <SAEditProfileRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

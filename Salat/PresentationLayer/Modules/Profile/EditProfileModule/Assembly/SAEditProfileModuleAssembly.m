//
//  SAEditProfileModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAEditProfileModuleAssembly.h"
#import "SAEditProfileVC.h"
#import "SAEditProfilePresenter.h"
#import "SAEditProfileInteractor.h"
#import "SAEditProfileRouter.h"

@implementation SAEditProfileModuleAssembly

- (SAEditProfileVC *)viewEditProfile {
    return [TyphoonDefinition withClass:[SAEditProfileVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterEditProfile]];
    }];
}

- (SAEditProfilePresenter *)presenterEditProfile {
    return [TyphoonDefinition withClass:[SAEditProfilePresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewEditProfile]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorEditProfile]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerEditProfile]];
                          }];
}

- (SAEditProfileInteractor *)interactorEditProfile {
    return [TyphoonDefinition withClass:[SAEditProfileInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterEditProfile]];
                              [definition injectProperty:@selector(userService)
                                                    with:[self.serviceComponents userService]];
                              [definition injectProperty:@selector(profileMapper)
                                                    with:[self.coreComponents profileMapper]];
    }];
}

- (SAEditProfileRouter *)routerEditProfile {
    return [TyphoonDefinition withClass:[SAEditProfileRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewEditProfile]];
    }];
}

@end

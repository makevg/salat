//
//  SAEditProfileInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAUserService.h"

@class SAProfilePlainObject;

@protocol SAEditProfileInteractorInput <NSObject>

- (SAProfilePlainObject *)obtainProfile;

- (void)obtainProfileFromNetwork;

- (void)saveProfile:(SAProfilePlainObject *)profilePlainObject;

- (void)validatePhone:(NSString *)phone;

- (void)bindProvider:(SocialRegisterType)provider token:(NSString *)token;

- (void)unbindProvider:(SocialRegisterType)provider;

@end

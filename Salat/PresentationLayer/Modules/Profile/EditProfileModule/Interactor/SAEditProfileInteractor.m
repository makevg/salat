//
//  SAEditProfileInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAEditProfileInteractor.h"
#import "SAEditProfileInteractorOutput.h"
#import "SAProfilePlainObject.h"
#import "SAPrototypeMapper.h"

@implementation SAEditProfileInteractor

#pragma mark - SAEditProfileInteractorInput

- (SAProfilePlainObject *)obtainProfile {
    Profile *profile = [self.userService obtainLocalProfile];
    SAProfilePlainObject *profilePlainObject = [SAProfilePlainObject new];
    [self.profileMapper fillObject:profilePlainObject withObject:profile];
    return profilePlainObject;
}

- (void)obtainProfileFromNetwork {
    __weak typeof(self) weakSelf = self;

    [self.userService obtainProfile:^(Profile *profile) {
                SAProfilePlainObject *profilePlainObject = [SAProfilePlainObject new];
                [weakSelf.profileMapper fillObject:profilePlainObject withObject:profile];

                dispatch_safe_main_async(^{
                    [weakSelf.output didObtainProfile:profilePlainObject];
                });
            }
                              error:^(NSError *error) {
                                  dispatch_safe_main_async(^{
                                      [weakSelf.output didObtainError:error];
                                  });
                              }];
}

- (void)saveProfile:(SAProfilePlainObject *)profilePlainObject {
    __weak typeof(self) weakSelf = self;

    [self.userService updateProfileByFirstName:profilePlainObject.firstName
                                    secondName:profilePlainObject.lastName
                                    completion:^{
                                        dispatch_safe_main_async(^{
                                            [weakSelf.output didProfileSaved];
                                        });
                                    }
                                         error:^(NSError *error) {
                                             dispatch_safe_main_async(^{
                                                 [weakSelf.output didObtainError:error];
                                             });
                                         }];
}

- (void)validatePhone:(NSString *)phone {
    __weak typeof(self) weakSelf = self;

    [self.userService validatePhoneFirstStepWithPhone:phone
                                           completion:^{
                                               dispatch_safe_main_async(^{
                                                   [weakSelf.output didSendCode];
                                               });
                                           }
                                                error:^(NSError *error) {
                                                    dispatch_safe_main_async(^{
                                                        [weakSelf.output didObtainError:error];
                                                    });
                                                }];
}

- (void)bindProvider:(SocialRegisterType)provider token:(NSString *)token {
    __weak typeof(self) weakSelf = self;

    [self.userService bindProvider:provider
                             token:token
                        completion:^{
                            dispatch_safe_main_async(^{
                                [weakSelf.output didBind];
                            });
                        }
                             error:^(NSError *error) {
                                 dispatch_safe_main_async(^{
                                     [weakSelf.output didObtainError:error];
                                 });
                             }];
}

- (void)unbindProvider:(SocialRegisterType)provider {
    __weak typeof(self) weakSelf = self;

    [self.userService unbindProvider:provider
                          completion:^{
                              dispatch_safe_main_async(^{
                                  [weakSelf.output didUnbind];
                              });
                          }
                               error:^(NSError *error) {
                                   dispatch_safe_main_async(^{
                                       [weakSelf.output didObtainError:error];
                                   });
                               }];
}


@end

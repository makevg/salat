//
//  SAEditProfileInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProfilePlainObject;

@protocol SAEditProfileInteractorOutput <NSObject>

@required

- (void)didObtainProfile:(SAProfilePlainObject *)profile;

- (void)didProfileSaved;

- (void)didUnbind;

- (void)didBind;

- (void)didSendCode;

- (void)didObtainError:(NSError *)error;

@end

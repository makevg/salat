//
//  SAEditProfileInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAEditProfileInteractorInput.h"

@protocol SAEditProfileInteractorOutput;
@class SAUserService;
@protocol SAPrototypeMapper;

@interface SAEditProfileInteractor : NSObject <SAEditProfileInteractorInput>

@property (weak, nonatomic) id<SAEditProfileInteractorOutput> output;
@property (nonatomic) SAUserService *userService;
@property (nonatomic) id<SAPrototypeMapper> profileMapper;

@end

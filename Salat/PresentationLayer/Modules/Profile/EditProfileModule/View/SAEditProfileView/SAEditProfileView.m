//
//  SAEditProfileView.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAEditProfileView.h"
#import "SAProfilePlainObject.h"
#import "SABorderedPhoneField.h"
#import "NBPhoneNumberUtil.h"

@interface SAEditProfileView ()

@property(weak, nonatomic) IBOutlet UILabel *accauntsLabel;

@property(nonatomic, strong) NSString *myPhone;
@property(nonatomic, strong) NBPhoneNumberUtil *phoneUtil;

@end

@implementation SAEditProfileView

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = [SAStyle backgroundColor];
    self.accauntsLabel.font = [SAStyle regularFontOfSize:15.f];
    self.fbProfile.font = [SAStyle regularFontOfSize:15.f];
    self.vkProfile.font = [SAStyle regularFontOfSize:15.f];

    [self.phoneField.formatter setDefaultOutputPattern:@"(###) ###-##-##"];
    [self.phoneField.formatter setPrefix:@"+7 "];

    __weak typeof(self) weakSelf = self;

    self.phoneField.textDidChangeBlock = ^(UITextField *textField) {
        NSError *anError = nil;
        NBPhoneNumber *number = [weakSelf.phoneUtil parse:weakSelf.phoneField.phoneNumber
                                            defaultRegion:@"RU"
                                                    error:&anError];

        if ([weakSelf.myPhone isEqualToString:weakSelf.phoneField.phoneNumber] || ![weakSelf.phoneUtil isValidNumber:number]) {
            weakSelf.buttonHeight.constant = 0.f;
        } else {
            weakSelf.buttonHeight.constant = 40.f;
        }
    };
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAProfilePlainObject class]]) {
        [self configureWithProfile:model];
    }
}

#pragma mark - Private

- (void)configureWithProfile:(SAProfilePlainObject *)profile {
    self.phoneUtil = [[NBPhoneNumberUtil alloc] init];
    self.buttonHeight.constant = 0;
    self.nameField.text = profile.firstName;
    self.surnameField.text = profile.lastName;
    [self setPhone:profile.phoneNumber];

    self.vkProfile.text = profile.vkName ?: @"Не подключен";
    self.vkProfileUnbind.hidden = profile.vkName == nil;

    self.fbProfile.text = profile.fbName ?: @"Не подключен";
    self.fbProfileUnbind.hidden = profile.fbName == nil;
}

- (void)hideValidateButton {
    self.buttonHeight.constant = 0.f;
}

- (void)setPhone:(NSString *)phone {
    self.phoneField.text = phone;
    self.myPhone = self.phoneField.phoneNumber;
}


@end

//
//  SAEditProfileView.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"
#import "SABorderedField.h"

@class SABorderedPhoneField;
@class SABorderButton;

@interface SAEditProfileView : SABaseView
@property(weak, nonatomic) IBOutlet SABorderedField *nameField;
@property(weak, nonatomic) IBOutlet SABorderedField *surnameField;
@property(weak, nonatomic) IBOutlet SABorderedPhoneField *phoneField;

@property(weak, nonatomic) IBOutlet UILabel *vkProfile;
@property(weak, nonatomic) IBOutlet UIButton *vkProfileButton;
@property(weak, nonatomic) IBOutlet UIImageView *vkProfileUnbind;

@property(weak, nonatomic) IBOutlet UILabel *fbProfile;
@property(weak, nonatomic) IBOutlet UIButton *fbProfileButton;
@property(weak, nonatomic) IBOutlet UIImageView *fbProfileUnbind;

@property(weak, nonatomic) IBOutlet SABorderButton *validateButton;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeight;

- (void) hideValidateButton;

- (void) setPhone:(NSString *)phone;

@end

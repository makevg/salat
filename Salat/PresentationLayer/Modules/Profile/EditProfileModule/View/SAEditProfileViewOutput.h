//
//  SAEditProfileViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SAEditProfileViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;

- (void)didTapDoneButton;

- (void)didTapValidatePhoneButton:(NSString *) phone;

- (void)didTapBindVKButton:(NSString *)token;

- (void)didTapBindFBButton:(NSString *)token;

- (void)didTapUnBindVK;

- (void)didTapUnBindFB;

@end

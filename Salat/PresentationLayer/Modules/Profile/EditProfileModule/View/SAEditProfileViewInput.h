//
//  SAEditProfileViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAProfilePlainObject;

@protocol SAEditProfileViewInput <NSObject>

- (void)updateWithProfile:(SAProfilePlainObject *)profile;

- (void)hideValidateButton;

- (void)setPhone:(NSString *)phone;

- (void)setErrorState:(NSError *)error;

- (void)showIndicator:(BOOL)show;

- (SAProfilePlainObject *)obtainProfileData;

@end

//
//  SAEditProfileVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAEditProfileVC.h"
#import "SAEditProfileView.h"
#import "SAEditProfileViewOutput.h"
#import "SASocialHelper.h"
#import "SAProfilePlainObject.h"
#import "SABorderedPhoneField.h"
#import "SAIndicatorViewHelper.h"

@interface SAEditProfileVC () <SASocialHelperDelegate>

@property (strong, nonatomic) IBOutlet SAEditProfileView *contentView;
@property (strong, nonatomic) SAProfilePlainObject *profile;
@property (strong, nonatomic) SASocialHelper *socialHelper;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;

@end

@implementation SAEditProfileVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }
    
    return _indicator;
}

#pragma mark - Super

- (void)configureController {
    [super configureController];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
            initWithTarget:self.view
                    action:@selector(endEditing:)];

    [self.view addGestureRecognizer:tap];

    self.socialHelper = [[SASocialHelper alloc] initWithDelegate:self];

    [self.output didTriggerViewDidLoadEvent];
}

-(void)dismissKeyboard {
    [self.contentView resignFirstResponder];
}

#pragma mark - Actions

- (IBAction)tappedDoneButton:(id)sender {
    [self.output didTapDoneButton];
}

- (IBAction)tappedVkButton:(id)sender {
    if (self.profile.vkName)
        [self.output didTapUnBindVK];
    else
        [self.socialHelper authVK];
}

- (IBAction)tappedValidatePhone:(id)sender {
    [self.output didTapValidatePhoneButton:self.contentView.phoneField.phoneNumber];
}

- (IBAction)tappedFbButton:(id)sender {
    if (self.profile.fbName)
        [self.output didTapUnBindFB];
    else
        [self.socialHelper authFB];
}

#pragma mark - SAEditProfileViewInput

- (void)updateWithProfile:(SAProfilePlainObject *)profile {
    self.profile = profile;
    [self.contentView setModel:profile];
}

- (void)hideValidateButton {
    [self.contentView hideValidateButton];
}

- (void)setPhone:(NSString *)phone {
    [self.contentView setPhone:phone];
}

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)setErrorState:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

- (SAProfilePlainObject *)obtainProfileData {
    SAProfilePlainObject *plainObject = [SAProfilePlainObject new];
    plainObject.firstName = self.contentView.nameField.text;
    plainObject.lastName = self.contentView.surnameField.text;

    return plainObject;
}

#pragma mark - SASocialHelperDelegate

//TODO: write

- (void)vkAuthResultError:(NSError *)error {
//                                            [indicatorView stopAnimating];
//                                        self.signInButton.enabled = YES;
    if (error)
        [self showErrorMessage:error.localizedDescription];
}

- (void)vkAuthResultSuccess:(NSString *)token {
    //                                        [indicatorView startAnimating];
    [self.output didTapBindVKButton:token];
}

- (void)fbAuthResultError:(NSError *)error {
//                                        [indicatorView stopAnimating];
//                                        self.signInButton.enabled = YES;
    if (error)
        [self showErrorMessage:error.localizedDescription];
}

- (void)fbAuthResultSuccess:(NSString *)token {
    //                                        [indicatorView startAnimating];
    [self.output didTapBindFBButton:token];
}

@end

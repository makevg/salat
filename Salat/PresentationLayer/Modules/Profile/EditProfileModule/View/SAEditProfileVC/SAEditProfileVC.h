//
//  SAEditProfileVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAEditProfileViewInput.h"

@protocol SAEditProfileViewOutput;

@interface SAEditProfileVC : SABaseVC <SAEditProfileViewInput>

@property (nonatomic) id<SAEditProfileViewOutput> output;

@end

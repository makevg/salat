//
//  SAEditProfilePresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAEditProfilePresenter.h"
#import "SAEditProfileViewInput.h"
#import "SAEditProfileInteractorInput.h"
#import "SAEditProfileRouterInput.h"
#import "SAEditProfileModuleOutput.h"

@interface SAEditProfilePresenter () <RamblerViperModuleInput>
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, weak) id <SAEditProfileModuleOutput> outputModule;
@end

@implementation SAEditProfilePresenter

- (void)setModuleOutput:(id <RamblerViperModuleOutput>)moduleOutput {
    self.outputModule = (id <SAEditProfileModuleOutput>) moduleOutput;
}

#pragma mark - SAEditProfileViewOutput

- (void)didTriggerViewDidLoadEvent {
    SAProfilePlainObject *profile = [self.interactor obtainProfile];
    [self.view updateWithProfile:profile];
}

- (void)didTapDoneButton {
    [self.view showIndicator:YES];
    [self.interactor saveProfile:[self.view obtainProfileData]];
}

- (void)didTapValidatePhoneButton:(NSString *)phone {
    [self.view showIndicator:YES];
    self.phone = phone;
    [self.interactor validatePhone:phone];
}

- (void)didTapBindVKButton:(NSString *)token {
    [self.view showIndicator:YES];
    [self.interactor bindProvider:eSRTVK
                            token:token];
}

- (void)didTapBindFBButton:(NSString *)token {
    [self.view showIndicator:YES];
    [self.interactor bindProvider:eSRTFB token:token];
}

- (void)didTapUnBindVK {
    [self.view showIndicator:YES];
    [self.interactor unbindProvider:eSRTVK];
}

- (void)didTapUnBindFB {
    [self.view showIndicator:YES];
    [self.interactor unbindProvider:eSRTFB];
}

#pragma mark - SAEditProfileInteractorOutput

- (void)didObtainProfile:(SAProfilePlainObject *)profile {
    [self.view showIndicator:NO];
    [self.view updateWithProfile:profile];

    [self.outputModule changeProfile:profile];
}

- (void)didProfileSaved {
    [self.view showIndicator:NO];
    SAProfilePlainObject *profilePlainObject = [self.interactor obtainProfile];
    [self.outputModule changeProfile:profilePlainObject];
    [self.router closeCurrentModule];
}

- (void)didUnbind {
    [self.interactor obtainProfileFromNetwork];
}

- (void)didBind {
    [self.interactor obtainProfileFromNetwork];
}

- (void)didSendCode {
    [self.view showIndicator:NO];
    [self.router openValidateCodeModule:self.phone
                        andModuleOutput:self];
}

- (void)didObtainError:(NSError *)error {
    [self.view showIndicator:NO];
    [self.view setErrorState:error];
}

#pragma mark - SASecondStepPhoneConfirmationModuleOutput

- (void)configure:(NSString *)phone {
    self.phone = phone;
    [self.view hideValidateButton];
    [self.view setPhone:phone];

    SAProfilePlainObject *profile = [self.interactor obtainProfile];
    [self.outputModule changeProfile:profile];
}

@end

//
//  SAEditProfilePresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 09.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAEditProfileViewOutput.h"
#import "SAEditProfileInteractorOutput.h"
#import "SASecondStepPhoneConfirmationModuleOutput.h"

@protocol SAEditProfileViewInput;
@protocol SAEditProfileInteractorInput;
@protocol SAEditProfileRouterInput;

@interface SAEditProfilePresenter : NSObject <SAEditProfileViewOutput, SAEditProfileInteractorOutput, SASecondStepPhoneConfirmationModuleOutput>

@property (weak, nonatomic) id<SAEditProfileViewInput> view;
@property (nonatomic) id<SAEditProfileInteractorInput> interactor;
@property (nonatomic) id<SAEditProfileRouterInput> router;

@end

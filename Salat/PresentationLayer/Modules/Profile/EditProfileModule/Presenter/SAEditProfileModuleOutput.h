//
// Created by mtx on 11.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@class SAProfilePlainObject;

@protocol SAEditProfileModuleOutput <RamblerViperModuleOutput>

- (void)changeProfile:(SAProfilePlainObject *)profile;

@end
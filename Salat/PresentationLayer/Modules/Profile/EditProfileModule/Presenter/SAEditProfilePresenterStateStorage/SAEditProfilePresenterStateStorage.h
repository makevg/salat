//
// Created by Максимычев Е.О. on 28.06.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAEditProfilePresenterStateStorage : NSObject

@property (nonatomic) NSNumber *profileId;

@end
//
//  SANewsListRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsListRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SANewsListRouter : NSObject <SANewsListRouterInput>

@property (weak, nonatomic) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

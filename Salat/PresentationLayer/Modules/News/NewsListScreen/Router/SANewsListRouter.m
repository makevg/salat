//
//  SANewsListRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsListRouter.h"
#import "SANewsModuleInput.h"

static NSString *const kNewsListModuleToNewsModuleSegue = @"kNewsListModuleToNewsModuleSegue";

@implementation SANewsListRouter

#pragma mark - SANewsListRouterInput

- (void)openMenuNewsModuleWithNewsId:(NSNumber *)newsId {
    [[self.transitionHandler openModuleUsingSegue:kNewsListModuleToNewsModuleSegue] thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<SANewsModuleInput> moduleInput) {
        [moduleInput configureModuleWithNewsId:newsId];
        return nil;
    }];
}

@end

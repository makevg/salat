//
//  SANewsListModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsListModuleAssembly.h"
#import "SANewsListVC.h"
#import "SANewsListPresenter.h"
#import "SANewsListInteractor.h"
#import "SANewsListRouter.h"

@implementation SANewsListModuleAssembly

- (SANewsListVC *)viewNewsList {
    return [TyphoonDefinition withClass:[SANewsListVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsList]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerNewsList]];
                              [definition injectProperty:@selector(cartButton)
                                                    with:[self.cartButtonComponents viewCartButton]];
    }];
}

- (SANewsListPresenter *)presenterNewsList {
    return [TyphoonDefinition withClass:[SANewsListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewNewsList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorNewsList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerNewsList]];
    }];
}

- (SANewsListInteractor *)interactorNewsList {
    return [TyphoonDefinition withClass:[SANewsListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNewsList]];
                              [definition injectProperty:@selector(newsService)
                                                    with:[self.serviceComponents newsService]];
                              [definition injectProperty:@selector(newsMapper)
                                                    with:[self.coreComponents newsMapper]];
    }];
}

- (SANewsListRouter *)routerNewsList {
    return [TyphoonDefinition withClass:[SANewsListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewNewsList]];
    }];
}

- (SANewsListDataDisplayManager *)displayManagerNewsList {
    return [TyphoonDefinition withClass:[SANewsListDataDisplayManager class]];
}

@end

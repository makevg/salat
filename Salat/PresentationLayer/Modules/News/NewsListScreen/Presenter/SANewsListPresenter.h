//
//  SANewsListPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SANewsListViewOutput.h"
#import "SANewsListInteractorOutput.h"

@protocol SANewsListViewInput;
@protocol SANewsListInteractorInput;
@protocol SANewsListRouterInput;

@interface SANewsListPresenter : NSObject <SANewsListViewOutput, SANewsListInteractorOutput>

@property (weak, nonatomic) id<SANewsListViewInput> view;
@property (nonatomic) id<SANewsListInteractorInput> interactor;
@property (nonatomic) id<SANewsListRouterInput> router;

@end

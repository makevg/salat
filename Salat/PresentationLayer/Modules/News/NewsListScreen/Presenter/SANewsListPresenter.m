//
//  SANewsListPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsListPresenter.h"
#import "SANewsListViewInput.h"
#import "SANewsListInteractorInput.h"
#import "SANewsListRouterInput.h"
#import "SANewsPlainObject.h"

@implementation SANewsListPresenter

#pragma mark - Memory managment

- (instancetype)init {
    self = [super init];
    
    if (self) {
        [self addObservers];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

#pragma mark - Private

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateNews)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateNews {
    [self.view showIndicator:YES];
    [self.interactor obtainNewsFromNetwork];
}

#pragma mark - SANewsListViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view showIndicator:YES];
    [self.interactor obtainNews];
}

- (void)didTapNewsCell:(SANewsPlainObject *)news {
    [self.router openMenuNewsModuleWithNewsId:news.newsId];
}

- (void)didTapUpdateButton {
    [self updateNews];
}

- (void)networkDidChanged:(BOOL)state {
    if (state) {
        [self updateNews];
    }
}

#pragma mark - SANewsListInteractorOutput

- (void)didObtainNews:(NSArray<SANewsPlainObject *> *)news {
    [self.view updateWithNews:news];
    [self.view showIndicator:NO];
}

- (void)didObtainError:(NSError *)error {
    [self.view setErrorState:error];
    [self.view showIndicator:NO];
}

@end

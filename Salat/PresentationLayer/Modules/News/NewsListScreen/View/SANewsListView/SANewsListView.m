//
//  SANewsListView.m
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsListView.h"

@implementation SANewsListView

#pragma mark - Setup

- (void)setup {
    self.tableView.tableFooterView = [UIView new];
}

@end

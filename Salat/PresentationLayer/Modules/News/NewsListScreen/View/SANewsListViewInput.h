//
//  SANewsListViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SANewsPlainObject;

@protocol SANewsListViewInput <NSObject>

- (void)updateWithNews:(NSArray<SANewsPlainObject *> *)news;

- (void)showIndicator:(BOOL)show;

- (void)setErrorState:(NSError *)error;

@end

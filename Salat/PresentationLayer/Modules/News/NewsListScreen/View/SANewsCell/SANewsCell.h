//
//  SANewsCell.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SABaseTableViewCell.h"

@interface SANewsCell : SABaseTableViewCell

@end

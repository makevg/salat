//
//  SANewsCell.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsCell.h"
#import "SADataFormatter.h"
#import "UIImageView+AsyncLoad.h"
#import "SANewsPlainObject.h"

@interface SANewsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *newsImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation SANewsCell

#pragma mark - Public

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SANewsPlainObject class]]) {
        [self configureCellByNews:model];
    }
}

- (void)configureCell {
    self.titleLabel.textColor = [SAStyle whiteColor];
    self.titleLabel.font = [SAStyle regularFontOfSize:16.f];
    self.dateLabel.textColor = [SAStyle whiteColor];
    self.dateLabel.font = [SAStyle regularFontOfSize:13.f];
}

#pragma mark - Private

- (void)configureCellByNews:(SANewsPlainObject *)news {
    self.titleLabel.text = news.title;
    self.dateLabel.text = [SADataFormatter stringByUnixTimeStamp:news.publishedAt];
    [self.newsImage loadAsyncFromUrl:news.imageUrl];
}

@end

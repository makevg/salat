//
//  SANewsListViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SANewsPlainObject;

@protocol SANewsListViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didTapNewsCell:(SANewsPlainObject *)news;
- (void)didTapUpdateButton;
- (void)networkDidChanged:(BOOL)state;

@end

//
//  SANewsListVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 06.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SANewsListViewInput.h"
#import "SANewsListDataDisplayManager.h"

@protocol SANewsListViewOutput;

@interface SANewsListVC : SABaseVC <SANewsListViewInput>

@property (nonatomic) id<SANewsListViewOutput> output;
@property (nonatomic) SANewsListDataDisplayManager *displayManager;

@end

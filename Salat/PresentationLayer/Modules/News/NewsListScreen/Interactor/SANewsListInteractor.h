//
//  SANewsListInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SANewsListInteractorInput.h"

@class SANewsService;
@class SANewsMapper;
@protocol SANewsListInteractorOutput;

@interface SANewsListInteractor : NSObject <SANewsListInteractorInput>

@property (weak, nonatomic) id<SANewsListInteractorOutput> output;
@property (nonatomic) SANewsService *newsService;
@property (nonatomic) SANewsMapper *newsMapper;

@end

//
//  SANewsListInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SANewsPlainObject;

@protocol SANewsListInteractorOutput <NSObject>

- (void)didObtainNews:(NSArray<SANewsPlainObject *> *)news;

- (void)didObtainError:(NSError *)error;

@end

//
//  SANewsListInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsListInteractor.h"
#import "SANewsListInteractorOutput.h"
#import "SANewsService.h"
#import "SANewsMapper.h"
#import "SANewsPlainObject.h"
#import "News.h"
#import "SAServiceLayer.h"

@implementation SANewsListInteractor

#pragma mark - SANewsListInteractorInput

- (void)obtainNews {
    __weak typeof(self) weakSelf = self;

    [self.newsService obtainNews:^(NSArray<News *> *data) {
                __strong typeof(weakSelf) strongSelf = weakSelf;

                NSArray<SANewsPlainObject *> *news = [strongSelf getPlainNewsFromManagedObjects:data];

                dispatch_safe_main_async(^{
                    [strongSelf.output didObtainNews:news];
                });
            }
                           error:^(NSError *error) {
                               dispatch_safe_main_async(^{
                                   [weakSelf.output didObtainError:error];
                               });
                           }];
}

- (void)obtainNewsFromNetwork {
    __weak typeof(self) weakSelf = self;
    
    [self.newsService obtainNewsFromNetwork:^(NSArray<News *> *data) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        NSArray<SANewsPlainObject *> *news = [strongSelf getPlainNewsFromManagedObjects:data];
        
        dispatch_safe_main_async(^{
            [strongSelf.output didObtainNews:news];
        });
    }
                                      error:^(NSError *error) {
                                          dispatch_safe_main_async(^{
                                              [weakSelf.output didObtainError:error];
                                          });
                                      }];
}

#pragma mark - Private

- (NSArray<SANewsPlainObject *> *)getPlainNewsFromManagedObjects:(NSArray<News *> *)managedObjectNews {
    NSMutableArray<SANewsPlainObject *> *newsPlainObjects = [NSMutableArray array];
    for (News *managedObjectSale in managedObjectNews) {
        SANewsPlainObject *newsPlainObject = [SANewsPlainObject new];
        [self.newsMapper fillObject:newsPlainObject withObject:managedObjectSale];
        [newsPlainObjects addObject:newsPlainObject];
    }
    return newsPlainObjects;
}

@end

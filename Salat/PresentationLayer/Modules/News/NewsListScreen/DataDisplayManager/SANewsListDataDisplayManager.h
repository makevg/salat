//
//  SANewsListDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SANewsPlainObject;
@protocol SANewsListDataDisplayManagerDelegate;

@interface SANewsListDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;
@property (weak, nonatomic) id<SANewsListDataDisplayManagerDelegate> delegate;

- (void)configureDisplayManagerWithNews:(NSArray<SANewsPlainObject *> *)news;

- (BOOL)hasData;

@end

@protocol SANewsListDataDisplayManagerDelegate <NSObject>

- (void)displayManager:(SANewsListDataDisplayManager *)displayManager didTapNewsCell:(SANewsPlainObject *)news;

@end

//
//  SANewsRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@class URBMediaFocusViewController;

@interface SANewsRouter : NSObject <SANewsRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;
@property (nonatomic) URBMediaFocusViewController *mediaFocusController;

@end

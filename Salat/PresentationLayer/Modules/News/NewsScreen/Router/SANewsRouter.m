//
//  SANewsRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsRouter.h"
#import "SANewsVC.h"
#import <URBMediaFocusViewController/URBMediaFocusViewController.h>

@implementation SANewsRouter

- (void)openMediaModuleWithImageUrl:(NSString *)imageUrl {
    SANewsVC *vc = (SANewsVC *)self.transitionHandler;
    [self.mediaFocusController showImageFromURL:[NSURL URLWithString:imageUrl] fromView:vc.view inViewController:vc];
}

@end

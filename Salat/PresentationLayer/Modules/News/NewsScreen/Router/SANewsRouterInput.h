//
//  SANewsRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SANewsRouterInput <NSObject>

- (void)openMediaModuleWithImageUrl:(NSString *)imageUrl;

@end

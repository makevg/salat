//
//  SANewsPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsPresenter.h"
#import "SANewsViewInput.h"
#import "SANewsInteractorInput.h"
#import "SANewsPresenterStateStorage.h"
#import "SANewsPlainObject.h"
#import "SANewsRouterInput.h"
#import <URBMediaFocusViewController/URBMediaFocusViewController.h>

@interface SANewsPresenter () <URBMediaFocusViewControllerDelegate>

@end

@implementation SANewsPresenter

#pragma mark - SANewsModuleInput

- (void)configureModuleWithNewsId:(NSNumber *)newsId {
    self.presenterStateStorage.newsId = newsId;
}

#pragma mark - SANewsViewOutput

- (void)didTriggerViewDidLoadEvent {
    [self.view showIndicator:YES];
    [self.interactor obtainNewsById:self.presenterStateStorage.newsId];
}

- (void)didTapNewsImageView {
    [self.view showIndicator:YES];
    [self.router openMediaModuleWithImageUrl:self.presenterStateStorage.newsImageUrl];
}

#pragma mark - SANewsInteractorOutput

- (void)didObtainNews:(SANewsPlainObject *)news {
    self.presenterStateStorage.newsImageUrl = news.imageUrl;
    [self.view updateWithNews:news];
    [self.view showIndicator:NO];
}

#pragma mark - URBMediaFocusViewControllerDelegate

- (void)mediaFocusViewController:(URBMediaFocusViewController *)mediaFocusViewController
           didFinishLoadingImage:(UIImage *)image {
    [self.view showIndicator:NO];
}

- (void)mediaFocusViewController:(URBMediaFocusViewController *)mediaFocusViewController
    didFailLoadingImageWithError:(NSError *)error {
    [self.view setErrorState:error];
    [self.view showIndicator:NO];
}

@end

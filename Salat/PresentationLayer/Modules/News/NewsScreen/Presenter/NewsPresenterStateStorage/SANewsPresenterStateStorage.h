//
//  SANewsPresenterStateStorage.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SANewsPresenterStateStorage : NSObject

@property (nonatomic) NSNumber *newsId;
@property (nonatomic) NSString *newsImageUrl;

@end

//
//  SANewsPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SANewsModuleInput.h"
#import "SANewsViewOutput.h"
#import "SANewsInteractorOutput.h"

@protocol SANewsViewInput;
@protocol SANewsInteractorInput;
@class SANewsPresenterStateStorage;
@protocol SANewsRouterInput;

@interface SANewsPresenter : NSObject <SANewsModuleInput, SANewsViewOutput, SANewsInteractorOutput>

@property (weak, nonatomic) id<SANewsViewInput> view;
@property (nonatomic) id<SANewsInteractorInput> interactor;
@property (nonatomic) id<SANewsRouterInput> router;
@property (nonatomic) SANewsPresenterStateStorage *presenterStateStorage;

@end

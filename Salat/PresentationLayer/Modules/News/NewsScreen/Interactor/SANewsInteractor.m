//
//  SANewsInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsInteractor.h"
#import "SANewsInteractorOutput.h"
#import "SANewsService.h"
#import "SANewsMapper.h"
#import "SANewsPlainObject.h"
#import "News.h"

@implementation SANewsInteractor

#pragma mark - SANewsInteractorInput

- (void)obtainNewsById:(NSNumber *)newsId {
    News *newsManagedObject = [self.newsService obtainNewsById:newsId];
    SANewsPlainObject *news = [SANewsPlainObject new];
    [self.newsMapper fillObject:news withObject:newsManagedObject];
    [self.output didObtainNews:news];
}

@end

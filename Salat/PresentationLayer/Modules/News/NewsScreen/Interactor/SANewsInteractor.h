//
//  SANewsInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SANewsInteractorInput.h"

@protocol SANewsInteractorOutput;
@class SANewsService;
@class SANewsMapper;

@interface SANewsInteractor : NSObject <SANewsInteractorInput>

@property (weak, nonatomic) id<SANewsInteractorOutput> output;
@property (nonatomic) SANewsService *newsService;
@property (nonatomic) SANewsMapper *newsMapper;

@end

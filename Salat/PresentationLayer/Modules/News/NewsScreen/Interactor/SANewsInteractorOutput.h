//
//  SANewsInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SANewsPlainObject;

@protocol SANewsInteractorOutput <NSObject>

- (void)didObtainNews:(SANewsPlainObject *)news;

@end

//
//  SANewsInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SANewsInteractorInput <NSObject>

- (void)obtainNewsById:(NSNumber *)newsId;

@end

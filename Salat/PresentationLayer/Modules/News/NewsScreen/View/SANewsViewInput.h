//
//  SANewsViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SANewsPlainObject;

@protocol SANewsViewInput <NSObject>

- (void)updateWithNews:(SANewsPlainObject *)news;

- (void)showIndicator:(BOOL)show;

- (void)setErrorState:(NSError *)error;

@end

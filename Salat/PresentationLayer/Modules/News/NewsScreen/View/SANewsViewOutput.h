//
//  SANewsViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SANewsViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;
- (void)didTapNewsImageView;

@end

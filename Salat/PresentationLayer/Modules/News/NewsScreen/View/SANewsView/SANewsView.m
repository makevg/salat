//
//  SANewsView.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsView.h"
#import "SANewsPlainObject.h"
#import "SADataFormatter.h"
#import "UIImageView+AsyncLoad.h"

@interface SANewsView () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstraint;
@end

@implementation SANewsView

#pragma mark - Setup

- (void)setup {
    self.titleLabel.font = [SAStyle regularFontOfSize:15.f];
    self.dateLabel.font = [SAStyle regularFontOfSize:13.f];
    self.webView.hidden = YES;
    self.webView.delegate = self;
    self.webView.scrollView.scrollEnabled = NO;
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SANewsPlainObject class]]) {
        [self configureByNews:model];
    }
}

#pragma mark - Private

- (void)configureByNews:(SANewsPlainObject *)news {
    self.titleLabel.text = news.title;
    self.dateLabel.text = [SADataFormatter stringByUnixTimeStamp:news.publishedAt];
    [self.newsImageView loadAsyncFromUrl:news.imageUrl];
    [self.webView loadHTMLString:news.content baseURL:nil];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webview {
    NSString *contentHeightStr = [webview stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    CGFloat padding = 20.f;
    CGFloat contentHeight = (CGFloat)[contentHeightStr floatValue] + padding;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, contentHeight + self.webView.frame.origin.y);
    self.webViewHeightConstraint.constant = contentHeight;
    self.webView.hidden = NO;
}

@end

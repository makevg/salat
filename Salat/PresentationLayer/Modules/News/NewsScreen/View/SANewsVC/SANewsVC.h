//
//  SANewsVC.h
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SANewsViewInput.h"

@protocol SANewsViewOutput;

@interface SANewsVC : SABaseVC <SANewsViewInput>

@property (nonatomic) id<SANewsViewOutput> output;

@end

//
//  SANewsVC.m
//  Salat
//
//  Created by Maximychev Evgeny on 07.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsVC.h"
#import "SANewsView.h"
#import "SANewsViewOutput.h"
#import "SAIndicatorViewHelper.h"

@interface SANewsVC ()
@property (strong, nonatomic) IBOutlet SANewsView *contentView;
@property (nonatomic) PCAngularActivityIndicatorView *indicator;
@end

@implementation SANewsVC

#pragma mark - Lazy init

- (PCAngularActivityIndicatorView *)indicator {
    if (!_indicator) {
        _indicator = [SAIndicatorViewHelper getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                                 forView:self.contentView];
        _indicator.color = [SAStyle mediumGreenColor];
    }
    
    return _indicator;
}

#pragma mark - Public

- (BOOL)showCartButton {
    return YES;
}

- (void)configureController {
    [super configureController];

    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Actions

- (IBAction)tappedNewsImageView:(id)sender {
    [self.output didTapNewsImageView];
}

#pragma mark - SANewsViewInput

- (void)updateWithNews:(SANewsPlainObject *)news {
    [self.contentView setModel:news];
}

- (void)showIndicator:(BOOL)show {
    show ? [self.indicator startAnimating] : [self.indicator stopAnimating];
    self.contentView.userInteractionEnabled = !show;
}

- (void)setErrorState:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

@end

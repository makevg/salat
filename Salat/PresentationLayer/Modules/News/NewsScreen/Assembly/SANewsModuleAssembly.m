//
//  SANewsModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 20.05.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SANewsModuleAssembly.h"
#import "SANewsVC.h"
#import "SANewsPresenter.h"
#import "SANewsInteractor.h"
#import "SANewsPresenterStateStorage.h"
#import "SANewsRouter.h"
#import <URBMediaFocusViewController/URBMediaFocusViewController.h>

@implementation SANewsModuleAssembly

- (SANewsVC *)viewNews {
    return [TyphoonDefinition withClass:[SANewsVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNews]];
    }];
}

- (SANewsPresenter *)presenterNews {
    return [TyphoonDefinition withClass:[SANewsPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewNews]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorNews]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerNews]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorageNews]];
    }];
}

- (SANewsInteractor *)interactorNews {
    return [TyphoonDefinition withClass:[SANewsInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterNews]];
                              [definition injectProperty:@selector(newsService)
                                                    with:[self.serviceComponents newsService]];
                              [definition injectProperty:@selector(newsMapper)
                                                    with:[self.coreComponents newsMapper]];
    }];
}

- (SANewsRouter *)routerNews {
    return [TyphoonDefinition withClass:[SANewsRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewNews]];
                              [definition injectProperty:@selector(mediaFocusController)
                                                    with:[self mediaFocusView]];
    }];
}

- (SANewsPresenterStateStorage *)presenterStateStorageNews {
    return [TyphoonDefinition withClass:[SANewsPresenterStateStorage class]];
}

- (URBMediaFocusViewController *)mediaFocusView {
    return [TyphoonDefinition withClass:[URBMediaFocusViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(delegate)
                                                    with:[self presenterNews]];
    }];
}

@end

//
//  SAAddressPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//


#import "SAChooseAddressModuleOutput.h"
#import "SAChooseAddressModuleInput.h"
#import "SAChooseAddressViewOutput.h"
#import "SAChooseAddressPresenter.h"
#import "SAChooseAddressInteractorInput.h"
#import "SAChooseAddressViewInput.h"

static double const cDefaultLatitude = 57.6309;
static double const cDefaultLongitude = 39.8871;

@interface SAChooseAddressPresenter()

@property (weak, nonatomic) id<SAChooseAddressModuleOutput> delegate;

@property (nonatomic, assign) YMKMapCoordinate coordinate;

@property (nonatomic, assign) BOOL coordinatesChanged;

@end

@implementation SAChooseAddressPresenter

#pragma mark - SAChooseAddressModuleInput

- (instancetype)init {
    if( self = [super init] ) {
        _coordinatesChanged = NO;
    }
    
    return self;
}

- (void)setModuleOutput:(id <RamblerViperModuleOutput>)moduleOutput {
    self.delegate = (id <SAChooseAddressModuleOutput>) moduleOutput;
}

- (void)configureModuleWithCoordinate:(YMKMapCoordinate)coordinate {
    if (coordinate.latitude > 0 && coordinate.longitude > 0) {
        self.coordinatesChanged = YES;
    }
    self.coordinate = coordinate;
}

#pragma mark - SAAddressViewOutput

- (void)didTriggerViewDidLoadEvent {
    if( !self.coordinate.latitude && !self.coordinate.longitude )
        self.coordinate = YMKMapCoordinateMake(cDefaultLatitude, cDefaultLongitude);

    [self.view setStartCoordinate:self.coordinate];
    [self.interactor obtainAddressByLocation:self.coordinate];
}

- (void)didTapSave:(CLLocationCoordinate2D)coordinate {
    [self.delegate chooseLocation:coordinate];
    [((UIViewController *) self.view).navigationController popViewControllerAnimated:YES];
}

- (void)didChangeLocation:(CLLocationCoordinate2D)location {
    self.coordinatesChanged = YES;
    
    if (location.latitude > 0 && location.longitude > 0) {
        [self.interactor obtainAddressByLocation:location];
    }
}

- (void)didChangeAutoLocation:(CLLocationCoordinate2D)location {
    if( self.coordinatesChanged && self.coordinate.latitude && self.coordinate.longitude )
        return;
    
    [self.interactor obtainAddressByLocation:location];
}


#pragma mark - SAChooseAddressInteractorOutput

- (void)didObtainAddress:(SAAddressPlainObject *)address {
    [self.view updateWithAddress:address];
}

- (void)didObtainError:(NSError *)error {
    //TODO: qwe
}


@end

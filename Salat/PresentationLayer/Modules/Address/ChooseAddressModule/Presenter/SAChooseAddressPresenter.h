//
//  SAAddressPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAChooseAddressViewOutput.h"
#import "SAChooseAddressInteractorOutput.h"
#import "SAChooseAddressModuleInput.h"

@protocol SAChooseAddressViewInput;
@protocol SAChooseAddressViewOutput;
@protocol SAChooseAddressModuleInput;
@protocol SAChooseAddressInteractorInput;

@interface SAChooseAddressPresenter : NSObject <SAChooseAddressModuleInput, SAChooseAddressViewOutput, SAChooseAddressInteractorOutput>

@property (weak, nonatomic) id<SAChooseAddressViewInput> view;
@property (nonatomic, strong) id<SAChooseAddressInteractorInput> interactor;

@end

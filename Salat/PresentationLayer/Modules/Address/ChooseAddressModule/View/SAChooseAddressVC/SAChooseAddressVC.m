//
//  SAChooseAddressVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAChooseAddressVC.h"
#import "SAChooseAddressView.h"
#import "SAChooseAddressViewOutput.h"
#import "SALocationHelper.h"
#import "SAAddressAnnotation.h"
#import "SAAddressPlainObject.h"

static NSUInteger const cDefaultZoom = 16;

@interface SAChooseAddressVC () <CLLocationManagerDelegate, YMKMapViewDelegate, SALocationHelperDelegate>

@property(strong, nonatomic) IBOutlet SAChooseAddressView *contentView;

@property(nonatomic, assign) BOOL userDragged;

@property(strong, nonatomic) SAAddressPlainObject *address;

@property(strong, nonatomic) SALocationHelper *locationHelper;

@property(strong, nonatomic) SAAddressAnnotation *annotation;

@property(strong, nonatomic) UIBarButtonItem *saveButton;

@end

@implementation SAChooseAddressVC

#pragma mark - Super

- (void)viewDidLoad {
    [super viewDidLoad];

    self.saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Выбрать"
                                                       style:UIBarButtonItemStylePlain
                                                      target:self
                                                      action:@selector(saveTapped)];

    [self.locationHelper requestPermissions];
    self.userDragged = NO;

    self.contentView.mapView.showsUserLocation = YES;
    self.contentView.mapView.tracksUserLocation = NO;
    self.contentView.mapView.showTraffic = NO;
    self.contentView.mapView.delegate = self;
    [self.contentView.mapView addAnnotation:self.annotation];

    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Private

- (void)saveTapped {
    [self.output didTapSave:self.contentView.mapView.centerCoordinate];
}

- (SALocationHelper *)locationHelper {
    if (!_locationHelper) {
        _locationHelper = [[SALocationHelper alloc] initWithDelegate:self];
    }

    return _locationHelper;
}

- (SAAddressAnnotation *)annotation {
    if (!_annotation) {
        _annotation = [SAAddressAnnotation pointAnnotation];
    }

    return _annotation;
}

#pragma mark - YMKMapViewDelegate

- (void)mapViewWasDragged:(YMKMapView *)mapView {
    self.navigationItem.rightBarButtonItem = nil;
    self.userDragged = YES;
    self.annotation.coordinate = mapView.centerCoordinate;
}

- (void)mapView:(YMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (self.userDragged) {
        self.userDragged = NO;
        [self.output didChangeLocation:self.contentView.mapView.centerCoordinate];
    }
}

- (YMKAnnotationView *)mapView:(YMKMapView *)aMapView viewForAnnotation:(id <YMKAnnotation>)anAnnotation {
    static NSString *identifier = @"pointAnnotation";
    YMKPinAnnotationView *view = (YMKPinAnnotationView *) [aMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (view == nil) {
        view = [[YMKPinAnnotationView alloc] initWithAnnotation:anAnnotation
                                                reuseIdentifier:identifier];
    }

    [self configureAnnotationView:view forAnnotation:anAnnotation];
    return view;
}

- (void)configureAnnotationView:(YMKPinAnnotationView *)aView forAnnotation:(id <YMKAnnotation>)anAnnotation {
    if (anAnnotation == self.annotation) {
        aView.image = [UIImage imageNamed:@"Point"];
        aView.selectedImage = nil;
    }
}

#pragma mark - SAChooseAddressViewInput

- (void)setStartCoordinate:(YMKMapCoordinate)coordinate {
    [self.contentView.mapView setCenterCoordinate:coordinate
                                      atZoomLevel:cDefaultZoom
                                         animated:NO];

    self.navigationItem.rightBarButtonItem = self.saveButton;
}

- (void)updateWithAddress:(SAAddressPlainObject *)address {
    self.address = address;
    self.annotation.coordinate = address.location.coordinate;

    [self.contentView setModel:address];
    self.navigationItem.rightBarButtonItem = self.saveButton;
}

#pragma mark - SALocationHelperDelegate

- (void)didUpdateLocations:(CLLocationCoordinate2D)location {
    [self.output didChangeAutoLocation:location];
}

- (void)didFailWithError:(NSError *)error {
//    [self showErrorMessage:error.localizedDescription];
}

@end

//
//  SAAddressVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAAddressViewInput.h"
#import "SAChooseAddressViewInput.h"

@protocol SAChooseAddressViewOutput;

@interface SAChooseAddressVC : SABaseVC <SAChooseAddressViewInput>

@property(nonatomic, strong) id <SAChooseAddressViewOutput> output;

@end

//
//  SAChooseAddressViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol SAChooseAddressViewOutput <NSObject>

- (void)didTriggerViewDidLoadEvent;

- (void)didTapSave:(CLLocationCoordinate2D)coordinate;

- (void)didChangeLocation:(CLLocationCoordinate2D)location;

- (void)didChangeAutoLocation:(CLLocationCoordinate2D)location;

@end

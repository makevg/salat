//
//  SAChooseAddressView.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAChooseAddressView.h"
#import "SAAddressPlainObject.h"
#import "SAAddressAnnotation.h"

@interface SAChooseAddressView () <YMKMapViewDelegate>

@end

@implementation SAChooseAddressView

#pragma mark - Setup

- (void)setup {
    self.addressLabel.font = [SAStyle regularFontOfSize:14.f];
    self.addressLabel.text = @"Выберите адрес";
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAAddressPlainObject class]]) {
        [self configureWithAddress:model];
    }
}

#pragma mark - Private

- (void)configureWithAddress:(SAAddressPlainObject *)address {
    self.addressLabel.text = [NSString stringWithFormat:@"Ярославль, %@, %@", address.street, address.house];
    [self.mapView setCenterCoordinate:address.location.coordinate
                             animated:YES];
}

@end

//
//  SAChooseAddressView.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"
#import "SABorderedField.h"
#import <YandexMapKit/YandexMapKit.h>

@interface SAChooseAddressView : SABaseView

@property (weak, nonatomic) IBOutlet YMKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

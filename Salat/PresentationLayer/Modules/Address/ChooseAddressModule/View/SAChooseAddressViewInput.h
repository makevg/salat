//
//  SAChooseAddressViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YandexMapKit/YMKMapStructs.h>

@class SAAddressPlainObject;

@protocol SAChooseAddressViewInput <NSObject>

- (void)setStartCoordinate:(YMKMapCoordinate)coordinate;

- (void)updateWithAddress:(SAAddressPlainObject *)address;

@end

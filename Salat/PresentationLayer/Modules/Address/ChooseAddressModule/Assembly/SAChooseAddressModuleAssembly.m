//
//  SAAddressModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAChooseAddressModuleAssembly.h"
#import "SAChooseAddressVC.h"
#import "SAChooseAddressInteractor.h"
#import "SAChooseAddressPresenter.h"

@implementation SAChooseAddressModuleAssembly

- (SAChooseAddressVC *)viewChooseAddress {
    return [TyphoonDefinition withClass:[SAChooseAddressVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChooseAddress]];
                          }];
}

- (SAChooseAddressPresenter *)presenterChooseAddress {
    return [TyphoonDefinition withClass:[SAChooseAddressPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewChooseAddress]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorChooseAddress]];
                          }];
}

- (SAChooseAddressInteractor *)interactorChooseAddress {
    return [TyphoonDefinition withClass:[SAChooseAddressInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterChooseAddress]];
                              [definition injectProperty:@selector(addressService)
                                                    with:[self.serviceComponents addressService]];
                              [definition injectProperty:@selector(addressMapper)
                                                    with:[self.coreComponents addressMapper]];
                          }];
}

@end

//
//  SAAddressInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAChooseAddressInteractor.h"
#import "SAChooseAddressInteractorOutput.h"
#import "SAAddressService.h"
#import "SAAddressPlainObject.h"
#import "SAServiceLayer.h"

static NSTimeInterval const cServerRequestDelay = 0.5f;

@implementation SAChooseAddressInteractor {
    CLLocationCoordinate2D selectedLocation;
}

#pragma mark - SAAddressInteractorInput

- (void)obtainAddressByLocation:(CLLocationCoordinate2D)location {
    selectedLocation = location;
    SEL findAddressSelector = @selector(findAddress);
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:findAddressSelector object:nil];
    [self performSelector:findAddressSelector withObject:nil afterDelay:cServerRequestDelay];
}

#pragma mark - Private

- (void)findAddress {
    __weak __typeof(self) weakSelf = self;
    [self.addressService obtainAddressByLocation:selectedLocation
                                         success:^(SAAddressPlainObject *address) {
                                             dispatch_safe_main_async(^{
                                                 [weakSelf.output didObtainAddress:address];
                                             });
                                         }
                                           error:^(NSError *error) {
                                               dispatch_safe_main_async(^{
                                                   [weakSelf.output didObtainError:error];
                                               });
                                           }];
}

@end

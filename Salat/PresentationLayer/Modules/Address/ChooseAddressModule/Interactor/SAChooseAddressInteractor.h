//
//  SAAddressInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAChooseAddressInteractorInput.h"

@protocol SAChooseAddressInteractorOutput;
@class SAAddressService;
@protocol SAPrototypeMapper;

@interface SAChooseAddressInteractor : NSObject <SAChooseAddressInteractorInput>

@property (weak, nonatomic) id<SAChooseAddressInteractorOutput> output;
@property (nonatomic) SAAddressService *addressService;
@property (nonatomic) id<SAPrototypeMapper> addressMapper;

@end

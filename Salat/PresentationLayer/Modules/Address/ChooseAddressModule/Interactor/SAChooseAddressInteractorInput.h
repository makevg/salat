//
//  SAAddressInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol SAChooseAddressInteractorInput <NSObject>

- (void)obtainAddressByLocation:(CLLocationCoordinate2D)location;

@end

//
//  SAAddressInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAddressPlainObject;

@protocol SAChooseAddressInteractorOutput <NSObject>

- (void)didObtainAddress:(SAAddressPlainObject *)address;

- (void)didObtainError:(NSError *)error;

@end

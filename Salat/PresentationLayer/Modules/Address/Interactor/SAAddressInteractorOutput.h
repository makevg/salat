//
//  SAAddressInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SAAddressPlainObject;

@protocol SAAddressInteractorOutput <NSObject>

- (void)didObtainAddress:(SAAddressPlainObject *)address;

- (void)didAddressSaved;

- (void)didObtainError:(NSError *)error;

- (void)didObtianGeocode:(CLLocationCoordinate2D)geopoint;

@end

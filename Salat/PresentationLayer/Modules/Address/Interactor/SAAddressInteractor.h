//
//  SAAddressInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAAddressInteractorInput.h"

@protocol SAAddressInteractorOutput;
@class SAAddressService;
@protocol SAPrototypeMapper;

@interface SAAddressInteractor : NSObject <SAAddressInteractorInput>

@property (weak, nonatomic) id<SAAddressInteractorOutput> output;
@property (nonatomic) SAAddressService *addressService;
@property (nonatomic) id<SAPrototypeMapper> addressMapper;

@end

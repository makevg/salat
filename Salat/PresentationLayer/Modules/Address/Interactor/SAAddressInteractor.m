//
//  SAAddressInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddressInteractor.h"
#import "SAAddressInteractorOutput.h"
#import "SAAddressService.h"
#import "SAAddressPlainObject.h"
#import "SAPrototypeMapper.h"
#import "SAServiceLayer.h"
#import "SACommon.h"

static NSTimeInterval const cServerRequestDelay = 0.5f;

@implementation SAAddressInteractor {
    CLLocationCoordinate2D selectedLocation;
}

#pragma mark - SAAddressInteractorInput

- (void)obtainAddressByLocation:(CLLocationCoordinate2D)location {
    selectedLocation = location;
    SEL findAddressSelector = @selector(findAddress);
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:findAddressSelector object:nil];
    [self performSelector:findAddressSelector withObject:nil afterDelay:cServerRequestDelay];
}

- (void)obtainAddressById:(NSNumber *)addressId {
    Address *address = [self.addressService obtainAddressById:addressId];
    SAAddressPlainObject *addressPlainObject = [SAAddressPlainObject new];
    [self.addressMapper fillObject:addressPlainObject withObject:address];
    [self.output didObtainAddress:addressPlainObject];
}

- (void)obtainGeoPoint:(SAAddressPlainObject *)addressPlainObject {
    weakifySelf;

    [self.addressService obtainGeocodeByStreet:addressPlainObject.street
                                         house:addressPlainObject.house
                                       success:^(CLLocationCoordinate2D coordinate) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [weakSelf.output didObtianGeocode:coordinate];
                                           });
                                       }
                                         error:^(NSError *error) {
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 [weakSelf.output didObtainError:error];
                                             });
                                         }];
}

- (void)saveAddress:(SAAddressPlainObject *)addressPlainObject {
    weakifySelf;

    [self.addressService saveAddress:addressPlainObject
                             success:^{
                                 dispatch_safe_main_async(^{
                                     [weakSelf.output didAddressSaved];
                                 })
                             }
                               error:^(NSError *error) {
                                   dispatch_safe_main_async(^{
                                       [weakSelf.output didObtainError:error];
                                   })
                               }];
}


#pragma mark - Private

- (void)findAddress {
    __weak __typeof(self) weakSelf = self;
    [self.addressService obtainAddressByLocation:selectedLocation success:^(SAAddressPlainObject *address) {
        dispatch_safe_main_async(^{
            [weakSelf.output didObtainAddress:address];
        });
    } error:^(NSError *error){
        dispatch_safe_main_async(^{
            [weakSelf.output didObtainError:error];
        });
    }];
}

@end

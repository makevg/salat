//
//  SAAddressView.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddressView.h"
#import "SAAddressPlainObject.h"
#import "SAAddressMapView.h"

@implementation SAAddressView

#pragma mark - Setup

- (void)setup {
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SAAddressPlainObject class]]) {
        [self configureWithAddress:model];
    }
}

#pragma mark - Public

- (SAAddressPlainObject *)collectData {
    SAAddressPlainObject *data = [SAAddressPlainObject new];

    data.title = self.titleLabel.text;
    data.street = self.streetLabel.text;
    data.house = self.houseLabel.text;
    data.flat = self.apartmentLabel.text;
    data.entrance = self.porchLabel.text;
    data.floor = self.floorLabel.text;
    data.intercomCode = self.porchCodeLabel.text;

    CLLocationCoordinate2D coordinate = self.mapImage.showedLocation;

    data.location = [[CLLocation alloc] initWithLatitude:coordinate.latitude
                                               longitude:coordinate.longitude];

    return data;
}

#pragma mark - Private

- (void)configureWithAddress:(SAAddressPlainObject *)address {
    self.titleLabel.text = [address.title length] ? address.title : self.titleLabel.text;
    self.streetLabel.text = address.street;
    self.houseLabel.text = address.house;
    self.apartmentLabel.text = address.flat;
    self.porchLabel.text = address.entrance;
    self.floorLabel.text = address.floor;
    self.porchCodeLabel.text = address.intercomCode;
    self.mapImage.showedLocation = address.location.coordinate;

    self.chooseAddressLabel.hidden = YES;
}


@end

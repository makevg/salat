//
//  SAAddressView.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"
#import "SABorderedField.h"
#import <YandexMapKit/YandexMapKit.h>

@class SAAddressMapView;
@class SAAddressPlainObject;

@interface SAAddressView : SABaseView

//@property (weak, nonatomic) IBOutlet YMKMapView *mapView;
@property (weak, nonatomic) IBOutlet SAAddressMapView *mapImage;
@property (weak, nonatomic) IBOutlet SABorderedField *titleLabel;
@property (weak, nonatomic) IBOutlet SABorderedField *cityLabel;
@property (weak, nonatomic) IBOutlet SABorderedField *streetLabel;
@property (weak, nonatomic) IBOutlet SABorderedField *houseLabel;
@property (weak, nonatomic) IBOutlet SABorderedField *apartmentLabel;
@property (weak, nonatomic) IBOutlet SABorderedField *porchLabel;
@property (weak, nonatomic) IBOutlet SABorderedField *floorLabel;
@property (weak, nonatomic) IBOutlet SABorderedField *porchCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *chooseAddressLabel;

- (SAAddressPlainObject *)collectData;

@end

//
//  SAAddressViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class SAAddressPlainObject;

@protocol SAAddressViewOutput <NSObject>

- (void)didChangeAddress:(NSString *)house street:(NSString *)street;

- (void)didTriggerViewDidLoadEvent;

- (void)didTapSave:(SAAddressPlainObject *)address;

- (void)didTapMap:(CLLocationCoordinate2D)location;

- (void)didChangeLocation:(CLLocationCoordinate2D)location;

@end

//
//  SAAddressVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SAAddressViewInput.h"

@protocol SAAddressViewOutput;

@interface SAAddressVC : SABaseVC <SAAddressViewInput>

@property(nonatomic, strong) id <SAAddressViewOutput> output;

@end

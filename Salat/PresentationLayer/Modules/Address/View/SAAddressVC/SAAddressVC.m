//
//  SAAddressVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddressVC.h"
#import "SAAddressView.h"
#import "SAAddressViewOutput.h"
#import "SAAddressPlainObject.h"
#import "SAAddressMapView.h"

@interface SAAddressVC ()

@property(strong, nonatomic) IBOutlet SAAddressView *contentView;

@property(strong, nonatomic) SAAddressPlainObject *address;

@end

@implementation SAAddressVC

#pragma mark - Super

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(saveTapped)];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(tapGestureOccured:)];
    [self.contentView.mapImage addGestureRecognizer:tapGesture];
    [self.output didTriggerViewDidLoadEvent];
}

- (void)saveTapped {
    [self.output didTapSave:[self.contentView collectData]];
}

- (IBAction)addressChanged {
    [self.output didChangeAddress:self.contentView.houseLabel.text
                           street:self.contentView.streetLabel.text];
}

#pragma mark - Gestures

- (void)tapGestureOccured:(UITapGestureRecognizer *)tapGesture {
    [self.output didTapMap:self.contentView.mapImage.showedLocation];
}

#pragma mark - SAAddressViewInput

- (void)updateWithAddress:(SAAddressPlainObject *)address {
    self.address = address;
    [self.contentView setModel:self.address];
}

- (void)setManualGeopoint:(CLLocationCoordinate2D)point {
    self.contentView.mapImage.showedLocation = point;
    self.contentView.chooseAddressLabel.hidden = point.latitude > 0 && point.longitude > 0;
}


@end

//
// Created by mtx on 12.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAAddressMapView.h"

static const int kZoom = 16;

@interface SAAddressMapView () <YMKMapImageBuilderDelegate, YMKMapViewDelegate>

@property(nonatomic, strong) YMKMapView *mapView;
@property(nonatomic, strong) YMKMapImageBuilder *mapImageBuilder;
@property(nonatomic, strong) UIImageView *pointer;

@end

@implementation SAAddressMapView

- (YMKMapView *)mapView {
    if (!_mapView) {
        _mapView = [[YMKMapView alloc] init];
        _mapView.showsUserLocation = NO;
        _mapView.tracksUserLocation = NO;
        _mapView.delegate = self;

        self.pointer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Point"]];
        self.pointer.translatesAutoresizingMaskIntoConstraints = false;

        [self addSubview:self.pointer];

        [self addConstraints:@[
                [NSLayoutConstraint constraintWithItem:self.pointer
                                             attribute:NSLayoutAttributeCenterX
                                             relatedBy:NSLayoutRelationEqual
                                                toItem:self
                                             attribute:NSLayoutAttributeCenterX
                                            multiplier:1.0
                                              constant:0],
                [NSLayoutConstraint constraintWithItem:self.pointer
                                             attribute:NSLayoutAttributeCenterY
                                             relatedBy:NSLayoutRelationEqual
                                                toItem:self
                                             attribute:NSLayoutAttributeCenterY
                                            multiplier:1.0
                                              constant:0]
        ]];

    }

    return _mapView;
}

- (YMKMapImageBuilder *)mapImageBuilder {
    if (!_mapImageBuilder) {
        _mapImageBuilder = [[YMKMapImageBuilder alloc] init];
        _mapImageBuilder.delegate = self;
    }
    return _mapImageBuilder;
}

- (void)setShowedLocation:(YMKMapCoordinate)showedLocation {
    _showedLocation = showedLocation;

    if( showedLocation.longitude > 0 && showedLocation.latitude > 0 ) {
        [self.mapView setCenterCoordinate:self.showedLocation
                              atZoomLevel:kZoom
                                 animated:NO];

        [self createStaticMap];
        self.pointer.hidden = NO;
    } else {
        self.image = nil;
        self.pointer.hidden = YES;
    }

//    self.addressAnnotation.coordinate = showedLocation;
//    self.mapView.selectedAnnotation = self.addressAnnotation;

}

- (void)createStaticMap {
    [self.mapImageBuilder cancel];

//    self.mapImageBuilder.annotations = self.mapView.annotations;
    self.mapImageBuilder.centerCoordinate = self.mapView.centerCoordinate;
    self.mapImageBuilder.zoomLevel = self.mapView.zoomLevel;
    self.mapImageBuilder.layerIdentifier = self.mapView.visibleLayerIdentifier;
    self.mapImageBuilder.imageSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, self.frame.size.height);

    [self.mapImageBuilder build];
}

- (void)renderingDoneWithImage:(UIImage *)image {
    self.image = image;
}

#pragma mark - YMKMapViewDelegate

- (YMKAnnotationView *)mapView:(YMKMapView *)aMapView viewForAnnotation:(id <YMKAnnotation>)anAnnotation {
    static NSString *identifier = @"pointAnnotation";
    YMKPinAnnotationView *view = (YMKPinAnnotationView *) [aMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (view == nil) {
        view = [[YMKPinAnnotationView alloc] initWithAnnotation:anAnnotation
                                                reuseIdentifier:identifier];
    }

    [self configureAnnotationView:view forAnnotation:anAnnotation];
    return view;
}

- (void)configureAnnotationView:(YMKPinAnnotationView *)aView forAnnotation:(id <YMKAnnotation>)anAnnotation {
    aView.image = [UIImage imageNamed:@"Point"];
    aView.selectedImage = nil;
}

#pragma mark - YMKMapImageBuilderDelegate


- (void)mapImageBuilder:(YMKMapImageBuilder *)builder builtImage:(UIImage *)image {
    [self renderingDoneWithImage:image];
}

- (void)mapImageBuilderFailedToLoadCompleteImage:(YMKMapImageBuilder *)builder partialImage:(UIImage *)image {
    [self renderingDoneWithImage:image];
}

- (void)mapImageBuilderWasCancelled:(YMKMapImageBuilder *)builder {
    [self renderingDoneWithImage:self.image];
}

@end
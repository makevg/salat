//
// Created by mtx on 12.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <YandexMapKit/YandexMapKit.h>

@interface SAAddressMapView : UIImageView

@property (nonatomic, assign) YMKMapCoordinate showedLocation;

@end
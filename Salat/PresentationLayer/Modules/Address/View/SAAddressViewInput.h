//
//  SAAddressViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class SAAddressPlainObject;

@protocol SAAddressViewInput <NSObject>

- (void)updateWithAddress:(SAAddressPlainObject *)address;

- (void)setManualGeopoint:(CLLocationCoordinate2D)point;

@end

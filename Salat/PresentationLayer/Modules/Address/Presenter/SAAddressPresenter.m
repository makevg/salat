//
//  SAAddressPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <YandexMapKit/YMKMapStructs.h>
#import "SAAddressPresenter.h"
#import "SAAddressViewInput.h"
#import "SAAddressInteractorInput.h"
#import "SAAddressRouterInput.h"
#import "SAAddressPlainObject.h"
#import "SAAddressPresenterStateStorage.h"
#import "SAAddressModuleOutput.h"

@interface SAAddressPresenter ()

@property(nonatomic, weak) id <SAAddressModuleOutput> delegate;
@property(nonatomic, strong) SAAddressPlainObject *checkingAddress;

@end

@implementation SAAddressPresenter

#pragma mark - SAAddressModuleInput

- (void)setModuleOutput:(id <RamblerViperModuleOutput>)moduleOutput {
    self.delegate = (id <SAAddressModuleOutput>) moduleOutput;
}

- (void)configureModuleWithAddressId:(NSNumber *)addressId {
    self.presenterStateStorage.addressId = addressId;
}

#pragma mark - SAAddressViewOutput

- (void)didChangeAddress:(NSString *)house street:(NSString *)street {
    SEL findAddressSelector = @selector(obtainGeoPoint:);
    [NSObject cancelPreviousPerformRequestsWithTarget:self.interactor selector:findAddressSelector object:self.checkingAddress];

    if ([street length] < 3 || ![house length]) {
        return;
    }

    SAAddressPlainObject *addressPlainObject = [SAAddressPlainObject new];
    addressPlainObject.street = street;
    addressPlainObject.house = house;
    self.checkingAddress = addressPlainObject;

    [self.interactor performSelector:findAddressSelector withObject:self.checkingAddress afterDelay:1.0f];
}

- (void)didTriggerViewDidLoadEvent {
    if (self.presenterStateStorage.addressId) {
        [self.interactor obtainAddressById:self.presenterStateStorage.addressId];
    }
}

- (void)didTapSave:(SAAddressPlainObject *)address {
    address.addressId = self.presenterStateStorage.addressId;
    [self.interactor saveAddress:address];
}

- (void)didTapMap:(CLLocationCoordinate2D)location {
    [self.router openMapModuleWithOutput:self
                              coordinate:location];
}

- (void)didChangeLocation:(CLLocationCoordinate2D)location {
    if (location.latitude > 0 && location.longitude > 0) {
        [self.interactor obtainAddressByLocation:location];
    }
}

#pragma mark - SAAddressInteractorOutput

- (void)didObtainAddress:(SAAddressPlainObject *)address {
    [self.view updateWithAddress:address];
}

- (void)didAddressSaved {
    [self.delegate didAddressSaved];
    [self.router closeCurrentModule];
}

- (void)didObtainError:(NSError *)error {
    //TODO: write
}

- (void)didObtianGeocode:(CLLocationCoordinate2D)geopoint {
    [self.view setManualGeopoint:geopoint];
}

#pragma mark - SAChooseAddressModuleOutput

- (void)chooseLocation:(YMKMapCoordinate)coordinate {
    [self didChangeLocation:coordinate];
}

@end

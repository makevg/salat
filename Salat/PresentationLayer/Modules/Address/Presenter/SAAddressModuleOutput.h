//
// Created by mtx on 13.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SAAddressModuleOutput <RamblerViperModuleOutput>

- (void)didAddressSaved;

@end
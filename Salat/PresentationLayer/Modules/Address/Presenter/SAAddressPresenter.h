//
//  SAAddressPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAAddressViewOutput.h"
#import "SAAddressInteractorOutput.h"
#import "SAAddressModuleInput.h"
#import "SAChooseAddressModuleOutput.h"

@protocol SAAddressViewInput;
@protocol SAAddressInteractorInput;
@protocol SAAddressRouterInput;
@class SAAddressPresenterStateStorage;

@interface SAAddressPresenter : NSObject <SAAddressModuleInput, SAAddressViewOutput, SAAddressInteractorOutput, SAChooseAddressModuleOutput>

@property(weak, nonatomic) id <SAAddressViewInput> view;
@property(nonatomic) NSObject <SAAddressInteractorInput> *interactor;
@property(nonatomic) id <SAAddressRouterInput> router;
@property(nonatomic) SAAddressPresenterStateStorage *presenterStateStorage;

@end

//
//  SAAddressModuleInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 28.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol SAAddressModuleInput <RamblerViperModuleInput>

- (void)configureModuleWithAddressId:(NSNumber *)addressId;

@end

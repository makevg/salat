//
// Created by mtx on 12.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YandexMapKit/YMKMapStructs.h>
#import <YandexMapKit/YMKDraggableAnnotation.h>

@interface SAAddressAnnotation : NSObject <YMKDraggableAnnotation>

+ (id)pointAnnotation;

@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * subtitle;
@property (nonatomic, assign) YMKMapCoordinate coordinate;

@end

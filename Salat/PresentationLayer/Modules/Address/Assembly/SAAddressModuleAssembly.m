//
//  SAAddressModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddressModuleAssembly.h"
#import "SAAddressVC.h"
#import "SAAddressPresenter.h"
#import "SAAddressInteractor.h"
#import "SAAddressRouter.h"
#import "SAAddressPresenterStateStorage.h"

@implementation SAAddressModuleAssembly

- (SAAddressVC *)viewAddress {
    return [TyphoonDefinition withClass:[SAAddressVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAddress]];
                          }];
}

- (SAAddressPresenter *)presenterAddress {
    return [TyphoonDefinition withClass:[SAAddressPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewAddress]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorAddress]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerAddress]];
                              [definition injectProperty:@selector(presenterStateStorage)
                                                    with:[self presenterStateStorageAddresses]];
                          }];
}

- (SAAddressInteractor *)interactorAddress {
    return [TyphoonDefinition withClass:[SAAddressInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterAddress]];
                              [definition injectProperty:@selector(addressService)
                                                    with:[self.serviceComponents addressService]];
                              [definition injectProperty:@selector(addressMapper)
                                                    with:[self.coreComponents addressMapper]];
                          }];
}

- (SAAddressRouter *)routerAddress {
    return [TyphoonDefinition withClass:[SAAddressRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewAddress]];
    }];
}

- (SAAddressPresenterStateStorage *)presenterStateStorageAddresses {
    return [TyphoonDefinition withClass:[SAAddressPresenterStateStorage class]];
}

@end

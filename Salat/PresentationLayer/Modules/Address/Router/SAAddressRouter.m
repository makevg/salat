//
//  SAAddressRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAAddressRouter.h"
#import "SAChooseAddressModuleOutput.h"
#import "SAChooseAddressModuleInput.h"

@implementation SAAddressRouter

- (void)openMapModuleWithOutput:(id <RamblerViperModuleOutput>)moduleOutput coordinate:(CLLocationCoordinate2D)coordinate {
    [[self.transitionHandler openModuleUsingSegue:@"kChooseLocation"]
            thenChainUsingBlock:(RamblerViperModuleLinkBlock) ^id <RamblerViperModuleOutput>(id <SAChooseAddressModuleInput> moduleInput) {
                            [moduleInput configureModuleWithCoordinate:coordinate];
                            return moduleOutput;
                        }];
}

- (void)closeCurrentModule {
    [self.transitionHandler closeCurrentModule:YES];
}


@end

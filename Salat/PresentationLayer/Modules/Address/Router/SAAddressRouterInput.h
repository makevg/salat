//
//  SAAddressRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 14.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol RamblerViperModuleOutput;

@protocol SAAddressRouterInput <NSObject>

- (void)openMapModuleWithOutput:(id<RamblerViperModuleOutput>)moduleOutput coordinate:(CLLocationCoordinate2D)coordinate;

- (void)closeCurrentModule;

@end

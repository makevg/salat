//
//  SACartInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SACartItemPlainObject;
@class SACartCalculationPO;
@class SACartObject;

@protocol SACartInteractorOutput <NSObject>

- (void)didObtainCartInfo:(SACartObject *)cartObject;

- (void)didObtainCartFromNet:(SACartObject *)cartObject;

- (void)didObtainForCheck:(SACartObject *)cartObject;

- (void)didObtainError:(NSError *)error;

@end

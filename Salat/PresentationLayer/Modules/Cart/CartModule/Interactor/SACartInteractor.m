//
//  SACartInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartInteractor.h"
#import "SACartInteractorOutput.h"
#import "SACartService.h"
#import "SACartItemPlainObject.h"
#import "SACartItemMapper.h"
#import "SACommon.h"
#import "SAOrdersService.h"
#import "SACheckoutPlainObject.h"
#import "SACartCalculationPO.h"
#import "SACartObject.h"
#import "SAServiceLayer.h"

@implementation SACartInteractor

#pragma mark - SACartInteractorInput

- (void)obtainForCheck {
    weakifySelf;

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        [weakSelf.orderService calculateCartData:[weakSelf generateRequestParams]
                                      completion:^(SACartCalculationPO *po) {
                                          dispatch_safe_main_async(^{
                                              [weakSelf.output didObtainForCheck:[weakSelf getFromCalculation:po]];
                                          });
                                      }
                                           error:^(NSError *error) {
                                               dispatch_safe_main_async(^{
                                                   [weakSelf.output didObtainError:error];
                                               });
                                           }];
    });
}

- (void)obtainCartItems {
    weakifySelf;

    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.output didObtainCartInfo:[weakSelf getFromCalculation:nil]];
    });

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        [weakSelf.orderService calculateCartData:[weakSelf generateRequestParams]
                                      completion:^(SACartCalculationPO *po) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [weakSelf.output didObtainCartFromNet:[weakSelf getFromCalculation:po]];
                                          });
                                      }
                                           error:^(NSError *error) {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   [weakSelf.output didObtainError:error];
                                               });
                                           }];
    });
}

- (void)clearCart {
    weakifySelf;

    [self.cartService clearCart:^{
        [weakSelf.output didObtainCartInfo:nil];
    }];
}

- (BOOL)checkSuggestion {
    return [self.cartService hasSuggestion];
}

- (void)didChangeItemQuantity:(NSUInteger)quantity item:(SACartItemPlainObject *)item {
    weakifySelf;

    [self.cartService changeItemQuantity:@(quantity) itemId:item.itemId];

    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.output didObtainCartInfo:[weakSelf getFromCalculation:nil]];
    });

}

#pragma mark - Private

- (NSArray<SACartItemPlainObject *> *)getPlainCartItemsFromManagedObjects:(NSArray<CartItem *> *)managedObjectCartItems {
    NSMutableArray<SACartItemPlainObject *> *cartItemPlainObjects = [NSMutableArray array];
    for (CartItem *managedObjectCartItem in managedObjectCartItems) {
        SACartItemPlainObject *cartItemPlainObject = [SACartItemPlainObject new];
        [self.cartItemMapper fillObject:cartItemPlainObject withObject:managedObjectCartItem];
        [cartItemPlainObjects addObject:cartItemPlainObject];
    }
    return cartItemPlainObjects;
}

- (SACartObject *)getFromCalculation:(SACartCalculationPO *)po {
    NSArray<CartItem *> *managedObjectCartItems = [self.cartService obtainCartItems];
    NSArray<SACartItemPlainObject *> *cartItems = [self getPlainCartItemsFromManagedObjects:managedObjectCartItems];

    SACartObject *cartObject = [SACartObject new];
    cartObject.cartItems = cartItems;
    cartObject.fullPrice = [self.cartService obtainTotalAmount];
    cartObject.suggestionProductId = po.suggestionProduct.productId;
    cartObject.discountInfo = po.discountInfo;

    return cartObject;
}

- (SACheckoutPlainObject *)generateRequestParams {
    SACheckoutPlainObject *plainObject = [SACheckoutPlainObject new];
    NSArray<CartItem *> *managedObjectCartItems = [self.cartService obtainCartItems];
    NSArray<SACartItemPlainObject *> *cartItems = [self getPlainCartItemsFromManagedObjects:managedObjectCartItems];
    plainObject.products = cartItems;

    return plainObject;
}

@end

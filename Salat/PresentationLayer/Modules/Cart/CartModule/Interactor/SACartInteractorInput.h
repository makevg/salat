//
//  SACartInteractorInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SACartItemPlainObject;

@protocol SACartInteractorInput <NSObject>

- (void)obtainCartItems;

- (void)obtainForCheck;

- (void)clearCart;

- (BOOL)checkSuggestion;

- (void)didChangeItemQuantity:(NSUInteger)quantity item:(SACartItemPlainObject *)item;

@end

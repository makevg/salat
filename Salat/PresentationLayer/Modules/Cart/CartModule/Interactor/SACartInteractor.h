//
//  SACartInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SACartInteractorInput.h"

@protocol SACartInteractorOutput;
@class SACartService;
@class SACartItemMapper;
@class SARestaurantService;
@class SAOrdersService;

@interface SACartInteractor : NSObject <SACartInteractorInput>

@property(weak, nonatomic) id <SACartInteractorOutput> output;
@property(nonatomic) SACartService *cartService;
@property(nonatomic) SAOrdersService *orderService;
@property(nonatomic) SARestaurantService *restaurantService;
@property(nonatomic) SACartItemMapper *cartItemMapper;

@end

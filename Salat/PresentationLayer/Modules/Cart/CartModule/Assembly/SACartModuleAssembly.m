//
//  SACartModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartModuleAssembly.h"
#import "SACartVC.h"
#import "SACartPresenter.h"
#import "SACartInteractor.h"
#import "SACartRouter.h"
#import "SACartDataDisplayManager.h"

@implementation SACartModuleAssembly

- (SACartVC *)viewCart {
    return [TyphoonDefinition withClass:[SACartVC class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCart]];
                              [definition injectProperty:@selector(displayManager)
                                                    with:[self displayManagerCart]];
    }];
}

- (SACartPresenter *)presenterCart {
    return [TyphoonDefinition withClass:[SACartPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCart]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCart]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCart]];
    }];
}

- (SACartInteractor *)interactorCart {
    return [TyphoonDefinition withClass:[SACartInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCart]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
                              [definition injectProperty:@selector(orderService)
                                                    with:[self.serviceComponents ordersService]];
                              [definition injectProperty:@selector(restaurantService)
                                                    with:[self.serviceComponents restaurantService]];
                              [definition injectProperty:@selector(cartItemMapper)
                                                    with:[self.coreComponents cartItemMapper]];
    }];
}

- (SACartRouter *)routerCart {
    return [TyphoonDefinition withClass:[SACartRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCart]];
    }];
}

- (SACartDataDisplayManager *)displayManagerCart {
    return [TyphoonDefinition withClass:[SACartDataDisplayManager class]];
}

@end

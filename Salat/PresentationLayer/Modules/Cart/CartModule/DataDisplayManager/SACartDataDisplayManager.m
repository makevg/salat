//
//  SACartDataDisplayManager.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartDataDisplayManager.h"
#import "SACartItemPlainObject.h"
#import "SACartItemCell.h"
#import "SACartSumCell.h"
#import "SACartClearCell.h"
#import "SACartCheckoutCell.h"
#import "SACartInfoCell.h"
#import "UIScrollView+EmptyDataSet.h"

typedef NS_ENUM(NSUInteger, SACartInfoSectionType) {
    SACartInfoSectionTypeItems = 0,
    SACartInfoSectionTypeInfo,
    SACartInfoSectionTypeAmount,
    SACartInfoSectionTypeClearCart,
    SACartInfoSectionTypeCheckout
};

@interface SACartDataDisplayManager () <SACartItemCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property(nonatomic) NSMutableArray *cartInfo;
@end

@implementation SACartDataDisplayManager

#pragma mark - Lazy init

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.emptyDataSetSource = self;
    _tableView.emptyDataSetDelegate = self;
}

#pragma mark  - Public

- (void)configureWithCartInfo:(NSArray *)cartInfo {
    self.cartInfo = [cartInfo mutableCopy];
}

#pragma mark - Private

- (NSString *)cellIdentifierAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case SACartInfoSectionTypeItems:
            return [SACartItemCell cellIdentifier];
        case SACartInfoSectionTypeInfo:
            return [SACartInfoCell cellIdentifier];
        case SACartInfoSectionTypeAmount:
            return [SACartSumCell cellIdentifier];
        case SACartInfoSectionTypeClearCart:
            return [SACartClearCell cellIdentifier];
        case SACartInfoSectionTypeCheckout:
            return [SACartCheckoutCell cellIdentifier];
        default:
            return nil;
    }
}

- (CGFloat)cellHeightAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case SACartInfoSectionTypeItems:
            return [SACartItemCell cellHeight];
        case SACartInfoSectionTypeInfo:
            return [SACartInfoCell cellHeight];
        case SACartInfoSectionTypeAmount:
            return [SACartSumCell cellHeight];
        case SACartInfoSectionTypeClearCart:
            return [SACartClearCell cellHeight];
        case SACartInfoSectionTypeCheckout:
            return [SACartCheckoutCell cellHeight];
        default:
            return [SABaseTableViewCell cellHeight];
    }
}

- (void)checkItemCell:(SACartItemCell *)cell quantity:(NSUInteger)quantity {
    if (quantity < 1 && [self.cartInfo[SACartInfoSectionTypeItems] count] == 1) {
        [self.cartInfo removeAllObjects];
        [self.tableView reloadData];
        [self.delegate dataIsEmpty];
    } else if (quantity < 1) {
        [self removeCell:cell];
    }
}

- (void)removeCell:(SACartItemCell *)cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSMutableArray *data = [self.cartInfo[SACartInfoSectionTypeItems] mutableCopy];
    [data removeObjectAtIndex:(NSUInteger) indexPath.row];
    self.cartInfo[SACartInfoSectionTypeItems] = data;
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)notifyDelegateWithItemQuantity:(NSUInteger)quantity item:(SACartItemPlainObject *)item {
    if (self.delegate && [self.delegate respondsToSelector:@selector(displayManager:didChangeItemQuantity:item:)]) {
        [self.delegate displayManager:self didChangeItemQuantity:quantity item:item];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.cartInfo count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cartInfo[(NSUInteger) section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = [self cellIdentifierAtIndexPath:indexPath];
    SABaseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier
                                                                forIndexPath:indexPath];

    if ([identifier isEqualToString:[SACartItemCell cellIdentifier]]) {
        ((SACartItemCell *)cell).delegate = self;
    }

    [cell setModel:self.cartInfo[(NSUInteger) indexPath.section][(NSUInteger) indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellHeightAtIndexPath:indexPath];
}

#pragma mark - SACartItemCellDelegate

- (void)cartItemCell:(SACartItemCell *)cell didChangeItemQuantity:(NSUInteger)quantity cartItem:(SACartItemPlainObject *)cartItem {
    [self checkItemCell:cell quantity:quantity];
    [self notifyDelegateWithItemQuantity:quantity item:cartItem];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"Корзина пуста";
    NSDictionary *attributes = @{
            NSFontAttributeName : [SAStyle regularFontOfSize:25.f],
            NSForegroundColorAttributeName : [SAStyle grayColor]
    };
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

@end

//
//  SACartDataDisplayManager.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SACartItemPlainObject;
@protocol SACartDataDisplayManagerDelegate;

@interface SACartDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;
@property (weak, nonatomic) id<SACartDataDisplayManagerDelegate> delegate;

- (void)configureWithCartInfo:(NSArray *)cartInfo;

@end

@protocol SACartDataDisplayManagerDelegate <NSObject>

- (void)displayManager:(SACartDataDisplayManager *)displayManager didChangeItemQuantity:(NSUInteger)quantity item:(SACartItemPlainObject *)item;

- (void)dataIsEmpty;

@end

//
//  SACartRouterInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SACartObject;

@protocol SAProductModuleOutput;

@protocol SACartRouterInput <NSObject>

- (void)openAboutDeliveryModule;

- (void)closeCurrentModule;

- (void)openCheckoutModuleWith:(SACartObject *)cartObject;

- (void)openProductModule:(id<SAProductModuleOutput>)delegate withProductId:(NSNumber *)productId;

@end

//
//  SACartRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartRouter.h"
#import "SAAboutDeliveryVC.h"
#import "SAProductModuleInput.h"
#import "SACartService.h"
#import "SACheckoutPresenter.h"

static NSString *const cOpenCheckoutModuleInCartModuleSegue = @"openCheckoutModuleInCartModuleSegue";

@implementation SACartRouter

#pragma mark - SACartRouterInput

- (void)openAboutDeliveryModule {
    [((UIViewController *) self.transitionHandler).navigationController
            pushViewController:[self getAboutDeliveryVC]
                      animated:YES];
}

- (void)closeCurrentModule {
    [self.transitionHandler closeCurrentModule:YES];
}

- (void)openCheckoutModuleWith:(SACartObject *)cartObject {
    [[self.transitionHandler openModuleUsingSegue:cOpenCheckoutModuleInCartModuleSegue] thenChainUsingBlock:^id <RamblerViperModuleOutput>(id <RamblerViperModuleInput> moduleInput) {
        id <SACheckoutModuleInput> mi = (id <SACheckoutModuleInput>) moduleInput;
        [mi setCartObject:cartObject];
        return nil;
    }];
}

- (void)openProductModule:(id <SAProductModuleOutput>)delegate withProductId:(NSNumber *)productId {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Product"
                                                         bundle:nil];
    UIViewController *vc = [storyboard instantiateInitialViewController];
    id <SAProductModuleInput> input = [vc valueForKey:@"output"];
    [input configureWithProductId:productId
                      productType:SACartServiceProductTypeSuggestion
                         delegate:delegate];

    [(UIViewController *) self.transitionHandler presentViewController:vc
                                                              animated:YES
                                                            completion:nil];
}


#pragma mark - Private

- (SAAboutDeliveryVC *)getAboutDeliveryVC {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[SAAboutDeliveryVC storyboardName] bundle:nil];
    SAAboutDeliveryVC *vc = [sb instantiateViewControllerWithIdentifier:[SAAboutDeliveryVC identifier]];
    return vc;
}

@end

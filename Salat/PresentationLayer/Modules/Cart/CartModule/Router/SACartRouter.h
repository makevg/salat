//
//  SACartRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartRouterInput.h"
#import <ViperMcFlurry/ViperMcFlurry.h>

@interface SACartRouter : NSObject <SACartRouterInput>

@property (weak, nonatomic) id <RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

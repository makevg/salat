//
//  SACartVC.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "SACartVC.h"
#import "SACartView.h"
#import "SACartViewOutput.h"
#import "SACartDataDisplayManager.h"
#import "SARestaurantWorkingHeader.h"

static NSString *const cCartStoryboardName = @"Cart";
static NSString *const cAboutDeliveryButtonTitle = @"О доставке";

@interface SACartVC () <SACartDataDisplayManagerDelegate>
@property(strong, nonatomic) IBOutlet SACartView *contentView;
@property(strong, nonatomic) PCAngularActivityIndicatorView* indicator;

@property(weak, nonatomic) IBOutlet UIView <SARestaurantWorkingHeaderProtocol> *workingHeader;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *workingHeaderHeight;
@end

@implementation SACartVC

#pragma mark - Super

- (void)networkStateDidChanged:(NSNotification *)notification {
    [self.output networkChanged:[notification.object boolValue]];
}

+ (NSString *)storyboardName {
    return cCartStoryboardName;
}

+ (BOOL)isInitial {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self prepareAboutDeliveryButton];
    [self.workingHeader willAppeared];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [self.workingHeader willDisappeared];
}

- (void)configureController {
    [super configureController];
    
    self.indicator = [self getLoadingIndicatorWithStyle:PCAngularActivityIndicatorViewStyleDefault
                                                forView:self.contentView];
    self.indicator.color = [SAStyle mediumGreenColor];

    self.displayManager.tableView = self.contentView.tableView;
    self.displayManager.delegate = self;

    [self.workingHeader setHeightConstraint:self.workingHeaderHeight];
    self.workingHeader.delegate = self.output;

    [self.output didTriggerViewDidLoadEvent];
}

#pragma mark - Private

- (void)prepareAboutDeliveryButton {
    UIBarButtonItem *aboutDeliveryButton = [[UIBarButtonItem alloc] initWithTitle:cAboutDeliveryButtonTitle
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(tappedAboutDeliveryButton)];
    self.navigationItem.rightBarButtonItem = aboutDeliveryButton;
}

#pragma mark - Actions

- (void)tappedAboutDeliveryButton {
    [self.output didTapAboutDeliveryButton];
}

- (IBAction)tappedClearCartButton:(id)sender {
    [self.output didTapClearCartButton];
}

- (IBAction)tappedCheckoutButton:(id)sender {
    [self.output didTapCheckoutButton];
}

#pragma mark - SACartViewInput

- (void)updateWithCartInfo:(NSArray *)cartInfo {
    [self.displayManager configureWithCartInfo:cartInfo];
    [self.contentView.tableView reloadData];
}

- (void)setErrorState:(NSError *)error {
    [self showErrorMessage:error.localizedDescription];
}

- (void)setLoadingState:(BOOL) loading {
    if( loading )
        [self.indicator startAnimating];
    else
        [self.indicator stopAnimating];
    
    self.indicator.hidden = !loading;
    self.contentView.userInteractionEnabled = !loading;
}

#pragma mark - SACartDataDisplayManagerDelegate

- (void)displayManager:(SACartDataDisplayManager *)displayManager didChangeItemQuantity:(NSUInteger)quantity item:(SACartItemPlainObject *)item {
    [self.output didChangeItemQuantity:quantity item:item];
}

- (void)dataIsEmpty {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

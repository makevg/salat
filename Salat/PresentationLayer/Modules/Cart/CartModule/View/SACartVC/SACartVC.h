//
//  SACartVC.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseVC.h"
#import "SACartViewInput.h"

@protocol SACartViewOutput;
@class SACartDataDisplayManager;
@protocol SARestaurantWorkingHeaderDelegate;

@interface SACartVC : SABaseVC <SACartViewInput>

@property(nonatomic) id<SACartViewOutput, SARestaurantWorkingHeaderDelegate> output;
@property(nonatomic) SACartDataDisplayManager *displayManager;

@end

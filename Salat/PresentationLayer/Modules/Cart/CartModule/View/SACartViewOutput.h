//
//  SACartViewOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SACartItemPlainObject;

@protocol SACartViewOutput <NSObject>

- (void)networkChanged:(BOOL)hasNetwork;

- (void)didTriggerViewDidLoadEvent;

- (void)didTapAboutDeliveryButton;

- (void)didTapClearCartButton;

- (void)didTapCheckoutButton;

- (void)didChangeItemQuantity:(NSUInteger)quantity item:(SACartItemPlainObject *)item;

@end

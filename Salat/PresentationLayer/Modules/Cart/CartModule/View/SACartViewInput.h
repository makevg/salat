//
//  SACartViewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SACartItemPlainObject;

@protocol SACartViewInput <NSObject>

- (void)updateWithCartInfo:(NSArray *)cartInfo;

- (void)setErrorState:(NSError *)error;

- (void)setLoadingState: (BOOL) loading;

@end

//
//  SACartView.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartView.h"

@implementation SACartView

#pragma mark - Configure

- (void)setup {
    self.tableView.backgroundColor = [SAStyle clearColor];
}

@end

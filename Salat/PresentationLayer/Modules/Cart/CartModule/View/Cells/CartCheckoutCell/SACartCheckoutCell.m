//
//  SACartCheckoutCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 06.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartCheckoutCell.h"
#import "SABorderButton.h"

static CGFloat const cCartCheckoutCell = 90.f;

@interface SACartCheckoutCell ()
@property (weak, nonatomic) IBOutlet SABorderButton *checkoutButton;
@end

@implementation SACartCheckoutCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cCartCheckoutCell;
}

#pragma mark - Configure

- (void)configureCell {
    self.backgroundColor = [SAStyle clearColor];
    [self prepareCheckoutButton];
}

- (void)setModel:(id)model {
    if([model isKindOfClass:[NSNumber class]]) {
        self.checkoutButton.enabled = [model boolValue];
    }
}

#pragma mark - Private

- (void)prepareCheckoutButton {
    self.checkoutButton.titleLabel.font = [SAStyle regularFontOfSize:16.f];
    self.checkoutButton.enabled = NO;
}

@end

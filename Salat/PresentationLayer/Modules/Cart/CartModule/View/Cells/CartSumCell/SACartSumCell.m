//
//  SACartSumCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 06.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartSumCell.h"
#import "SACartService.h"
#import "NSString+Currency.h"

static CGFloat const cCartSumCellHeight = 60.f;

@interface SACartSumCell ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) IBOutlet UILabel *subSumLabel;
@end

@implementation SACartSumCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cCartSumCellHeight;
}

#pragma mark - Configure

- (void)configureCell {
    self.backgroundColor = [SAStyle clearColor];
    self.infoLabel.font = [SAStyle regularFontOfSize:18.f];
    self.infoLabel.textColor = [SAStyle mediumGrayColor];
    self.sumLabel.font = [SAStyle boldFontOfSize:18.f];
    self.sumLabel.textColor = [SAStyle orangeColor];
    self.subSumLabel.font = [SAStyle regularFontOfSize:13.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[NSNumber class]]) {
        [self configureWithSum:model];
    }
}

#pragma mark - Private

- (void)configureWithSum:(NSNumber *)sum {
    self.sumLabel.text = [NSString priceWithCurrencySymbol:sum];
}

@end

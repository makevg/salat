//
//  SACartClearCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 06.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartClearCell.h"

static CGFloat const cCartClearCellHeight = 50.f;

@interface SACartClearCell ()
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@end

@implementation SACartClearCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cCartClearCellHeight;
}

#pragma mark - Configure

- (void)configureCell {
    self.backgroundColor = [SAStyle clearColor];
    self.clearButton.titleLabel.font = [SAStyle regularFontOfSize:10.f];
}

@end

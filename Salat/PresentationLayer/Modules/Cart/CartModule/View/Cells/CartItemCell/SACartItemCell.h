//
//  SACartItemCell.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseTableViewCell.h"

@protocol SACartItemCellDelegate;
@class SACartItemPlainObject;

@interface SACartItemCell : SABaseTableViewCell

@property (weak, nonatomic) id<SACartItemCellDelegate> delegate;

@end

@protocol SACartItemCellDelegate <NSObject>

- (void)cartItemCell:(SACartItemCell *)cell didChangeItemQuantity:(NSUInteger)quantity cartItem:(SACartItemPlainObject *)cartItem;

@end

//
//  SACartItemCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartItemCell.h"
#import "SACartItemPlainObject.h"
#import "UIImageView+AsyncLoad.h"
#import "NSString+Currency.h"

static CGFloat const cCartItemCellHeight = 70.f;

@interface SACartItemCell ()
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDescrLabel;
@property (weak, nonatomic) IBOutlet UILabel *productCountLabel;
@end

@implementation SACartItemCell {
    SACartItemPlainObject *cartItemPlainObject;
    NSUInteger itemQuantity;
}

+ (CGFloat)cellHeight {
    return cCartItemCellHeight;
}

#pragma mark - Configure

- (void)configureCell {
    self.productImageView.layer.cornerRadius = CGRectGetWidth(self.productImageView.frame)/2;
    self.productImageView.clipsToBounds = YES;
    self.backgroundColor = [SAStyle clearColor];
    self.productTitleLabel.font = [SAStyle regularFontOfSize:13.f];
    self.productDescrLabel.textColor = [SAStyle grayColor];
    self.productDescrLabel.font = [SAStyle regularFontOfSize:12.f];
    self.productCountLabel.font = [SAStyle regularFontOfSize:11.f];
}

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SACartItemPlainObject class]]) {
        [self configureWithCartItem:model];
    }
}

#pragma mark - Private

- (void)configureWithCartItem:(SACartItemPlainObject *)cartItem {
    cartItemPlainObject = cartItem;
    itemQuantity = [cartItem.quantity unsignedIntegerValue];
    [self.productImageView loadAsyncFromUrl:cartItem.productImageUrl];
    self.productTitleLabel.text = cartItem.productTitle;
    self.productDescrLabel.text = cartItem.productDescr;
    [self updateCountLabel];
}

- (void)updateCountLabel {
    NSString *priceString = [NSString priceWithCurrencySymbol:cartItemPlainObject.cost];
    self.productCountLabel.text = [NSString stringWithFormat:@"%lu x %@", (unsigned long)itemQuantity, priceString];
}

- (void)notifyDelegate {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cartItemCell:didChangeItemQuantity:cartItem:)]) {
        [self.delegate cartItemCell:self didChangeItemQuantity:itemQuantity cartItem:cartItemPlainObject];
    }
}

#pragma mark - Actions

- (IBAction)tappedPlusButton:(id)sender {
    itemQuantity++;
    [self updateCountLabel];
    [self notifyDelegate];
}

- (IBAction)tappedMinusButton:(id)sender {
    itemQuantity--;
    [self updateCountLabel];
    [self notifyDelegate];
}

@end

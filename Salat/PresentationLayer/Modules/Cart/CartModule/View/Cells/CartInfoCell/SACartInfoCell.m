//
//  SACartInfoCell.m
//  Salat
//
//  Created by Максимычев Е.О. on 06.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartInfoCell.h"
#import "SACartCalculationPO.h"

static CGFloat const cCartInfoCell = 55.f;

@interface SACartInfoCell ()
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *descrLabel;
@property(weak, nonatomic) IBOutlet UILabel *valueLabel;
@end

@implementation SACartInfoCell

#pragma mark - Super

+ (CGFloat)cellHeight {
    return cCartInfoCell;
}

#pragma mark - Configure

- (void)setModel:(id)model {
    if ([model isKindOfClass:[SACartCalculationDiscountPO class]]) {
        SACartCalculationDiscountPO *mdl = model;
        self.titleLabel.text = @"Скидка";
        self.descrLabel.text = mdl.descr;
        self.valueLabel.text = [NSString stringWithFormat:@"%@%%", [mdl.percent stringValue]];
    }
}

- (void)configureCell {
    self.backgroundColor = [SAStyle clearColor];
    self.titleLabel.font = [SAStyle regularFontOfSize:15.f];
    self.titleLabel.textColor = [SAStyle grayColor];
    self.descrLabel.font = [SAStyle regularFontOfSize:12.f];
    self.descrLabel.textColor = [SAStyle grayColor];
    self.valueLabel.font = [SAStyle regularFontOfSize:15.f];
    self.valueLabel.textColor = [SAStyle grayColor];
}

@end

//
//  SACartPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartPresenter.h"
#import "SACartViewInput.h"
#import "SACartInteractorInput.h"
#import "SACartRouterInput.h"
#import "SAProductModuleOutput.h"
#import "SACartCalculationPO.h"
#import "SACartObject.h"

@interface SACartPresenter () <SAProductModuleOutput>

@property(nonatomic, strong) NSNumber *isWorking;

@property(nonatomic, strong) NSNumber *isChanged;

@property(nonatomic, strong) NSNumber *cartChecked;

@property(nonatomic, strong) SACartObject *cartObject;

@property(nonatomic, strong) NSError *error;

@end

@implementation SACartPresenter

- (instancetype)init {
    self = [super init];
    self.isWorking = @NO;
    self.isChanged = @NO;
    self.cartObject = [SACartObject new];

    return self;
}

#pragma mark - SACartViewOutput

- (void)networkChanged:(BOOL)hasNetwork {
    if( hasNetwork && self.error && ![self.cartChecked boolValue] ) {
        [self.view setLoadingState:YES];
        [self.interactor obtainCartItems];
    }
}

- (void)didTriggerViewDidLoadEvent {
    [self.view setLoadingState:YES];
    [self.interactor obtainCartItems];
}

- (void)didTapAboutDeliveryButton {
    [self.router openAboutDeliveryModule];
}

- (void)didTapClearCartButton {
    [self.interactor clearCart];
    [self.router closeCurrentModule];
}

- (void)didTapCheckoutButton {
    if( [self.isChanged boolValue] ) {
        [self.view setLoadingState:YES];
        [self.interactor obtainForCheck];
        return;
    }
    
    [self goNext];
}

- (void)didChangeItemQuantity:(NSUInteger)quantity item:(SACartItemPlainObject *)item {
    self.isChanged = @YES;
    [self.interactor didChangeItemQuantity:quantity item:item];
    
    if( ![self.cartChecked boolValue] ) {
        [self.view setLoadingState:YES];
        [self.interactor obtainCartItems];
    }
}

#pragma mark - SACartInteractorOutput

- (void)didObtainCartInfo:(SACartObject *)cartObject {
    [self.cartObject from:cartObject];

    if (!cartObject.cartItems)
        return;

    NSNumber* canCheckout = @([self.isWorking boolValue] && [self.cartChecked boolValue]);
    
    NSArray *cartInfo = @[
            self.cartObject.cartItems, //items
            self.cartObject.discountInfo ? @[self.cartObject.discountInfo] : @[], //discount
            @[@(self.cartObject.totalAmount)], //total summ
            @[@""], //clear cart
            @[canCheckout] //checkout
    ];

    [self.view updateWithCartInfo:cartInfo];
}

- (void)didObtainCartFromNet:(SACartObject *)cartObject {
    [self.view setLoadingState:NO];
    self.isChanged = @NO;
    self.cartChecked = @YES;
    
    [self didObtainCartInfo:cartObject];
}

- (void)didObtainForCheck:(SACartObject *)cartObject {
    [self didObtainCartFromNet:cartObject];
    
    [self goNext];
}

- (void)didObtainError:(NSError *)error {
    [self.view setLoadingState:NO];
    
    self.error = error;
    [self.view setErrorState:error];
}

- (void)goNext {
    if (![self.interactor checkSuggestion] && self.cartObject.suggestionProductId)
        [self.router openProductModule:self
                         withProductId:self.cartObject.suggestionProductId];
    else
        [self.router openCheckoutModuleWith:self.cartObject];
}

#pragma mark - SAProductModuleOutput

- (void)productModuleIsClosed {
    [self.router openCheckoutModuleWith:self.cartObject];
}

- (void)productSelected {
    [self.view setLoadingState:YES];
    [self.interactor obtainCartItems];
}

#pragma mark - SARestaurantWorkingHeaderDelegate

- (void)restaurantClosed {
    BOOL isWorking = [self.isWorking boolValue];
    
    self.isWorking = @NO;
    
    if( isWorking )
        [self didObtainCartInfo:self.cartObject];
}

- (void)restaurantOpened {
    BOOL isWorking = [self.isWorking boolValue];
    
    self.isWorking = @YES;
    
    if( !isWorking )
        [self didObtainCartInfo:self.cartObject];
}

@end

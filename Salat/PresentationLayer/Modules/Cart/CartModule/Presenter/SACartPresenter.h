//
//  SACartPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SACartViewOutput.h"
#import "SACartInteractorOutput.h"
#import "SARestaurantWorkingHeader.h"

@protocol SACartViewInput;
@protocol SACartInteractorInput;
@protocol SACartRouterInput;

@interface SACartPresenter : NSObject <SACartViewOutput, SACartInteractorOutput, SARestaurantWorkingHeaderDelegate>

@property (weak, nonatomic) id<SACartViewInput> view;
@property (nonatomic) id<SACartInteractorInput> interactor;
@property (nonatomic) id<SACartRouterInput> router;

@end

//
// Created by mtx on 06.08.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SACartItemPlainObject;
@class SACartCalculationDiscountPO;


@interface SACartObject : NSObject

@property(nonatomic, copy) NSArray<SACartItemPlainObject *> *cartItems;
@property(nonatomic, assign) double fullPrice;
@property(nonatomic, strong) SACartCalculationDiscountPO *discountInfo;
@property(nonatomic, strong) NSNumber *suggestionProductId;

- (double)totalAmount;

- (void)from:(SACartObject *)fromObject;

@end
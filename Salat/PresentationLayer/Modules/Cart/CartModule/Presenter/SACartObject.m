//
// Created by mtx on 06.08.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SACartObject.h"
#import "SACartCalculationPO.h"


@implementation SACartObject

- (double)totalAmount {
    return (int)(self.fullPrice - self.fullPrice * [self.discountInfo.percent integerValue] / 100);
}

- (void)from:(SACartObject *)fromObject {
    self.cartItems = [fromObject.cartItems copy];
    self.suggestionProductId = fromObject.suggestionProductId;
    self.fullPrice = fromObject.fullPrice;

    if( fromObject.discountInfo )
        self.discountInfo = fromObject.discountInfo;
}

@end
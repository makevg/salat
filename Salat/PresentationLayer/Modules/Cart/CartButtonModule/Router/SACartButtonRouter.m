//
//  SACartButtonRouter.m
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonRouter.h"
#import "SABaseVC.h"
#import "SACartVC.h"
#import <RESideMenu/RESideMenu.h>

@implementation SACartButtonRouter

#pragma mark - SACartButtonRouterInput

- (void)openCartModule {
    UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    RESideMenu *resideVC = [((UINavigationController *)rootVC).childViewControllers lastObject];
    UINavigationController *vc = (UINavigationController *)resideVC.contentViewController;
    [vc pushViewController:[self getCartVC] animated:YES];
}

#pragma mark - Private

- (SACartVC *)getCartVC {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:[SACartVC storyboardName] bundle:nil];
    SACartVC *vc = [sb instantiateInitialViewController];
    return vc;
}

@end

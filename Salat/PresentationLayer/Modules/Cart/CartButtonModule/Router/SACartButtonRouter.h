//
//  SACartButtonRouter.h
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SACartButtonRouterInput.h"

@interface SACartButtonRouter : NSObject <SACartButtonRouterInput>

@end

//
//  SACartButtonView.h
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"

@interface SACartButtonView : SABaseView

- (void)setCost:(NSNumber *)cost;

@end

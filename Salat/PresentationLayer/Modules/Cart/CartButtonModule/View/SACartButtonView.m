//
//  SACartButtonView.m
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonView.h"
#import "NSString+Currency.h"

static NSTimeInterval cAnimationDuration = 0.5f;

@interface SACartButtonView ()
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceViewWidthConstraint;
@end

@implementation SACartButtonView {
    BOOL initState;
}

#pragma mark - Setup

- (void)setup {
    initState = YES;
    self.userInteractionEnabled = NO;
    self.backgroundColor = [SAStyle clearColor];
    [self prepareSubviews];
}

#pragma mark - Private

- (void)prepareSubviews {
    self.priceView.backgroundColor = [SAStyle orangeColor];
    self.priceView.layer.cornerRadius = 12.f;
    self.priceView.clipsToBounds = YES;
    self.priceLabel.font = [SAStyle defaultFontOfSize:14.f];
}

- (void)showPriceView:(BOOL)show {
    if (initState) {
        self.priceView.alpha = 0.f;
    } else {
        CGFloat alpha = show ? 1.f : 0.f;
        __weak __typeof(self) weakSelf = self;
        [UIView animateWithDuration:cAnimationDuration animations:^{
            weakSelf.priceView.alpha = alpha;
        }];
    }
}

#pragma mark - Public

- (void)setCost:(NSNumber *)cost {
    if ([cost integerValue] == 0) {
        [self showPriceView:NO];
    } else {
        initState = NO;
        [self showPriceView:YES];
        NSString *costText = [NSString priceWithCurrencySymbol:cost];
        NSDictionary *attributes = @{NSFontAttributeName:[SAStyle defaultFontOfSize:12.f]};
        CGRect rect = [costText boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes
                                             context:nil];
        CGFloat padding = 45.f;
        self.priceLabel.text = costText;
        self.priceViewWidthConstraint.constant = rect.size.width + padding;
    }
}

@end

//
//  SACartButtonItem.h
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SACartButtonViewInput.h"
#import "SACartButtonModuleInput.h"

@protocol SACartButtonViewOutput;

@interface SACartButtonItem : UIBarButtonItem <SACartButtonModuleInput, SACartButtonViewInput>

@property (nonatomic) id<SACartButtonViewOutput> output;

- (instancetype)initCustom;

@end

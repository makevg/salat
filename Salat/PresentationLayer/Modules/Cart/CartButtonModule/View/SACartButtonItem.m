//
//  SACartButtonItem.m
//  Salat
//
//  Created by Maximychev Evgeny on 09.04.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonItem.h"
#import "SACartButtonView.h"
#import "SACartButtonViewOutput.h"

@interface SACartButtonItem ()
@property (nonatomic) SACartButtonView *cartButtonView;
@property (nonatomic) UIButton *button;
@end

@implementation SACartButtonItem

#pragma mark - Init

- (instancetype)initCustom {
    if (self = [super init]) {
        self.cartButtonView = (SACartButtonView *)[SABaseView loadViewFromNib:NSStringFromClass([SACartButtonView class])];
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.frame = CGRectMake(0, 0, self.cartButtonView.frame.size.width, self.cartButtonView.frame.size.height);
        [self.button addSubview:self.cartButtonView];
        [self.button addTarget:self action:@selector(tappedCart) forControlEvents:UIControlEventTouchUpInside];
        self = [super initWithCustomView:self.button];
    }
    return self;
}

- (void)dealloc {
    [self.output didTriggerViewDeallocEvent];
}

#pragma mark - Public

- (void)updateByCost:(NSNumber *)cost {
    [self.cartButtonView setCost:cost];
    self.button.frame = CGRectMake(self.button.frame.origin.x, self.button.frame.origin.y, self.cartButtonView.frame.size.width, self.cartButtonView.frame.size.height);
}

#pragma mark - Actions

- (void)tappedCart {
    [self.output didTapCart];
}

#pragma mark - SACartButtonModuleInput

- (void)configureModule {
    [self.output didTriggerViewInitEvent];
}

#pragma mark - SACartButtonViewInput

- (void)updateWithAmount:(NSNumber *)amount {
    [self updateByCost:amount];
}

@end
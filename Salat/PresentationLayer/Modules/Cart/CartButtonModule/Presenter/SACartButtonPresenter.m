//
//  SACartButtonPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonPresenter.h"
#import "SACartButtonViewInput.h"
#import "SACartButtonInteractorInput.h"
#import "SACartButtonRouterInput.h"
#import "SACartService.h"
#import "TSMessage.h"
#import "SAStyle.h"

@implementation SACartButtonPresenter

#pragma mark - SACartButtonViewOutput

- (void)didTriggerViewInitEvent {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateWithNotify:)
                                                 name:SACartServiceTotalAmountChangedNotificationName
                                               object:nil];

    [self.interactor obtainTotalAmount];
}

- (void)didTriggerViewDeallocEvent {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SACartServiceTotalAmountChangedNotificationName
                                                  object:nil];
}

- (void)didTapCart {
    if (![self.interactor cartIsEmpty]) {
        [self.router openCartModule];
    } else {
        [TSMessage showNotificationWithTitle:@"Корзина пуста"
                                  titleColor:[SAStyle whiteColor]
                                   titleFont:[SAStyle boldFontOfSize:15.f]
                                        type:TSMessageNotificationTypeWarning
                                    duration:2.f];
    }
}

#pragma mark - SACartButtonInteractorOutput

- (void)didObtainTotalAmount:(NSNumber *)amount {
    [self.view updateWithAmount:amount];
}

#pragma mark - Notifications

- (void)updateWithNotify:(NSNotification *)notify {
    NSNumber *amount = notify.userInfo[SACartServiceTotalAmountChangedNotificationKey];
    [self.view updateWithAmount:amount];
}

@end

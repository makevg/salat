//
//  SACartButtonPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonViewOutput.h"
#import "SACartButtonInteractorOutput.h"

@protocol SACartButtonViewInput;
@protocol SACartButtonInteractorInput;
@protocol SACartButtonRouterInput;

@interface SACartButtonPresenter : NSObject <SACartButtonViewOutput, SACartButtonInteractorOutput>

@property (weak, nonatomic) id<SACartButtonViewInput> view;
@property (nonatomic) id<SACartButtonInteractorInput> interactor;
@property (nonatomic) id<SACartButtonRouterInput> router;

@end

//
//  SACartButtonModuleAssembly.h
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import "SACartButtonComponents.h"
#import "SAServiceComponents.h"

@interface SACartButtonModuleAssembly : TyphoonAssembly <SACartButtonComponents>

@property (nonatomic, readonly) TyphoonAssembly <SAServiceComponents> *serviceComponents;

@end

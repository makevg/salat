//
//  SACartButtonModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonModuleAssembly.h"
#import "SACartButtonItem.h"
#import "SACartButtonPresenter.h"
#import "SACartButtonInteractor.h"
#import "SACartButtonRouter.h"

@implementation SACartButtonModuleAssembly

- (SACartButtonItem *)viewCartButton {
    return [TyphoonDefinition withClass:[SACartButtonItem class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initCustom)];
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCartButton]];
    }];
}

- (SACartButtonPresenter *)presenterCartButton {
    return [TyphoonDefinition withClass:[SACartButtonPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCartButton]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCartButton]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCartButton]];
    }];
}

- (SACartButtonInteractor *)interactorCartButton {
    return [TyphoonDefinition withClass:[SACartButtonInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCartButton]];
                              [definition injectProperty:@selector(cartService)
                                                    with:[self.serviceComponents cartService]];
    }];
}

- (SACartButtonRouter *)routerCartButton {
    return [TyphoonDefinition withClass:[SACartButtonRouter class]
                          configuration:^(TyphoonDefinition *definition) {
    }];
}

@end

//
//  SACartButtonComponents.h
//  Salat
//
//  Created by Максимычев Е.О. on 03.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TyphoonAssembly.h"

@class SACartButtonItem;

@protocol SACartButtonComponents <NSObject>

- (SACartButtonItem *)viewCartButton;

@end

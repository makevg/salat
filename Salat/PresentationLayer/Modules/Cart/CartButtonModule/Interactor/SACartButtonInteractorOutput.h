//
//  SACartButtonInteractorOutput.h
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SACartButtonInteractorOutput <NSObject>

- (void)didObtainTotalAmount:(NSNumber *)amount;

@end

//
//  SACartButtonInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonInteractorInput.h"

@protocol SACartButtonInteractorOutput;
@class SACartService;

@interface SACartButtonInteractor : NSObject <SACartButtonInteractorInput>

@property (weak, nonatomic) id<SACartButtonInteractorOutput> output;
@property (nonatomic) SACartService *cartService;

@end

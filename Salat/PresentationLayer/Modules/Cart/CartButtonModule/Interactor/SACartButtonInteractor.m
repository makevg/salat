//
//  SACartButtonInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 02.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SACartButtonInteractor.h"
#import "SACartButtonInteractorOutput.h"
#import "SACartService.h"

@implementation SACartButtonInteractor

#pragma mark - SACartButtonInteractorInput

- (void)obtainTotalAmount {
    [self.output didObtainTotalAmount:@([self.cartService obtainTotalAmount])];
}

- (BOOL)cartIsEmpty {
    return [self.cartService cartIsEmpty];
}

@end

//
//  SARestaurantWorkingHeaderInteractor.m
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SARestaurantWorkingHeaderInteractor.h"
#import "SAServiceLayer.h"
#import "SAWeakTimerTarget.h"
#import "SACommon.h"
#import "SARestaurantService.h"
#import "Restaurant.h"
#import "SARestaurantWorkingHeaderInteractorOutput.h"

@interface SARestaurantWorkingHeaderInteractor ()

@property(nonatomic, assign) BOOL haveData;;

@end

@implementation SARestaurantWorkingHeaderInteractor {
    NSTimer *timeTimer;
    NSUInteger interval;
    BOOL isObserving;
}

- (instancetype)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(invalidate)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(validate)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];

        isObserving = false;
        interval = 0;
        self.haveData = false;
    }

    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)fire {
    weakifySelf;

    if (!self.haveData) {
        [self.restaurantService obtainRestaurant:^(Restaurant *restaurant) {
                    strongifySelf;
                    strongSelf.haveData = YES;
                    [strongSelf checkWorking];
                }
                                           error:^(NSError *error) {

                                           }];
    } else {
        [self checkWorking];
    }
}

- (void)startObserving {
    isObserving = YES;
    [self validate];
}

- (void)stopObserving {
    isObserving = NO;
    [self invalidate];
}

#pragma mark - Private

- (void)checkWorking {
    weakifySelf;

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        NSString *notWorkPeriod = [weakSelf.restaurantService checkWorking];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (notWorkPeriod) {
                [weakSelf.output restaurantIsClosedAbout:notWorkPeriod];
            } else {
                [weakSelf.output restaurantIsWorking];
            }
        });
    });
}

- (void)validate {
    if (isObserving && !timeTimer) {
        timeTimer = [NSTimer scheduledTimerWithTimeInterval:10
                                                     target:[[SAWeakTimerTarget alloc] initWithTarget:self
                                                                                             selector:@selector(fire)]
                                                   selector:@selector(timerDidFire:)
                                                   userInfo:nil
                                                    repeats:YES];

        [timeTimer fire];
    }
}

- (void)invalidate {
    [timeTimer invalidate];
    timeTimer = nil;
}

@end

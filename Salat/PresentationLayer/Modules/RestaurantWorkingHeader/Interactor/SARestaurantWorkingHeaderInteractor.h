//
//  SARestaurantWorkingHeaderInteractor.h
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SARestaurantWorkingHeaderInteractorInput.h"

@protocol SARestaurantWorkingHeaderInteractorOutput;
@class SARestaurantService;

@interface SARestaurantWorkingHeaderInteractor : NSObject <SARestaurantWorkingHeaderInteractorInput>

@property (weak, nonatomic) id<SARestaurantWorkingHeaderInteractorOutput> output;
@property (nonatomic) SARestaurantService *restaurantService;

@end

//
//  SARestaurantWorkingHeaderModuleAssembly.m
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARestaurantWorkingHeaderModuleAssembly.h"
#import "SARestaurantWorkingHeader.h"
#import "SARestaurantWorkingHeaderPresenter.h"
#import "SARestaurantWorkingHeaderInteractor.h"

@implementation SARestaurantWorkingHeaderModuleAssembly

- (SARestaurantWorkingHeader *)viewRestaurantWorkingHeader {
    return [TyphoonDefinition withClass:[SARestaurantWorkingHeader class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(restaurantWorkingHeader)];
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRestaurantWorkingHeader]];
                          }];
}

- (SARestaurantWorkingHeaderPresenter *)presenterRestaurantWorkingHeader {
    return [TyphoonDefinition withClass:[SARestaurantWorkingHeaderPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewRestaurantWorkingHeader]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorRestaurantWorkingHeader]];
                          }];
}

- (SARestaurantWorkingHeaderInteractor *)interactorRestaurantWorkingHeader {
    return [TyphoonDefinition withClass:[SARestaurantWorkingHeaderInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterRestaurantWorkingHeader]];
                              [definition injectProperty:@selector(restaurantService)
                                                    with:[self.serviceComponents restaurantService]];
                          }];
}

@end

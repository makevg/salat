//
//  SARestaurantWorkingHeaderPresenter.h
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SARestaurantWorkingHeaderViewOutput.h"
#import "SARestaurantWorkingHeaderInteractorOutput.h"

@protocol SARestaurantWorkingHeaderViewInput;
@protocol SARestaurantWorkingHeaderInteractorInput;

@interface SARestaurantWorkingHeaderPresenter : NSObject <SARestaurantWorkingHeaderViewOutput, SARestaurantWorkingHeaderInteractorOutput>

@property(weak, nonatomic) id <SARestaurantWorkingHeaderViewInput> view;
@property(nonatomic) id <SARestaurantWorkingHeaderInteractorInput> interactor;

@end

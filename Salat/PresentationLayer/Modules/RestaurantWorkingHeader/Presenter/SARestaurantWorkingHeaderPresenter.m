//
//  SARestaurantWorkingHeaderPresenter.m
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARestaurantWorkingHeaderPresenter.h"
#import "SARestaurantWorkingHeaderVIewInput.h"
#import "SARestaurantWorkingHeaderInteractorInput.h"

@implementation SARestaurantWorkingHeaderPresenter

#pragma mark - SARestaurantWorkingHeaderViewOutput

- (void)didTriggerViewWillAppearedEvent {
    [self.interactor startObserving];
}

- (void)didTriggerViewWillDisappearedEvent {
    [self.interactor stopObserving];
}

- (void)restaurantIsWorking {
    [self.view hideClosedState];
}

- (void)restaurantIsClosedAbout:(NSString *)closedInterval {
    [self.view showClosedState:closedInterval];
}


@end

//
//  SARestaurantWorkingHeader.h
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SABaseView.h"
#import "SARestaurantWorkingHeaderVIewInput.h"

@protocol SARestaurantWorkingHeaderViewOutput;

@protocol SARestaurantWorkingHeaderDelegate

- (void)restaurantClosed;

- (void)restaurantOpened;

@end

@protocol SARestaurantWorkingHeaderProtocol

@property(nonatomic, weak) id<SARestaurantWorkingHeaderDelegate> delegate;

- (void)setHeightConstraint:(NSLayoutConstraint *)constraint;

- (void)willDisappeared;

- (void)willAppeared;

@end

@interface SARestaurantWorkingHeader : SABaseView <SARestaurantWorkingHeaderViewInput, SARestaurantWorkingHeaderProtocol>

@property (nonatomic) id<SARestaurantWorkingHeaderViewOutput> output;

@property (nonatomic, weak) id<SARestaurantWorkingHeaderDelegate> delegate;

+ (SARestaurantWorkingHeader *)restaurantWorkingHeader;

@end

//
//  SARestaurantWorkingHeader.m
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SARestaurantWorkingHeader.h"
#import "SARestaurantWorkingHeaderViewOutput.h"

@interface SARestaurantWorkingHeader ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) NSLayoutConstraint *heightConstraint;

@end

@implementation SARestaurantWorkingHeader

#pragma mark - Setup

- (void)setup {
    [super setup];

    [self prepareSubviews];
}

#pragma mark - Public

+ (SARestaurantWorkingHeader *)restaurantWorkingHeader {
    return (SARestaurantWorkingHeader *)[SABaseView loadViewFromNib:NSStringFromClass([self class])];
}

#pragma mark - Private

- (void)prepareSubviews {
    self.infoLabel.font = [SAStyle regularFontOfSize:14.f];
    self.timeLabel.font = [SAStyle regularFontOfSize:14.f];
}

- (void)willDisappeared {
    [self.output didTriggerViewWillDisappearedEvent];
}

- (void)willAppeared {
    [self.output didTriggerViewWillAppearedEvent];
}

- (void)showClosedState:(NSString *)interval {
    if( self.heightConstraint.constant != 75.0f ) {
        self.heightConstraint.constant = 75.0f;
    }

    self.timeLabel.text = interval;
    [self.delegate restaurantClosed];
}

- (void)hideClosedState {
    if( self.heightConstraint.constant != 0 ) {
        self.heightConstraint.constant = 0;
    }

    [self.delegate restaurantOpened];
}

@end

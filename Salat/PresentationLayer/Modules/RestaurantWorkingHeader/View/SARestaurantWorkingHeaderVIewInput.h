//
//  SARestaurantWorkingHeaderVIewInput.h
//  Salat
//
//  Created by Максимычев Е.О. on 08.06.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SARestaurantWorkingHeaderViewInput <NSObject>

- (void)showClosedState:(NSString *)interval;

- (void)hideClosedState;

@end

//
// Created by mtx on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VK-ios-sdk/VKSdk.h>

@protocol SASocialHelperDelegate

@required

- (void)vkAuthResultError:(NSError *)error;

- (void)vkAuthResultSuccess:(NSString *)token;

- (void)fbAuthResultError:(NSError *)error;

- (void)fbAuthResultSuccess:(NSString *)token;

@end

@interface SASocialHelper : NSObject <VKSdkDelegate, VKSdkUIDelegate>

- (instancetype)initWithDelegate:(UIViewController<SASocialHelperDelegate>*)delegate;

- (void)authVK;

- (void)authFB;

@end
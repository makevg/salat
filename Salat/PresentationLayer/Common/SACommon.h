//
// Created by mtx on 17.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef weakifySelf
#define weakifySelf __weak __typeof(self) weakSelf = self;
#endif

#ifndef strongifySelf
#define strongifySelf __strong typeof(weakSelf) strongSelf = weakSelf;
#endif

#define dispatch_safe_main_sync(block)\
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_sync(dispatch_get_main_queue(), block);\
}

#define dispatch_safe_main_async(block)\
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}

@interface SACommon : NSObject
@end
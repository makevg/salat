//
// Created by mtx on 06.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <Crashlytics/Crashlytics/CLSLogging.h>
#import "SASocialHelper.h"
#import "SAServiceLayer.h"
#import "FBSDKAccessToken.h"

NSString *kVkApiID = @"5274496";

@interface SASocialHelper ()

@property(weak, nonatomic) UIViewController <SASocialHelperDelegate> *delegate;

@end

@implementation SASocialHelper

- (void)prepareVkSdk {
    [[VKSdk initializeWithAppId:kVkApiID] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    [VKSdk wakeUpSession:@[] completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            CLSLog(@"work started");
        } else if (error) {
            CLSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (instancetype)initWithDelegate:(UIViewController <SASocialHelperDelegate> *)delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        [self prepareVkSdk];
    }

    return self;
}

- (void)authVK {
    if ([VKSdk accessToken]) {
        [self.delegate vkAuthResultSuccess:[VKSdk accessToken].accessToken];
    } else {
        [VKSdk authorize:@[]];
    }
}

- (void)authFB {
    __weak typeof(self) weakSelf = self;

    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile"]
                 fromViewController:self.delegate
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error || result.isCancelled) {
                                    dispatch_safe_main_sync(^{
                                        [weakSelf.delegate fbAuthResultError:error];
                                    });
                                } else {
                                    dispatch_safe_main_async(^{
                                        [weakSelf.delegate fbAuthResultSuccess:result.token.tokenString];
                                    });
                                }
                            }];
}

#pragma mark - VKSdkDelegate

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.delegate.navigationController.topViewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [VKSdk authorize:@[]];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    __weak typeof(self) weakSelf = self;

    if (result.token) {
        dispatch_safe_main_sync(^{
            [weakSelf.delegate vkAuthResultSuccess:result.token.accessToken];
        });
    } else if (result.error) {
        CLSLog(@"%@", [NSString stringWithFormat:@"Access denied\n%@", result.error]);
        dispatch_safe_main_sync(^{
            [weakSelf.delegate vkAuthResultError:result.error];
        });
    }
}

- (void)vkSdkUserAuthorizationFailed {
    [self.delegate.navigationController popToRootViewControllerAnimated:YES];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.delegate.navigationController.topViewController presentViewController:controller
                                                                       animated:YES
                                                                     completion:nil];
}

@end
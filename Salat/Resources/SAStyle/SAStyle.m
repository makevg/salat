//
//  SAStyle.m
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import "SAStyle.h"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:1.f]

NSString *const cDefaultFontName = @"HelveticaNeue";
NSString *const cRegularFontName = @"PTSans-Regular";
NSString *const cBoldFontName    = @"PTSans-Bold";

@implementation SAStyle

// Colors

+ (UIColor *)backgroundColor {
    return RGB(89, 151, 0);
}

+ (UIColor *)clearColor {
    return [UIColor clearColor];
}

+ (UIColor *)whiteColor {
    return [UIColor whiteColor];
}

+ (UIColor *)lightGrayColor {
    return RGB(214, 213, 211);
}

+ (UIColor *)mediumGrayColor {
    return RGB(119, 117, 109);
}

+ (UIColor *)grayColor {
    return RGB(183, 182, 178);
}

+ (UIColor *)buttonGreenColor {
    return RGB(149, 189, 89);
}

+ (UIColor *)lightGreenColor {
    return RGB(149, 189, 89);
}

+ (UIColor *)mediumGreenColor {
    return RGB(91, 150, 14);
}

+ (UIColor *)darkGreenColor {
    return RGB(52, 89, 0);
}

+ (UIColor *)orangeColor {
    return RGB(255, 173, 27);
}

+ (UIColor *)redColor {
    return RGB(255, 36, 27);
}

+ (UIColor *)errorMessageColor {
    return RGB(147, 18, 34);
}

+ (UIColor *)normalMessageColor {
    return [self whiteColor];
}


// Fonts

+ (UIFont *)defaultFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:cDefaultFontName size:size];
}

+ (UIFont *)regularFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:cRegularFontName size:size];
}

+ (UIFont *)boldFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:cBoldFontName size:size];
}

+ (NSString *)regularFontName {
    return cRegularFontName;
}

+ (NSString *)boldFontName {
    return cBoldFontName;
}

@end

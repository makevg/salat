//
//  SAStyle.h
//  Salat
//
//  Created by Maximychev Evgeny on 01.02.16.
//  Copyright © 2016 Salat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface SAStyle : NSObject

// Colors

+ (UIColor *)backgroundColor;
+ (UIColor *)clearColor;
+ (UIColor *)whiteColor;
+ (UIColor *)lightGrayColor;
+ (UIColor *)mediumGrayColor;
+ (UIColor *)grayColor;
+ (UIColor *)buttonGreenColor;
+ (UIColor *)lightGreenColor;
+ (UIColor *)mediumGreenColor;
+ (UIColor *)darkGreenColor;
+ (UIColor *)orangeColor;

+ (UIColor *)redColor;
+ (UIColor *)errorMessageColor;
+ (UIColor *)normalMessageColor;

// Fonts

+ (UIFont *)defaultFontOfSize:(CGFloat)size;
+ (UIFont *)regularFontOfSize:(CGFloat)size;
+ (UIFont *)boldFontOfSize:(CGFloat)size;
+ (NSString *)regularFontName;
+ (NSString *)boldFontName;

@end

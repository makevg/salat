//
// Created by mtx on 09.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <objc/message.h>
#import "SAWeakTimerTarget.h"


@interface SAWeakTimerTarget ()

@property(nonatomic, weak) id target;
@property(nonatomic) SEL selector;

@end

@implementation SAWeakTimerTarget

#pragma mark - Initialization

- (instancetype)initWithTarget:(id)target selector:(SEL)selector {
    if (self = [super init]) {
        _target = target;
        _selector = selector;
    }

    return self;
}

#pragma mark - SAWeakTimerTarget

- (void)timerDidFire:(NSTimer *)timer {
    if (self.target) {
        IMP imp = [self.target methodForSelector:self.selector];
        if( imp ) {
            void (*func)(id, SEL) = (void *) imp;
            func(self.target, self.selector);
        }
    }
    else {
        [timer invalidate];
    }
}

@end

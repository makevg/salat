//
// Created by mtx on 14.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SAIndicatorViewHelper.h"
#import "SAStyle.h"


@implementation SAIndicatorViewHelper

+ (PCAngularActivityIndicatorView *)getLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style forView:(UIView *)view {
    PCAngularActivityIndicatorView *loadingIndicator = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    loadingIndicator.color = [SAStyle whiteColor];
    loadingIndicator.translatesAutoresizingMaskIntoConstraints = NO;

    [view addSubview:loadingIndicator];
    [view addConstraints:@[
            [NSLayoutConstraint constraintWithItem:loadingIndicator
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:view
                                         attribute:NSLayoutAttributeCenterX
                                        multiplier:1
                                          constant:0],
            [NSLayoutConstraint constraintWithItem:loadingIndicator
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:view
                                         attribute:NSLayoutAttributeCenterY
                                        multiplier:1
                                          constant:0]
    ]];

    return loadingIndicator;
}

@end
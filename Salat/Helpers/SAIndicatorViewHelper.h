//
// Created by mtx on 14.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PCAngularActivityIndicatorView.h"


@interface SAIndicatorViewHelper : NSObject

+ (PCAngularActivityIndicatorView *)getLoadingIndicatorWithStyle:(PCAngularActivityIndicatorViewStyle)style
                                                         forView:(UIView *)view;

@end
//
// Created by mtx on 12.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import "SALocationHelper.h"

@interface SALocationHelper () <CLLocationManagerDelegate>

@property(nonatomic, strong) CLLocationManager *locationManager;

@property(nonatomic, weak) id <SALocationHelperDelegate> delegate;

@end

@implementation SALocationHelper

- (instancetype)initWithDelegate:(id <SALocationHelperDelegate>)delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        [self setupLocationManager];
    }

    return self;
}

- (void)requestPermissions {
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    if ([self isLocationEnabled:authorizationStatus]) {
        [self.locationManager startUpdatingLocation];
    } else {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (void)dealloc {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - Private

- (BOOL)isLocationEnabled:(CLAuthorizationStatus)status {
    return (status == kCLAuthorizationStatusAuthorizedAlways ||
            status == kCLAuthorizationStatusAuthorizedWhenInUse);
}

- (void)setupLocationManager {
    self.locationManager = [CLLocationManager new];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.distanceFilter = 50;
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self.delegate didUpdateLocations:[locations lastObject].coordinate];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([self isLocationEnabled:status]) {
        [manager startUpdatingLocation];
    } else {
        [manager stopUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    [self.delegate didFailWithError:error];
}

@end
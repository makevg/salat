//
// Created by mtx on 12.07.16.
// Copyright (c) 2016 Salat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol SALocationHelperDelegate

- (void)didUpdateLocations:(CLLocationCoordinate2D)location;

- (void)didFailWithError:(NSError *)error;

@end

@interface SALocationHelper : NSObject

- (instancetype)initWithDelegate:(id<SALocationHelperDelegate>)delegate;

- (void)requestPermissions;

@end